﻿using ShopCP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public partial class TN_Mics
{
//    public static List<Order_Model> LoiBanHang_DanhSach(DateTime FromDate, DateTime ToDate, string PartnerNumber)
//    {
//        string zSQL = @"
//SELECT 
//SUM((B.ItemPrice - dbo.LAYGIAVON(B.OrderKey, B.ItemKey)) * B.ItemQty)
//FROM SAL_Order A
//LEFT JOIN SAL_Order_Item B ON A.OrderKey = B.OrderKey
//WHERE 
//A.RecordStatus <> 99
//AND A.PartnerNumber = @PartnerNumber
//AND A.Slug IN (202, 402)
//AND A.CreatedOn BETWEEN @FromDate AND @ToDate
//AND B.RecordStatus <> 99";

//        string zConnectionString = TN_Helper.ConnectionString;
//        try
//        {
//            SqlConnection zConnect = new SqlConnection(zConnectionString);
//            zConnect.Open();
//            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
//            zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
//            zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
//            zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
//            zResult = Convert.ToDouble(zCommand.ExecuteScalar());
//            zCommand.Dispose();
//            zConnect.Close();
//        }
//        catch (Exception ex)
//        {

//        }

//        return zResult;
//    }
    public static double LoiBanhang(DateTime FromDate, DateTime ToDate, string PartnerNumber)
    {
        double zResult = 0;
        string zSQL = @"
SELECT 
SUM((B.ItemPrice - dbo.LAYGIAVON(B.OrderKey, B.ItemKey)) * B.ItemQty)
FROM SAL_Order A
LEFT JOIN SAL_Order_Item B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND A.Slug IN (302, 402)
AND A.CreatedOn BETWEEN @FromDate AND @ToDate
AND B.RecordStatus <> 99";

        string zConnectionString = TN_Helper.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
            zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            zResult = Convert.ToDouble(zCommand.ExecuteScalar());
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {

        }

        return zResult;
    }
    public static double MuaHang(DateTime FromDate, DateTime ToDate, string PartnerNumber)
    {
        double zResult = 0;
        string zSQL = @"SELECT 
SUM(B.ItemPrice * B.ItemQty)
FROM SAL_Purchase A
LEFT JOIN SAL_Purchase_Item B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND A.Slug IN (101)
AND A.CreatedOn BETWEEN @FromDate AND @ToDate
AND B.RecordStatus <> 99";

        string zConnectionString = TN_Helper.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
            zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            zResult = Convert.ToDouble(zCommand.ExecuteScalar());
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {

        }

        return zResult;
    }
    public static double BanHang(DateTime FromDate, DateTime ToDate, string PartnerNumber)
    {
        double zResult = 0;
        string zSQL = @"SELECT 
SUM(A.TotalMoney)
FROM SAL_Order A
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND A.Slug IN (302, 402)
AND A.CreatedOn BETWEEN @FromDate AND @ToDate ";

        string zConnectionString = TN_Helper.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
            zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            zResult = Convert.ToDouble(zCommand.ExecuteScalar());
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {

        }

        return zResult;
    }

    /// <summary>
    /// LƯU phiếu thu [tự động] dùng chung
    /// </summary>
    /// <param name="zOrder"></param>
    /// <param name="CompleteDate"></param>
    /// <param name="Message"></param>
    /// <returns></returns>
    public static bool Create_Receipt(Order_Model zOrder, long CompleteDate, out string Message)
    {
        string ID;
        var zDate = DateTime.Now;
        if (CompleteDate != 0)
            zDate = TN_Utils.UnixTimeStampToDateTime(CompleteDate);

        #region [Lưu phiếu thu]
        if (zOrder.OrderID == string.Empty)
            ID = zOrder.S_OrderID;
        else
            ID = zOrder.OrderID;

        var zModel = new Receipt_Model
        {
            ReceiptID = Receipt_Data.Auto_Receipt_ID(TN_Helper.PartnerNumber),
            ReceiptDescription = "Thu bán đơn hàng [" + ID + "] tự động !.",
            Slug = 9, // fix thu bán hàng tự động
            DocumentID = zOrder.OrderKey,
            ReceiptDate = zDate,
            AmountCurrencyMain = zOrder.TotalMoney,
            Address = zOrder.BuyerAddress,
            CustomerName = zOrder.BuyerName.ExTrim(),
            CustomerPhone = zOrder.BuyerPhone.ExTrim(),
            PartnerNumber = TN_Helper.PartnerNumber,
            CreatedName = "Auto",
            ModifiedName = "Auto",
            CreatedBy = "Auto",
            ModifiedBy = "Auto"
        };

        var zReceipt = new Receipt_Info();
        zReceipt.Receipt = zModel;
        zReceipt.Create_ServerKey();

        bool Success;
        if (zReceipt.Code != "200" &&
            zReceipt.Code != "201")
        {
            Success = false;
            Message = "Lưu Receipt: " + zReceipt.Message.GetFirstLine();
        }
        else
        {
            Success = true;
            Message = string.Empty;
        }
        #endregion

        return Success;
    }
    /// <summary>
    /// LƯU phiếu chi [tự động] dùng chung
    /// </summary>
    /// <param name="zOrder"></param>
    /// <param name="Message"></param>
    /// <returns></returns>
    public static bool Create_Payment(Purchase_Model zOrder, out string Message)
    {
        var zModel = new Payment_Model
        {
            PaymentID = Payment_Data.Auto_Payment_ID(TN_Helper.PartnerNumber),
            PaymentDescription = "Chi mua đơn hàng [" + zOrder.OrderID + "] tự động !.",
            Slug = 9, // fix chi mua hàng tự động
            DocumentID = zOrder.OrderKey,
            PaymentDate = DateTime.Now,
            AmountCurrencyMain = zOrder.TotalMoney,
            Address = zOrder.VendorAddress,
            CustomerName = zOrder.VendorName.ExTrim(),
            CustomerPhone = zOrder.VendorPhone.ExTrim(),
            PartnerNumber = TN_Helper.PartnerNumber,
            CreatedName = "Auto",
            ModifiedName = "Auto",
            CreatedBy = "Auto",
            ModifiedBy = "Auto"
        };

        var zPayment = new Payment_Info();
        zPayment.Payment = zModel;
        zPayment.Create_ServerKey();

        bool Success;
        if (zPayment.Code != "200" &&
            zPayment.Code != "201")
        {
            Success = false;
            Message = "Lưu Payment: " + zPayment.Message.GetFirstLine();
        }
        else
        {
            Success = true;
            Message = string.Empty;
        }

        return Success;
    }
    /// <summary>
    /// kiễm tra dữ liệu phát sinh
    /// </summary>
    /// <param name="ProductKey"></param>
    /// <returns>true là có, false là chưa phát sinh</returns>
    public static bool Check_Product_RelationShip(string ProductKey)
    {
        float zResult = 0;
        //---------- String SQL Access Database ---------------
        string zSQL = " SELECT COUNT(*) FROM SAL_Purchase_Item WHERE ItemKey = @ItemKey";
        zSQL += " SELECT COUNT(*) FROM SAL_Order_Item WHERE ItemKey = @ItemKey";
        string zConnectionString = TN_Helper.ConnectionString;
        SqlConnection zConnect = new SqlConnection(zConnectionString);
        zConnect.Open();
        try
        {
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
            zResult = zCommand.ExecuteScalar().ToFloat();
            zCommand.Dispose();

            if (zResult != 0)
                return true;
        }
        catch (Exception)
        {

        }
        finally
        {
            zConnect.Close();
        }
        return false;
    }
    //CÁC HÀM CHỈ CHẠY 1 LẦN
    //TÍNH GIÁ VỐN LẦN TỒN KHO ĐẦU TIÊN
    public static void Begin_PriceStock(string PartnerNumber)
    {
        DateTime date = DateTime.Now.AddDays(-1);
        DateTime FromDate = new DateTime(date.Year, date.Month, 1, 0, 0, 0);
        DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
        ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

        var zTable = new DataTable();
        string zSQL = @"SAL_QtyStockMonth_V2";

        string zConnectionString = TN_Helper.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.CommandType = CommandType.StoredProcedure;
            zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
            zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zAdapter.Fill(zTable);
            zCommand.Dispose();
            zConnect.Close();

        }
        catch (Exception ex)
        {

        }

        StringBuilder zSb = new StringBuilder();
        foreach (DataRow r in zTable.Rows)
        {
            zSb.AppendLine(@"
INSERT INTO ATO_Track_FIFO 
([OrderStock], [OrderSale], [OrderPurchase], [ProductKey], [Qty], [PriceStock], [PriceSold], [QtyRemain], [Description], [PartnerNumber], [CreatedBy], [CreatedName], [ModifiedBy], [ModifiedName])
VALUES 
('TON-" + DateTime.Now.ToString("yyyy-MM-dd") + "', '', '', '" + r["ProductKey"].ToString() + "', 0, '" + r["BeginMoney"].ToDouble().ToString() + "', 0, '" + r["BeginQty"].ToFloat().ToString() + "', N'Tồn lần 1', '" + TN_Helper.PartnerNumber + "', 'Auto', 'Auto', 'Auto', 'Auto')");
        }

        try
        {
            zSQL = zSb.ToString();
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.ExecuteNonQuery();
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception)
        {

        }
    }
    public static double No_Receipt(DateTime FromDate, DateTime ToDate, string PartnerNumber)
    {
        double zResult = 0;
        string zSQL = @"SELECT SUM(AmountCurrencyMain) FROM FNC_Receipt WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
        if (FromDate != DateTime.MinValue &&
            ToDate != DateTime.MinValue)
        {
            zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate ";
        }
        string zConnectionString = TN_Helper.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            }

            zResult = Convert.ToDouble(zCommand.ExecuteScalar());
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {

        }

        return zResult;
    }
    public static double No_Payment(DateTime FromDate, DateTime ToDate, string PartnerNumber)
    {
        double zResult = 0;
        string zSQL = @"SELECT SUM(AmountCurrencyMain) FROM FNC_Payment WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
        if (FromDate != DateTime.MinValue &&
            ToDate != DateTime.MinValue)
        {
            zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate ";
        }
        string zConnectionString = TN_Helper.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            }

            zResult = Convert.ToDouble(zCommand.ExecuteScalar());
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {

        }

        return zResult;
    }
}
