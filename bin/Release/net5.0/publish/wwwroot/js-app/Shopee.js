﻿
$(document).ready(function () {
    Get_MetaShop();

    $("#cbo_shipper").select2();
    $("#cbo_shipper").change(function () {
        var id = $(this).val();
        var option = $("#option").val();
        var page = $("#page").val();
        Get_List(option, page, id);
    });
});
function PrintTo() {
    var divToPrint = document.getElementById("printData");
    var newWin = window.open('', 'Print-Window');
    var script = "<script>function loadPrint() { window.print(); setTimeout(function () { window.close(); }, 100);}</" + "script>";
    newWin.document.open();
    newWin.document.write('<html><body onload="window.print()" style="margin: 10px; font-family: tahoma !important; font-size:8px;">' + divToPrint.innerHTML + script + '</body></html>');
    newWin.document.close();
}
function Open(id, type) {
    $.ajax({
        url: "/Shopee/Print_Preview",
        type: "GET",
        data: {
            "Id": id,
            "ViewType": type
        },
        beforeSend: function () {
            $("#modal").empty();
            Utils.LoadIn();
        },
        success: function (r) {
            $("#modal").append(r);
        },
        error: function (err) {

        },
        complete: function () {
            Utils.OpenMagnific("#modal");
            Utils.LoadOut();
        }
    });
}
function Confirm() {
    var remark = $("#txt_remark").val();
    var time = $("#cbo_time").val();

    $.ajax({
        url: "/Shopee/Init_Order",
        type: "POST",
        data: {
            "time": time,
            "remark": remark
        },
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.success) {
                var option = $('ul.mailbox-bullets').find('li.active').attr("href").substring(1);
                Get_List(option);
            }
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {
            Utils.LoadOut();
        }
    });
}
function Get_List(option, page, shipper) {
    $.ajax({
        url: "/Shopee/Get_List",
        type: "GET",
        data: {
            "Option": option,
            "Page": page,
            "Shipper": shipper
        },
        beforeSend: function () {
            $("#divViewData").empty();
            $("#ajaxloading").fadeIn("slow");
        },
        success: function (r) {
            $("#divViewData").append(r);
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {
            $("li a.menu-item").removeClass("active");
            $("li a.menu-item[href='#" + option + "']").addClass("active");

            var totalItems = $("#" + option).text();
            Init_Pager(option, totalItems, page);
            $("#ajaxloading").fadeOut("slow");
            $("#option").val(option);
            $("#page").val(page);
            $("#divSearch").show();
        }
    });
}
function Get_Time(id) {
    $.ajax({
        url: "/Shopee/Get_TimeSlot",
        type: "GET",
        data: {
            "Id": id,
        },
        beforeSend: function () {
            $("#modal").empty();
            Utils.LoadIn();
        },
        success: function (r) {
            $("#modal").append(r);
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {
            Utils.OpenMagnific("#modal");
            Utils.LoadOut();

            $("#modal select").select2({
                minimumResultsForSearch: -1
            });
        }
    });
}
function Get_Product() {
    $.ajax({
        url: "/Shopee/Get_Product",
        type: "GET",
        data: {

        },
        beforeSend: function () {
            $("#divViewData").empty();
            $("#divPager").empty();
            $("#ajaxloading").fadeIn("slow");
        },
        success: function (r) {
            $("#divViewData").append(r);
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {
            $("li a.menu-item").removeClass("active");
            $("li a.menu-item[href='#productimport']").addClass("active");
            $("#ajaxloading").fadeOut("slow");
            $("#divSearch").hide();
        }
    });
}
function Get_MetaShop() {
    $.ajax({
        url: "/Shopee/Get_MetaShop",
        type: "GET",
        data: {

        },
        beforeSend: function () {

        },
        success: function (r) {
            var obj = r.data;
            $("#all").text(obj.all);
            $("#to_process").text(obj.to_process);
            $("#processed").text(obj.processed);
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {

        }
    });
}
function Init_Pager(option, totalItems, page) {
    $.ajax({
        url: "/Shopee/Get_Pages",
        type: "GET",
        data: {
            "totalItems": totalItems,
            "page": page,
            "Option": option,
        },
        beforeSend: function () {

        },
        success: function (r) {
            $("#divPager").html(r);
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {
        }
    });
}