﻿const userAgent = navigator.userAgent.toLowerCase();
const isTablet = /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);
var width = $(window).width();
if (isTablet || width <= 1366) {
    $("html").addClass("sidebar-left-collapsed");
}

$(".se-pre-con").fadeOut("slow");
if (localStorage.getItem('url') !== null) {
    var url = localStorage.getItem('url');
    $('a[href="' + url + '"]').addClass('active')
        .closest('li')
        .parents('li.nav-parent')
        .addClass('nav-expanded');
}
else {
    var pathname = document.location.pathname;
    $('#sidebar-left a').each(function () {
        var value = jQuery(this).attr('href');
        if (pathname.indexOf(value) > -1) {
            $(this).addClass('active')
                .closest('li')
                .parents('li.nav-parent')
                .addClass('nav-expanded');
            return false;
        }
    });
}
$("#sidebar-left").on("click", "a", function () {
    var link = $(this).attr("href");
    if (link !== "#") {
        $(".se-pre-con").fadeIn("slow");
        localStorage.setItem("url", link);
    }
});
$(".select2").select2({
    width: "100%",
    placeholder: "--Chọn--",
    tags: true,
    createTag: function (params) {
        return {
            id: params.term,
            text: params.term,
            newOption: true
        };
    }
});
$(".datepicker").datepicker({
    todayHighlight: true,
    autoclose: true,
    format: "dd/mm/yyyy"
});
$("div[data-plugin-datepicker]").datepicker({
    todayHighlight: true,
    format: "dd/mm/yyyy",
}).on("changeDate", function (e) {
    var date = $(this).datepicker("getFormattedDate");
    var attr = $(this).attr("name");
    $("#txt_" + attr).val(date);
});
$(document).on("click", ".modal-dismiss", function (e) {
    e.preventDefault();
    $.magnificPopup.close();
    $('.error').remove();
});
$(document).on("click", ".modal-confirm", function (e) {
    if ($(this).attr("type") === "submit") {
        return;
    }
    e.preventDefault();
    $.magnificPopup.close();
});
//$(document).on("click", ".card-body li a", function () {
//    tab = $(this).attr("href").substring(1);;
//    $(".card-body ul.nav li").each(function () {
//        $(this).removeClass("active");
//        $(this).find("a").removeClass("active");
//    });

//    $(this).addClass("active").closest("li").addClass("active");

//    $(".card-body .tab-pane").removeClass("active");
//    $(".card-body div[id=" + tab + "]").addClass("active");
//});
$(".number").each(function () {
    $(this).number(true, 0, '.', ',');
});

sessionTimeout({
    warnAfter: 3600000,
    titleText: "Thông báo.",
    message: "Phiên làm việc của bạn sắp hết thời gian, phần mềm sẽ quay về trang đăng nhập trong lần thao tác kế tiếp !.",
    keepAliveUrl: "/Base/KeepSessionAlive",
    logOutUrl: "/Auth/SignIn",
    timeOutUrl: "/Auth/SignIn",
    logOutBtnText: "Kết thúc phiên làm việc",
    stayConnectedBtnText: "Giữ phiên làm việc"
});