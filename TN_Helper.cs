﻿using Microsoft.AspNetCore.Http;
using ShopCP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public partial class TN_Helper
{
    public static string BusinessName { get; set; } = "";
    public static string ConnectionString { get; set; } = "";
    public static string PartnerNumber { get; set; } = "";
    public static string MessageUpload { get; set; } = "";
    public static async Task<string> UploadAsync(IFormFile file, string Folder)
    {
        var zResult = "";

        if (file == null)
            return zResult;

        var zWebPath = "_FileUpload/" + PartnerNumber + "/" + Folder + "/";
        try
        {
            var fileExt = Path.GetExtension(file.FileName);
            var fileName = Path.GetFileNameWithoutExtension(file.FileName);
            var fileSave = fileName + "_" + ((DateTimeOffset)DateTime.Now).ToUnixTimeMilliseconds() + fileExt;
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + zWebPath);
            var fileNameWithPath = string.Concat(filePath, "\\", fileSave);

            if (File.Exists(filePath))
            {
                // If file found, delete it    
                File.Delete(filePath);
            }
            Directory.CreateDirectory(filePath);

            using var stream = new FileStream(fileNameWithPath, FileMode.Create);
            await file.CopyToAsync(stream);

            zResult = "~/" + zWebPath + fileSave;
        }
        catch (Exception ex)
        {
            MessageUpload = ex.ToString();
        }

        return zResult;
    }
    public static double RoundUpValue(double value, int decimalpoint)
    {
        var result = Math.Round(value, decimalpoint);
        if (result < value)
        {
            result += Math.Pow(10, -decimalpoint);
        }
        return result;
    }
    public static void DeleteFile(string WebPath)
    {
        var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + WebPath);

        if (File.Exists(filePath))
        {
            // If file found, delete it    
            File.Delete(filePath);
        }
    }
    public static byte[] GetBytes(FileStream formFile)
    {
        using (var memoryStream = new MemoryStream())
        {
            formFile.CopyTo(memoryStream);
            return memoryStream.ToArray();
        }
    }
    /// <summary>
    /// LƯU tracking theo dõi giá vốn, tồn kho [tự động] dùng chung
    /// </summary>
    /// <param name="zOrder"></param>
    /// <param name="Message"></param>
    /// <returns></returns>
}

public static class Ext
{
    public static IEnumerable<T> Traverse<T>(this T e, Func<T, IEnumerable<T>> childrenProvider)
    {
        return TraverseMany(new[] { e }, childrenProvider);
    }

    public static IEnumerable<T> TraverseMany<T>(this IEnumerable<T> collection, Func<T, IEnumerable<T>> childrenProvider)
    {
        var stack = new Stack<T>();
        foreach (var c in collection)
        {
            stack.Push(c);
        }
        while (stack.Count > 0)
        {
            var i = stack.Pop();
            yield return i;
            var children = childrenProvider(i);
            if (children != null)
            {
                foreach (var c in children)
                {
                    stack.Push(c);
                }
            }
        }
    }
}