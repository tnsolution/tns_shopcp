﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopCP.Models;
using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;

namespace ShopCP.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Index(string FromDate, string ToDate)
        {
            string Color = "#0088cc82; #e361598f; #383f4882; #2baab194; #8000807a; #ffa50080";
            string Option = "Tất cả; Shopee; Lazada; Tiki; Khác; Offline";

            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate != null && ToDate != null)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                zFromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }

            //tuỳ chỉnh giao diện biểu đồ
            if (zToDate.Month == DateTime.Now.Month)
            {
                zToDate = new DateTime(zToDate.Year, zToDate.Month, DateTime.Now.Day, 23, 0, 0);
            }

            DataTable zTable;
            #region BIỂU ĐỒ ĐƠN HÀNG ĐÃ SOẠN - TẤT CẢ ĐƠN HÀNG
            zTable = Report.ChartOrder(UserLog.PartnerNumber, zFromDate, zToDate, Option, Color, out _);
            if (zTable.Rows.Count > 0)
            {
                ViewBag.Label1 = ChartJS_XLabelName(zTable);
                ViewBag.Dataset1 = ChartJS_Dataset(zTable);

                zTable.Columns.Add("Total", typeof(decimal));
                foreach (DataRow row in zTable.Rows)
                {
                    double rowSum = 0;
                    for (int i = 3; i < zTable.Columns.Count; i++)
                    {
                        DataColumn col = zTable.Columns[i];
                        rowSum += row[col].ToDouble();
                    }
                    row.SetField("Total", rowSum);
                }

                ViewBag.G1All = zTable.Rows[0][zTable.Columns.Count - 1].ToString();
                ViewBag.G1Spe = zTable.Rows[1][zTable.Columns.Count - 1].ToString();
                ViewBag.G1Laz = zTable.Rows[2][zTable.Columns.Count - 1].ToString();
                ViewBag.G1Tiki = zTable.Rows[3][zTable.Columns.Count - 1].ToString();
                ViewBag.G1Other = zTable.Rows[4][zTable.Columns.Count - 1].ToString();
                ViewBag.G1Off = zTable.Rows[5][zTable.Columns.Count - 1].ToString();
            }
            #endregion

            #region BIỂU ĐỒ ĐƠN HÀNG ĐÃ GIAO THÀNH CÔNG - CÁC ĐƠN HÀNG CÓ PHIẾU THU > TÍNH THEO NGÀY THU
            zTable = Report.ChartComplete(UserLog.PartnerNumber, zFromDate, zToDate, Option, Color, out _);
            if (zTable.Rows.Count > 0)
            {
                ViewBag.Label2 = ChartJS_XLabelName(zTable);
                ViewBag.Dataset2 = ChartJS_Dataset(zTable);

                zTable.Columns.Add("Total", typeof(decimal));
                foreach (DataRow row in zTable.Rows)
                {
                    double rowSum = 0;
                    for (int i = 3; i < zTable.Columns.Count; i++)
                    {
                        DataColumn col = zTable.Columns[i];
                        rowSum += row[col].ToDouble();
                    }
                    row.SetField("Total", rowSum);
                }

                ViewBag.G2All = zTable.Rows[0][zTable.Columns.Count - 1].ToString();
                ViewBag.G2Spe = zTable.Rows[1][zTable.Columns.Count - 1].ToString();
                ViewBag.G2Laz = zTable.Rows[2][zTable.Columns.Count - 1].ToString();
                ViewBag.G2Tiki = zTable.Rows[3][zTable.Columns.Count - 1].ToString();
                ViewBag.G2Other = zTable.Rows[4][zTable.Columns.Count - 1].ToString();
                ViewBag.G2Off = zTable.Rows[5][zTable.Columns.Count - 1].ToString();
            }
            #endregion

            #region [CHỈ SỐ PANEL]
            var LoiBanHang = TN_Mics.LoiBanhang(zFromDate, zToDate, UserLog.PartnerNumber);
            var MuaHang = TN_Mics.MuaHang(zFromDate, zToDate, UserLog.PartnerNumber);
            var Banhang = TN_Mics.BanHang(zFromDate, zToDate, UserLog.PartnerNumber);

            ViewBag.Banhang = Banhang;
            ViewBag.MuaHang = MuaHang;
            ViewBag.LoiBanHang = LoiBanHang;
            #endregion


            //#region BIỂU ĐỒ DOANH SỐ > CÁC PHIẾU THU
            //zTable = Report.ChartMoney(UserLog.PartnerNumber, zFromDate, zToDate, Option, Color);
            //if (zTable.Rows.Count > 0)
            //{
            //    ViewBag.Label3 = ChartJS_XLabelName(zTable);
            //    ViewBag.Dataset3 = ChartJS_Dataset(zTable);

            //    zTable.Columns.Add("Total", typeof(decimal));
            //    foreach (DataRow row in zTable.Rows)
            //    {
            //        double rowSum = 0;
            //        for (int i = 3; i < zTable.Columns.Count; i++)
            //        {
            //            DataColumn col = zTable.Columns[i];
            //            rowSum += row[col].ToDouble();
            //        }
            //        row.SetField("Total", rowSum);
            //    }

            //    ViewBag.G3All = zTable.Rows[0][zTable.Columns.Count - 1].ToString();
            //    ViewBag.G3Spe = zTable.Rows[1][zTable.Columns.Count - 1].ToString();
            //    ViewBag.G3Laz = zTable.Rows[2][zTable.Columns.Count - 1].ToString();
            //    ViewBag.G3Tiki = zTable.Rows[3][zTable.Columns.Count - 1].ToString();
            //    ViewBag.G3Other = zTable.Rows[4][zTable.Columns.Count - 1].ToString();
            //    ViewBag.G3Off = zTable.Rows[5][zTable.Columns.Count - 1].ToString();
            //}
            //#endregion

            //#region BIỂU ĐỒ LỢI NHUẬN > TỔNG THU - TỔNG CHI
            //zTable = Report.ChartProfit(UserLog.PartnerNumber, zFromDate, zToDate, Option, Color);
            //if (zTable.Rows.Count > 0)
            //{
            //    ViewBag.Label4 = ChartJS_XLabelName(zTable);
            //    ViewBag.Dataset4 = ChartJS_Dataset(zTable);

            //    zTable.Columns.Add("Total", typeof(decimal));
            //    foreach (DataRow row in zTable.Rows)
            //    {
            //        double rowSum = 0;
            //        for (int i = 3; i < zTable.Columns.Count; i++)
            //        {
            //            DataColumn col = zTable.Columns[i];
            //            rowSum += row[col].ToDouble();
            //        }
            //        row.SetField("Total", rowSum);
            //    }

            //    ViewBag.G4All = zTable.Rows[0][zTable.Columns.Count - 1].ToString();
            //    ViewBag.G4Spe = zTable.Rows[1][zTable.Columns.Count - 1].ToString();
            //    ViewBag.G4Laz = zTable.Rows[2][zTable.Columns.Count - 1].ToString();
            //    ViewBag.G4Tiki = zTable.Rows[3][zTable.Columns.Count - 1].ToString();
            //    ViewBag.G4Other = zTable.Rows[4][zTable.Columns.Count - 1].ToString();
            //    ViewBag.G4Off = zTable.Rows[5][zTable.Columns.Count - 1].ToString();
            //}
            //#endregion

            ViewBag.Start = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.End = zToDate.ToString("dd/MM/yyyy");

            return View();
        }
        private string ChartJS_XLabelName(DataTable Table)
        {
            string zLabel = "";
            if (Table.Columns.Count > 4)
            {
                zLabel = "[";
                for (int i = 3; i < Table.Columns.Count; i++)
                {
                    DataColumn c = Table.Columns[i];
                    zLabel += "'" + c.ColumnName + "',";
                }
                zLabel = zLabel.Remove(zLabel.LastIndexOf(','), 1);
                zLabel += "]";
            }
            else
            {
                zLabel = "['',";

                DataColumn c = Table.Columns[3];
                zLabel += "'" + c.ColumnName + "',";

                zLabel += ",'']";
            }
            return zLabel;
        }
        private string ChartJS_Dataset(DataTable Table)
        {
            string zDataset = "";
            if (Table.Columns.Count > 4)
            {
                foreach (DataRow r in Table.Rows)
                {
                    int Num = TN_Utils.RandomNumber(0, 299);

                    zDataset += "{ label: '" + r[0].ToString() + "',";
                    zDataset += " borderColor: '" + r[1].ToString() + "',";
                    zDataset += " backgroundColor: '" + r[1].ToString() + "',";
                    zDataset += " borderWidth: 1,";
                    zDataset += " data: [";
                    for (int i = 3; i < Table.Columns.Count; i++)
                    {
                        zDataset += r[i].ToDouble() + ",";
                    }
                    zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
                    zDataset += " ],";
                    zDataset += " datalabels: {align: 'end',anchor: 'end'}";
                    zDataset += "},";
                }
                zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
            }
            else
            {
                foreach (DataRow r in Table.Rows)
                {
                    int Num = TN_Utils.RandomNumber(0, 299);

                    zDataset += "{ label: '" + r[0].ToString() + "',";
                    zDataset += " borderColor: '" + r[1].ToString() + "',";
                    zDataset += " backgroundColor: '" + r[1].ToString() + "',";
                    zDataset += " borderWidth: 1,";
                    zDataset += " data: [";

                    zDataset += "NaN," + r[3].ToDouble() + ",NaN,";

                    zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
                    zDataset += " ],";
                    zDataset += " datalabels: {align: 'end',anchor: 'end'}";
                    zDataset += "},";
                }
                zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
            }

            return zDataset;
        }

        public IActionResult ProductTop(string FromDate, string ToDate,string Category="")
        {
            if(Category==null)
            {
                Category = "";
            }   
            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate != null && ToDate != null)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                zFromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }

            ////tuỳ chỉnh giao diện biểu đồ
            if (zToDate.Month == DateTime.Now.Month)
            {
                zToDate = new DateTime(zToDate.Year, zToDate.Month, DateTime.Now.Day, 23, 0, 0);
            }
            ViewBag.ListCategory = Product_Category_Data.List(TN_Helper.PartnerNumber, 0, out string Error);
            ViewBag.ListChild = Product_Category_Data.ListChild(TN_Helper.PartnerNumber, out string zError);

            ViewBag.CategoryKey = Category.ToString();
            int Total = 0;
            DataTable zTable1 = Report.ProductGroupTop(UserLog.PartnerNumber, zFromDate, zToDate, Category, out _);
            if (zTable1.Rows.Count > 0)
            {
                string Label = "[";
                string Data = "[";
              
                foreach (DataRow row in zTable1.Rows)
                {
                    Label += "'" + row[0].ToString() + "',";
                    Data += row[1].ToDouble() + ",";
                    Total += row[1].ToInt();
                }
                Label = Label.Remove(Label.LastIndexOf(','), 1);
                Data = Data.Remove(Data.LastIndexOf(','), 1);
                Label += "]";
                Data += "]";

                ViewBag.Label1 = Label;
                ViewBag.Dataset1 = Data;


            }
            ViewBag.Total = Total.ToString("n0").Replace(",",".");
            DataTable zTable2 = Report.ProductTop(UserLog.PartnerNumber, zFromDate, zToDate, Category, out _);
            if (zTable2.Rows.Count > 0)
            {
                string Label = "[";
                string Data = "[";
                foreach (DataRow row in zTable2.Rows)
                {
                    Label += "'" + row[1].ToString() + "',";
                    Data += row[2].ToDouble() +",";
                }
                Label=Label.Remove(Label.LastIndexOf(','), 1);
                Data = Data.Remove(Data.LastIndexOf(','), 1);
                Label += "]";
                Data += "]";

                ViewBag.Label2 = Label;
                ViewBag.Dataset2 = Data;

                
            }
            
            ViewBag.Start = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.End = zToDate.ToString("dd/MM/yyyy");

            return View();
        }

        public IActionResult Buyer(string filter,string Option)
        {
            if(filter==null)
            {
                filter = "";
            }    
            if(Option==null)
            {
                Option = "0";
            }    
            ViewBag.Buyer = Report.List_Buyer(UserLog.PartnerNumber, filter.ExTrim(),Option.ToInt(), out _);
            return View();
        }

        public IActionResult YearIndex(string FromDate, string ToDate)
        {

            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate != null && ToDate != null)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                zFromDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
                zToDate = zFromDate.AddYears(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }

            //tuỳ chỉnh giao diện biểu đồ
            if (zToDate.Month == DateTime.Now.Month)
            {
                zToDate = new DateTime(zToDate.Year, zToDate.Month, DateTime.Now.Day, 23, 0, 0);
            }

            #region BIỂU ĐỒ NHẬP HÀNG
            DataTable zTable1 = Report.NhapHang(UserLog.PartnerNumber, zFromDate, zToDate, out _);
            if (zTable1.Rows.Count > 0)
            {

                if (zTable1.Rows.Count > 0)
                {
                    string Label = "[";
                    string Data = "{ label: 'Nhập hàng', borderColor: '#0088cc82', backgroundColor: '#0088cc82', borderWidth: 1, data:[";

                    foreach (DataRow row in zTable1.Rows)
                    {
                        Label += "'" + row[0].ToString() + "',";
                        Data += row[1].ToDouble() + ",";
                    }
                    Label = Label.Remove(Label.LastIndexOf(','), 1);
                    Data = Data.Remove(Data.LastIndexOf(','), 1);
                    Label += "]";
                    Data += "], datalabels: {align: 'end',anchor: 'end'}}";

                    ViewBag.Label1 = Label;
                    ViewBag.Dataset1 = Data;


                }
            }
            #endregion
            #region BIỂU ĐỒ DOANH SỐ
            DataTable zTable2 = Report.Doanhso(UserLog.PartnerNumber, zFromDate, zToDate, out _);
            if (zTable2.Rows.Count > 0)
            {

                if (zTable2.Rows.Count > 0)
                {
                    string Label = "[";
                    string Data = "{ label: 'Doanh số', borderColor: '#0088cc82', backgroundColor: '#0088cc82', borderWidth: 1, data:[";

                    foreach (DataRow row in zTable2.Rows)
                    {
                        Label += "'" + row[0].ToString() + "',";
                        Data += row[1].ToDouble() + ",";
                    }
                    Label = Label.Remove(Label.LastIndexOf(','), 1);
                    Data = Data.Remove(Data.LastIndexOf(','), 1);
                    Label += "]";
                    Data += "], datalabels: {align: 'end',anchor: 'end'}}";

                    ViewBag.Label2 = Label;
                    ViewBag.Dataset2 = Data;


                }
            }
            #endregion


            ViewBag.Start = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.End = zToDate.ToString("dd/MM/yyyy");

            return View();
        }
        public IActionResult DanhSachTienLoi(string FromDate, string ToDate,string Name,string Status)
        {
            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate != null && ToDate != null)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                zFromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }

            //tuỳ chỉnh giao diện biểu đồ
            if (zToDate.Month == DateTime.Now.Month)
            {
                zToDate = new DateTime(zToDate.Year, zToDate.Month, DateTime.Now.Day, 23, 0, 0);
            }
            if (Status == null)
            {
                Status = " 302,402 ";
            }
            
            ViewBag.Order = Order_Data.DanhSachTienLoi(UserLog.PartnerNumber,Status, zFromDate,zToDate, Name.ExTrim(), out _);
            ViewBag.Start = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.End = zToDate.ToString("dd/MM/yyyy");
            ViewBag.Status = Status;
            return View();
        }
        public IActionResult DanhSachDoanhSo(string FromDate, string ToDate, string Name, string Status)
        {
            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate != null && ToDate != null)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                zFromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }

            //tuỳ chỉnh giao diện biểu đồ
            if (zToDate.Month == DateTime.Now.Month)
            {
                zToDate = new DateTime(zToDate.Year, zToDate.Month, DateTime.Now.Day, 23, 0, 0);
            }
            if (Status == null)
            {
                Status = " 302,402 ";
            }

            ViewBag.Order = Order_Data.DanhSachDoanhSo(UserLog.PartnerNumber, Status, zFromDate, zToDate, Name.ExTrim(), out _);
            ViewBag.Start = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.End = zToDate.ToString("dd/MM/yyyy");
            ViewBag.Status = Status;
            return View();
        }
        public IActionResult DanhSachMuaHang(string FromDate, string ToDate, string Name, string Status)
        {
            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate != null && ToDate != null)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                zFromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }

            //tuỳ chỉnh giao diện biểu đồ
            if (zToDate.Month == DateTime.Now.Month)
            {
                zToDate = new DateTime(zToDate.Year, zToDate.Month, DateTime.Now.Day, 23, 0, 0);
            }
            if (Status == null)
            {
                Status = " 101 ";
            }

            ViewBag.Order = Order_Data.DanhSachMuaHang(UserLog.PartnerNumber, Status, zFromDate, zToDate, Name.ExTrim(), out _);
            ViewBag.Start = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.End = zToDate.ToString("dd/MM/yyyy");
            ViewBag.Status = Status;
            return View();
        }
    }
}