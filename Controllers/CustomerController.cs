﻿using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;

namespace ShopCP.Controllers
{
    public class CustomerController : BaseController
    {
        #region [--KHÁCH HÀNG--]
        [Route("khach-hang")]
        public IActionResult List_Customer(string filter)
        {
            ViewBag.Customer = Customer_Data.List_Customer(UserLog.PartnerNumber, filter.ExTrim(), out _);
            return View("~/Views/Customer/Individual/List.cshtml");
        }
        public IActionResult Edit_Customer(string CustomerKey)
        {
            var zReferrer = new List<string>();
            zReferrer.Add("Shopee");
            zReferrer.Add("Lazada");
            zReferrer.Add("Tiki");
            zReferrer.Add("Sendo");
            zReferrer.Add("Khác");
            ViewBag.Referrer = zReferrer;

            Customer_Info zInfo = new(CustomerKey);
            return View("~/Views/Customer/Individual/_Edit.cshtml", zInfo.Customer);
        }
        [HttpPost]
        public JsonResult Save_Customer(
            string CustomerKey, string Referrer, string FullName, string Birthday, string Phone,
            string Email, string Address, string Note)
        {
            var zResult = new ServerResult();
            DateTime zDate = DateTime.MinValue;
            if (!DateTime.TryParseExact(Birthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDate))
            {
                zResult.Success = false;
                zResult.Message = "Sai định dạng ngày !.";
                return Json(zResult);
            }

            try
            {
                Customer_Model zModel = new();
                zModel.CustomerKey = CustomerKey.ExTrim();
                zModel.Referrer = Referrer.ExTrim();
                zModel.FullName = FullName.ExTrim();
                zModel.Birthday = zDate;
                zModel.Phone = Phone.ExTrim();
                zModel.Email = Email.ExTrim();
                zModel.Address = Address.ExTrim();
                zModel.Note = Note.ExTrim();
                zModel.CustomerVendor = 0; //0: nhà khách hàng
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedName = UserLog.UserName;
                zModel.ModifiedName = UserLog.UserName;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.ModifiedBy = UserLog.UserKey;

                //save data
                Customer_Info zInfo = new(CustomerKey);
                zInfo.Customer = zModel;
                if (zInfo.Code == "404")
                {
                    zInfo.Create_KeyAuto();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = "TempData error vui lòng liên hệ IT";
                }

            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Delete_Customer(string CustomerKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Customer_Info();
            zInfo.Customer.CustomerKey = CustomerKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        public IActionResult Export_Customer()
        {
            var zList = Customer_Data.List_Customer(UserLog.PartnerNumber, string.Empty, out _);
            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Khách hàng");
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.Cells.Style.Font.Size = 12;
                worksheet.Cells.AutoFilter = true;
                worksheet.Cells.AutoFitColumns();
                worksheet.View.FreezePanes(4, 4);

                #region[Fill data]
                // First row
                var row = 2;
                worksheet.Cells[row, 4].Value = "DANH SÁCH KHÁCH HÀNG";
                row = 3;
                worksheet.Cells[row, 1].Value = "STT";

                worksheet.Cells[row, 2].Value = "Họ và tên";

                worksheet.Cells[row, 3].Value = "Số điện thoại";

                worksheet.Cells[row, 4].Value = "Địa chỉ";

                worksheet.Cells[row, 5].Value = "Email";

                worksheet.Cells[row, 6].Value = "Ghi chú";

                foreach (Customer_Model zItem in zList)
                {
                    row++;
                    worksheet.Cells[row, 1].Value = row - 4;
                    worksheet.Cells[row, 2].Value = zItem.FullName;
                    worksheet.Cells[row, 3].Value = zItem.Phone;
                    worksheet.Cells[row, 4].Value = zItem.Address;
                    worksheet.Cells[row, 5].Value = zItem.Email;
                    worksheet.Cells[row, 6].Value = zItem.Note;
                }
                #endregion
                //Custom

                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 55;
                worksheet.Column(2).Style.WrapText = true;
                worksheet.Column(3).Width = 15;
                worksheet.Column(4).Width = 55;
                worksheet.Column(5).Width = 30;
                worksheet.Column(6).Width = 30;

                //Rowheader và row tổng

                worksheet.Row(2).Height = 30;
                worksheet.Row(2).Style.WrapText = false;
                worksheet.Row(2).Style.Font.Bold = true;
                worksheet.Row(2).Style.Font.Color.SetColor(Color.Blue);
                worksheet.Row(2).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Row(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.WrapText = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Style.Font.Color.SetColor(Color.Blue);
                worksheet.Row(3).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                package.Save();
            }

            stream.Position = 0;
            string filename = "Danh sách khách hàng.xlsx";
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
        }
        [HttpGet]
        public JsonResult CheckCustomerPhone(string Phone)
        {
            var zResult = new ServerResult();
            var Exists = Customer_Data.CheckCustomerPhone(Phone, UserLog.PartnerNumber);
            zResult.Success = Exists;
            return Json(zResult);
        }
        #endregion

        #region [--NHÀ CUNG CẤP--]
        [Route("nha-cung-cap")]
        public IActionResult List_Vendor(string filter)
        {
            ViewBag.Customer = Customer_Data.List_Vendor(UserLog.PartnerNumber, filter.ExTrim(), out _);
            return View("~/Views/Customer/Vendor/List.cshtml");
        }
        public IActionResult Edit_Vendor(string CustomerKey)
        {
            var zReferrer = new List<string>();
            zReferrer.Add("Shopee");
            zReferrer.Add("Lazada");
            zReferrer.Add("Tiki");
            zReferrer.Add("Sendo");
            zReferrer.Add("Khác");
            ViewBag.Referrer = zReferrer;

            Customer_Info zInfo = new(CustomerKey);
            return View("~/Views/Customer/Vendor/_Edit.cshtml", zInfo.Customer);
        }
        [HttpPost]
        public JsonResult Save_Vendor(
            string CustomerKey, string Referrer, string FullName, string Birthday, string Phone,
            string Email, string Address, string Note)
        {
            var zResult = new ServerResult();
            DateTime zDate = DateTime.MinValue;
            if (!DateTime.TryParseExact(Birthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDate))
            {
                zResult.Success = false;
                zResult.Message = "Sai định dạng ngày !.";
                return Json(zResult);
            }

            try
            {
                Customer_Model zModel = new();
                zModel.CustomerKey = CustomerKey;
                zModel.Birthday = zDate;
                zModel.Referrer = Referrer.ExTrim();
                zModel.FullName = FullName.ExTrim();
                zModel.Phone = Phone.ExTrim();
                zModel.Email = Email.ExTrim();
                zModel.Address = Address.ExTrim();
                zModel.Note = Note.ExTrim();
                zModel.CustomerVendor = 1; //1: nhà cung cấp
                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedName = UserLog.UserName;
                zModel.ModifiedName = UserLog.UserName;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.ModifiedBy = UserLog.UserKey;

                //save data
                Customer_Info zInfo = new(CustomerKey);
                zInfo.Customer = zModel;
                if (zInfo.Code == "404")
                {
                    zInfo.Create_KeyAuto();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = "TempData error vui lòng liên hệ IT";
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Delete_Vendor(string CustomerKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Customer_Info();
            zInfo.Customer.CustomerKey = CustomerKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        public IActionResult Export_Vendor()
        {
            var zList = Customer_Data.List_Vendor(UserLog.PartnerNumber, string.Empty, out _);
            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Nhà cung cấp");
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.Cells.Style.Font.Size = 12;
                worksheet.Cells.AutoFilter = true;
                worksheet.Cells.AutoFitColumns();
                worksheet.View.FreezePanes(4, 4);

                #region[Fill data]
                // First row
                var row = 2;
                worksheet.Cells[row, 4].Value = "DANH SÁCH NHÀ CUNG CẤP";
                row = 3;
                worksheet.Cells[row, 1].Value = "STT";

                worksheet.Cells[row, 2].Value = "Họ và tên";

                worksheet.Cells[row, 3].Value = "Số điện thoại";

                worksheet.Cells[row, 4].Value = "Địa chỉ";

                worksheet.Cells[row, 5].Value = "Email";

                worksheet.Cells[row, 6].Value = "Ghi chú";

                foreach (Customer_Model zItem in zList)
                {
                    row++;
                    worksheet.Cells[row, 1].Value = row - 4;
                    worksheet.Cells[row, 2].Value = zItem.FullName;
                    worksheet.Cells[row, 3].Value = zItem.Phone;
                    worksheet.Cells[row, 4].Value = zItem.Address;
                    worksheet.Cells[row, 5].Value = zItem.Email;
                    worksheet.Cells[row, 6].Value = zItem.Note;
                }
                #endregion
                //Custom

                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 55;
                worksheet.Column(2).Style.WrapText = true;
                worksheet.Column(3).Width = 15;
                worksheet.Column(4).Width = 55;
                worksheet.Column(5).Width = 30;
                worksheet.Column(6).Width = 30;

                //Rowheader và row tổng

                worksheet.Row(2).Height = 30;
                worksheet.Row(2).Style.WrapText = false;
                worksheet.Row(2).Style.Font.Bold = true;
                worksheet.Row(2).Style.Font.Color.SetColor(Color.Blue);
                worksheet.Row(2).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Row(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.WrapText = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Style.Font.Color.SetColor(Color.Blue);
                worksheet.Row(3).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                package.Save();
            }

            stream.Position = 0;
            string filename = "Danh sách nhà cung cấp.xlsx";
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
        }
        [HttpGet]
        public JsonResult CheckVendorPhone(string Phone)
        {
            var zResult = new ServerResult();
            var Exists = Customer_Data.CheckVendorPhone(Phone, UserLog.PartnerNumber);
            zResult.Success = Exists;
            return Json(zResult);
        }
        #endregion
    }
}