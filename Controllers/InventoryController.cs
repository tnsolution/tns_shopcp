﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OfficeOpenXml;
using SDK.Shopee;
using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

/*

    **  nhập hàng    **
mua                     - slug = 101 x nhập kho
khách trả               - slug = 102 x  nhập kho
nhập tái chế sl       - slug = 103 x nhập kho sl, không tính giá

    **  xuất hàng   **
huỷ                      -  slug = 201 x    xuất kho    xx tính giá
trả nhà cung cấp    - slug = 202 x  xuất kho    xx tính giá
xuất tái chế sl           - slug = 203 x xuất kho, không tính giá

    **  bán hàng shopee   **
giao hàng      - slug = 301  -   OrderStatus = 1    > xuất kho
hoàn thành    - slug = 302  -   OrderStatus = 2 xx tính giá
huỷ                - slug = 303  -   OrderStatus = 9    > nhập kho slug = 604 (nhập luân chuyển/ chỉnh số lượng)

    **  bán hàng cửa hàng   **
giao hàng      - slug = 401  -   OrderStatus = 1    > xuất kho
hoàn thành    - slug = 402  -   OrderStatus = 2 xx tính giá
huỷ                - slug = 403  -   OrderStatus = 9    > nhập kho  slug = 604 (nhập luân chuyển/ chỉnh số lượng)

    **  sản xuất    **
nhap -  slug = 501 x    nhập kho
xuat -  slug = 502 x    xuất kho

 */

namespace ShopCP.Controllers
{
    public class InventoryController : BaseController
    {
        private readonly ILogger<InventoryController> _Logger;
        public InventoryController(ILogger<InventoryController> logger)
        {
            _Logger = logger;
        }


        #region [--INPUT--]
        [Route("don-mua-hang/{OrderKey}/{Slug}")]
        public IActionResult Edit_Input(string OrderKey, int Slug)
        {
            string Title = "";
            var zInfo = new Purchase_Info(OrderKey);
            var zOrder = zInfo.Purchase;
            zOrder.OrderDate = DateTime.Now;
            zOrder.ListItem = Purchase_Item_Data.List(UserLog.PartnerNumber, OrderKey, out _);
            zOrder.Slug = Slug;

            if (Slug == 101)
            {
                zOrder.OrderID = "INT-" + TN_Utils.RandomPassword().ToUpper();
                Title = "Nhập mua hàng ";
            }
            if (Slug == 102)
            {
                zOrder.OrderID = "RTN-" + TN_Utils.RandomPassword().ToUpper();
                Title = "Khách trả hàng ";
            }

            if (zInfo.Code == "404")
            {
                zOrder.OrderKey = OrderKey;
                ViewBag.Record = "IsNew";
                Title = Title + "(Mới)";
            }
            else
            {
                ViewBag.Record = "IsUpdate";
                Title = Title + "(Điều chỉnh)";
            }
            ViewBag.Title = Title;
            HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zOrder));
            return View("~/Views/Inventory/Input/Edit.cshtml", zOrder);
        }
        [Route("danh-sach-don-mua-hang/{Slug}")]
        public IActionResult List_Input(int Slug)
        {
            string Title = "";
            if (Slug == 101)
            {
                Title = "Nhập mua hàng ";
            }
            if (Slug == 102)
            {
                Title = "Khách trả hàng ";
            }
            ViewBag.Title = Title;
            ViewBag.List = Purchase_Data.List(UserLog.PartnerNumber, Slug, out _);
            return View("~/Views/Inventory/Input/List.cshtml");
        }
        [HttpPost]
        public JsonResult Delete_Input(string OrderKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Purchase_Info();
            zInfo.Purchase.OrderKey = OrderKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        public JsonResult Save_Input(string OrderKey, string inputId, string inputDate, string VendorName, string VendorAddress, string Description, string TotalMoney, int Slug)
        {
            var zResult = new ServerResult();
            if (!DateTime.TryParseExact(inputDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zDate))
            {
                zResult.Success = false;
                zResult.Message = "Sai định dạng ngày !.";
                return Json(zResult);
            }

            try
            {
                #region [Save Input]
                string sPurchase = HttpContext.Session.GetString(OrderKey);
                var zPurchase = JsonConvert.DeserializeObject<Purchase_Model>(sPurchase);
                int Count = 0;
                if (Slug == 101)
                {
                    foreach (var item in zPurchase.ListItem)
                    {

                        if (item.ItemMoney == 0)
                        {
                            Count++;
                        }
                    }
                }
                if (Count > 0)
                {
                    zResult.Success = false;
                    zResult.Message = "Giá nhập hàng không thể bằng 0!.";
                    return Json(zResult);
                }
                zPurchase.Description = Description.ExTrim();
                zPurchase.OrderID = inputId.ExTrim();
                zPurchase.VendorAddress = VendorAddress.ExTrim();
                zPurchase.VendorName = VendorName.ExTrim();
                zPurchase.TotalMoney = TotalMoney.ToDouble();
                zPurchase.OrderDate = zDate;
                zPurchase.Slug = Slug;    // "mua hàng"
                zPurchase.PartnerNumber = UserLog.PartnerNumber;
                zPurchase.CreatedName = UserLog.UserName;
                zPurchase.ModifiedName = UserLog.UserName;
                zPurchase.CreatedBy = UserLog.UserKey;
                zPurchase.ModifiedBy = UserLog.UserKey;
                #endregion

                //save data
                Purchase_Info zInfo = new(OrderKey);
                zInfo.Purchase = zPurchase;
                if (zInfo.Code == "404")
                {
                    zInfo.Purchase.OrderKey = OrderKey;
                    zInfo.Create_ClientKey();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    #region [Save Child]
                    zInfo.DeleteItem();
                    foreach (var item in zPurchase.ListItem)
                    {
                        item.OrderKey = OrderKey;
                        item.PartnerNumber = UserLog.PartnerNumber;
                        item.CreatedName = UserLog.UserName;
                        item.ModifiedName = UserLog.UserName;
                        item.CreatedBy = UserLog.UserKey;
                        item.ModifiedBy = UserLog.UserKey;

                        Purchase_Item_Info zItemInfo = new();
                        zItemInfo.Purchase_Item = item;
                        zItemInfo.Create_ServerKey();

                        if (zItemInfo.Code != "200" &&
                            zItemInfo.Code != "201")
                        {
                            zResult.Success = false;
                            zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                            break;
                        }
                        else
                        {
                            zResult.Success = true;
                        }
                    }
                    #endregion

                    //nếu là mua hàng thi tự động lập phiếu chi
                    if (zResult.Success && 
                        zPurchase.Slug == 101)
                    {
                        zResult.Success = TN_Mics.Create_Payment(zPurchase, out string Message);
                        zResult.Message = Message;
                    }
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }

                //clear session
                HttpContext.Session.Remove(OrderKey);
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }
        [HttpPost]
        public IActionResult Add_Item(string Id, string OrderKey)
        {
            Purchase_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(OrderKey);

                if (!string.IsNullOrEmpty(sPurchase))
                {
                    zPurchase = JsonConvert.DeserializeObject<Purchase_Model>(sPurchase);
                }
                else
                {
                    zPurchase = new Purchase_Model();
                }

                Purchase_Item_Model zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zItem = new Purchase_Item_Model();
                    var Product = new Product_Info(Id).Product;
                    zItem.ItemKey = Product.ProductKey;
                    zItem.ItemName = Product.ProductName;
                    zItem.ItemPhoto = Product.PhotoPath;
                    zItem.ItemQty = 1;
                    zItem.ItemPrice = Product.StandardCost;
                    zItem.ItemMoney = Product.StandardCost;
                    zPurchase.ListItem.Add(zItem);
                }

                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zPurchase));

            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }

            return View("~/Views/Inventory/Input/_Table_Item.cshtml", zPurchase);
        }
        [HttpPost]
        public IActionResult Del_Item(string Id, string OrderKey)
        {
            Purchase_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(OrderKey);
                zPurchase = JsonConvert.DeserializeObject<Purchase_Model>(sPurchase);
                var zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null && zItem.ItemQty > 1)
                {
                    zItem.ItemQty--;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zPurchase.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Input/_Table_Item.cshtml", zPurchase);
        }
        [HttpPost]
        public IActionResult Remove_Item(string Id, string OrderKey)
        {
            Purchase_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(OrderKey);
                zPurchase = JsonConvert.DeserializeObject<Purchase_Model>(sPurchase);
                var zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zPurchase.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Input/_Table_Item.cshtml", zPurchase);
        }
        [HttpPost]
        public IActionResult Qty_Item(string Id, string OrderKey, float qty, float pric)
        {
            Purchase_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(OrderKey);
                zPurchase = JsonConvert.DeserializeObject<Purchase_Model>(sPurchase);
                var zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);

                zItem.ItemQty = qty;
                zItem.ItemPrice = pric;
                zItem.ItemMoney = qty * pric;

                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Input/_Table_Item.cshtml", zPurchase);
        }
        [HttpPost]
        public IActionResult Reverse_Input(string OrderKey, string total)
        {
            Purchase_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(OrderKey);
                zPurchase = JsonConvert.DeserializeObject<Purchase_Model>(sPurchase);

                var Qty = zPurchase.ListItem[0].ItemQty;
                var StandardCost = total.ToDouble() / Qty;
                var Money = Qty * StandardCost;

                zPurchase.ListItem[0].ItemPrice = StandardCost;
                zPurchase.ListItem[0].ItemMoney = Money;

                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Input/_Table_Item.cshtml", zPurchase);
        }
        #endregion

        [HttpGet]
        public IActionResult Print_Preview(string OrderKey, string ViewType)
        {
            var zInfo = new Order_Info(OrderKey);
            var zListItem = Order_Item_Data.List(UserLog.PartnerNumber, OrderKey, out _);
            zInfo.Order.ListItem = zListItem;

            #region Get Shopee Info
            var tmp = JsonConvert.DeserializeObject<SDK.Shopee.Root_OrderInfo.Data>(zInfo.Order.S_JSON);

            string Jss = HttpContext.Session.GetString("Jss");
            string gsession = Guid.NewGuid().ToString();
            var zClient = new ShopeeClient(gsession);
            if (Jss == null)
            {
                var linkshopee = new Link3rt_Info(UserLog.PartnerNumber, "SHOPEE");
                if (linkshopee.Code == "404" || linkshopee.Link3rt.RecordStatus == 0)
                {
                    TempData["Error"] = "Bạn chưa liên kết Shopee !.";
                }
                else
                {
                    string Username = linkshopee.Link3rt.UserName;
                    string Password = linkshopee.Link3rt.Password;
                    string Cook = linkshopee.Link3rt.Cookies;

                    zClient = new(gsession, Cook);
                    var zRoot = zClient.LoginOTP(Username, Password);

                    var ssShopee = new ShopeeSession();
                    ssShopee.GSession = gsession;
                    ssShopee.Cookie = zClient.GetCookie();
                    ssShopee.ShopId = zRoot.data.shopid;
                    ssShopee.ShopName = zRoot.data.display_name;

                    HttpContext.Session.SetString("Jss", JsonConvert.SerializeObject(ssShopee));
                }
            }
            else
            {
                var Sspe = JsonConvert.DeserializeObject<ShopeeSession>(Jss);
                zClient = new(Sspe.GSession, Sspe.Cookie);
            }

            var zRootOrder = zClient.OrderInfo(tmp.order_id);
            if (zRootOrder.data != null)
            {
                ViewBag.Shopee = zRootOrder.data;
                ViewBag.Barcode = TN_Utils.BarCode(zRootOrder.data.MaVanDon, 35, 250, true, out _);
                ViewBag.Qrcode = TN_Utils.QRCode(zRootOrder.data.MaVanDon, 150, 120, out _);
            }
            else
            {
                ViewBag.Message = zClient.Message;
            }
            #endregion


            if (ViewType == "k80")
            {
                return View("~/Views/Inventory/Output/_PrintK80.cshtml", zInfo.Order);
            }
            if (ViewType == "k58")
            {
                return View("~/Views/Inventory/Output/_PrintK58.cshtml", zInfo.Order);
            }
            if (ViewType == "code")
            {
                return View("~/Views/Inventory/Output/_PrintCode.cshtml", zInfo.Order);
            }

            return View("~/Views/Inventory/Output/_PrintK58.cshtml", zInfo.Order);
        }

        [HttpGet]
        public JsonResult CheckStatus()
        {
            string Message = "";

            var zResult = new ServerResult();

            var zInfo = new Link3rt_Info(UserLog.PartnerNumber, "SHOPEE");
            string Username = zInfo.Link3rt.UserName;
            string Password = zInfo.Link3rt.Password;
            string Cook = zInfo.Link3rt.Cookies;

            string gsession = Guid.NewGuid().ToString();
            ShopeeClient zClient = new(gsession, Cook);
            var zRoot = zClient.LoginOTP(Username, Password);
            if (zRoot.data == null)
            {
                zResult.Success = false;
                zResult.Message = "Không thể đăng nhập SHOPEE";
            }

            var zList = Order_Data.List(UserLog.PartnerNumber, 1, DateTime.MinValue, DateTime.MinValue, string.Empty, out _);
            zList = zList.Where(s => s.S_OrderID != string.Empty).ToList();
            var total = 0;
            foreach (var rec in zList)
            {
                var item = JsonConvert.DeserializeObject<SDK.Shopee.Root_OrderInfo.Data>(rec.S_JSON);
                if (item != null)
                {
                    item = zClient.OrderInfo(item.order_id).data;
                    if (item.status == 4)
                    {
                        total++;

                        var OrderInfo = new Order_Info(rec.OrderKey);
                        OrderInfo.Order.Slug = 302;
                        OrderInfo.Close();
                        zResult.Success = TN_Mics.Create_Receipt(OrderInfo.Order, item.complete_time, out Message);
                        zResult.Message = Message;
                    }
                }
            }

            zResult.Data = "Tất cả có " + total + " đã hoàn thành !.";
            zResult.Message = Message;
            return Json(zResult);
        }

        #region [--TỔNG HỢP ĐƠN HÀNG--]
        [Route("cac-don-hang/{Status}")]
        public IActionResult SaleOrder(int Status)
        {
            var zFromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            var zToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            ViewBag.Start = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.End = zToDate.ToString("dd/MM/yyyy");
            ViewBag.Status = Status;

            var zList = Order_Data.List(UserLog.PartnerNumber, Status, zFromDate, zToDate, string.Empty, out _);
            return View("~/Views/Inventory/Sale/Order.cshtml", zList);
        }
        public IActionResult SaleOrderSearch(int Status, string FromDate, string ToDate, string Name)
        {
            var zFromDate = DateTime.Now;
            var zToDate = DateTime.Now;

            if (FromDate != null && ToDate != null)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, 1, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }

            var zList = Order_Data.List(UserLog.PartnerNumber, Status, zFromDate, zToDate, Name.ExTrim(), out _);
            return View("~/Views/Inventory/Sale/_Table_Sale.cshtml", zList);
        }
        [HttpGet]
        public IActionResult SaleView(string Id)
        {
            var zInfo = new Order_Info(Id);
            var zOrder = zInfo.Order;
            zOrder.ListItem = Order_Item_Data.List(UserLog.PartnerNumber, Id, out _);
            return View("~/Views/Inventory/Sale/View.cshtml", zOrder);
        }
        [HttpPost]
        public JsonResult SaleClose(string Id)
        {
            var zResult = new ServerResult();
            var zInfo = new Order_Info(Id);
            zInfo.Order.ModifiedBy = UserLog.UserKey;
            zInfo.Order.ModifiedName = UserLog.UserName;

            if (zInfo.Order.Slug == 301)
            {
                zInfo.Order.Slug = 302;
            }
            if (zInfo.Order.Slug == 401)
            {
                zInfo.Order.Slug = 402;
            }

            zInfo.Close();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                //nếu là bán hàng thành công  thi tự động lập phiếu thu
                if (zInfo.Order.Slug == 302 || 
                    zInfo.Order.Slug == 402)
                {
                    zResult.Success = TN_Mics.Create_Receipt(zInfo.Order, 0, out string Message);
                    zResult.Message = Message;
                }
                else
                {
                    zResult.Success = true;
                    zResult.Message = "Cập nhật thành công !.";
                }
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult SaleCancel(string Id)
        {
            var zResult = new ServerResult();
            var zInfo = new Order_Info(Id);
            zInfo.Order.ModifiedBy = UserLog.UserKey;
            zInfo.Order.ModifiedName = UserLog.UserName;

            if (zInfo.Order.Slug == 301)
            {
                zInfo.Order.Slug = 303;
            }
            if (zInfo.Order.Slug == 401)
            {
                zInfo.Order.Slug = 403;
            }

            zInfo.Cancel();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            //huỷ sau khi chốt tồn kho > phải nhập lại
            //chốt tồn kho vào mỗi đầu tháng
            //if (zInfo.Order.CreatedOn.Month < DateTime.Now.Month)
            //{
            var zOrder = zInfo.Order;
            var zListItem = Order_Item_Data.List(UserLog.PartnerNumber, Id, out _);

            string zInputKey = Guid.NewGuid().ToString();
            var zInput = new Purchase_Model();
            zInput.OrderKey = zInputKey;
            zInput.OrderDate = DateTime.Now;
            zInput.OrderID = "RTN-" + TN_Utils.RandomPassword().ToUpper();
            zInput.Slug = 604;
            zInput.PartnerNumber = UserLog.PartnerNumber;
            zInput.CreatedBy = UserLog.UserKey;
            zInput.CreatedName = UserLog.UserName;
            zInput.ModifiedBy = UserLog.UserKey;
            zInput.ModifiedName = UserLog.UserName;
            zInput.ProduceKey = zOrder.OrderKey;
            zInput.Description = "Nhập lại từ đơn huỷ " + zOrder.OrderID + zOrder.S_OrderID;

            foreach (var rec in zListItem)
            {
                zInput.ListItem.Add(new Purchase_Item_Model()
                {
                    ItemKey = rec.ItemKey,
                    ItemName = rec.ItemName,
                    ItemPhoto = rec.ItemPhoto,
                    ItemPrice = rec.ItemPrice,
                    ItemQty = rec.ItemQty,
                    ItemMoney = rec.ItemMoney,
                    PartnerNumber = rec.PartnerNumber,
                    RecordStatus = 0,
                    OrderKey = zInputKey,
                });
            }

            var zInputInfo = new Purchase_Info();
            zInputInfo.Purchase = zInput;
            zInputInfo.Create_ClientKey();
            //}

            return Json(zResult);
        }
        [Route("mat-hang-dang-giao")]
        public IActionResult SaleItem()
        {
            var zList = Order_Data.TrackItem(UserLog.PartnerNumber, out _);
            return View("~/Views/Inventory/Sale/Item.cshtml", zList);
        }
        public IActionResult SaleWithItem(string Id)
        {
            var zPro = new Product_Info(Id);
            ViewBag.Name = zPro.Product.ProductName;

            var zList = Order_Data.ListByItem(UserLog.PartnerNumber, Id, out _);
            return View("~/Views/Inventory/Sale/OrdersWithItem.cshtml", zList);
        }
        public IActionResult SaleDetail(string Id)
        {
            var zList = Order_Item_Data.List(UserLog.PartnerNumber, Id, out _);
            return View("~/Views/Inventory/Sale/_Table_Detail.cshtml", zList);
        }
        #endregion

        //-------------------------------------------------------------------------------------------------------------------
        [Route("ton-kho-v2")]
        public IActionResult IndexV2(string DateView = "")
        {
            #region [--Datetime string to fromdate - todate--]
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }
            var zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            var zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            var zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            var List = Product_Data.ReportStock(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
            ViewBag.List = List;
            TempData["Error"] = Message;
            ViewBag.DateView = zDateView.ToString("MM/yyyy");
            return View();
        }

        #region [--OUTPUT / SHOPEE --]
        [Route("danh-sach-don-ban-hang")]
        public IActionResult List_Output()
        {
            #region [--Datetime string to fromdate - todate--]
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            var zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            var zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            var zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion
            ViewBag.List = Order_Data.Search_Shopee(UserLog.PartnerNumber, zFromDate, zToDate, string.Empty, out _);
            ViewBag.Start = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.End = zToDate.ToString("dd/MM/yyyy");
            return View("~/Views/Inventory/Output/List.cshtml");
        }
        [HttpGet]
        public IActionResult List_Output_Data(string FromDate, string ToDate, string Name)
        {
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zFromDate);
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zToDate);

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            var zList = Order_Data.Search_Shopee(UserLog.PartnerNumber, zFromDate, zToDate, Name, out _);
            return View("~/Views/Inventory/Output/_Table_Order.cshtml", zList);
        }
        [Route("don-ban-hang/{OrderKey}")]
        public IActionResult Edit_Output(string OrderKey)
        {
            //clear session
            HttpContext.Session.Remove("Output");

            var zInfo = new Order_Info(OrderKey);
            var zOrder = zInfo.Order;
            zOrder.ListItem = Order_Item_Data.List(UserLog.PartnerNumber, OrderKey, out _);

            HttpContext.Session.SetString("Output", JsonConvert.SerializeObject(zOrder));
            if (zOrder.S_JSON.Length > 0)
            {
                var Shopee = JsonConvert.DeserializeObject<SDK.Shopee.Root_OrderInfo.Data>(zOrder.S_JSON);
                ViewBag.ShopeeOrder = Shopee;
            }
            if (zInfo.Code == "404")
            {
                zOrder.OrderID = "OUT-" + TN_Utils.RandomPassword().ToUpper();
                ViewBag.Record = "IsNew";
            }
            else
            {
                ViewBag.Record = "IsUpdate";
            }

            return View("~/Views/Inventory/Output/Edit.cshtml", zOrder);
        }
        [HttpPost]
        public JsonResult Delete_Output(string OrderKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Order_Info();
            zInfo.Order.OrderKey = OrderKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Save_Output(string OrderKey, string Id, string Description)
        {
            var zResult = new ServerResult();
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString("Output");
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                zOrder.OrderID = Id.ExTrim();
                zOrder.Description = Description.ExTrim();
                zOrder.Slug = 1;

                //giá thực thu từ shopee
                var Shopee = JsonConvert.DeserializeObject<SDK.Shopee.Root_OrderInfo.Data>(zOrder.S_JSON);
                var totalItem = Shopee.order_items.Sum(s => (s.item_price.ToDouble() * s.amount));   //tổng tiền hàng * đơn giá - phí shopee
                zOrder.TotalMoney = totalItem - Shopee.card_txn_fee_info.card_txn_fee.ToDouble();

                zOrder.PartnerNumber = UserLog.PartnerNumber;
                zOrder.CreatedName = UserLog.UserName;
                zOrder.ModifiedName = UserLog.UserName;
                zOrder.CreatedBy = UserLog.UserKey;
                zOrder.ModifiedBy = UserLog.UserKey;

                Order_Info zInfo = new(OrderKey);
                zInfo.Order = zOrder;

                if (zInfo.Code == "404")
                {
                    zInfo.Create_ClientKey();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zInfo.DeleteItem();
                    foreach (var item in zOrder.ListItem)
                    {
                        item.OrderKey = OrderKey;
                        item.PartnerNumber = UserLog.PartnerNumber;
                        item.CreatedName = UserLog.UserName;
                        item.ModifiedName = UserLog.UserName;
                        item.CreatedBy = UserLog.UserKey;
                        item.ModifiedBy = UserLog.UserKey;

                        Order_Item_Info zItemInfo = new();
                        zItemInfo.Order_Item = item;
                        zItemInfo.Create_ServerKey();

                        if (zItemInfo.Code != "200" &&
                            zItemInfo.Code != "201")
                        {
                            zResult.Success = false;
                            zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                            break;
                        }
                    }

                    zResult.Success = true;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }

                //clear session
                HttpContext.Session.Remove("Order");
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }
        [HttpGet]
        public JsonResult Check_Item_Output(string Id)
        {
            var zResult = new ServerResult();
            Order_Model zOrder = new();

            try
            {
                string sOrder = HttpContext.Session.GetString("Output");

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    zItem.ItemQty = 1;
                }

                float QtyGet = zItem.ItemQty;
                float QtyStock = Product_Data.GetStock(Id);

                if (QtyStock < QtyGet)
                {
                    zResult.Success = false;
                    zResult.Message = "Vượt quá số lượng còn lại cho phép là (" + QtyStock + ").";
                }
                else
                {
                    zResult.Success = true;
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }
        [HttpPost]
        public IActionResult Add_Item_Output(string Id)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString("Output");

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    var Product = new Product_Info(Id).Product;
                    zItem.ItemKey = Product.ProductKey;
                    zItem.ItemName = Product.ProductName;
                    zItem.ItemPhoto = Product.PhotoPath;
                    zItem.ItemQty = 1;
                    zItem.ItemPrice = Product.SalePrice;    //đơn giá mua vào ***
                    zItem.ItemMoney = Product.SalePrice;    //đơn giá mua vào ****
                    zOrder.ListItem.Add(zItem);
                }

                HttpContext.Session.SetString("Output", JsonConvert.SerializeObject(zOrder));

            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }

            return View("~/Views/Inventory/Output/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Del_Item_Output(string Id)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString("Output");
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null && zItem.ItemQty > 1)
                {
                    zItem.ItemQty--;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zOrder.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString("Output", JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Output/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Remove_Item_Output(string Id)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString("Output");
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zOrder.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString("Output", JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Output/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Qty_Item_Output(string Id, float qty, float pric)
        {
            Order_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString("Output");
                zPurchase = JsonConvert.DeserializeObject<Order_Model>(sPurchase);
                var zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);

                {
                    zItem.ItemQty = qty;
                    zItem.ItemPrice = pric;
                    zItem.ItemMoney = qty * pric;
                }

                HttpContext.Session.SetString("Output", JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Output/_Table_Item.cshtml", zPurchase);
        }
        #endregion

        #region [--PRODUCE--]
        //In =  Order   Xuất
        //Out = Purchase    Nhập

        [Route("danh-sach-don-san-xuat")]
        public IActionResult List_Produce()
        {
            ViewBag.List = Produce_Data.List(UserLog.PartnerNumber, out _);
            return View("~/Views/Inventory/Produce/List.cshtml");
        }
        [Route("don-san-xuat/{Key}")]
        public IActionResult Edit_Produce(int Key)
        {
            var zInfo = new Produce_Info(Key);
            var zModel = zInfo.Produce;
            if (zInfo.Code == "404")
            {
                zModel.ProduceDate = DateTime.Now;
                zModel.ProduceID = "SX-" + TN_Utils.RandomPassword().ToUpper();
                zModel.QtyLoss = 0;

                ViewBag.Record = "IsNew";
                ViewBag.Session = Guid.NewGuid().ToString();
            }
            else
            {
                string SessionIn = "In" + Key;
                string SessionOut = "Out" + Key;

                string ProduceRef = Key + "-" + zModel.ProduceID;
                string Input = Order_Data.Check_Produce_In(ProduceRef, out _);
                string Output = Order_Data.Check_Produce_Out(ProduceRef, out _);

                var ListItemIn = Order_Item_Data.List(UserLog.PartnerNumber, Input, out _); // xuất sản xuất
                var SOrder = new Order_Model();
                SOrder.ListItem = ListItemIn;
                HttpContext.Session.SetString(SessionIn, JsonConvert.SerializeObject(SOrder));

                var ListItemOut = Purchase_Item_Data.List(UserLog.PartnerNumber, Output, out _);    // nhập thành phẩm
                var POrder = new Purchase_Model();
                POrder.ListItem = ListItemOut;
                HttpContext.Session.SetString(SessionOut, JsonConvert.SerializeObject(POrder));

                zModel.Order.ListItem = ListItemIn;
                zModel.Purchase.ListItem = ListItemOut;

                ViewBag.Session = Key;
                ViewBag.Record = "IsUpdate";
            }
            return View("~/Views/Inventory/Produce/Edit.cshtml", zModel);
        }

        [HttpPost]
        public JsonResult Save_Produce(long Key, string ProduceDate, string ProduceID, float QtyLoss, string Description, string Session)
        {
            string SessionIn = "In" + Session;
            string SessionOut = "Out" + Session;
            var zResult = new ServerResult();
            try
            {
                if (!DateTime.TryParseExact(ProduceDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zDate))
                {
                    zResult.Success = false;
                    zResult.Message = "Sai định dạng ngày !.";
                    return Json(zResult);
                }

                #region [--Produce--]
                var zModel = new Produce_Model
                {
                    ProduceDate = zDate,
                    ProduceID = ProduceID,
                    QtyLoss = QtyLoss,
                    Description = Description.ExTrim(),
                    PartnerNumber = UserLog.PartnerNumber,
                    CreatedBy = UserLog.UserKey,
                    CreatedName = UserLog.UserName,
                    ModifiedBy = UserLog.UserKey,
                    ModifiedName = UserLog.UserName
                };

                var zInfo = new Produce_Info(Key);
                zInfo.Produce = zModel;
                if (zInfo.Code == "404")
                {
                    zInfo.Create_KeyAuto();
                    Key = zInfo.Produce.AutoKey;
                }
                else
                {
                    zInfo.Produce.AutoKey = Key;
                    zInfo.Update();
                }
                #endregion

                if ((zInfo.Code == "200" ||
                     zInfo.Code == "201") && Key != 0)
                {
                    string ProduceRef = Key + "-" + zModel.ProduceID;
                    string OKey = Order_Data.Check_Produce_In(ProduceRef, out _);
                    if (OKey == string.Empty)
                    {
                        #region [--Lưu mới xuất kho--]
                        string sOutput = HttpContext.Session.GetString(SessionIn);
                        Order_Model zOModel = new();
                        zOModel = JsonConvert.DeserializeObject<Order_Model>(sOutput);
                        zOModel.OrderKey = Guid.NewGuid().ToString();
                        zOModel.OrderID = "SXA-" + TN_Utils.RandomPassword().ToUpper();
                        zOModel.OrderDate = zDate;
                        zOModel.OrderStatus = 2;
                        zOModel.Slug = 502;   //SX
                        zOModel.ProduceKey = Key + "-" + ProduceID;
                        zOModel.Description = "[-Auto-] Xuất sản xuất";
                        zOModel.PartnerNumber = UserLog.PartnerNumber;
                        zOModel.CreatedBy = UserLog.UserKey;
                        zOModel.CreatedName = UserLog.UserName;
                        zOModel.ModifiedBy = UserLog.UserKey;
                        zOModel.ModifiedName = UserLog.UserName;

                        Order_Info zOinfo = new();
                        zOinfo.Order = zOModel;
                        zOinfo.Create_ClientKey();

                        if (zOinfo.Code == "200" ||
                            zOinfo.Code == "201")
                        {
                            zOinfo.DeleteItem();
                            foreach (var item in zOModel.ListItem)
                            {
                                item.OrderKey = zOModel.OrderKey;
                                item.PartnerNumber = UserLog.PartnerNumber;
                                item.CreatedName = UserLog.UserName;
                                item.ModifiedName = UserLog.UserName;
                                item.CreatedBy = UserLog.UserKey;
                                item.ModifiedBy = UserLog.UserKey;

                                Order_Item_Info zItemInfo = new();
                                zItemInfo.Order_Item = item;
                                zItemInfo.Create_ServerKey();

                                if (zItemInfo.Code != "200" &&
                                    zItemInfo.Code != "201")
                                {
                                    zResult.Success = false;
                                    zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region [--Cập nhật chi tiết xuất kho--]
                        string sOutput = HttpContext.Session.GetString(SessionIn);
                        var zOModel = JsonConvert.DeserializeObject<Order_Model>(sOutput);
                        zOModel.OrderKey = OKey;

                        Order_Info zOinfo = new(OKey);
                        zOinfo.DeleteItem();
                        foreach (var item in zOModel.ListItem)
                        {
                            item.OrderKey = zOModel.OrderKey;
                            item.PartnerNumber = UserLog.PartnerNumber;
                            item.CreatedName = UserLog.UserName;
                            item.ModifiedName = UserLog.UserName;
                            item.CreatedBy = UserLog.UserKey;
                            item.ModifiedBy = UserLog.UserKey;

                            Order_Item_Info zItemInfo = new();
                            zItemInfo.Order_Item = item;
                            zItemInfo.Create_ServerKey();

                            if (zItemInfo.Code != "200" &&
                                zItemInfo.Code != "201")
                            {
                                zResult.Success = false;
                                zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                                break;
                            }
                        }
                        #endregion
                    }
                    OKey = Order_Data.Check_Produce_Out(ProduceRef, out _);
                    if (OKey == string.Empty)
                    {
                        #region [--Lưu mới nhập kho--]
                        string sInput = HttpContext.Session.GetString(SessionOut);
                        Purchase_Model zIModel = new();
                        zIModel = JsonConvert.DeserializeObject<Purchase_Model>(sInput);
                        zIModel.OrderKey = Guid.NewGuid().ToString();
                        zIModel.OrderID = "SXA-" + TN_Utils.RandomPassword().ToUpper();
                        zIModel.OrderDate = zDate;
                        zIModel.OrderStatus = 2;
                        zIModel.Slug = 501;    // SX
                        zIModel.ProduceKey = Key + "-" + ProduceID;
                        zIModel.PartnerNumber = UserLog.PartnerNumber;
                        zIModel.CreatedName = UserLog.UserName;
                        zIModel.ModifiedName = UserLog.UserName;
                        zIModel.CreatedBy = UserLog.UserKey;
                        zIModel.ModifiedBy = UserLog.UserKey;

                        Purchase_Info zIinfo = new();
                        zIinfo.Purchase = zIModel;
                        zIinfo.Create_ClientKey();

                        if (zIinfo.Code == "200" ||
                            zIinfo.Code == "201")
                        {
                            zIinfo.DeleteItem();
                            foreach (var item in zIModel.ListItem)
                            {
                                item.OrderKey = zIModel.OrderKey;
                                item.PartnerNumber = UserLog.PartnerNumber;
                                item.CreatedName = UserLog.UserName;
                                item.ModifiedName = UserLog.UserName;
                                item.CreatedBy = UserLog.UserKey;
                                item.ModifiedBy = UserLog.UserKey;

                                Purchase_Item_Info zItemInfo = new();
                                zItemInfo.Purchase_Item = item;
                                zItemInfo.Create_ServerKey();

                                if (zItemInfo.Code != "200" &&
                                    zItemInfo.Code != "201")
                                {
                                    zResult.Success = false;
                                    zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region [--Cập nhật chi tiết nhập kho--]
                        string sInput = HttpContext.Session.GetString(SessionOut);
                        var zIModel = JsonConvert.DeserializeObject<Purchase_Model>(sInput);
                        zIModel.OrderKey = OKey;
                        Purchase_Info zIinfo = new(OKey);
                        zIinfo.DeleteItem();
                        foreach (var item in zIModel.ListItem)
                        {
                            item.OrderKey = zIModel.OrderKey;
                            item.PartnerNumber = UserLog.PartnerNumber;
                            item.CreatedName = UserLog.UserName;
                            item.ModifiedName = UserLog.UserName;
                            item.CreatedBy = UserLog.UserKey;
                            item.ModifiedBy = UserLog.UserKey;

                            Purchase_Item_Info zItemInfo = new();
                            zItemInfo.Purchase_Item = item;
                            zItemInfo.Create_ServerKey();

                            if (zItemInfo.Code != "200" &&
                                zItemInfo.Code != "201")
                            {
                                zResult.Success = false;
                                zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                                break;
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    zResult.Message = "Error Produce Parent Method";
                    zResult.Success = false;
                }

                zResult.Success = true;
            }
            catch (Exception ex)
            {
                zResult.Message = ex.ToString().GetFirstLine();
                zResult.Success = false;
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Delete_Produce(long Key)
        {
            var zResult = new ServerResult();
            var zInfo = new Produce_Info();
            zInfo.Produce.AutoKey = Key;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }

        //---------------------------------------------------------------------------------In
        [HttpGet]
        public JsonResult Check_Item_Inmake(string Id, string Key)
        {
            string SessionKey = "In" + Key;
            var zResult = new ServerResult();
            Order_Model zOrder = new();

            try
            {
                string sOrder = HttpContext.Session.GetString(SessionKey);

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    zItem.ItemQty = 1;
                }

                float QtyGet = zItem.ItemQty;
                float QtyStock = Product_Data.GetStock(Id);

                if (QtyStock < QtyGet)
                {
                    zResult.Success = false;
                    zResult.Message = "Vượt quá số lượng còn lại cho phép là (" + QtyStock + ").";
                }
                else
                {
                    zResult.Success = true;
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }
        [HttpPost]
        public IActionResult Add_Item_Inmake(string Id, string Key)
        {
            string SessionKey = "In" + Key;
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString(SessionKey);

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    var Product = new Product_Info(Id).Product;
                    zItem.ItemKey = Product.ProductKey;
                    zItem.ItemName = Product.ProductName;
                    zItem.ItemPhoto = Product.PhotoPath;
                    zItem.ItemQty = 1;
                    zItem.ItemPrice = 0;    //đơn giá mua vào ***
                    zItem.ItemMoney = 0;    //đơn giá mua vào ****
                    zOrder.ListItem.Add(zItem);
                }

                HttpContext.Session.SetString(SessionKey, JsonConvert.SerializeObject(zOrder));

            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }

            return View("~/Views/Inventory/Produce/_Table_Item_Inmake.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Del_Item_Inmake(string Id, string Key)
        {
            string SessionKey = "In" + Key;
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString(SessionKey);
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null && zItem.ItemQty > 1)
                {
                    zItem.ItemQty--;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zOrder.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString(SessionKey, JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Produce/_Table_Item_Inmake.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Remove_Item_Inmake(string Id, string Key)
        {
            string SessionKey = "In" + Key;
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString(SessionKey);
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zOrder.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString(SessionKey, JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Produce/_Table_Item_Inmake.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Qty_Item_Inmake_Local(string Id, string Key, float qty, float pric)
        {
            string SessionKey = "In" + Key;
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString(SessionKey);
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);

                {
                    zItem.ItemQty = qty;
                    zItem.ItemPrice = pric;
                    zItem.ItemMoney = qty * pric;
                }

                HttpContext.Session.SetString(SessionKey, JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Produce/_Table_Item_Inmake.cshtml", zOrder);
        }
        //----------------------------------------------------------------------------------Out

        [HttpPost]
        public IActionResult Add_Item_Outmake(string Id, string Key)
        {
            string SessionKey = "Out" + Key;
            Purchase_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(SessionKey);

                if (!string.IsNullOrEmpty(sPurchase))
                {
                    zPurchase = JsonConvert.DeserializeObject<Purchase_Model>(sPurchase);
                }
                else
                {
                    zPurchase = new Purchase_Model();
                }

                Purchase_Item_Model zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                    zItem.ItemPrice = zItem.ItemPrice;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zItem = new Purchase_Item_Model();
                    var Product = new Product_Info(Id).Product;
                    zItem.ItemKey = Product.ProductKey;
                    zItem.ItemName = Product.ProductName;
                    zItem.ItemPhoto = Product.PhotoPath;
                    zItem.ItemQty = 1;
                    zItem.ItemPrice = Product.StandardCost;
                    zItem.ItemMoney = Product.StandardCost * 1;
                    zPurchase.ListItem.Add(zItem);
                }

                HttpContext.Session.SetString(SessionKey, JsonConvert.SerializeObject(zPurchase));

            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }

            return View("~/Views/Inventory/Produce/_Table_Item_Outmake.cshtml", zPurchase);
        }
        [HttpPost]
        public IActionResult Del_Item_Outmake(string Id, string Key)
        {
            string SessionKey = "Out" + Key;
            Purchase_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(SessionKey);
                zPurchase = JsonConvert.DeserializeObject<Purchase_Model>(sPurchase);
                var zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null && zItem.ItemQty > 1)
                {
                    zItem.ItemQty--;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zPurchase.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString(SessionKey, JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Produce/_Table_Item_Outmake.cshtml", zPurchase);
        }
        [HttpPost]
        public IActionResult Remove_Item_Outmake(string Id, string Key)
        {
            string SessionKey = "Out" + Key;
            Purchase_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(SessionKey);
                zPurchase = JsonConvert.DeserializeObject<Purchase_Model>(sPurchase);
                var zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zPurchase.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString(SessionKey, JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Produce/_Table_Item_Outmake.cshtml", zPurchase);
        }
        [HttpPost]
        public IActionResult Qty_Item_Outmake_Local(string Id, string Key, float qty, float pric)
        {
            string SessionKey = "Out" + Key;
            Purchase_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(SessionKey);
                zPurchase = JsonConvert.DeserializeObject<Purchase_Model>(sPurchase);
                var zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);

                {
                    zItem.ItemQty = qty;
                    zItem.ItemPrice = pric;
                    zItem.ItemMoney = qty * pric;
                }

                HttpContext.Session.SetString(SessionKey, JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Produce/_Table_Item_Outmake.cshtml", zPurchase);
        }
        //-----------------------------------------------------------------------------------
        #endregion

        #region [--OUTPUT / LOCAL --]
        [Route("don-ngoai/{OrderKey}")]
        public IActionResult Edit_Output_Local(string OrderKey)
        {
            var zInfo = new Order_Info(OrderKey);
            var zOrder = zInfo.Order;

            if (zInfo.Code == "404")
            {
                zOrder.OrderID = "LOC-" + TN_Utils.RandomPassword().ToUpper();
                zOrder.OrderDate = DateTime.Now;
                zOrder.OrderKey = OrderKey;
                ViewBag.Record = "IsNew";
            }
            else
            {
                zOrder.ListItem = Order_Item_Data.List(UserLog.PartnerNumber, OrderKey, out _);
                ViewBag.Record = "IsUpdate";
            }

            HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zOrder));

            return View("~/Views/Inventory/Output_Local/Edit.cshtml", zOrder);
        }
        [HttpPost]
        public JsonResult Delivery(string OrderKey, string Id, string OrderDate, string Description, string BuyerName, string BuyerPhone, string BuyerAddress, string TotalMoney)
        {
            var zResult = new ServerResult();
            try
            {
                if (!DateTime.TryParseExact(OrderDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zDate))
                {
                    zResult.Success = false;
                    zResult.Message = "Sai định dạng ngày !.";
                    return Json(zResult);
                }

                string sOrder = HttpContext.Session.GetString(OrderKey);
                #region [Save Parent Data]
                Order_Model zOrder = new();
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                zOrder.OrderID = Id.ExTrim();
                zOrder.Description = Description.ExTrim();
                zOrder.BuyerAddress = BuyerAddress.ExTrim();
                zOrder.BuyerName = BuyerName.ExTrim();
                zOrder.BuyerPhone = BuyerPhone.ExTrim();
                zOrder.OrderDate = zDate;
                zOrder.OrderStatus = 1;
                zOrder.Slug = 401;
                zOrder.TotalMoney = TotalMoney.ToDouble();
                zOrder.PartnerNumber = UserLog.PartnerNumber;
                zOrder.CreatedName = UserLog.UserName;
                zOrder.ModifiedName = UserLog.UserName;
                zOrder.CreatedBy = UserLog.UserKey;
                zOrder.ModifiedBy = UserLog.UserKey;

                Order_Info zInfo = new(OrderKey);
                zInfo.Order = zOrder;
                #endregion

                if (zInfo.Code == "404")
                {
                    zInfo.Order.OrderKey = OrderKey;
                    zInfo.Create_ClientKey();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zInfo.DeleteItem();
                    #region [Save Child Data]
                    foreach (var item in zOrder.ListItem)
                    {
                        item.OrderKey = OrderKey;
                        item.PartnerNumber = UserLog.PartnerNumber;
                        item.CreatedName = UserLog.UserName;
                        item.ModifiedName = UserLog.UserName;
                        item.CreatedBy = UserLog.UserKey;
                        item.ModifiedBy = UserLog.UserKey;

                        Order_Item_Info zItemInfo = new();
                        zItemInfo.Order_Item = item;
                        zItemInfo.Create_ServerKey();

                        if (zItemInfo.Code != "200" &&
                            zItemInfo.Code != "201")
                        {
                            zResult.Success = false;
                            zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                            break;
                        }
                    }
                    #endregion

                    zResult.Success = true;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }

                //clear session
                HttpContext.Session.Remove(OrderKey);
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Close(string OrderKey, string Id, string OrderDate, string Description, string BuyerName, string BuyerPhone, string BuyerAddress, string TotalMoney)
        {
            var zResult = new ServerResult();
            try
            {
                if (!DateTime.TryParseExact(OrderDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zDate))
                {
                    zResult.Success = false;
                    zResult.Message = "Sai định dạng ngày !.";
                    return Json(zResult);
                }

                #region [Lưu bảng cha]
                string sOrder = HttpContext.Session.GetString(OrderKey);
                Order_Model zOrder = new();
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                zOrder.OrderID = Id.ExTrim();
                zOrder.Description = Description.ExTrim();
                zOrder.BuyerAddress = BuyerAddress.ExTrim();
                zOrder.BuyerName = BuyerName.ExTrim();
                zOrder.BuyerPhone = BuyerPhone.ExTrim();
                zOrder.OrderDate = zDate;
                zOrder.OrderStatus = 2;
                zOrder.Slug = 402;
                zOrder.TotalMoney = TotalMoney.ToDouble();
                zOrder.PartnerNumber = UserLog.PartnerNumber;
                zOrder.CreatedName = UserLog.UserName;
                zOrder.ModifiedName = UserLog.UserName;
                zOrder.CreatedBy = UserLog.UserKey;
                zOrder.ModifiedBy = UserLog.UserKey;

                Order_Info zInfo = new(OrderKey);
                zInfo.Order = zOrder;
                #endregion

                if (zInfo.Code == "404")
                {
                    zInfo.Order.OrderKey = OrderKey;
                    zInfo.Create_ClientKey();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zInfo.DeleteItem();
                    bool SavedChild = true;

                    #region [Lưu bảng con]
                    foreach (var item in zOrder.ListItem)
                    {
                        item.OrderKey = OrderKey;
                        item.PartnerNumber = UserLog.PartnerNumber;
                        item.CreatedName = UserLog.UserName;
                        item.ModifiedName = UserLog.UserName;
                        item.CreatedBy = UserLog.UserKey;
                        item.ModifiedBy = UserLog.UserKey;

                        Order_Item_Info zItemInfo = new();
                        zItemInfo.Order_Item = item;
                        zItemInfo.Create_ServerKey();

                        if (zItemInfo.Code != "200" &&
                            zItemInfo.Code != "201")
                        {
                            SavedChild = false;
                            zResult.Success = false;
                            zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                            break;
                        }
                    }
                    #endregion

                    if (SavedChild)
                    {
                        zResult.Success = true;

                        //#region [Lưu phiếu thu]
                        //var zReceipt = new Receipt_Info();
                        //var zModel = new Receipt_Model();
                        //zModel.ReceiptID = Receipt_Data.Auto_Receipt_ID(UserLog.PartnerNumber);
                        //zModel.ReceiptDescription = "Thu bán đơn hàng [" + Id.ExTrim() + "] tự động !.";
                        //zModel.Slug = 9; // fix thu bán hàng tự động
                        //zModel.DocumentID = OrderKey;
                        //zModel.ReceiptDate = zDate;
                        //zModel.AmountCurrencyMain = TotalMoney.ToDouble();
                        //zModel.Address = BuyerAddress.ExTrim();
                        //zModel.CustomerName = BuyerName.ExTrim();
                        //zModel.CustomerPhone = BuyerPhone.ExTrim();
                        //zModel.PartnerNumber = UserLog.PartnerNumber;
                        //zModel.CreatedName = UserLog.UserName;
                        //zModel.ModifiedName = UserLog.UserName;
                        //zModel.CreatedBy = UserLog.UserKey;
                        //zModel.ModifiedBy = UserLog.UserKey;
                        //zReceipt.Receipt = zModel;
                        //zReceipt.Create_ClientKey();
                        //if (zReceipt.Code != "200" &&
                        //    zReceipt.Code != "201")
                        //{
                        //    zResult.Success = false;
                        //    zResult.Message = "Lưu Receipt: " + zReceipt.Message.GetFirstLine();
                        //}
                        //#endregion
                    }
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Save_Output_Local(string OrderKey, string Id, string OrderDate, string Description, string BuyerName, string BuyerPhone, string BuyerAddress, string Discount, string TotalMoney)
        {
            var zResult = new ServerResult();
            try
            {
                if (!DateTime.TryParseExact(OrderDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zDate))
                {
                    zResult.Success = false;
                    zResult.Message = "Sai định dạng ngày !.";
                    return Json(zResult);
                }

                #region [Lưu bảng cha]
                string sOrder = HttpContext.Session.GetString(OrderKey);
                Order_Model zOrder = new();
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                zOrder.OrderID = Id.ExTrim();
                zOrder.Description = Description.ExTrim();
                zOrder.BuyerAddress = BuyerAddress.ExTrim();
                zOrder.BuyerName = BuyerName.ExTrim();
                zOrder.BuyerPhone = BuyerPhone.ExTrim();
                zOrder.Discount = Discount.ToDouble();
                zOrder.TotalMoney = TotalMoney.ToDouble();
                zOrder.OrderDate = zDate;
                zOrder.Slug = 400;
                zOrder.PartnerNumber = UserLog.PartnerNumber;
                zOrder.CreatedName = UserLog.UserName;
                zOrder.ModifiedName = UserLog.UserName;
                zOrder.CreatedBy = UserLog.UserKey;
                zOrder.ModifiedBy = UserLog.UserKey;
                #endregion

                Order_Info zInfo = new(OrderKey);
                if (zInfo.Code == "404")
                {
                    zOrder.OrderStatus = 0;
                    zOrder.OrderKey = OrderKey;
                    zInfo.Order = zOrder;
                    zInfo.Create_ClientKey();
                }
                else
                {
                    zInfo.Order = zOrder;
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zInfo.DeleteItem();
                    bool SavedChild = true;

                    #region [Lưu bảng con]
                    foreach (var item in zOrder.ListItem)
                    {
                        item.OrderKey = OrderKey;
                        item.PartnerNumber = UserLog.PartnerNumber;
                        item.CreatedName = UserLog.UserName;
                        item.ModifiedName = UserLog.UserName;
                        item.CreatedBy = UserLog.UserKey;
                        item.ModifiedBy = UserLog.UserKey;

                        Order_Item_Info zItemInfo = new();
                        zItemInfo.Order_Item = item;
                        zItemInfo.Create_ServerKey();

                        if (zItemInfo.Code != "200" &&
                            zItemInfo.Code != "201")
                        {
                            SavedChild = false;
                            zResult.Success = false;
                            zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                            break;
                        }
                    }
                    #endregion

                    if (SavedChild)
                    {
                        zResult.Success = true;
                    }
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }

        [Route("danh-sach-don-ngoai")]
        public IActionResult List_Output_Local()
        {
            var zFromDate = DateTime.Now;
            var zToDate = DateTime.Now;

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, 1, 0, 0, 0);
            zToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            ViewBag.List = Order_Data.Search_Offline(UserLog.PartnerNumber, zFromDate, DateTime.Now, string.Empty, out _);
            ViewBag.Start = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.End = DateTime.Now.ToString("dd/MM/yyyy");
            return View("~/Views/Inventory/Output_Local/List.cshtml");
        }
        //tìm danh sách đơn ngoài
        public IActionResult List_Output_Local_Data(string FromDate, string ToDate, string Name)
        {
            var zFromDate = DateTime.Now;
            var zToDate = DateTime.Now;
            if (FromDate == null || ToDate == null)
            {
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, 1, 0, 0, 0);
                zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }
            else
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }

            var zList = Order_Data.Search_Offline(UserLog.PartnerNumber, zFromDate, zToDate, Name.ExTrim(), out _);
            return View("~/Views/Inventory/Output_Local/_Table_Order.cshtml", zList);
        }
        [HttpPost]
        public JsonResult Delete_Output_Local(string OrderKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Order_Info();
            zInfo.Order.OrderKey = OrderKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }

        //-----------------------------------------------------------------------------------------------------------------------------------Action trên bảng con
        [HttpGet]
        public JsonResult Check_Item_Output_Local(string Id, string OrderKey)
        {
            var zResult = new ServerResult();
            Order_Model zOrder = new();

            try
            {
                string sOrder = HttpContext.Session.GetString(OrderKey);

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    zItem.ItemQty = 1;
                }

                float QtyGet = zItem.ItemQty;
                float QtyStock = Product_Data.GetStock(Id);

                if (QtyStock < QtyGet)
                {
                    zResult.Success = false;
                    zResult.Message = "Vượt quá số lượng còn lại cho phép là (" + QtyStock + ").";
                }
                else
                {
                    zResult.Success = true;
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }
        [HttpPost]
        public IActionResult Add_Item_Output_Local(string Id, string OrderKey)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString(OrderKey);

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);

                if (zItem != null)
                {
                    zItem.ItemQty++;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    var Product = new Product_Info(Id).Product;
                    zItem.ItemKey = Product.ProductKey;
                    zItem.ItemName = Product.ProductName;
                    zItem.ItemPhoto = Product.PhotoPath;
                    zItem.ItemPrice = Product.SalePrice;
                    zItem.ItemMoney = Product.SalePrice;
                    zItem.ItemQty = 1;
                    zOrder.ListItem.Add(zItem);
                }

                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zOrder));

            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }

            return View("~/Views/Inventory/Output_Local/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Del_Item_Output_Local(string Id, string OrderKey)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString(OrderKey);
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null && zItem.ItemQty > 1)
                {
                    zItem.ItemQty--;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zOrder.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Output_Local/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Remove_Item_Output_Local(string Id, string OrderKey)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString(OrderKey);
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zOrder.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Output_Local/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Qty_Item_Output_Local(string Id, string OrderKey, float qty, float pric)
        {
            Order_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(OrderKey);
                zPurchase = JsonConvert.DeserializeObject<Order_Model>(sPurchase);
                var zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);

                zItem.ItemQty = qty;
                zItem.ItemPrice = pric;
                zItem.ItemMoney = qty * pric;

                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Output_Local/_Table_Item.cshtml", zPurchase);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [HttpGet]
        public IActionResult Print_Output_Local(string OrderKey)
        {
            var zInfo = new Order_Info(OrderKey);
            var zOrder = zInfo.Order;
            zOrder.ListItem = Order_Item_Data.List(UserLog.PartnerNumber, OrderKey, out _);

            return View("~/Views/Inventory/Output_Local/_PrintK58.cshtml", zOrder);
        }
        #endregion

        #region [--TRASH--]
        [Route("cac-lenh-huy-hang/{Slug}")]
        public IActionResult List_Trash(int Slug)
        {
            var zFromDate = DateTime.Now;
            var zToDate = DateTime.Now;

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            ViewBag.Start = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.End = DateTime.Now.ToString("dd/MM/yyyy");
            string Title = "";
            if (Slug == 201)
            {
                Title = "Các đơn huỷ hàng ";
            }
            if (Slug == 202)
            {
                Title = "Các đơn trả nhà cung cấp ";
            }
            ViewBag.Title = Title;
            return View("~/Views/Inventory/Trash/List.cshtml");
        }
        public IActionResult Edit_Trash(string OrderKey, int Slug)
        {
            string Title = "";
            var zInfo = new Order_Info(OrderKey);
            var zOrder = zInfo.Order;
            zOrder.ListItem = Order_Item_Data.List(UserLog.PartnerNumber, OrderKey, out _);
            zOrder.OrderDate = DateTime.Now;
            zOrder.Slug = Slug;

            if (Slug == 201)
            {
                zOrder.OrderID = "TAH-" + TN_Utils.RandomPassword().ToUpper();
                Title = "Xuất huỷ hàng";
            }
            if (Slug == 202)
            {
                zOrder.OrderID = "RVD-" + TN_Utils.RandomPassword().ToUpper();
                Title = "Xuất trả hàng nhà cung cấp ";
            }

            if (zInfo.Code == "404")
            {
                zOrder.OrderKey = OrderKey;
                ViewBag.Record = "IsNew";
            }
            else
            {

                ViewBag.Record = "IsUpdate";
            }

            ViewBag.Title = Title;

            HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zOrder));
            return View("~/Views/Inventory/Trash/Edit.cshtml", zOrder);
        }
        [HttpPost]
        public JsonResult Save_Trash(string OrderKey, string OrderID, string OrderDate, string Description, int Slug)
        {
            var zResult = new ServerResult();
            try
            {
                if (!DateTime.TryParseExact(OrderDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zDate))
                {
                    zResult.Success = false;
                    zResult.Message = "Sai định dạng ngày !.";
                    return Json(zResult);
                }

                #region [Lưu bảng cha]
                string sOrder = HttpContext.Session.GetString(OrderKey);
                Order_Model zOrder = new();
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                zOrder.OrderID = OrderID.ExTrim();
                zOrder.Description = Description.ExTrim();
                zOrder.OrderDate = zDate;
                zOrder.Slug = Slug;
                zOrder.PartnerNumber = UserLog.PartnerNumber;
                zOrder.CreatedName = UserLog.UserName;
                zOrder.ModifiedName = UserLog.UserName;
                zOrder.CreatedBy = UserLog.UserKey;
                zOrder.ModifiedBy = UserLog.UserKey;
                #endregion

                Order_Info zInfo = new(OrderKey);
                if (zInfo.Code == "404")
                {
                    zOrder.OrderStatus = 0;
                    zOrder.OrderKey = OrderKey;
                    zInfo.Order = zOrder;
                    zInfo.Create_ClientKey();
                }
                else
                {
                    zInfo.Order = zOrder;
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zInfo.DeleteItem();
                    bool SavedChild = true;

                    #region [Lưu bảng con]
                    foreach (var item in zOrder.ListItem)
                    {
                        item.OrderKey = OrderKey;
                        item.PartnerNumber = UserLog.PartnerNumber;
                        item.CreatedName = UserLog.UserName;
                        item.ModifiedName = UserLog.UserName;
                        item.CreatedBy = UserLog.UserKey;
                        item.ModifiedBy = UserLog.UserKey;

                        Order_Item_Info zItemInfo = new();
                        zItemInfo.Order_Item = item;
                        zItemInfo.Create_ServerKey();

                        if (zItemInfo.Code != "200" &&
                            zItemInfo.Code != "201")
                        {
                            SavedChild = false;
                            zResult.Success = false;
                            zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                            break;
                        }
                    }
                    #endregion

                    if (SavedChild)
                    {
                        //zResult.Success = TN_Mics.Create_Tracking(zInfo.Order, out string Message);
                        //zResult.Message = Message;
                    }
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Delete_Trash(string OrderKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Order_Info();
            zInfo.Order.OrderKey = OrderKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------Action trên bảng con
        [HttpGet]
        public JsonResult Check_Item_Trash(string Id, string OrderKey)
        {
            var zResult = new ServerResult();
            Order_Model zOrder = new();

            try
            {
                string sOrder = HttpContext.Session.GetString(OrderKey);

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    zItem.ItemQty = 1;
                }

                float QtyGet = zItem.ItemQty;
                float QtyStock = Product_Data.GetStock(Id);

                if (QtyStock < QtyGet)
                {
                    zResult.Success = false;
                    zResult.Message = "Vượt quá số lượng còn lại cho phép là (" + QtyStock + ").";
                }
                else
                {
                    zResult.Success = true;
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }
        [HttpPost]
        public IActionResult Add_Item_Trash(string Id, string OrderKey)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString(OrderKey);

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);

                if (zItem != null)
                {
                    zItem.ItemQty++;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    var Product = new Product_Info(Id).Product;
                    zItem.ItemKey = Product.ProductKey;
                    zItem.ItemName = Product.ProductName;
                    zItem.ItemPhoto = Product.PhotoPath;
                    zItem.ItemPrice = Product.SalePrice;
                    zItem.ItemMoney = Product.SalePrice;
                    zItem.ItemQty = 1;
                    zOrder.ListItem.Add(zItem);
                }

                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zOrder));

            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }

            return View("~/Views/Inventory/Trash/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Del_Item_Trash(string Id, string OrderKey)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString(OrderKey);
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null && zItem.ItemQty > 1)
                {
                    zItem.ItemQty--;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zOrder.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Trash/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Remove_Item_Trash(string Id, string OrderKey)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString(OrderKey);
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zOrder.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Trash/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Qty_Item_Trash(string Id, string OrderKey, float qty, float pric)
        {
            Order_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString(OrderKey);
                zPurchase = JsonConvert.DeserializeObject<Order_Model>(sPurchase);
                var zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);

                zItem.ItemQty = qty;
                zItem.ItemPrice = pric;
                zItem.ItemMoney = qty * pric;

                HttpContext.Session.SetString(OrderKey, JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.ToString());
                ViewBag.Error = ex.ToString();
            }
            return View("~/Views/Inventory/Trash/_Table_Item.cshtml", zPurchase);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        #endregion

        [Route("nhap-ton-dau")]
        public IActionResult BeginStock()
        {
            ViewBag.DateView = DateTime.Now.ToString("MM/yyyy");
            return View("~/Views/Inventory/Import/BeginStock.cshtml");
        }
        public IActionResult ExportStock()
        {
            var zList = Product_Data.List(UserLog.PartnerNumber, string.Empty, out _);

            var stream = new MemoryStream();

            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("sheetNo1");
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.Cells.Style.Font.Size = 12;
                worksheet.Cells.AutoFilter = true;
                worksheet.Cells.AutoFitColumns();


                #region[Fill data]
                // First row
                var row = 1;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 2].Value = "Key";
                worksheet.Cells[row, 3].Value = "SKU";
                worksheet.Cells[row, 4].Value = "Tên sản phẩm";
                worksheet.Cells[row, 5].Value = "Số lượng tồn";
                worksheet.Cells[row, 6].Value = "Số tiền";

                foreach (Product_Model zItem in zList)
                {
                    row++;
                    worksheet.Cells[row, 1].Value = row - 1;
                    worksheet.Cells[row, 2].Value = zItem.ProductKey;
                    worksheet.Cells[row, 3].Value = zItem.SKU;
                    worksheet.Cells[row, 4].Value = zItem.ProductName;
                    worksheet.Cells[row, 5].Value = 0;
                    worksheet.Cells[row, 6].Value = 0;
                }
                #endregion
                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 45;
                worksheet.Column(3).Width = 17;
                worksheet.Column(4).Width = 50;
                worksheet.Column(5).Width = 17;
                worksheet.Column(6).Width = 17;
                worksheet.Column(7).Width = 17;

                worksheet.Column(4).Style.WrapText = true;

                worksheet.Column(5).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                worksheet.Column(6).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;


                //Rowheader và row tổng
                worksheet.Row(1).Height = 30;
                worksheet.Row(1).Style.WrapText = true;
                worksheet.Row(1).Style.Font.Bold = true;
                worksheet.Row(1).Style.Font.Color.SetColor(Color.Blue);
                worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                package.Save();
            }

            stream.Position = 0;
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Danh sách import tồn đầu.xlsx");
        }
        [HttpPost]
        public async Task<JsonResult> ImportStock(IFormFile fileData, string DateView)
        {
            var zResult = new ServerResult();
            if (fileData == null || fileData.Length <= 0)
            {
                zResult.Success = false;
                zResult.Message = "Không có file";
                return Json(zResult);
            }

            if (!Path.GetExtension(fileData.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                zResult.Success = false;
                zResult.Message = "Tập tin không đúng định dạng !.";
                return Json(zResult);
            }

            string Uploaded = await TN_Helper.UploadAsync(fileData, "Excel");
            Uploaded = Uploaded.Replace("/", "\\").Replace("~", "");
            var temp = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + Uploaded);

            var Table = GetTable(temp);
            //var file = new FileInfo(temp);
            //ExcelPackage package = new(file);

            //ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
            //var rowCount = worksheet.Dimension.Rows;

            //DateTime zDate;
            //DateTime.TryParseExact(DateView, "MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDate);

            //string zMonth = zDate.Month.ToString();
            //if (zMonth.Length == 1)
            //    zMonth = zMonth + "0";
            //int zYear = zDate.Year;

            //int zCount = 0;
            //string zMessage = "";

            //for (int row = 2; row <= rowCount; row++)
            //{
            //    string zProductKey = worksheet.Cells[row, 2].Value.ToString().Trim();
            //    string zSKU = worksheet.Cells[row, 2].Value.ToString().Trim();
            //    float zBeginQty = worksheet.Cells[row, 5].Value.ToFloat();
            //    double zBeginMoney = worksheet.Cells[row, 6].Value.ToDouble();

            //    CloseMonth_Info zInfo = new CloseMonth_Info(zMonth, zYear, zProductKey);
            //    zInfo.CloseMonth.CloseMonth = zMonth;
            //    zInfo.CloseMonth.Year = zYear;
            //    zInfo.CloseMonth.ProductKey = zProductKey;
            //    zInfo.CloseMonth.BeginQty = zBeginQty;
            //    zInfo.CloseMonth.BeginMoney = zBeginMoney;
            //    if (zInfo.CloseMonth.AutoKey == 0)
            //    {
            //        zInfo.Create_ServerKey();
            //    }
            //    else
            //    {
            //        zInfo.Update();
            //    }
            //    if (zInfo.Code != "200" &&
            //        zInfo.Code != "201")
            //    {
            //        zCount++;
            //        zMessage = "Lỗi SKU: " + zSKU + "; \n";
            //    }

            //}
            //if (zCount == 0)
            //{
            //    zResult.Success = true;
            //    return Json(zResult);
            //}
            //else
            //{
            //    zResult.Success = false;
            //    zResult.Message = zMessage;
            //    return Json(zResult);
            //}
            return Json(zResult);
        }
        private static DataTable GetTable(string FilePath)
        {
            DataTable dt = new DataTable();
            FileInfo fi = new FileInfo(FilePath);

            // Check if the file exists
            if (!fi.Exists)
            {
                throw new Exception("File " + FilePath + " Does Not Exists");
            }

            using (ExcelPackage xlPackage = new ExcelPackage(fi))
            {
                // get the first worksheet in the workbook
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.First();
                // Fetch the WorkSheet size
                ExcelCellAddress startCell = worksheet.Dimension.Start;
                ExcelCellAddress endCell = worksheet.Dimension.End;

                // create all the needed DataColumn
                for (int col = startCell.Column; col <= endCell.Column; col++)
                {
                    dt.Columns.Add(col.ToString());
                }

                // place all the data into DataTable [ + 1 là trừ dòng đầu]
                for (int row = startCell.Row + 1; row <= endCell.Row; row++)
                {
                    DataRow dr = dt.NewRow();
                    int x = 0;
                    for (int col = startCell.Column; col <= endCell.Column; col++)
                    {
                        dr[x++] = worksheet.Cells[row, col].Value;
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        //-------------------- Other function
        [HttpGet]
        public IActionResult Search_Product(string name)
        {
            ViewBag.ListProduct = Product_Data.List(UserLog.PartnerNumber, name.ExTrim(), out _);
            return View("~/Views/Shared/_Table_Product.cshtml");
        }
        [HttpGet]
        public IActionResult Search_Product_Produce(string name, string option)
        {
            option ??= "IN";
            ViewBag.Option = option;
            ViewBag.ListProduct = Product_Data.List(UserLog.PartnerNumber, name.ExTrim(), out _);
            if (option == "IN")
                return View("~/Views/Inventory/Produce/_Table_Product_Inmake.cshtml");
            else
                return View("~/Views/Inventory/Produce/_Table_Product_Outmake.cshtml");
        }
        public IActionResult ExportToExcel(string DateView = "")
        {
            #region --Datetime string to fromdate - todate--
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != string.Empty)
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }
            var zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            var zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            var zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            var zList = Product_Data.ReportStock(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Tồn kho");
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.Cells.Style.Font.Size = 12;
                worksheet.Cells.AutoFilter = true;
                worksheet.Cells.AutoFitColumns();
                worksheet.View.FreezePanes(5, 4);

                #region[Fill data]
                // First row
                var row = 2;
                worksheet.Cells[row, 6].Value = "BÁO CÁO TỒN KHO THÁNG " + zFromDate.ToString("MM/yyyy");
                row = 3;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells["A3:A4"].Merge = true;

                worksheet.Cells[row, 2].Value = "SKU";
                worksheet.Cells["B3:B4"].Merge = true;

                worksheet.Cells[row, 3].Value = "Sản phẩm";
                worksheet.Cells["C3:C4"].Merge = true;

                worksheet.Cells[row, 4].Value = "Đầu kỳ";
                worksheet.Cells["D3:E3"].Merge = true;

                worksheet.Cells[row, 6].Value = "Nhập - xuất trong kỳ";
                worksheet.Cells["F3:I3"].Merge = true;

                worksheet.Cells[row, 10].Value = "Cuối kỳ";
                worksheet.Cells["J3:K3"].Merge = true;

                worksheet.Cells[row, 12].Value = "Ghi chú";
                worksheet.Cells["L3:L4"].Merge = true;

                row = 4;
                worksheet.Cells[row, 4].Value = "SL";
                worksheet.Cells[row, 5].Value = "ST";
                worksheet.Cells[row, 6].Value = "SL";
                worksheet.Cells[row, 7].Value = "ST";
                worksheet.Cells[row, 8].Value = "SL";
                worksheet.Cells[row, 9].Value = "ST";
                worksheet.Cells[row, 10].Value = "SL";
                worksheet.Cells[row, 11].Value = "ST";

                foreach (QtyStock zItem in zList)
                {
                    row++;
                    worksheet.Cells[row, 1].Value = row - 4;
                    worksheet.Cells[row, 2].Value = zItem.SKU;
                    worksheet.Cells[row, 3].Value = zItem.ProductName;
                    worksheet.Cells[row, 4].Value = zItem.BeginQty.ToString("n0");
                    worksheet.Cells[row, 5].Value = zItem.BeginMoney.ToString("n0");
                    worksheet.Cells[row, 6].Value = zItem.InQty.ToString("n0");
                    worksheet.Cells[row, 7].Value = zItem.InMoney.ToString("n0");
                    worksheet.Cells[row, 8].Value = zItem.OutQty.ToString("n0");
                    worksheet.Cells[row, 9].Value = zItem.OutMoney.ToString("n0");
                    worksheet.Cells[row, 10].Value = zItem.EndQty.ToString("n0");
                    worksheet.Cells[row, 11].Value = zItem.EndMoney.ToString("n0");
                    if (zItem.EndQty < 5 && zItem.EndQty > 0)
                        worksheet.Cells[row, 12].Value = "Chú ý sắp hết hàng";
                    if (zItem.EndQty == 0)
                        worksheet.Cells[row, 12].Value = "Hết hàng";
                }
                #endregion
                //Custom

                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 15;
                worksheet.Column(3).Width = 45;
                worksheet.Column(3).Style.WrapText = true;
                for (int i = 4; i <= 11; i++)
                {
                    worksheet.Column(i).Width = 13;
                    worksheet.Column(i).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                }
                worksheet.Column(12).Width = 20;

                //Rowheader và row tổng

                worksheet.Row(2).Height = 30;
                worksheet.Row(2).Style.WrapText = false;
                worksheet.Row(2).Style.Font.Bold = true;
                worksheet.Row(2).Style.Font.Color.SetColor(Color.Blue);
                worksheet.Row(2).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Row(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.WrapText = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Style.Font.Color.SetColor(Color.Blue);
                worksheet.Row(3).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Row(4).Height = 20;
                worksheet.Row(4).Style.WrapText = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Style.Font.Color.SetColor(Color.Blue);
                worksheet.Row(4).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                package.Save();
            }

            stream.Position = 0;
            string filename = "Báo cáo tồn kho tháng " + zFromDate.ToString("MM/yyyy") + ".xlsx";
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);

        }
        [HttpGet]
        public IActionResult InputView(string Id)
        {
            var zInfo = new Purchase_Info(Id);
            var zOrder = zInfo.Purchase;
            zOrder.ListItem = Purchase_Item_Data.List(UserLog.PartnerNumber, Id, out _);
            return View("~/Views/Inventory/Input/View.cshtml", zOrder);
        }
    }
}