﻿using Microsoft.AspNetCore.Mvc;
using SDK.Shopee;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ShopCP.Controllers
{
    public class ConfigController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult StockBegin()
        {
            ViewBag.ListProduct = Product_Data.List(UserLog.PartnerNumber, 0, out string Error);
            TempData["Error"] = Error;
            return View();
        }

        [HttpPost]
        public JsonResult Save(List<QtyModel> list, string datestock)
        {
            var zResult = new ServerResult();
            try
            {
                if (DateTime.TryParseExact(datestock, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zDate))
                {
                    foreach (var rec in list)
                    {
                        var zInfo = new CloseMonth_Info();
                        zInfo.CloseMonth.BeginQty = rec.qty;
                        zInfo.CloseMonth.ProductKey = rec.id;
                        zInfo.CloseMonth.CloseMonth = zDate.ToString("dd/MM");
                        zInfo.CloseMonth.CreatedBy = UserLog.UserKey;
                        zInfo.CloseMonth.ModifiedBy = UserLog.UserKey;
                        zInfo.CloseMonth.CreatedName = UserLog.UserName;
                        zInfo.CloseMonth.ModifiedName = UserLog.UserName;
                        zInfo.CloseMonth.PartnerNumber = UserLog.PartnerNumber;
                        zInfo.Create_ServerKey();
                    }

                    zResult.Success = true;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = "Error Date Code";
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult);
        }

        public IActionResult ListConnect()
        {
            ViewBag.List = Link3rt_Data.List(UserLog.PartnerNumber, out _);
            return View();
        }

        [HttpPost]
        public JsonResult LoginShopee(string Username, string Password, string Otp, string Capcha, string Capchaid)
        {
            var zResult = new ServerResult();
            string Cookies = "";

            Otp ??= "";
            Capcha ??= "";
            Capchaid ??= "";
            string gsession = Guid.NewGuid().ToString();
            ShopeeClient zClient = new(gsession);
            zClient.LoginOTP(Username, Password, Otp, Capcha, Capchaid);

            if (zClient.Message == "481")
            {
                zResult.Success = false;
                zResult.Data = "OTP";
                zResult.Message = "Phải nhập mã OTP";
                return Json(zResult);
            }
            if (zClient.Message == "482")
            {
                zResult.Success = false;
                zResult.Data = "OTP";
                zResult.Message = "OTP không đúng";
                return Json(zResult);
            }
            if (zClient.Message == "470")
            {
                zResult.Success = false;
                zResult.Data = "CAPCHA";
                zResult.Message = "Bạn phải nhập CAPCHA";
                return Json(zResult);
            }
            if (zClient.Message == "490")
            {
                zResult.Success = false;
                zResult.Data = "CAPCHA";
                zResult.Message = "Không đúng CAPCHA";
                return Json(zResult);
            }
            if (zClient.Message == "OK")
            {
                zResult.Data = "OK";
                zResult.Success = true;
                zResult.Message = "Đăng nhập thành công !.";

                Cookies = zClient.GetCookie();

                if (zResult.Success)
                {
                    #region [--Save LOGIN--]
                    var zModel = new Link3rt_Model();
                    zModel.Password = Password;
                    zModel.UserName = Username;
                    zModel.Cookies = Cookies;
                    zModel.Web = "SHOPEE";
                    zModel.PartnerNumber = UserLog.PartnerNumber;
                    zModel.CreatedBy = UserLog.UserKey;
                    zModel.ModifiedBy = UserLog.UserKey;
                    zModel.CreatedName = UserLog.UserName;
                    zModel.ModifiedName = UserLog.UserName;
                    zModel.RecordStatus = 1;

                    var zInfo = new Link3rt_Info(UserLog.PartnerNumber, "SHOPEE");
                    zModel.AutoKey = zInfo.Link3rt.AutoKey;
                    zInfo.Link3rt = zModel;
                    if (zInfo.Code == "404")
                        zInfo.Create_KeyAuto();
                    else
                        zInfo.Update();
                    #endregion
                }

                return Json(zResult);
            }
            if (zClient.Message != string.Empty)
            {
                zResult.Data = "ERR";
                zResult.Success = false;
                zResult.Message = "Thông tin đăng nhập không đúng !.";
                return Json(zResult);
            }

            return Json(zResult);
        }

        [HttpPost]
        public JsonResult StatusShopee()
        {
            var zResult = new ServerResult();
            var zInfo = new Link3rt_Info(UserLog.PartnerNumber, "SHOPEE");
            if (zInfo.Code == "404")
            {
                zResult.Success = false;
                return Json(zResult);
            }
            var zModel = zInfo.Link3rt;
            if (zModel.RecordStatus == 1)
            {
                zModel.RecordStatus = 0;
            }
            else
            {
                zModel.RecordStatus = 1;
            }
            zInfo.Link3rt = zModel;
            zInfo.UpdateStatus();
            zResult.Success = true;
            return Json(zResult);
        }

        [HttpGet]
        public JsonResult GetCapcha()
        {
            var zResult = new ServerResult();
            string gsession = Guid.NewGuid().ToString();
            ShopeeClient zClient = new(gsession);
            zResult.Data = zClient.CapCha().data;
            return Json(zResult);
        }
    }
}