﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SDK.Lazada;

namespace ShopCP.Controllers
{
    public class LazadaController : Controller
    {
        public IActionResult Index()
        {
            string gsession = Guid.NewGuid().ToString();
            LazadaClient zClient = new();
            var zRoot = zClient.Login();
            ViewBag.Message = zClient.Message;
            return View();
        }
    }
}