﻿using ShopCP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SDK.Shopee;
using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Logging;

namespace ShopCP.Controllers
{
    public class ShopeeController : BaseController
    {
        private readonly ILogger<ShopeeController> _Logger;
        public ShopeeController(ILogger<ShopeeController> logger)
        {
            _Logger = logger;
        }

        [Route("cua-hang-shopee")]
        public IActionResult Index()
        {
            var zInfo = new Link3rt_Info(UserLog.PartnerNumber, "SHOPEE");
            if (zInfo.Code == "404" || zInfo.Link3rt.RecordStatus == 0)
            {
                TempData["Error"] = "Bạn chưa liên kết Shopee !.";
            }
            else
            {
                string Username = zInfo.Link3rt.UserName;
                string Password = zInfo.Link3rt.Password;
                string Cook = zInfo.Link3rt.Cookies;

                string gsession = Guid.NewGuid().ToString();
                ShopeeClient zClient = new(gsession, Cook);
                var zRoot = zClient.LoginOTP(Username, Password);

                if (zRoot.data == null)
                {
                    TempData["Error"] = zClient.Message;
                }

                ViewBag.ShopName = zRoot.data.display_name;
                ViewBag.ShipId = zRoot.data.shopid;

                var ssShopee = new ShopeeSession();
                ssShopee.GSession = gsession;
                ssShopee.Cookie = zClient.GetCookie();
                ssShopee.ShopId = zRoot.data.shopid;
                ssShopee.ShopName = zRoot.data.display_name;

                HttpContext.Session.SetString("Jss", JsonConvert.SerializeObject(ssShopee));

                var zRootShipper = zClient.GetShipperList();
                ViewBag.Shipper = zRootShipper.data;
            }

            return View();
        }

        [Route("soan-don-hang/{Id}")]
        public IActionResult Prepare_Order(long Id)
        {
            string Jss = HttpContext.Session.GetString("Jss");
            var Sspe = JsonConvert.DeserializeObject<ShopeeSession>(Jss);

            ShopeeClient zClient = new(Sspe.GSession, Sspe.Cookie);
            var zRoot = zClient.OrderInfo(Id);
            var zTimeSlot = zClient.GetTimeSlot(Id);
            var zAddress = zClient.GetAddress(Id);

            ViewBag.TimeSlot = zTimeSlot.data;
            ViewBag.PickupAddress = zAddress.data;

            TempData["PostInit"] = JsonConvert.SerializeObject(zTimeSlot.PostInit);

            Order_Model zOrder = new();
            zOrder.OrderDate = TN_Utils.UnixTimeStampToDateTime(zRoot.data.create_time);
            zOrder.BuyerName = zRoot.data.buyer_address_name;
            zOrder.BuyerPhone = zRoot.data.buyer_address_phone;
            zOrder.ShippingAddress = zRoot.data.shipping_address;
            zOrder.S_OrderID = zRoot.data.order_sn;
            zOrder.S_JSON = JsonConvert.SerializeObject(zRoot.data);
            zOrder.PartnerNumber = UserLog.PartnerNumber;

            #region [--Auto Map Data--]
            foreach (var rec in zRoot.data.order_items)
            {
                var sku = rec.item_model.sku;
                if (sku.Length > 0)
                {
                    var zInfo = new Product_Info();

                    //ƯU TIÊN - kiểm tra gói sản phẩm
                    var zPag = new Package_Info(sku, true);
                    if (zPag.Code == "200")
                    {
                        //get danh sách sản phẩm trong gói
                        var list = Package_Item_Data.List(UserLog.PartnerNumber, zPag.Package.PackageKey, out _);
                        foreach (var p in list)
                        {
                            zInfo = new Product_Info(p.ItemKey);
                            if (zInfo.Code == "200")
                            {
                                var Product = zInfo.Product;
                                var outqty = rec.amount;
                                var checkqty = Product_Data.GetStock(Product.ProductKey);
                                //kiểm tra số lượng trước khi add vào danh sách
                                if (outqty <= checkqty)
                                {
                                    var zItem = new Order_Item_Model();
                                    zItem.ItemKey = p.ItemKey;
                                    zItem.ItemName = p.ItemName;
                                    zItem.ItemQty = rec.amount;
                                    zItem.ItemPhoto = p.ItemPhoto;
                                    zItem.ItemPrice = Product.SalePrice;
                                    zItem.ItemMoney = rec.amount * Product.SalePrice;
                                    zOrder.ListItem.Add(zItem);
                                }
                            }
                        }
                    }

                    //kiem tra sản phẩm
                    zInfo = new Product_Info(sku, true);
                    if (zInfo.Code == "200")
                    {
                        var Product = zInfo.Product;
                        var outqty = rec.amount;
                        var checkqty = Product_Data.GetStock(Product.ProductKey);
                        //kiểm tra số lượng trước khi add vào danh sách
                        if (outqty <= checkqty)
                        {
                            var zItem = new Order_Item_Model();

                            zItem.ItemKey = Product.ProductKey;
                            zItem.ItemName = Product.ProductName;
                            zItem.ItemQty = rec.amount;
                            zItem.ItemPhoto = Product.PhotoPath;
                            zItem.ItemPrice = Product.SalePrice;
                            zItem.ItemMoney = rec.amount * Product.SalePrice;
                            zOrder.ListItem.Add(zItem);
                        }
                        else
                        {
                            TempData["Error"] += "[" + Product.ProductName + "] đã hết tồn kho, ";
                        }
                    }
                }
            }

            // xử lý trùng sản phẩm
            #region [------]
            var listex = zOrder.ListItem;
            listex = listex
   .GroupBy(i => i.ItemKey)
   .Select(g => new Order_Item_Model
   {
       ItemKey = g.Key,
       ItemName = g.Select(i => i.ItemName).First(),
       ItemQty = g.Sum(i => i.ItemQty),
       ItemPhoto = g.Select(i => i.ItemPhoto).First(),
       ItemPrice = g.Select(i => i.ItemPrice).First(),
       ItemMoney = g.Sum(i => i.ItemMoney),
   })
   .ToList();
            zOrder.ListItem = listex;
            #endregion

            HttpContext.Session.SetString("Order", JsonConvert.SerializeObject(zOrder));
            ViewBag.LocalOrder = zOrder;
            #endregion

            return View(zRoot.data);
        }

        //[HttpPost]
        //public JsonResult Update_ShopeeProduct(string ProductKey)
        //{
        //    var zResult = new ServerResult();
        //    var zInfo = new Product_Info(ProductKey);
        //    if (zInfo.Code != "404")
        //    {
        //        var zProduct = zInfo.Product;

        //        string file = Url.Content(zProduct.PhotoPath);
        //        byte[] fileb = TN_Helper.GetBytes(new FileStream(file, FileMode.Open, FileAccess.Read));

        //        string Jss = HttpContext.Session.GetString("Jss");
        //        var Sspe = JsonConvert.DeserializeObject<ShopeeSession>(Jss);
        //        ShopeeClient zClient = new(Sspe.GSession, Sspe.Cookie);
        //        string imgId = zClient.UploadImage(TN_Utils.RandomString(10), fileb);

        //        bool isExist = false;

        //        #region [Tạo sản phẩm để post shopee] các thông số bắt buột
        //        var ProductPost = new SDK.Shopee.Root_ProductPost.Root();
        //        // không nhãn hiệu
        //        ProductPost.brand_id = 0;
        //        // mô tả sản phẩm
        //        ProductPost.description = zProduct.Description.StripHtml(); //bỏ html tag ở database ngoài
        //        // tên sản phẩm
        //        ProductPost.name = zProduct.ProductName;
        //        // giá bán
        //        ProductPost.price = zProduct.SalePrice.ToString();
        //        // tồn kho
        //        ProductPost.stock = Convert.ToInt64(Product_Data.GetStock(ProductKey));
        //        // cân nặng đơn vị tính bằng gr
        //        ProductPost.weight = "500";
        //        // ẩn hiện sp
        //        ProductPost.unlisted = false;
        //        #endregion


        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = "Local product error vui lòng liên hệ IT !.";
        //    }
        //    return Json(zResult);
        //}

        [HttpGet]
        public IActionResult Get_Category(int id = 0)
        {
            var zlist = new List<SDK.Shopee.Root_Category.List>();
            string catSp = HttpContext.Session.GetString("Cate");
            if (catSp == null)
            {
                ShopeeClient zClient;
                string Jss = HttpContext.Session.GetString("Jss");
                if (Jss == null)
                {
                    var zInfo = new Link3rt_Info(UserLog.PartnerNumber, "SHOPEE");
                    string Username = zInfo.Link3rt.UserName;
                    string Password = zInfo.Link3rt.Password;
                    string Cook = zInfo.Link3rt.Cookies;

                    string gsession = Guid.NewGuid().ToString();
                    zClient = new(gsession, Cook);
                    var zLog = zClient.LoginOTP(Username, Password);

                    if (zLog.data == null)
                    {
                        TempData["Error"] = zClient.Message;
                    }

                    var ssShopee = new ShopeeSession();
                    ssShopee.GSession = gsession;
                    ssShopee.Cookie = zClient.GetCookie();
                    ssShopee.ShopId = zLog.data.shopid;
                    ssShopee.ShopName = zLog.data.display_name;
                    HttpContext.Session.SetString("Jss", JsonConvert.SerializeObject(ssShopee));
                }
                else
                {
                    var Sspe = JsonConvert.DeserializeObject<ShopeeSession>(Jss);
                    zClient = new(Sspe.GSession, Sspe.Cookie);
                }

                var zRoot = zClient.GetCategory();
                zlist = zRoot.data.list;
                HttpContext.Session.SetString("Cate", JsonConvert.SerializeObject(zlist));
            }
            else
            {
                var temp = HttpContext.Session.GetString("Cate");
                zlist = JsonConvert.DeserializeObject<List<SDK.Shopee.Root_Category.List>>(temp);
            }
            zlist = zlist.TraverseMany(s => s.children).ToList();

            if (id != 0)
            {
                var item = zlist.SingleOrDefault(s => s.id == id);
                if (item != null && item.has_children)
                {
                    zlist = zlist.Where(s => s.parent_id == id).ToList();
                }
                else
                {
                    zlist = new List<SDK.Shopee.Root_Category.List>();
                }
            }
            else
            {
                zlist = zlist.Where(s => s.parent_id == 0).ToList();
            }

            return View("~/Views/Shopee/_Category.cshtml", zlist);
        }
        [HttpPost]
        public JsonResult Push(string productkey, string name, string brand, string sku, string weight, string saleprice, string description, string category)
        {
            var zResult = new ServerResult();
            try
            {
                string Jss = HttpContext.Session.GetString("Jss");
                var Sspe = JsonConvert.DeserializeObject<ShopeeSession>(Jss);
                ShopeeClient zClient = new(Sspe.GSession, Sspe.Cookie);

                #region [Danh sách sp SHOPEE]
                var zRoot = zClient.ProductList(1);
                var zList = new List<SDK.Shopee.Root_ProductList.List>();
                if (zRoot.message == "success")
                {
                    zList.AddRange(zRoot.data.list);
                    float total = (float)zRoot.data.page_info.total / 24;   //sẩn phầm shopee hiển thị 24 trên 1 trang
                    var page = TN_Helper.RoundUpValue(total, 0);

                    for (int i = 2; i <= page; i++)
                    {
                        zRoot = zClient.ProductList(i);
                        zList.AddRange(zRoot.data.list);
                    }
                }
                #endregion

                #region [Tìm mã SKU trên shopee]
                long idparent = 0;
                long idmodel = 0;
                bool found = false;
                foreach (var rec in zList)
                {
                    //kiểm tra các model của sản phẩm // SKU
                    foreach (var item in rec.model_list)
                    {
                        if (item.sku == sku)
                        {
                            found = true;
                            idparent = rec.id;
                            idmodel = item.id;
                            break;
                        }
                    }

                    if (found)
                    {
                        break;
                    }
                }
                #endregion

                #region [Tạo thông tin Push Shopee]
                var zListPhoto = Product_Photo_Data.List(productkey, out _);
                var zListIdsImg = new List<string>();
                foreach (var img in zListPhoto)
                {
                    string file = Url.Content(img.PhotoPath).Replace("/", "\\").Replace("~", "");
                    var physicfile = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + file);
                    byte[] fileb = TN_Helper.GetBytes(new FileStream(physicfile, FileMode.Open, FileAccess.Read));
                    string uploadedId = zClient.UploadImage(TN_Utils.RandomString(10), fileb);
                    zListIdsImg.Add(uploadedId);
                }

                long stock = Convert.ToInt64(Product_Data.GetStock(productkey));

                var zPost = new SDK.Shopee.Root_ProductPost.Root();
                zPost.id = idparent;
                zPost.brand_id = brand.ToInt(); //0 mặt định NoBrand;
                zPost.name = name.ExTrim();
                zPost.description = description.ExTrim();
                zPost.stock = stock;
                zPost.weight = weight;
                zPost.price = saleprice;
                zPost.model_list.Add(new SDK.Shopee.Root_ProductPost.ModelList()
                {
                    id = idmodel,
                    name = name,
                    price = saleprice.ToString(),
                    item_price = "",
                    stock = stock,
                    tier_index = new List<long>() { 0 },
                });
                zPost.images = zListIdsImg;

                var temp = category.Split(",");
                foreach (var s in temp)
                {
                    zPost.category_path.Add(Convert.ToInt64(s));
                }

                //thông số khác
                zPost.logistics_channels.Add(new SDK.Shopee.Root_ProductPost.LogisticsChannel()
                {
                    size = 0,
                    price = "35000",
                    cover_shipping_fee = false,
                    enabled = true,
                    item_flag = "0",
                    channelid = 5000,
                    sizeid = 0,
                });
                zPost.logistics_channels.Add(new SDK.Shopee.Root_ProductPost.LogisticsChannel()
                {
                    size = 0,
                    price = "16500",
                    cover_shipping_fee = false,
                    enabled = true,
                    item_flag = "0",
                    channelid = 5001,
                    sizeid = 0,
                });
                zPost.pre_order = false;
                zPost.condition = 1;
                zPost.days_to_ship = 2;
                zPost.unlisted = true;  //ẩn hiện shopee
                #endregion


                bool result = false;
                //UPDATE SHOPEE
                if (idparent != 0)
                {
                    result = zClient.UpdateProduct(zPost);
                }
                //CREATE SHOPEE
                else
                {
                    result = zClient.NewProduct(zPost);
                }
            }
            catch (Exception ex)
            {
                zResult.Message = ex.ToString();
                zResult.Success = false;
                _Logger.LogError(ex.ToString());
            }

            return Json(zResult);
        }
        [HttpGet]
        public IActionResult Commit(string productkey)
        {
            var zInfo = new Product_Info(productkey);
            var zListPhoto = Product_Photo_Data.List(productkey, out _);
            zInfo.Product.PhotoList = JsonConvert.SerializeObject(zListPhoto);
            return View("~/Views/Shopee/_Commit.cshtml", zInfo.Product);
        }
        [HttpGet]
        public JsonResult Get_MetaShop()
        {
            var zResult = new ServerResult();
            string Jss = HttpContext.Session.GetString("Jss");
            if (Jss != null)
            {
                var Sspe = JsonConvert.DeserializeObject<ShopeeSession>(Jss);
                ShopeeClient zClient = new(Sspe.GSession, Sspe.Cookie);
                var zRes = zClient.Get_MetaInfo(Sspe.ShopId);

                zResult.Data = new
                {
                    all = zRes.data.shop_list[0].all,
                    processed = zRes.data.shop_list[0].processed,
                    to_process = zRes.data.shop_list[0].to_process,
                };

                zResult.Success = true;
            }

            zResult.Success = false;
            return Json(zResult);
        }
        public IActionResult Get_List(string Option = "", int Page = 1, long Shipper = 0)
        {
            var IsOK = true;
            var zRoot = new SDK.Shopee.Root_OrderList.Root();
            string Jss = HttpContext.Session.GetString("Jss");
            var Sspe = JsonConvert.DeserializeObject<ShopeeSession>(Jss);

            ShopeeClient zClient = new(Sspe.GSession, Sspe.Cookie);
            var zRootListIds = zClient.ListIds(Option, Page, Shipper);

            if (zClient.Message == string.Empty)
            {
                var zListIds = zRootListIds.data.package_list;
                if (zListIds.Count > 0)
                {
                    for (int i = 0; i < zListIds.Count; i += 5)
                    {
                        var zRootPost = new SDK.Shopee.Root_OrderPost.Root();
                        var tmp = zListIds.Skip(i).Take(5);
                        foreach (var rec in tmp)
                        {
                            zRootPost.orders.Add(new SDK.Shopee.Root_OrderPost.Order()
                            {
                                order_id = rec.order_id,
                                region_id = rec.region_id,
                                shop_id = rec.shop_id,
                            });
                        }

                        var zJsonPost = JsonConvert.SerializeObject(zRootPost);
                        var zRes = zClient.ListOrder(zJsonPost, Option);

                        if (zClient.Message == string.Empty)
                        {
                            IsOK = true;
                            zRoot.data.viewtype = Option;
                            zRoot.data.orders.AddRange(zRes.data.orders);
                        }
                        else
                        {
                            IsOK = false;
                        }
                    }
                }
            }

            if (IsOK)
            {
                if (Option == "to_process")
                {
                    foreach (var order in zRoot.data.orders)
                    {
                        order.GoiYDuSanPham = Check_Prepare_Order(order);
                    }
                }
                return View("~/Views/Shopee/_Table_Order.cshtml", zRoot.data);
            }
            else
            {
                return View("~/Views/Shopee/_Error.cshtml", new ErrorViewModel { Name = Activity.Current.OperationName, RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            }
        }
        [HttpGet]
        public IActionResult Get_Pages(string Option, int totalItems, int page)
        {
            return ViewComponent("PagerShopee", new { totalItems = totalItems, page = page, option = Option });
        }
        [HttpGet]
        public IActionResult Get_Product()
        {
            var zRoot = new SDK.Shopee.Root_OrderList.Root();
            var zOption = "to_process";
            var zPage = 1;

            string Jss = HttpContext.Session.GetString("Jss");
            var Sspe = JsonConvert.DeserializeObject<ShopeeSession>(Jss);

            ShopeeClient zClient = new(Sspe.GSession, Sspe.Cookie);

            var zRootListIds = new SDK.Shopee.Root_PackageList.Root();

            #region [---Init Page 1---]
            zRootListIds = zClient.ListIds(zOption, zPage);
            var zListIds = zRootListIds.data.package_list;
            if (zListIds.Count > 0)
            {
                for (int i = 0; i < zListIds.Count; i += 5)
                {
                    var zRootPost = new SDK.Shopee.Root_OrderPost.Root();
                    var tmp = zListIds.Skip(i).Take(5);
                    foreach (var rec in tmp)
                    {
                        zRootPost.orders.Add(new SDK.Shopee.Root_OrderPost.Order()
                        {
                            order_id = rec.order_id,
                            region_id = rec.region_id,
                            shop_id = rec.shop_id,
                        });
                    }

                    var zJsonPost = JsonConvert.SerializeObject(zRootPost);
                    var zRes = zClient.ListOrder(zJsonPost, zOption);

                    if (zClient.Message == string.Empty)
                    {
                        zRoot.data.orders.AddRange(zRes.data.orders);
                    }
                }
            }
            #endregion

            float total = (float)zRootListIds.data.total / 40;  //sẩn phầm shopee hiển thị 40 trên 1 trang
            var result = TN_Helper.RoundUpValue(total, 0);

            zPage = 2;
            while (zPage <= result)
            {
                #region [---Loop Page ---]
                zRootListIds = zClient.ListIds(zOption, zPage);
                zListIds = zRootListIds.data.package_list;
                if (zListIds.Count > 0)
                {
                    for (int i = 0; i < zListIds.Count; i += 5)
                    {
                        var zRootPost = new SDK.Shopee.Root_OrderPost.Root();
                        var tmp = zListIds.Skip(i).Take(5);
                        foreach (var rec in tmp)
                        {
                            zRootPost.orders.Add(new SDK.Shopee.Root_OrderPost.Order()
                            {
                                order_id = rec.order_id,
                                region_id = rec.region_id,
                                shop_id = rec.shop_id,
                            });
                        }

                        var zJsonPost = JsonConvert.SerializeObject(zRootPost);
                        var zRes = zClient.ListOrder(zJsonPost, zOption);

                        if (zClient.Message == string.Empty)
                        {
                            zRoot.data.orders.AddRange(zRes.data.orders);
                        }
                    }
                }
                #endregion

                zPage++;
            }

            //xử lý gom nhóm
            var zListRequest = new List<ItemRequest>();
            foreach (var item in zRoot.data.orders)
            {
                for (int k = 0; k < item.order_items.Count; k++)
                {
                    var rec = item.order_items[k];
                    var name = rec.product.name;
                    var img = rec.product.images[0] + "_tn";
                    var qty = rec.amount;
                    var sku = rec.item_model.sku;
                    var stock = Product_Data.GetStockSKU(sku);

                    zListRequest.Add(new ItemRequest()
                    {
                        ItemImg = img,
                        ItemName = name,
                        ItemQty = qty,
                        ItemStock = stock,
                    });
                }
            }

            zListRequest = zListRequest.GroupBy(o => o.ItemName)
                                    .Select(i => new ItemRequest()
                                    {
                                        ItemName = i.First().ItemName,
                                        ItemImg = i.First().ItemImg,
                                        ItemQty = i.Sum(q => q.ItemQty),
                                        ItemStock = i.Sum(q => q.ItemStock),
                                    }).ToList();

            return View("~/Views/Shopee/_Table_Product.cshtml", zListRequest);
        }
        public IActionResult Print_Preview(long Id, string ViewType)
        {
            string Jss = HttpContext.Session.GetString("Jss");
            var Sspe = JsonConvert.DeserializeObject<ShopeeSession>(Jss);

            ShopeeClient zClient = new(Sspe.GSession, Sspe.Cookie);
            var zRoot = zClient.OrderInfo(Id);

            if (zRoot.data != null)
            {
                var Data = zRoot.data;

                ViewBag.Barcode = TN_Utils.BarCode(Data.MaVanDon, 35, 250, true, out _);
                ViewBag.Qrcode = TN_Utils.QRCode(Data.MaVanDon, 150, 120, out _);
                ViewBag.BarcodeOrder = TN_Utils.BarCode(Data.order_id.ToString(), 35, 250, true, out _);
            }
            else
            {
                ViewBag.Message = zClient.Message;
            }

            if (ViewType == "k80")
            {
                return View("~/Views/Shopee/_PrintK80.cshtml", zRoot.data);
            }
            if (ViewType == "k58")
            {
                return View("~/Views/Shopee/_PrintK58.cshtml", zRoot.data);
            }
            if (ViewType == "code")
            {
                return View("~/Views/Shopee/_PrintCode.cshtml", zRoot.data);
            }

            return View("~/Views/Shopee/_PrintK58.cshtml", zRoot.data);
        }
        public JsonResult Init_Order(long time, string remark, long addressId)
        {
            var zResult = new ServerResult();
            if (TempData["PostInit"] != null)
            {
                string temp = TempData["PostInit"].ToString();
                var zRootPost = JsonConvert.DeserializeObject<SDK.Shopee.Root_PostInitStatus.Root>(temp);
                zRootPost.remark = remark;
                zRootPost.pickup_time = time;
                zRootPost.pickup_address_id = addressId;
                string Jss = HttpContext.Session.GetString("Jss");
                var Sspe = JsonConvert.DeserializeObject<ShopeeSession>(Jss);
                ShopeeClient zClient = new(Sspe.GSession, Sspe.Cookie);
                var respo = zClient.InitOrder(zRootPost);
                zResult.Data = respo;
                //if (respo)
                //{
                //    zResult.Success = true;
                //    zResult.Message = "Đã gửi lệnh xác nhận cho shopee !.";
                //}
                //else
                //{
                //    zResult.Success = false;
                //    zResult.Message = zClient.Message;
                //}
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "PostInit error vui lòng liên hệ IT !.";
            }

            return Json(zResult);
        }

        [HttpGet]
        public JsonResult Check_Item(string Id)
        {
            var zResult = new ServerResult();
            Order_Model zOrder = new();

            try
            {
                string sOrder = HttpContext.Session.GetString("Order");

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    zItem.ItemQty = 1;
                }

                float QtyGet = zItem.ItemQty;
                float QtyStock = Product_Data.GetStock(Id);

                if (QtyStock < QtyGet)
                {
                    zResult.Success = false;
                    zResult.Message = "Vượt quá số lượng còn lại cho phép là (" + QtyStock + ").";
                }
                else
                {
                    zResult.Success = true;
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
                _Logger.LogError(ex.ToString());
            }

            return Json(zResult);
        }
        [HttpPost]
        public IActionResult Add_Item(string Id)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString("Order");

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    var Product = new Product_Info(Id).Product;
                    zItem.ItemKey = Product.ProductKey;
                    zItem.ItemName = Product.ProductName;
                    zItem.ItemQty = 1;
                    zItem.ItemPhoto = Product.PhotoPath;
                    zOrder.ListItem.Add(zItem);
                }

                HttpContext.Session.SetString("Order", JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
                _Logger.LogError(ex.ToString());
            }

            return View("~/Views/Shopee/Cart/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Del_Item(string Id)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString("Order");
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null && zItem.ItemQty > 1)
                {
                    zItem.ItemQty--;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zOrder.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString("Order", JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
                _Logger.LogError(ex.ToString());
            }
            return View("~/Views/Shopee/Cart/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public IActionResult Remove_Item(string Id)
        {
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString("Order");
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zOrder.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString("Order", JsonConvert.SerializeObject(zOrder));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
                _Logger.LogError(ex.ToString());
            }
            return View("~/Views/Shopee/Cart/_Table_Item.cshtml", zOrder);
        }
        [HttpPost]
        public JsonResult Save_Order()
        {
            var zResult = new ServerResult();
            Order_Model zOrder = new();
            try
            {
                string sOrder = HttpContext.Session.GetString("Order");
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);

                string NewKey = Guid.NewGuid().ToString();
                zOrder.OrderKey = NewKey;
                zOrder.Slug = 301;    // "bán hàng shopee"

                var Shopee = JsonConvert.DeserializeObject<SDK.Shopee.Root_OrderInfo.Data>(zOrder.S_JSON);
                var totalItem = Shopee.order_items.Sum(s => (s.item_price.ToDouble() * s.amount));   // + tiền hàng
                zOrder.TotalMoney = totalItem - Shopee.card_txn_fee_info.card_txn_fee.ToDouble();       // - phí shopee
                zOrder.OrderStatus = 1; //Đang vận chuyển
                zOrder.PartnerNumber = UserLog.PartnerNumber;
                zOrder.CreatedName = UserLog.UserName;
                zOrder.ModifiedName = UserLog.UserName;
                zOrder.CreatedBy = UserLog.UserKey;
                zOrder.ModifiedBy = UserLog.UserKey;

                //save data
                Order_Info zInfo = new();
                zInfo.Order = zOrder;
                zInfo.Create_ClientKey();

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    foreach (var item in zOrder.ListItem)
                    {
                        item.OrderKey = NewKey;
                        item.PartnerNumber = UserLog.PartnerNumber;
                        item.CreatedName = UserLog.UserName;
                        item.ModifiedName = UserLog.UserName;
                        item.CreatedBy = UserLog.UserKey;
                        item.ModifiedBy = UserLog.UserKey;

                        Order_Item_Info zItemInfo = new();
                        zItemInfo.Order_Item = item;
                        zItemInfo.Create_ServerKey();

                        if (zItemInfo.Code != "200" &&
                            zItemInfo.Code != "201")
                        {
                            zResult.Success = false;
                            zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                            break;
                        }
                    }

                    zResult.Success = true;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }

                //clear session
                HttpContext.Session.Remove("Order");
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
                _Logger.LogError(ex.ToString());
            }

            return Json(zResult);
        }
        [HttpGet]
        public JsonResult Check_Order()
        {
            var zResult = new ServerResult();
            Order_Model zOrder = new();

            try
            {
                string sOrder = HttpContext.Session.GetString("Order");
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);

                if (zOrder.ListItem.Count == 0)
                {
                    zResult.Message = "Bạn chưa soạn hàng xuất kho";
                    zResult.Success = false;
                }
                else
                {
                    zResult.Success = true;
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
                _Logger.LogError(ex.ToString());
            }

            return Json(zResult);
        }
        [HttpGet]
        public IActionResult Search_Product(string name)
        {
            ViewBag.ListProduct = Product_Data.List(UserLog.PartnerNumber, name.ExTrim(), out _);
            return View("~/Views/Shared/_Table_Product.cshtml");
        }

        private string Check_Prepare_Order(SDK.Shopee.Root_OrderList.Order order)
        {
            string zStatus = "";
            #region [--Auto Map Data--]
            foreach (var rec in order.order_items)
            {
                var sku = rec.item_model.sku;
                if (sku.Length > 0)
                {
                    var zInfo = new Product_Info();

                    //ƯU TIÊN - kiểm tra gói sản phẩm
                    var zPag = new Package_Info(sku, true);
                    if (zPag.Code == "200")
                    {
                        //get danh sách sản phẩm trong gói
                        var list = Package_Item_Data.List(UserLog.PartnerNumber, zPag.Package.PackageKey, out _);
                        foreach (var p in list)
                        {
                            zInfo = new Product_Info(p.ItemKey);
                            if (zInfo.Code == "200")
                            {
                                var Product = zInfo.Product;
                                var outqty = rec.amount;
                                var checkqty = Product_Data.GetStock(Product.ProductKey);
                                //kiểm tra số lượng trước khi add vào danh sách
                                if (outqty <= checkqty)
                                {
                                    zStatus += "";
                                }
                                else
                                {
                                    zStatus += "Không đủ hàng tồn kho";
                                }
                            }
                        }
                    }
                    else
                    {
                        zStatus += "Không tồn tại combo này";
                    }

                    //kiem tra sản phẩm
                    zInfo = new Product_Info(sku, true);
                    if (zInfo.Code == "200")
                    {
                        var Product = zInfo.Product;
                        var outqty = rec.amount;
                        var checkqty = Product_Data.GetStock(Product.ProductKey);
                        //kiểm tra số lượng trước khi add vào danh sách
                        if (outqty <= checkqty)
                        {
                            zStatus += "";
                        }
                        else
                        {
                            zStatus += "Không đủ hàng tồn kho";
                        }
                    }
                    else
                    {
                        zStatus += "Không tồn tại sản phẩm này";
                    }
                }
            }
            #endregion
            if (zStatus == "")
                return ("Có đủ hàng");
            else
                return "";
        }
    }
}