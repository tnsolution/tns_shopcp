﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace ShopCP.Controllers
{
    public class ReportController : BaseController
    {
        public IActionResult Index(string btnAction = "", string DateView = "")
        {
            #region [Datetime string to fromdate - todate]
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != "")
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            var DoanhSo = TN_Mics.No_Receipt(zFromDate, zToDate, UserLog.PartnerNumber);
            var NhapHang = TN_Mics.No_Payment(zFromDate, zToDate, UserLog.PartnerNumber);
            var DoanhThu = DoanhSo - NhapHang;

            ViewBag.Panel1 = DoanhSo.ToString("n0");
            ViewBag.Panel2 = NhapHang.ToString("n0");
            ViewBag.Panel4 = DoanhThu.ToString("n0");
            ViewBag.DateView = zToDate.Month + "/" + zToDate.Year;
            return View();
        }
    }
}
