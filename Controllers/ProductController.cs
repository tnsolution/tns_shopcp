﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using JsonResult = Microsoft.AspNetCore.Mvc.JsonResult;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace ShopCP.Controllers
{
    public class ProductController : BaseController
    {
        #region [--Product V2--]
        [HttpPost]
        public async Task<JsonResult> Upload_ImgList()
        {
            var zResult = new ServerResult();
            var files = Request.Form.Files;
            if (files != null && files.Count > 0)
            {
                var list = new List<string>();
                foreach (var file in files)
                {
                    var filepath = await TN_Helper.UploadAsync(file, "Product");
                    list.Add(filepath);
                }

                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(list);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "Bạn phải chọn hình";
            }
            return Json(zResult);
        }
        [HttpPost]
        public async Task<JsonResult> Upload_ImgMain(IFormFile file)
        {
            var zResult = new ServerResult();
            if (file != null)
            {
                string filepath = await TN_Helper.UploadAsync(file, "Product");
                zResult.Success = true;
                zResult.Data = filepath;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "Bạn phải chọn hình";
            }
            return Json(zResult);
        }

        [HttpPost]
        public async Task<JsonResult> Upload_ImgChild(IFormFile file)
        {
            var zResult = new ServerResult();
            if (file != null)
            {
                string filepath = await TN_Helper.UploadAsync(file, "Product");
                zResult.Success = true;
                zResult.Data = filepath;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "Bạn phải chọn hình";
            }
            return Json(zResult);
        }

        [Route("cac-mat-hang-v2/{CategoryKey}")]
        public IActionResult ListV2(int CategoryKey)
        {
            ViewBag.ListCategory = Product_Category_Data.List(TN_Helper.PartnerNumber, 0, out string Error);
            ViewBag.ListProduct = Product_Data.List(UserLog.PartnerNumber, CategoryKey, out Error);
            TempData["Error"] = Error;
            return View();
        }

        [Route("mat-hang-v2/{ProductKey}")]
        public IActionResult EditV2(string ProductKey)
        {
            var zInfo = new Product_Info(ProductKey);
            if (zInfo.Code != "404")
            {
                //load lại sản phẩm cũ
                var zListPhoto = Product_Photo_Data.List(ProductKey, out _);
                zInfo.Product.PhotoList = JsonConvert.SerializeObject(zListPhoto);
                var zListModel = Product_Data.ListModel(UserLog.PartnerNumber, ProductKey, out _);
                zInfo.Product.ListModel = zListModel;
                ViewBag.Status = "Edit";
            }
            else
            {
                ViewBag.Status = "New";
            }
            return View(zInfo.Product);
        }
        [HttpPost]
        public JsonResult SaveV2(string product)
        {
            var zProduct = JsonConvert.DeserializeObject<Product_Model>(product);

            var zResult = new ServerResult();
            zProduct.PartnerNumber = UserLog.PartnerNumber;
            zProduct.CreatedBy = UserLog.UserKey;
            zProduct.CreatedName = UserLog.UserName;
            zProduct.ModifiedBy = UserLog.UserKey;
            zProduct.ModifiedName = UserLog.UserName;

            string SKU = zProduct.SKU;
            string ProductKey = zProduct.ProductKey;

            var zInfo = new Product_Info(ProductKey);
            var Exists = Product_Data.CheckExistsID(SKU, UserLog.PartnerNumber);
            if (Exists && zInfo.Code == "404")
            {
                TempData["Error"] = "Mã này đã được sử dụng vui lòng chọn mã khác !.";
                zResult.Success = false;
                zResult.Message = "Mã này đã được sử dụng vui lòng chọn mã khác !.";
            }

            zInfo.Product = zProduct;

            if (zInfo.Code == "404")
            {
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                //lưu các hình phụ
                if (zProduct.PhotoList != string.Empty)
                {
                    var zList = JsonConvert.DeserializeObject<List<string>>(zProduct.PhotoList);
                    if (zList.Count > 0)
                    {
                        foreach (string s in zList)
                        {
                            var zPhoto = new Product_Photo_Info();
                            zPhoto.Product_Photo.ProductKey = ProductKey;
                            zPhoto.Product_Photo.PhotoPath = s;
                            zPhoto.Product_Photo.PartnerNumber = UserLog.PartnerNumber;
                            zPhoto.Product_Photo.CreatedBy = UserLog.UserKey;
                            zPhoto.Product_Photo.CreatedName = UserLog.UserName;
                            zPhoto.Product_Photo.ModifiedBy = UserLog.UserKey;
                            zPhoto.Product_Photo.ModifiedName = UserLog.UserName;
                            zPhoto.Create_ServerKey();
                        }
                    }
                }

                //lưu các phân loại hàng
                if (zProduct.ListModel.Count > 0)
                {
                    foreach (var rec in zProduct.ListModel)
                    {
                        var key = rec.ProductKey;
                        zInfo = new Product_Info(key);
                        zInfo.Product.CategoryPath = zProduct.CategoryPath;
                        zInfo.Product.CategoryKey = zProduct.CategoryKey;
                        zInfo.Product.CategoryName = zProduct.CategoryName;
                        zInfo.Product.ProductModel = zProduct.ProductKey;
                        zInfo.Product.PhotoList = zProduct.PhotoList;
                        zInfo.Product.PhotoPath = rec.PhotoPath;
                        zInfo.Product.SKU = rec.SKU;
                        zInfo.Product.ProductName = rec.ProductName;
                        zInfo.Product.StandardCost = rec.StandardCost;
                        zInfo.Product.SalePrice = rec.SalePrice;
                        zInfo.Product.PartnerNumber = UserLog.PartnerNumber;
                        zInfo.Product.CreatedBy = UserLog.UserKey;
                        zInfo.Product.CreatedName = UserLog.UserName;
                        zInfo.Product.ModifiedBy = UserLog.UserKey;
                        zInfo.Product.ModifiedName = UserLog.UserName;

                        if (zInfo.Code == "200")
                        {
                            zInfo.Update();
                        }
                        else
                        {
                            zInfo.Product.ProductKey = Guid.NewGuid().ToString();
                            zInfo.Create_ClientKey();
                        }
                    }
                }

                zResult.Success = true;
            }

            return Json(zResult);
        }

        [HttpPost]
        public JsonResult DeleteV2(string ProductKey)
        {
            var zResult = new ServerResult();

            var zExitst = TN_Mics.Check_Product_RelationShip(ProductKey);
            if (zExitst)
            {
                var zInfo = new Product_Info();
                zInfo.Product.ProductKey = ProductKey;
                zInfo.DeleteV2();

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                }
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "Sản phẩm này đã được sử dụng, vui lòng liên hệ IT để xoá dữ liệu !.";
            }

            return Json(zResult);
        }
        #endregion
        #region [--Product--]
        [HttpGet]
        public IActionResult ListProduct(int CategoryKey)
        {
            return ViewComponent("ListProduct", new { CategoryKey });
        }

        public JsonResult GetProduct(string Key)
        {
            var zResult = new ServerResult();
            var zInfo = new Product_Info(Key);
            var zModel = zInfo.Product;
            zResult.Data = zModel;
            return Json(zResult);
        }

        [HttpPost]
        public JsonResult Delete_Photo(string PhotoKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Product_Photo_Info(PhotoKey);
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                var Path = zInfo.Product_Photo.PhotoPath;
                TN_Helper.DeleteFile(Path);
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        #endregion
        #region [--Package--]
        [HttpPost]
        public JsonResult Delete_Package(string PackageKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Package_Info();
            zInfo.Package.PackageKey = PackageKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Save_Package(string PackageKey, string Sku, string Name)
        {
            PackageKey ??= Guid.NewGuid().ToString();

            var zResult = new ServerResult();
            try
            {
                string sPackage = HttpContext.Session.GetString("Package");
                var zPackage = JsonConvert.DeserializeObject<Package_Model>(sPackage);

                zPackage.PackageKey = PackageKey;
                zPackage.SKU = Sku.ExTrim();
                zPackage.Name = Name.ExTrim();
                zPackage.PartnerNumber = UserLog.PartnerNumber;
                zPackage.CreatedName = UserLog.UserName;
                zPackage.ModifiedName = UserLog.UserName;
                zPackage.CreatedBy = UserLog.UserKey;
                zPackage.ModifiedBy = UserLog.UserKey;

                //save data
                Package_Info zInfo = new(PackageKey);
                zInfo.Package = zPackage;

                if (zInfo.Code == "404")
                {
                    zInfo.Create_KeyManual();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {

                    zInfo.DeleteItem();
                    foreach (var item in zPackage.ListItem)
                    {
                        item.PackageKey = PackageKey;
                        item.PartnerNumber = UserLog.PartnerNumber;
                        item.CreatedName = UserLog.UserName;
                        item.ModifiedName = UserLog.UserName;
                        item.CreatedBy = UserLog.UserKey;
                        item.ModifiedBy = UserLog.UserKey;

                        Package_Item_Info zItemInfo = new();
                        zItemInfo.Package_Item = item;
                        zItemInfo.Create_KeyAuto();

                        if (zItemInfo.Code != "200" &&
                            zItemInfo.Code != "201")
                        {
                            zResult.Success = false;
                            zResult.Message = "Lưu Item: " + zItemInfo.Message.GetFirstLine();
                            break;
                        }
                    }
                    zResult.Success = true;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }

                //clear session
                HttpContext.Session.Remove("Package");
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
                zResult.Success = false;
                zResult.Message = "TempData error vui lòng liên hệ IT";
            }

            return Json(zResult);
        }
        [Route("cac-goi-hang")]
        public IActionResult List_Package()
        {
            ViewBag.List = Package_Data.List(UserLog.PartnerNumber, out string Error);
            TempData["Error"] = Error;
            return View("~/Views/Product/Package/List.cshtml");
        }
        [Route("thiet-lap-goi-hang/{PackageKey}")]
        public IActionResult Edit_Package(string PackageKey)
        {
            var zInfo = new Package_Info(PackageKey);
            var zModel = zInfo.Package;
            zModel.ListItem = Package_Item_Data.List(UserLog.PartnerNumber, PackageKey, out _);

            HttpContext.Session.SetString("Package", JsonConvert.SerializeObject(zModel));
            return View("~/Views/Product/Package/Edit.cshtml", zModel);
        }
        [HttpPost]
        public IActionResult Add_Item(string Id)
        {
            Package_Model zPackage = new();
            try
            {
                string sPackage = HttpContext.Session.GetString("Package");

                if (!string.IsNullOrEmpty(sPackage))
                {
                    zPackage = JsonConvert.DeserializeObject<Package_Model>(sPackage);
                }
                else
                {
                    zPackage = new Package_Model();
                }

                Package_Item_Model zItem = zPackage.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zItem.ItemQty++;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zItem = new Package_Item_Model();
                    var Product = new Product_Info(Id).Product;
                    zItem.ItemKey = Product.ProductKey;
                    zItem.ItemName = Product.ProductName;
                    zItem.ItemPhoto = Product.PhotoPath;
                    zItem.ItemQty = 1;
                    zItem.ItemPrice = Product.SalePrice;    //đơn giá bán  ***
                    zItem.ItemMoney = Product.SalePrice;    //đơn giá bán  ****
                    zPackage.ListItem.Add(zItem);
                }

                HttpContext.Session.SetString("Package", JsonConvert.SerializeObject(zPackage));

            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
            }

            return View("~/Views/Product/Package/_Table_Item.cshtml", zPackage);
        }
        [HttpPost]
        public IActionResult Del_Item(string Id)
        {
            Package_Model zPackage = new();
            try
            {
                string sPackage = HttpContext.Session.GetString("Package");
                zPackage = JsonConvert.DeserializeObject<Package_Model>(sPackage);
                var zItem = zPackage.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null && zItem.ItemQty > 1)
                {
                    zItem.ItemQty--;
                    zItem.ItemMoney = zItem.ItemQty * zItem.ItemPrice;
                }
                else
                {
                    zPackage.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString("Package", JsonConvert.SerializeObject(zPackage));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
            }
            return View("~/Views/Product/Package/_Table_Item.cshtml", zPackage);
        }
        [HttpPost]
        public IActionResult Remove_Item(string Id)
        {
            Package_Model zPackage = new();
            try
            {
                string sPackage = HttpContext.Session.GetString("Package");
                zPackage = JsonConvert.DeserializeObject<Package_Model>(sPackage);
                var zItem = zPackage.ListItem.SingleOrDefault(s => s.ItemKey == Id);
                if (zItem != null)
                {
                    zPackage.ListItem.Remove(zItem);
                }
                HttpContext.Session.SetString("Package", JsonConvert.SerializeObject(zPackage));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
            }
            return View("~/Views/Product/Package/_Table_Item.cshtml", zPackage);
        }

        [HttpPost]
        public IActionResult Qty_Item(string Id, float qty, float pric)
        {
            Package_Model zPurchase = new();
            try
            {
                string sPurchase = HttpContext.Session.GetString("Package");
                zPurchase = JsonConvert.DeserializeObject<Package_Model>(sPurchase);
                var zItem = zPurchase.ListItem.SingleOrDefault(s => s.ItemKey == Id);

                zItem.ItemQty = qty;
                zItem.ItemPrice = pric;
                zItem.ItemMoney = qty * pric;

                HttpContext.Session.SetString("Package", JsonConvert.SerializeObject(zPurchase));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.ToString();
            }
            return View("~/Views/Product/Package/_Table_Item.cshtml", zPurchase);
        }
        #endregion
        //----------------------------------------------------------------------other method
        [HttpGet]
        public IActionResult SelectCategory(int Parent)
        {
            return ViewComponent("SelectCategory", new { Parent });
        }
        [HttpGet]
        public IActionResult Search_Product(string name)
        {
            ViewBag.ListProduct = Product_Data.List(UserLog.PartnerNumber, name.ExTrim(), out _);
            return View("~/Views/Shared/_Table_Product.cshtml");
        }
        //
        [HttpGet]
        public JsonResult CheckID(string SKU)
        {
            var zResult = new ServerResult();
            var Exists = Product_Data.CheckExistsID(SKU, UserLog.PartnerNumber);
            zResult.Success = Exists;
            return Json(zResult);
        }
        public IActionResult ExportToExcel()
        {
            var TempData = Product_Data.List(UserLog.PartnerNumber, string.Empty, out _);

            List<Product_Model> zList = TempData as List<Product_Model>;

            var stream = new MemoryStream();

            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Danh sách sản phẩm");
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.Cells.Style.Font.Size = 12;
                worksheet.Cells.AutoFilter = true;
                worksheet.Cells.AutoFitColumns();


                #region[Fill data]
                // First row
                var row = 1;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 2].Value = "Key";
                worksheet.Cells[row, 3].Value = "SKU";
                worksheet.Cells[row, 4].Value = "Tên sản phẩm";
                worksheet.Cells[row, 5].Value = "Giá vốn";
                worksheet.Cells[row, 6].Value = "Giá bán";
                worksheet.Cells[row, 7].Value = "Ghi chú";

                foreach (Product_Model zItem in zList)
                {
                    row++;
                    worksheet.Cells[row, 1].Value = row - 1;
                    worksheet.Cells[row, 2].Value = zItem.ProductKey;
                    worksheet.Cells[row, 3].Value = zItem.SKU;
                    worksheet.Cells[row, 4].Value = zItem.ProductName;
                    worksheet.Cells[row, 5].Value = zItem.StandardCost.ToString("n0");
                    worksheet.Cells[row, 6].Value = zItem.SalePrice.ToString("n0");
                    worksheet.Cells[row, 7].Value = "";
                }
                #endregion
                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 45;
                worksheet.Column(3).Width = 17;
                worksheet.Column(4).Width = 50;
                worksheet.Column(5).Width = 17;
                worksheet.Column(6).Width = 17;
                worksheet.Column(7).Width = 17;

                worksheet.Column(4).Style.WrapText = true;

                worksheet.Column(5).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                worksheet.Column(6).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;


                //Rowheader và row tổng
                worksheet.Row(1).Height = 30;
                worksheet.Row(1).Style.WrapText = true;
                worksheet.Row(1).Style.Font.Bold = true;
                worksheet.Row(1).Style.Font.Color.SetColor(Color.Blue);
                worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                package.Save();
            }

            stream.Position = 0;
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "productlist.xlsx");

        }
    }
}