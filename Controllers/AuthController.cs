﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ShopCP.Controllers
{
    public class AuthController : Controller
    {
        [Route("dang-nhap")]
        public IActionResult Index()
        {
            ViewBag.Message = "NotAuthen";
            return View();
        }

        public IActionResult SignIn(string Username, string Password)
        {
            if (Username == null || 
                Password == null)
            {
                return View("~/Views/Auth/Index.cshtml");
            }

            if (Password.Length < 28)
            {
                Password = TN_Utils.HashPass(Password);
            }

            var zInfo = new User_Info(Username, Password);
            if (zInfo.Code == "200")
            {
                var key = "UserLogged";
                var obj = JsonConvert.SerializeObject(zInfo.User);
                HttpContext.Session.SetString(key, obj);
                ViewBag.Message = "IsAuthen";
                //Update Logged
                zInfo.UpdateLogged();
                //Cookies login
            }
            else
            {
                ViewBag.Message = "405";
            }

            return View("~/Views/Auth/Index.cshtml");
        }
    }
}
