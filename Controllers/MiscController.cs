﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopCP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MiscController : ControllerBase
    {
        [HttpGet]
        public JsonResult CheckID(string SKU)
        {
            var zResult = new ServerResult();
            var Exists = Product_Data.CheckExistsID(SKU);
            zResult.Success = Exists;
            return new JsonResult(zResult);
        }
    }
}