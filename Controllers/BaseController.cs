﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopCP.Controllers
{
    public class BaseController : Controller
    {
        public static User_Model UserLog = new();
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var session = HttpContext.Session;
            var key = "UserLogged";
            var UserModel = session.GetString(key);
            if (UserModel == null ||
                UserModel == string.Empty)
            {
                string url = Url.Action("Index", "Auth");
                filterContext.Result = new RedirectResult(url);
                return;
            }
            else
            {
                UserLog = JsonConvert.DeserializeObject<User_Model>(UserModel);
            }
        }

        [HttpPost]
        public JsonResult KeepSessionAlive()
        {
            var session = HttpContext.Session;
            var key = "UserLogged";
            var UserModel = session.GetString(key);

            return Json(UserModel);
        }
    }
}
