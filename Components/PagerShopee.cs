﻿using Microsoft.AspNetCore.Mvc;

namespace ShopCP.Components
{
    public class PagerShopee : ViewComponent
    {
        public IViewComponentResult Invoke(int totalItems, int page, string option)
        {
            ViewBag.Option = option;
            //show 40 rec/1 trang
            var jwPage = new JW.Pager(totalItems, page, 40);
            return View("~/Views/Components/PagerShopee.cshtml", jwPage);
        }
    }
}
