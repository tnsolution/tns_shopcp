﻿using Microsoft.AspNetCore.Mvc;

namespace ShopCP.Components
{
    public class SelectCategory: ViewComponent
    {
        public IViewComponentResult Invoke(int Parent)
        {
            ViewBag.List = Product_Category_Data.List(TN_Helper.PartnerNumber, Parent, out _);
            return View("~/Views/Components/SelectCategory.cshtml");
        }
    }
}