﻿using Microsoft.AspNetCore.Mvc;

namespace ShopCP.Component
{
    [ViewComponent]
    public class LeftSidebar : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Views/Components/LeftSidebar.cshtml");
        }
    }
}