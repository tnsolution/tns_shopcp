﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopCP.Components
{
    public class SelectProduct : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var List = Product_Data.List(TN_Helper.PartnerNumber, out _);
            return View("~/Views/Components/SelectProduct.cshtml", List);
        }
    }
}