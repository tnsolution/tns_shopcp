﻿using Microsoft.AspNetCore.Mvc;

namespace ShopCP.Components
{
    public class ListProduct : ViewComponent
    {
        public IViewComponentResult Invoke(int CategoryKey)
        {
            var List = Product_Data.List(TN_Helper.PartnerNumber, CategoryKey, out string Error);
            TempData["Error"] = Error;
            return View("~/Views/Components/ListProduct.cshtml", List);
        }
    }
}