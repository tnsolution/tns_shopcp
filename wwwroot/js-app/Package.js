﻿getProduct();
//
function getProduct() {
    var name = $("#txt_productName").val();
    $.ajax({
        url: Url_Search_Product,
        type: "GET",
        data: {
            "name": name,
        },
        beforeSend: function () {
            $("#divProduct").empty();
        },
        success: function (r) {
            $("#divProduct").append(r);
        },
        error: function (err) {

        },
        complete: function () {

        }
    });
}
//
function addCart(id) {
    $.post(Url_Add_Item, { "Id": id },
        function (r) {
            $("#table_item").empty().append(r);
            $(".number").each(function () {
                $(this).number(true, 0, '.', ',');
            });
        });
}
//
function deleteCart(id) {
    $.post(Url_Delete_Item, { "Id": id },
        function (r) {
            $("#table_item").empty().append(r);
            $(".number").each(function () {
                $(this).number(true, 0, '.', ',');
            });
        });
}
//
function removeCart(id) {
    $.post(Url_Remove_Item, { "Id": id },
        function (r) {
            $("#table_item").empty().append(r);
            $(".number").each(function () {
                $(this).number(true, 0, '.', ',');
            });
        });
}
//
function saveCart() {
    if ($("#txt_packageSku").val() === "") {
        Utils.OpenNotify("Thông báo", "Bạn phải nhập mã SKU", "warning");
        return;
    }
    if ($("#txt_packageName").val() === "") {
        Utils.OpenNotify("Thông báo", "Bạn phải nhập tên gói", "warning");
        return;
    }

    Utils.LoadIn();
    $.post(Url_Save_Order, {
        PackageKey: $("#txt_PackageKey").val(),
        Sku: $("#txt_packageSku").val(),
        Name: $("#txt_packageName").val()
    }, function (r) {
        if (!r.success) {
            Utils.OpenNotify("Thông báo", r.message, "error");
        }
        else {
            Utils.OpenNotify("Thông báo", "Đã lưu thành công !.", "success");
            Utils.BackWithRefresh();
        }
    });
}
//
function CheckID() {
    var key = $("#txt_packageKey").val();
    var sku = $("#txt_packageSku").val();
    var old = $("input[old]").val();

    if (sku.length <= 0) {
        Utils.OpenNotify("Thông báo", "Bạn phải nhập mã !.", "error");
        return false;
    }

    //chinh sua
    if (key.length >= 36) {
        if (sku.toLowerCase() !== old.toLowerCase()) {
            $.ajax({
                url: URL_CheckID,
                type: "GET",
                data: { SKU: sku },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (r) {
                    if (r.success) {
                        Utils.OpenNotify("Thông báo", "Mã này đã được sử dụng vui lòng nhập mã khác !.", "warning");
                        $("#txt_SKU").focus();
                        return false;
                    }
                    else {
                        return true;
                    }
                },
                error: function (err) {
                    Utils.OpenNotify("Thông báo", err.responseText, "error");
                },
                complete: function () {
                }
            });
        }
    }
    else {
        $.ajax({
            url: URL_CheckID,
            type: "GET",
            data: { SKU: sku },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (r) {
                if (r.success) {
                    Utils.OpenNotify("Thông báo", "Mã này đã được sử dụng vui lòng nhập mã khác !.", "warning");
                    $("#txt_SKU").focus();
                    return false;
                }
                else {
                    return true;
                }
            },
            error: function (err) {
                Utils.OpenNotify("Thông báo", err.responseText, "error");
            },
            complete: function () {
            }
        });
    }
}