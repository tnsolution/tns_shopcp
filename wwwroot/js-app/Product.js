﻿$(document).ready(function () {
    $("#pnLeft").on("click", "li", function () {
        $tr = $(this);
        var id = $tr.attr("id");
        $("#pnLeft li").removeClass("active");
        $("#pnLeft li[id=" + id + "]").addClass("active");
        GetName(1);
        Load_Category(id);
    });
    $("#pnRight").on("click", "li", function () {
        $tr = $(this);
        var id = $tr.attr("id");
        $("#pnRight li").removeClass("active");
        $("#pnRight li[id=" + id + "]").addClass("active");

        GetName(2);
    });

    $("html").addClass("sidebar-left-collapsed");

    $("#chkFrontPage").change(function () {
        if (this.checked) {
            $("#txt_FrontPage").val("true");
        }
        else {
            $("#txt_FrontPage").val("false");
        }
        console.log($("#txt_FrontPage").val());
    });
    $("#chkPublish").change(function () {
        if (this.checked) {
            $("#txt_Publish").val("true");
        }
        else {
            $("#txt_Publish").val("false");
        }
        console.log($("#txt_Publish").val());
    });

    $("#imgpreview").click(function () {
        $("#ImgMain").trigger("click");
    });
    $("#ImgMain").change(function () {
        Utils.PreviewImg(this, "#imgpreview");
        var str = $("#imgpreview").attr("src");
    });

    //tinymce.init({
    //    selector: "textarea#txt_ProductContent",
    //    paste_data_images: true,
    //    height: 500,
    //    menubar: false,
    //    plugins: [
    //        "image", "table", "fullscreen",
    //        "advlist autolink lists link image charmap print preview anchor",
    //        "searchreplace visualblocks code fullscreen",
    //        "insertdatetime media table paste code help wordcount"
    //    ],
    //    toolbar:
    //        "fullscreen | image |" +
    //        "table |" +
    //        "undo redo | formatselect | " +
    //        "bold italic backcolor | alignleft aligncenter " +
    //        "alignright alignjustify | bullist numlist outdent indent | " +
    //        "removeformat | help",
    //    content_style: "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }"
    //});

    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
    });

    var key = $("#txt_ProductKey").val();
    if (key.length >= 36) {
        var catePath = $("#txt_CategoryPath").val();
        var temp = catePath.split("-");
        if (temp.length > 2) {
            var lvl1 = temp[1];
            var lvl2 = temp[2];

            $("#pnLeft li[id=" + lvl1 + "]").addClass("active");
            Load_Category(lvl1, lvl2);
        }
        else {
            var lvl1 = temp[1];
            $("#pnLeft li[id=" + lvl1 + "]").addClass("active");
        }
    }

    $("#txt_SKU").blur(function () {
        CheckID();
    });
});
function GoBack() {
    var CategoryKey = $("#txt_CategoryKey").val();
    URL_List = URL_List.replace("_key_", CategoryKey);
    window.location = URL_List;
};
function Delete(ProductKey) {
    $.confirm({
        type: "red",
        typeAnimated: true,
        title: "Cảnh báo !.",
        content: "Bạn có chắc xóa thông tin này ?.",
        buttons: {
            confirm: {
                text: "Đồng ý",
                btnClass: "btn-red",
                action: function () {
                    $.ajax({
                        url: URL_Delete,
                        type: "POST",
                        data: {
                            "ProductKey": ProductKey
                        },
                        beforeSend: function () {
                        },
                        success: function (r) {
                            if (r.success) {
                                GoBack();
                            }
                            else {
                                Utils.OpenNotify(r.message, "Thông báo", "error");
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, "Thông báo", "error");
                        },
                        complete: function () {
                        }
                    });
                }
            },
            cancel: {
                text: "Hủy",
            }
        }
    });
}
function Delete_Photo(PhotoKey) {
    $.ajax({
        url: URL_Delete_Photo,
        type: "POST",
        data: {
            "PhotoKey": PhotoKey
        },
        beforeSend: function () {
        },
        success: function (r) {
            if (r.success) {
                var elm = $("#" + PhotoKey);
                Utils.RemoveRow(elm);
            }
            else {
                Utils.OpenNotify(r.message, "Thông báo", "error");
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
        }
    });
}
function Load_Category(Parent, select) {
    $.ajax({
        url: URL_Category,
        type: "GET",
        data: {
            "Parent": Parent
        },
        beforeSend: function () {
            $("#pnRight").empty();
        },
        success: function (r) {
            $("#pnRight").append(r);

        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, "Thông báo", "error");
        },
        complete: function () {
            if (select !== undefined) {
                $("#pnRight li[id=" + select + "]").addClass("active");
            }
        }
    });
}
function GetName(Action) {
    if (Action == 1) {
        var id1 = "";
        var name1 = "";

        id1 = $("#pnLeft li.active").attr("id");
        name1 = $("#pnLeft li.active").text();

        var cateVal = "0 - " + id1;
        var cateName = name1;

        $("#txt_CategoryPath").val(cateVal);
        $("#txt_CategoryName").val(cateName);
        $("#txt_CateName").text(cateName);
        $("#txt_CategoryKey").val(id1);
    }

    if (Action == 2) {
        var id1 = "";
        var name1 = "";

        id1 = $("#pnLeft li.active").attr("id");
        name1 = $("#pnLeft li.active").text();

        var id2 = "";
        var name2 = "";

        id2 = $("#pnRight li.active").attr("id");
        name2 = $("#pnRight li.active").text();

        var cateVal = "0 - " + id1 + " - " + id2;
        var cateName = name1 + " > " + name2

        $("#txt_CategoryPath").val(cateVal);
        $("#txt_CategoryName").val(cateName);
        $("#txt_CateName").text(cateName);
        $("#txt_CategoryKey").val(id2);
    }

    console.log($("#txt_CategoryPath").val());
}
function CheckID() {
    var key = $("#txt_ProductKey").val();
    var sku = $("#txt_SKU").val();
    var old = $("input[old]").val();

    if (sku.length <= 0) {
        Utils.OpenNotify("Thông báo", "Bạn phải nhập mã !.", "error");
        return false;
    }

    //chinh sua
    if (key.length >= 36) {
        if (sku.toLowerCase() !== old.toLowerCase()) {
            $.ajax({
                url: URL_CheckID,
                type: "GET",
                data: { SKU: sku },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (r) {
                    if (r.success) {
                        Utils.OpenNotify("Thông báo", "Mã này đã được sử dụng vui lòng nhập mã khác !.", "warning");
                        $("#txt_SKU").focus();
                        $("#txt_SKU")
                            .removeClass("valid-input")
                            .addClass("error-input");
                        return false;
                    }
                    else {
                        $("#txt_SKU")
                            .removeClass("error-input")
                            .addClass("valid-input");
                        return true;
                    }
                },
                error: function (err) {
                    Utils.OpenNotify("Thông báo", err.responseText, "error");
                },
                complete: function () {
                }
            });
        }
    }
    else {
        $.ajax({
            url: URL_CheckID,
            type: "GET",
            data: { SKU: sku },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (r) {
                if (r.success) {
                    Utils.OpenNotify("Thông báo", "Mã này đã được sử dụng vui lòng nhập mã khác !.", "warning");
                    $("#txt_SKU").focus();
                    $("#txt_SKU")
                        .removeClass("valid-input")
                        .addClass("error-input");
                    return false;
                }
                else {
                    $("#txt_SKU")
                        .removeClass("error-input")
                        .addClass("valid-input");
                    return true;
                }
            },
            error: function (err) {
                Utils.OpenNotify("Thông báo", err.responseText, "error");
            },
            complete: function () {
            }
        });
    }
}