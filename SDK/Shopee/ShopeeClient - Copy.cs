﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using xNet;
using XHttpRequest = xNet.HttpRequest;

namespace SDK.Shopee
{
    public class ShopeeClient
    {
        public string Message { get; set; } = "";
        public string ShopName { get; set; } = "";
        public long ShopId { get; set; } = 0;

        static string _ShopeeLog = "wwwroot/ShopeeLog/";
        static string _UserName = "";
        static string _Password = "";
        static string _SPC_CDS = "";
        static string _Cookie = "";
        XHttpRequest _Request;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SPC_CDS">Mã unique ngẫu nhiên duy nhất cho 1 lần làm việc</param>
        /// <param name="Cookie">Cookie login một khi đã có</param>
        public ShopeeClient(string SPC_CDS, string Cookie = "")
        {
            _Request = new();
            _Request.Cookies = new CookieDictionary();
            _SPC_CDS = SPC_CDS;
            _Cookie = Cookie;
        }

        /// <summary>
        /// 1 - Login to Shopee
        /// 2 - Get Account
        /// 3 - Login to banhang
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns>Account Shopee Info</returns>
        public Root_Account.Root Login(string UserName, string Password)
        {
            string zResult = "";
            string url_ref0 = "https://shopee.vn/";
            string url_ref1 = "https://shopee.vn﻿/buyer/login";
            string url_ref2 = "https://shopee.vn/api/v2/authentication/login";
            string url_ref3 = "https://shopee.vn/user/account/";
            string url_ref4 = "https://shopee.vn/api/v2/user/profile/get/";
            string url_ref5 = "https://banhang.shopee.vn/";
            string url_ref6 = "https://banhang.shopee.vn/api/v2/login/";

            var zRoot = new Root_Account.Root();

            try
            {
                _UserName = UserName;
                _Password = Password;

                string zJsonAuthen = GenerateAuthen(_UserName, _Password, out _);
                string zToken = TN_Utils.RandomString(32, true);

                WriteLog(_UserName, "[-----------------------------Đăng nhập shopee ------------------------------]");
                //----------------------------------Create session shopee--------------------------------------//
                WriteLog(_UserName, "Create session shopee");
                _Request.Cookies.Add("csrftoken", zToken);
                _Request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36");
                _Request.AddHeader("referer", url_ref0);
                _Request.AddHeader("x-csrftoken", zToken);
                var zResponse = _Request.Get(url_ref1);
                zResult = zResponse.ToString();

                //----------------------------------Begin login shopee--------------------------------------//
                WriteLog(_UserName, "Begin login shopee");
                _Request.AddHeader("referer", url_ref1);
                _Request.AddHeader("x-csrftoken", zToken);
                zResponse = _Request.Post(url_ref2, zJsonAuthen, "application/json");
                zResult = zResponse.ToString();

                //----------------------------------Login response shopee--------------------------------------//
                if (zResponse.StatusCode == xNet.HttpStatusCode.OK)
                {
                    //-------------------------------Get account shopee-----------------------------------------//
                    WriteLog(_UserName, "Get account shopee");
                    _Request.AddHeader("referer", url_ref3);
                    _Request.AddHeader("x-csrftoken", zToken);
                    zResponse = _Request.Get(url_ref4);
                    zResult = zResponse.ToString();
                    zRoot = JsonConvert.DeserializeObject<Root_Account.Root>(zResult);

                    if (zRoot.data != null)
                    {
                        //-------------------------------Begin login bán hàng shopee-----------------------------------------//
                        WriteLog(_UserName, "Begin login bán hàng shopee");
                        zResponse = _Request.Get(url_ref5);
                        zResponse.None();

                        _Request
                            .AddUrlParam("SPC_CDS", _SPC_CDS)
                            .AddUrlParam("SPC_CDS_VER", "2");
                        zResponse = _Request.Get(url_ref6);
                        zResponse.None();

                        //-------------------------------cookie login bán hàng shopee-----------------------------------------//
                        _Cookie = _Request.Cookies.ToString();
                        Message = string.Empty;
                        WriteLog(_UserName, "Cookie login bán hàng shopee (thành công)");
                    }
                    else
                    {
                        Message = "Không đăng nhập được vui lòng kiểm tra lại thông tin đăng nhập !.";
                        WriteLog(_UserName, Message + "\r\n" + zResult);
                    }
                }
                else
                {
                    Message = "Login result" + "Error " + zResponse.StatusCode;
                    WriteLog(_UserName, Message);
                }
            }
            catch (Exception ex)
            {
                Message = "Error client" + ex.ToString().GetFirstLine();
                WriteLog(_UserName, Message);
            }

            ShopName = zRoot.data.display_name;
            ShopId = zRoot.data.shopid;
            return zRoot;
        }

        public Root_MetaShopInfo.Root Get_MetaInfo(long shopId)
        {
            var zRoot = new Root_MetaShopInfo.Root();
            var zPost = new Root_MetaShopPost.Root();
            zPost.is_mass_ship = false;
            zPost.shop_list.Add(new Root_MetaShopPost.ShopList()
            {
                region_id = "VN",
                shop_id = shopId
            });

            string JsonPost = JsonConvert.SerializeObject(zPost);
            string url_ref0 = "https://banhang.shopee.vn/api/v3/order/get_shipment_meta_multi_shop";

            try
            {
                //?SPC_CDS=7e8467fd-22aa-4a30-bfb4-3795f9915565&SPC_CDS_VER=2
                WriteLog(_UserName, "Begin meta info shopee");

                if (!_Request.ContainsCookie("Cookie"))
                {
                    _Request.Cookies.Add("Cookie", _Cookie);
                }
                _Request
                    .AddUrlParam("SPC_CDS", _SPC_CDS)
                    .AddUrlParam("SPC_CDS_VER", "2");

                var zResponse = _Request.Post(url_ref0, JsonPost, "application/json");
                var zResult = zResponse.ToString();
                zRoot = JsonConvert.DeserializeObject<Root_MetaShopInfo.Root>(zResult);
            }
            catch (Exception ex)
            {
                Message = "Error client" + ex.ToString().GetFirstLine();
                WriteLog(_UserName, Message);
            }

            return zRoot;
        }

        /// <summary>
        /// ACTION danh sách ids
        /// </summary>
        /// <param name="Option">source ="" | "to_process" | "processed"</param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public Root_PackageList.Root ListIds(string Option = "", int Page = 1, long Shipper = 0)
        {
            WriteLog(_UserName, "[-----------------------------Thao tác lấy danh sách đơn hàng Ids------------------------------]");

            string url_ref0 = "https://banhang.shopee.vn/api/v3/order/get_package_list";
            var zRoot = new Root_PackageList.Root();
            try
            {
                WriteLog(_UserName, "Begin get list by ids shopee");
                if (!_Request.ContainsCookie("Cookie"))
                {
                    _Request.Cookies.Add("Cookie", _Cookie);
                }
                _Request
                    .AddUrlParam("SPC_CDS", _SPC_CDS)
                    .AddUrlParam("SPC_CDS_VER", "2")
                    .AddUrlParam("page_size", "40")
                    .AddUrlParam("page_number", Page)
                    .AddUrlParam("total", "0")
                    .AddUrlParam("sort_by", "confirmed_date_desc");

                if (Option != string.Empty)
                {
                    _Request.AddUrlParam("source", Option);
                }
                if (Shipper != 0)
                {
                    _Request.AddUrlParam("shipping_method", Shipper);
                }

                //------------------------------------------------------------------------//
                var zResponse = _Request.Get(url_ref0);
                var zResult = zResponse.ToString();
                zRoot = JsonConvert.DeserializeObject<Root_PackageList.Root>(zResult);

                if (zRoot.data != null)
                {
                    Message = string.Empty;
                    WriteLog(_UserName, "Kết quả: (thành công)");
                }
                else
                {
                    Message = "Result:" + zRoot.message;
                }
            }
            catch (Exception ex)
            {
                Message = "Error client" + ex.ToString().GetFirstLine();
                WriteLog(_UserName, Message);
            }

            return zRoot;
        }

        /// <summary>
        /// ACTION danh sách chi tiết
        /// </summary>
        /// <param name="Option">source ="" | "to_process" | "processed"</param>
        /// <param name="PostParam"> root paramater format in json</param>
        /// <returns></returns>
        public Root_OrderList.Root ListOrder(string PostParam, string Option = "")
        {
            WriteLog(_UserName, "[-----------------------------Thao tác lấy danh sách đơn hàng chi tiết------------------------------]");

            string url_ref1 = "https://banhang.shopee.vn/api/v3/order/get_shipment_order_list_by_order_ids_multi_shop";
            var zRoot = new Root_OrderList.Root();
            try
            {
                WriteLog(_UserName, "Begin get list in detail shopee");
                _Request.AddHeader("Referer", "https://banhang.shopee.vn/portal/sale/shipment");
                _Request
                    .AddUrlParam("SPC_CDS", _SPC_CDS)
                    .AddUrlParam("SPC_CDS_VER", "2")
                    .AddUrlParam("type", "toship");

                if (Option != string.Empty)
                {
                    _Request.AddUrlParam("source", Option);
                }

                var zResponse = _Request.Post(url_ref1, PostParam, "application/json");
                var zResult = zResponse.ToString();
                zRoot = JsonConvert.DeserializeObject<Root_OrderList.Root>(zResult);

                if (zRoot.data != null)
                {
                    Message = string.Empty;
                    WriteLog(_UserName, "Kết quả: (thành công)");
                }
                else
                {
                    Message = "Result:" + zRoot.message;
                }
            }
            catch (Exception ex)
            {
                Message = "Error client" + ex.ToString().GetFirstLine();
                WriteLog(_UserName, Message);
            }

            return zRoot;
        }

        /// <summary>
        /// ACTION Lấy chi tiết đơn hàng
        /// </summary>
        /// <param name="Id">id đơn hàng (long)</param>
        /// <returns></returns>
        public Root_OrderInfo.Root OrderInfo(long Id)
        {
            WriteLog(_UserName, "[-----------------------------Thao tác lấy 1 đơn hàng------------------------------]");

            var zRoot = new Root_OrderInfo.Root();
            string url_ref0 = "https://banhang.shopee.vn/api/v3/order/get_one_order";
            string url_ref1 = "https://banhang.shopee.vn/api/v3/order/get_package";
            string url_ref2 = "https://banhang.shopee.vn/api/v3/shipment/get_pickup";
            string url_ref3 = "https://banhang.shopee.vn/api/selleraccount/shop_info/";

            try
            {
                WriteLog(_UserName, "1. Begin get one order in detail shopee");
                if (!_Request.ContainsCookie("Cookie"))
                {
                    _Request.Cookies.Add("Cookie", _Cookie);
                }
                _Request
                        .AddUrlParam("SPC_CDS", _SPC_CDS)
                        .AddUrlParam("SPC_CDS_VER", "2")
                        .AddUrlParam("order_id", Id.ToString());

                var zResponse = _Request.Get(url_ref0);
                var zResult = zResponse.ToString();
                zRoot = JsonConvert.DeserializeObject<Root_OrderInfo.Root>(zResult);

                if (zRoot.data != null)
                {
                    Message = string.Empty;
                    WriteLog(_UserName, "Kết quả bước 1: (thành công)");

                    WriteLog(_UserName, "2. Begin get_package id shopee (lấy số vận chuyển)");
                    _Request
                            .AddUrlParam("SPC_CDS", _SPC_CDS)
                            .AddUrlParam("SPC_CDS_VER", "2")
                            .AddUrlParam("order_id", Id.ToString());

                    zResponse = _Request.Get(url_ref1);
                    zResult = zResponse.ToString();
                    var zRootTrack = JsonConvert.DeserializeObject<Root_TrackInfo.Root>(zResult);

                    if (zRootTrack.data != null && zRootTrack.data.order_info.package_list.Count > 0)
                    {
                        WriteLog(_UserName, "Kết quả bước 2 (thành công):");
                        string pnumber = zRootTrack.data.order_info.package_list[0].package_number;

                        WriteLog(_UserName, "3. Begin package_number id shopee (lấy mã vận đơn)");
                        _Request
                                .AddUrlParam("SPC_CDS", _SPC_CDS)
                                .AddUrlParam("SPC_CDS_VER", "2")
                                .AddUrlParam("order_id", Id.ToString())
                                .AddUrlParam("package_number", pnumber);

                        zResponse = _Request.Get(url_ref2);
                        zResult = zResponse.ToString();

                        var zRootPickup = JsonConvert.DeserializeObject<Root_PickUpInfo.Root>(zResult);

                        if (zRootPickup.data != null && zRootPickup.data.consignment_no.Length > 0)
                        {
                            var ma = zRootPickup.data.consignment_no;
                            WriteLog(_UserName, "Kết quả bước 3:" + ma);
                            zRoot.data.MaVanDon = ma;

                            WriteLog(_UserName, "4. Begin shop info shopee (lấy tên shop bán hàng)");
                            _Request
                                    .AddUrlParam("SPC_CDS", _SPC_CDS)
                                    .AddUrlParam("SPC_CDS_VER", "2");

                            zResponse = _Request.Get(url_ref3);
                            zResult = zResponse.ToString();
                            var zRootShopInfo = JsonConvert.DeserializeObject<Root_ShopInfo.Root>(zResult);
                            var name = zRootShopInfo.data.name;
                            WriteLog(_UserName, "Kết quả bước 4:" + name);
                            zRoot.data.TenShop = name;
                        }
                        else
                        {
                            Message = "Result:" + zRootPickup.message;
                        }
                    }
                    else
                    {
                        Message = "Result:" + zRootTrack.message;
                    }
                }
                else
                {
                    Message = "Result:" + zRoot.message;
                }
            }
            catch (Exception ex)
            {
                Message = "Error client" + ex.ToString().GetFirstLine();
                WriteLog(_UserName, Message);
            }
            return zRoot;
        }

        /// <summary>
        /// ACTION Lấy thời gian tới lấy hàng
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Root_PickUpTime.Root GetTimeSlot(long Id)
        {
            WriteLog(_UserName, "[-----------------------------Thao tác lấy thời gian vận chuyển đơn hàng------------------------------]");
            string package_number = "";
            string address_id = "";
            string channel_id = "";
            string shop_id = "";
            string donvigiaohang = "";

            var zRoot = new Root_PickUpTime.Root();
            string url_ref0 = "https://banhang.shopee.vn/api/v3/order/get_package";
            string url_ref1 = "https://banhang.shopee.vn/api/v3/shipment/get_pickup/";
            string url_ref2 = "https://banhang.shopee.vn/api/v3/logistics/get_pickup_address_list";
            string url_ref3 = "https://banhang.shopee.vn/api/v3/shipment/get_pickup_time_slots/";
            try
            {
                //Begin 1
                WriteLog(_UserName, "1. Begin get package number shopee");
                if (!_Request.ContainsCookie("Cookie"))
                {
                    _Request.Cookies.Add("Cookie", _Cookie);
                }
                _Request
                        .AddUrlParam("SPC_CDS", _SPC_CDS)
                        .AddUrlParam("SPC_CDS_VER", "2")
                        .AddUrlParam("order_id", Id.ToString());

                var zResponse = _Request.Get(url_ref0);
                var zResult = zResponse.ToString();
                var zRootTrack = JsonConvert.DeserializeObject<Root_TrackInfo.Root>(zResult);
                if (zRootTrack.data != null && zRootTrack.data.order_info.package_list.Count > 0)
                {
                    WriteLog(_UserName, "Kết quả bước 1 (thành công):");
                    package_number = zRootTrack.data.order_info.package_list[0].package_number;
                    //Begin 2
                    WriteLog(_UserName, "2. Begin get pickup shopee");
                    _Request
                            .AddUrlParam("SPC_CDS", _SPC_CDS)
                            .AddUrlParam("SPC_CDS_VER", "2")
                            .AddUrlParam("order_id", Id.ToString())
                            .AddUrlParam("package_number", package_number);
                    zResponse = _Request.Get(url_ref1);
                    zResult = zResponse.ToString();
                    var zRootPickup = JsonConvert.DeserializeObject<Root_PickUpInfo.Root>(zResult);
                    if (zRootPickup.data != null && zRootPickup.data.channel_id > 0)
                    {
                        WriteLog(_UserName, "Kết quả bước 2 (thành công):");
                        channel_id = zRootPickup.data.channel_id.ToString();

                        //Begin 3
                        WriteLog(_UserName, "3. Begin get address list shopee");
                        _Request
                                .AddUrlParam("SPC_CDS", _SPC_CDS)
                                .AddUrlParam("SPC_CDS_VER", "2")
                                .AddUrlParam("order_ids", Id.ToString());
                        zResponse = _Request.Get(url_ref2);
                        zResult = zResponse.ToString();
                        var zRootAddress = JsonConvert.DeserializeObject<Root_PickUpAddress.Root>(zResult);
                        if (zRootAddress.data != null && zRootAddress.data.list.Count > 0)
                        {
                            WriteLog(_UserName, "Kết quả bước 3 (thành công):");
                            var listAddress = zRootAddress.data.list;

                            foreach (var rec in listAddress)
                            {
                                if (rec.status == 1)
                                {
                                    address_id = rec.address_id.ToString();
                                    break;
                                }
                            }

                            //Begin 4
                            WriteLog(_UserName, "4. Tạo tham số post get time slot shopee");
                            string temp = " - order_id = " + Id + "";
                            temp += " - address_id = " + address_id + "";
                            temp += " - channel_id = " + channel_id + "";
                            WriteLog(_UserName, temp);

                            WriteLog(_UserName, "4.1. Begin get time shopee");
                            _Request
                                .AddUrlParam("SPC_CDS", _SPC_CDS)
                                .AddUrlParam("SPC_CDS_VER", "2")
                                .AddUrlParam("order_id", Id.ToString())
                                .AddUrlParam("address_id", address_id)
                                .AddUrlParam("channel_id", channel_id);

                            zResponse = _Request.Get(url_ref3);
                            zResult = zResponse.ToString();
                            zRoot = JsonConvert.DeserializeObject<Root_PickUpTime.Root>(zResult);
                            donvigiaohang = zRootTrack.data.order_info.package_list[0].fulfillment_carrier_name;
                            if (zRoot.data != null && zRoot.message == "success")
                            {
                                WriteLog(_UserName, "Kết quả bước 4 (thành công):");
                            }
                            else
                            {
                                Message = "Result : (Không tìm thấy time list )" + zRoot.message;
                                WriteLog(_UserName, Message);
                            }
                        }
                        else
                        {
                            Message = "Result : (Không tìm thấy address list )" + zRootAddress.message;
                            WriteLog(_UserName, Message);
                        }
                    }
                    else
                    {
                        Message = "Result : (Không tìm thấy số channel_id )" + zRootPickup.message;
                        WriteLog(_UserName, Message);
                    }
                }
                else
                {
                    Message = "Result : (Không tìm thấy số package_number )" + zRootTrack.message;
                    WriteLog(_UserName, Message);
                }
            }
            catch (Exception ex)
            {
                Message = "Error client" + ex.ToString().GetFirstLine();
                WriteLog(_UserName, Message);
            }

            #region [-- Dùng post khởi tạo/xác nhận đơn hàng --]

            Root_PostInitStatus.Root Post = new()
            {
                order_id = Id,
                package_number = package_number,
                pickup_address_id = Convert.ToInt64(address_id),
                pickup_time = 0,
                seller_real_name = "",
                shipping_mode = "",
                remark = "",
            };
            WriteLog(_UserName, "Tham số post khởi tạo/ xác nhận đơn hàng: " + JsonConvert.SerializeObject(Post));
            #endregion

            zRoot.data.DonViGiaohang = donvigiaohang;
            zRoot.PostInit = Post;
            return zRoot;
        }

        public Root_PickUpAddress.Root GetAddress(long Id)
        {
            var zRootAddress = new Root_PickUpAddress.Root();
            try
            {
                string url_ref0 = "https://banhang.shopee.vn/api/v3/logistics/get_pickup_address_list";

                WriteLog(_UserName, "1. Begin get địa chỉ lấy hàng shopee");
                if (!_Request.ContainsCookie("Cookie"))
                {
                    _Request.Cookies.Add("Cookie", _Cookie);
                }
                _Request
                    .AddUrlParam("SPC_CDS", _SPC_CDS)
                    .AddUrlParam("SPC_CDS_VER", "2")
                    .AddUrlParam("order_ids", Id.ToString());
                var zResponse = _Request.Get(url_ref0);
                var zResult = zResponse.ToString();
                zRootAddress = JsonConvert.DeserializeObject<Root_PickUpAddress.Root>(zResult);
            }
            catch (Exception ex)
            {
                Message = "Error client" + ex.ToString().GetFirstLine();
                WriteLog(_UserName, Message);
            }

            return zRootAddress;
        }

        /// <summary>
        /// ACTION xác nhận đơn hàng
        /// </summary>
        /// <param name="RootPost"></param>
        /// <returns></returns>
        public bool InitOrder(Root_PostInitStatus.Root RootPost)
        {
            WriteLog(_UserName, "[-----------------------------Thao tác chốt xác nhận đơn hàng------------------------------]");

            #region Param to send
            string channel_id = "";
            string region_id = "";
            string shop_id = "";
            string order_id = "";
            string package_number = "";
            #endregion

            var JsonPost = JsonConvert.SerializeObject(RootPost);
            order_id = RootPost.order_id.ToString();
            package_number = RootPost.package_number.ToString();

            bool Result = false;
            string url_ref0 = "https://banhang.shopee.vn/api/v3/shipment/init_order/";
            //string url_ref1 = "https://banhang.shopee.vn/api/selleraccount/shop_info/";
            //string url_ref2 = "https://banhang.shopee.vn/api/v3/shipment/batch_get_package_shipment_status_multi_shop";
            try
            {
                //Begin 1
                WriteLog(_UserName, "1. Begin init order shopee");
                if (!_Request.ContainsCookie("Cookie"))
                {
                    _Request.Cookies.Add("Cookie", _Cookie);
                }

                _Request
                        .AddUrlParam("SPC_CDS", _SPC_CDS)
                        .AddUrlParam("SPC_CDS_VER", "2");

                var zResponse = _Request.Post(url_ref0, JsonPost, "application/json");
                var zResult = zResponse.ToString();
                Message = "Kết quả gửi lệnh tạo đơn hàng" + zResult;
                WriteLog(_UserName, Message);

                #region [--Gửi lệnh kiem tra trạng thái xử lý--]
                //if (zResponse.StatusCode == HttpStatusCode.OK)
                //{
                //    //Begin 2
                //    //WriteLog(_UserName, "2. Begin get channel_id");
                //    //_Request
                //    //      .AddUrlParam("SPC_CDS", _SPC_CDS)
                //    //      .AddUrlParam("SPC_CDS_VER", "2")
                //    //      .AddUrlParam("order_id", order_id)
                //    //      .AddUrlParam("package_number", package_number);
                //    //zResponse = _Request.Get(url_ref1);
                //    //zResult = zResponse.ToString();
                //    //var zRootPickup = JsonConvert.DeserializeObject<Root_PickUpInfo.Root>(zResult);
                //    //channel_id = zRootPickup.data.channel_id.ToString();

                //    ////Begin 3
                //    //WriteLog(_UserName, "3. Begin shop info shopee (lấy tên shop bán hàng)");
                //    //_Request
                //    //        .AddUrlParam("SPC_CDS", _SPC_CDS)
                //    //        .AddUrlParam("SPC_CDS_VER", "2");

                //    //zResponse = _Request.Get(url_ref1);
                //    //zResult = zResponse.ToString();
                //    //var zRootShopInfo = JsonConvert.DeserializeObject<Root_ShopInfo.Root>(zResult);
                //    //shop_id = zRootShopInfo.data.shop_id.ToString();
                //    //region_id = zRootShopInfo.data.shop_region;

                //    ////Begin 4
                //    //WriteLog(_UserName, "4. Begin status trả về shopee // có thể lỗi '{0}' [ bool success = RootStatus.data.orders_status[0] ]");
                //    //var Post = new Root_PostGetStatus.Root();
                //    //Post.order_list = new List<Root_PostGetStatus.OrderList>();
                //    //Post.order_list.Add(new Root_PostGetStatus.OrderList()
                //    //{
                //    //    channel_id = Convert.ToInt64(channel_id),
                //    //    order_id = Convert.ToInt64(order_id),
                //    //    package_number = package_number,
                //    //    region_id = region_id,
                //    //    shop_id = Convert.ToInt64(shop_id),
                //    //});

                //    //JsonPost = JsonConvert.SerializeObject(Post);

                //    //_Request
                //    //     .AddUrlParam("SPC_CDS", _SPC_CDS)
                //    //     .AddUrlParam("SPC_CDS_VER", "2");
                //    //zResponse = _Request.Post(url_ref2, JsonPost, "application/json");
                //    //zResult = zResponse.ToString();
                //    //var RootStatus = JsonConvert.DeserializeObject<Root_CheckStatus.Root>(zResult);
                //    //if (RootStatus.data != null)
                //    //{
                //    //    bool success = RootStatus.data.orders_status[0].can_print_normal_waybill;
                //    //    WriteLog(_UserName, "4.1 Kết quả trả về !." + success);

                //    //    while (success == false)
                //    //    {
                //    //        WriteLog(_UserName, "4.1 chạy lặp khi kết quả = " + success);

                //    //        _Request
                //    //            .AddUrlParam("SPC_CDS", _SPC_CDS)
                //    //            .AddUrlParam("SPC_CDS_VER", "2");
                //    //        zResponse = _Request.Post(url_ref2, JsonPost, "application/json");
                //    //        zResult = zResponse.ToString();
                //    //        RootStatus = JsonConvert.DeserializeObject<Root_CheckStatus.Root>(zResult);
                //    //        success = RootStatus.data.orders_status[0].can_print_normal_waybill;
                //    //    }

                //    //    WriteLog(_UserName, "Kết quả trả về !." + success + "> tải lại trang !.");
                //    //}
                //}
                //else
                //{
                //    WriteLog(_UserName, "Lỗi client " + zResponse.StatusCode);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                Message = "Error client" + ex.ToString().GetFirstLine();
                WriteLog(_UserName, Message);
            }

            return Result;
        }

        /// <summary>
        /// ACTION Lấy danh sách shipper
        /// </summary>
        /// <returns></returns>
        public Root_ShipmentList.Root GetShipperList()
        {
            var zRoot = new Root_ShipmentList.Root();

            string url_ref0 = "https://banhang.shopee.vn/api/v3/logistics/get_optional_channel_list";//?SPC_CDS=af7971df-3511-4894-8707-73a0fa80565a&SPC_CDS_VER=2&source=shipment_list
            try
            {
                WriteLog(_UserName, "1. Begin get danh sách shipper shopee");
                if (!_Request.ContainsCookie("Cookie"))
                {
                    _Request.Cookies.Add("Cookie", _Cookie);
                }
                _Request
                    .AddUrlParam("SPC_CDS", _SPC_CDS)
                    .AddUrlParam("SPC_CDS_VER", "2")
                    .AddUrlParam("source", "shipment_list");
                var zResponse = _Request.Get(url_ref0);
                var zResult = zResponse.ToString();
                zRoot = JsonConvert.DeserializeObject<Root_ShipmentList.Root>(zResult);
            }
            catch (Exception ex)
            {
                Message = "Error client" + ex.ToString().GetFirstLine();
                WriteLog(_UserName, Message);
            }

            return zRoot;
        }


        /// <summary>
        /// Get cookie, sau khi đăng nhập
        /// </summary>
        /// <returns></returns>
        public string GetCookie()
        {
            return _Cookie;
        }

        //-----------------------------------------------------------------------------------------------------------//
        private string GenerateAuthen(string UserName, string Password, out string Option)
        {
            bool isNumeric = int.TryParse(UserName, out int n);
            string Authen;
            if (isNumeric)
            {
                var Data = new
                {
                    phone = "84" + n,
                    password = GetSha256(CreateMD5(Password).ToLower()),
                    support_ivs = true,
                    support_whats_app = true
                };

                Option = "phone";
                Authen = JsonConvert.SerializeObject(Data);
            }
            else
            {
                if (IsValidEmail(UserName))
                {
                    var Data = new
                    {
                        email = UserName,
                        password = GetSha256(CreateMD5(Password).ToLower()),
                        support_ivs = true,
                        support_whats_app = true
                    };
                    Option = "email";
                    Authen = JsonConvert.SerializeObject(Data);
                }
                else
                {
                    var Data = new
                    {
                        username = UserName,
                        password = GetSha256(CreateMD5(Password).ToLower()),
                        support_ivs = true,
                        support_whats_app = true
                    };
                    Option = "username";
                    Authen = JsonConvert.SerializeObject(Data);
                }
            }

            return Authen;
        }
        private bool IsValidEmail(string input)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(input);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private string GetSha256(string text)
        {
            if (text == null)
            {
                return string.Empty;
            }
            byte[] message = System.Text.Encoding.ASCII.GetBytes(text);
            SHA256Managed hashString256 = new SHA256Managed();
            byte[] hashValue = hashString256.ComputeHash(message);
            string hashString = string.Empty;
            foreach (byte x in hashValue)
            {
                hashString += string.Format("{0:x2}", x);
            }
            return hashString;
        }
        private string CreateMD5(string input)
        {
            //Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        private string RandomString(int size, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);

            // Unicode/ASCII Letters are divided into two blocks
            // (Letters 65–90 / 97–122):   
            // The first group containing the uppercase letters and
            // the second group containing the lowercase.  

            // char is a single Unicode character  
            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26; // A...Z or a..z: length = 26  

            for (var i = 0; i < size; i++)
            {
                var @char = (char)new Random().Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }
        private static object zLockerLog = new object();
        private static void WriteLog(string FileName, string Content)
        {
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), _ShopeeLog);

            lock (zLockerLog)
            {
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                File.AppendAllText(filePath + "\\" + FileName + ".txt", "\r\n" + DateTime.Now.ToString() + ": " + "\r\n" + Content + "\r\n");
            }
        }


        //-----------------------------------------------------------------------------------------------------------//
    }

    public class ShopeeSession
    {
        public string ShopName { get; set; } = "";
        public long ShopId { get; set; } = 0;
        public string Cookie { get; set; } = "";
        public string GSession { get; set; } = "";
    }
}