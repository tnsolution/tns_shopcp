﻿using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace SDK.Shopee.Root_GetCatId {
    public class Data
    {
        public List<object> cats { get; set; }
        public string ds_cat_rcmd_id { get; set; }
    }

    public class Root
    {
        public string message { get; set; }
        public long code { get; set; }
        public Data data { get; set; }
        public string user_message { get; set; }
    }

}

namespace SDK.Shopee.Root_PostAttr {
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class List
    {
        public string name { get; set; }
        public long category_id { get; set; }
        public string image { get; set; }
    }

    public class Root
    {
        public List<List> list { get; set; }
    }


}
namespace SDK.Shopee.Root_GetAttrId {
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Datum
    {
        public string ds_services_id { get; set; }
        public List<object> attribute_recommend_list { get; set; }
    }

    public class Root
    {
        public string message { get; set; }
        public long code { get; set; }
        public List<Datum> data { get; set; }
        public string user_message { get; set; }
    }


}

namespace SDK.Shopee.Root_ProductUpdate
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Result
    {
        public string message { get; set; }
        public long code { get; set; }
        public string user_message { get; set; }
    }

    public class Data
    {
        public List<Result> result { get; set; }
    }

    public class Root
    {
        public string message { get; set; }
        public long code { get; set; }
        public Data data { get; set; }
        public string user_message { get; set; }
    }


}

namespace SDK.Shopee.Root_ProductCreate
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Data
    {
        public long product_id { get; set; }
    }

    public class Root
    {
        public string message { get; set; }
        public long code { get; set; }
        public Data data { get; set; }
        public string user_message { get; set; }
    }


}

namespace SDK.Shopee.Root_ImgUpload
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Data
    {
        public string resource_id { get; set; }
    }

    public class Root
    {
        public long code { get; set; }
        public string message { get; set; }
        public string user_message { get; set; }
        public Data data { get; set; }
    }


}

namespace SDK.Shopee.Root_ProductPost
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ModelList
    {
        public object id { get; set; }
        public string sku { get; set; }
        public List<long> tier_index { get; set; }
        public bool is_default { get; set; }
        public string name { get; set; }
        public string item_price { get; set; }
        public string price { get; set; }
        public long stock { get; set; }
    }

    public class InstallmentTenures
    {
    }

    public class Dimension
    {
        public long width { get; set; }
        public long length { get; set; }
        public long height { get; set; }
    }

    public class TierVariation
    {
        public List<string> images { get; set; }
        public string name { get; set; }
        public List<string> options { get; set; }
    }

    public class LogisticsChannel
    {
        public long size { get; set; }
        public string price { get; set; }
        public bool cover_shipping_fee { get; set; }
        public bool enabled { get; set; }
        public string item_flag { get; set; }
        public long channelid { get; set; }
        public long sizeid { get; set; }
    }

    public class Root
    {
        public long id { get; set; }
        public string name { get; set; }
        public long brand_id { get; set; }
        public List<string> images { get; set; }
        public string description { get; set; }
        public List<ModelList> model_list { get; set; }
        public List<long> category_path { get; set; }
        public List<object> attributes { get; set; }
        public string parent_sku { get; set; }
        public List<object> wholesale_list { get; set; }
        public InstallmentTenures installment_tenures { get; set; }
        public string weight { get; set; }
        public Dimension dimension { get; set; }
        public bool pre_order { get; set; }
        public long days_to_ship { get; set; }
        public long condition { get; set; }
        public string size_chart { get; set; }
        public List<object> video_list { get; set; }
        public List<TierVariation> tier_variation { get; set; }
        public List<object> add_on_deal { get; set; }
        public string price { get; set; }
        public long stock { get; set; }
        public List<LogisticsChannel> logistics_channels { get; set; }
        public string ds_cat_rcmd_id { get; set; }
        public List<object> category_recommend { get; set; }
        public string ds_attr_rcmd_id { get; set; }
        public bool unlisted { get; set; }
    }


}

namespace SDK.Shopee.Root_ProductList
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class PageInfo
    {
        public long total { get; set; }
        public long page_number { get; set; }
        public long page_size { get; set; }
    }

    public class PriceInfo
    {
        public long price_lock_type { get; set; }
        public string input_normal_price { get; set; }
        public string input_promotion_price { get; set; }
        public string normal_price { get; set; }
        public string promotion_price { get; set; }
    }

    public class StockInfo
    {
        public long stock_lock_type { get; set; }
        public long normal_stock { get; set; }
        public long reserved_stock { get; set; }
        public long promotion_stock { get; set; }
        public long sip_stock { get; set; }
    }

    public class WmsStockInfo
    {
        public long stock_lock_type { get; set; }
        public long normal_stock { get; set; }
        public long reserved_stock { get; set; }
        public long promotion_stock { get; set; }
        public long sip_stock { get; set; }
    }

    public class ModelList
    {
        public bool add_on_deal { get; set; }
        public long id { get; set; }
        public bool is_default { get; set; }
        public PriceInfo price_info { get; set; }
        public long sku_type { get; set; }
        public long sold { get; set; }
        public StockInfo stock_info { get; set; }
        public WmsStockInfo wms_stock_info { get; set; }
        public string sku { get; set; }
        public string name { get; set; }
    }

    public class OngoingCampaign
    {
        public object product_id { get; set; }
        public object model_id { get; set; }
        public long campaign_type { get; set; }
        public long promo_source { get; set; }
        public long start_time { get; set; }
        public long end_time { get; set; }
        public string price { get; set; }
        public long stock { get; set; }
        public string input_price { get; set; }
    }

    public class OngoingUpcomingCampaigns
    {
        public List<OngoingCampaign> ongoing_campaigns { get; set; }
        public List<object> upcoming_campaigns { get; set; }
    }

    public class TierVariation
    {
        public string name { get; set; }
        public List<string> options { get; set; }
        public List<object> images { get; set; }
    }

    public class VideoList
    {
        public string vid { get; set; }
        public string video_id { get; set; }
        public string thumb_url { get; set; }
        public long duration { get; set; }
        public long version { get; set; }
    }

    public class List
    {
        public bool add_on_deal { get; set; }
        public long boost_cool_down_seconds { get; set; }
        public object bundle_deal { get; set; }
        public List<long> category_path { get; set; }
        public long flag { get; set; }
        public long id { get; set; }
        public List<string> images { get; set; }
        public bool in_promotion { get; set; }
        public long like_count { get; set; }
        public List<ModelList> model_list { get; set; }
        public string name { get; set; }
        public OngoingUpcomingCampaigns ongoing_upcoming_campaigns { get; set; }
        public string parent_sku { get; set; }
        public long sold { get; set; }
        public long status { get; set; }
        public List<TierVariation> tier_variation { get; set; }
        public long view_count { get; set; }
        public List<object> virtual_sku_info_list { get; set; }
        public bool wholesale { get; set; }
        public long category_status { get; set; }
        public List<VideoList> video_list { get; set; }
        public string size_chart { get; set; }
        public long brand_id { get; set; }
        public string brand { get; set; }
        public List<object> attributes { get; set; }
        public long create_time { get; set; }
        public long attribute_status { get; set; }
        public long modify_time { get; set; }
    }

    public class Data
    {
        public PageInfo page_info { get; set; }
        public List<List> list { get; set; }
        public long service_grade { get; set; }
    }

    public class Root
    {
        public long code { get; set; }
        public string message { get; set; }
        public string user_message { get; set; }
        public Data data { get; set; }
    }


}

namespace SDK.Shopee.Root_Category {
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class RegionSetting
    {
        public long low_stock_value { get; set; }
        public bool enable_size_chart { get; set; }
    }
    public class List
    {
        public RegionSetting region_setting { get; set; }
        public string display_name { get; set; }
        public string name { get; set; }
        public bool has_active_children { get; set; }
        public bool has_children { get; set; }
        public List<List> children { get; set; }
        public long parent_id { get; set; }
        public bool is_prohibit { get; set; }
        public long id { get; set; }
    }
    public class Data
    {
        public List<List> list { get; set; }
    }
    public class Root
    {
        public string message { get; set; }
        public long code { get; set; }
        public Data data { get; set; }
        public string user_message { get; set; }
    }
}
//------------------------------------------------------------------------------

namespace SDK.Shopee.Root_Capcha
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class CaptchaBody
    {
        public string bg_img { get; set; }
    }

    public class Data
    {
        public int captcha_type { get; set; }
        public string captcha_id { get; set; }
        public CaptchaBody captcha_body { get; set; }
    }

    public class Root
    {
        public string debug_message { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public Data data { get; set; }
    }
}

namespace SDK.Shopee.Root_AccountOtp
{
    public class Root
    {
        public string username { get; set; }
        public long shopid { get; set; }
        public string phone { get; set; }
        public string sso { get; set; }
        public string cs_token { get; set; }
        public string portrait { get; set; }
        public long id { get; set; }
        public string language { get; set; }
        public long errcode { get; set; }
        public string token { get; set; }
        public object sub_account_token { get; set; }
        public string email { get; set; }
    }
}

namespace SDK.Shopee.Root_Account
{
    public class Root
    {
        public object bff_meta { get; set; }
        public long error { get; set; }
        public object error_msg { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public long userid { get; set; }
        public long shopid { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string portrait { get; set; }
        public bool phone_verified { get; set; }
        public long gender { get; set; }
        public long birth_timestamp { get; set; }
        public bool not_new_user { get; set; }
        public string nickname { get; set; }
        public object adult_consent { get; set; }
        public object editable_username { get; set; }
        public bool has_legacy_wallet { get; set; }
        public string display_name { get; set; }
        public long wallet_provider { get; set; }
    }
}

namespace SDK.Shopee.Root_PackageList
{
    public class ItemList
    {
        public long item_id { get; set; }
        public long model_id { get; set; }
        public long item_price { get; set; }
        public long quantity { get; set; }
        public long virtual_item_id { get; set; }
        public long order_item_id { get; set; }
        public long virtual_model_id { get; set; }
        public long group_id { get; set; }
    }

    public class OrderAction
    {
        public bool can_split { get; set; }
        public bool can_unsplit { get; set; }
    }

    public class LogisticsLog
    {
        public long id { get; set; }
        public long logid { get; set; }
        public long flag { get; set; }
        public string description { get; set; }
        public long ctime { get; set; }
    }

    public class TrackingInfo
    {
        public string driver_name { get; set; }
        public string driver_phone { get; set; }
        public List<LogisticsLog> logistics_logs { get; set; }
    }

    public class PackageList
    {
        public string carrier_name { get; set; }
        public string masking_carrier_name { get; set; }
        public long channel_id { get; set; }
        public long masking_channel_id { get; set; }
        public string consignment_no { get; set; }
        public long dropoff_b2c { get; set; }
        public long expire_time { get; set; }
        public long extra_flag { get; set; }
        public string first_mile_tn { get; set; }
        public long logistics_flag { get; set; }
        public long pickup_time { get; set; }
        public string ref1 { get; set; }
        public string seller_remark { get; set; }
        public string shipping_method { get; set; }
        public string third_party_tn { get; set; }
        public string checkout_carrier_name { get; set; }
        public long checkout_channel_id { get; set; }
        public string package_number { get; set; }
        public long forder_logistics_status { get; set; }
        public long package_logistics_status { get; set; }
        public long forder_shipping_status { get; set; }
        public long package_shipping_status { get; set; }
        public long is_pickup { get; set; }
        public long is_pre_order { get; set; }
        public List<ItemList> item_list { get; set; }
        public OrderAction order_action { get; set; }
        public long order_create_time { get; set; }
        public long order_id { get; set; }
        public long order_logistics_status { get; set; }
        public string order_sn { get; set; }
        public long parcel_no { get; set; }
        public string parcel_price { get; set; }
        public string region_id { get; set; }
        public long ship_by_date { get; set; }
        public long shipping_confirm_time { get; set; }
        public long shop_id { get; set; }
        public long split_up { get; set; }
        public long allocating_status { get; set; }
        public TrackingInfo tracking_info { get; set; }
        public long user_id { get; set; }
        public string fulfillment_carrier_name { get; set; }
        public long fulfillment_shipping_method { get; set; }
        public long fulfillment_channel_id { get; set; }
        public object preferred_delivery_timeslot { get; set; }
        public bool is_affiliated_shop_order { get; set; }
        public string plp_number { get; set; }
        public long code { get; set; }
        public string transify_key { get; set; }
    }

    public class Data
    {
        public long total { get; set; }
        public long page_number { get; set; }
        public long page_size { get; set; }
        public List<PackageList> package_list { get; set; }
    }

    public class Root
    {
        public long code { get; set; }
        public Data data { get; set; }
        public string message { get; set; }
        public string user_message { get; set; }
    }
}

namespace SDK.Shopee.Root_OrderList
{
    public class DropshippingInfo
    {
        public long enabled { get; set; }
        public string name { get; set; }
        public string phone_number { get; set; }
    }

    public class ExtInfo
    {
        public string geo_info { get; set; }
        public Access access { get; set; }
        public bool disable_new_device_login_otp { get; set; }
        public long gender { get; set; }
        public long smid_status { get; set; }
        public long ba_check_status { get; set; }
        public bool feed_private { get; set; }
        public long delivery_address_id { get; set; }
        public long birth_timestamp { get; set; }
        public bool holiday_mode_on { get; set; }
        public long tos_accepted_time { get; set; }
        public AddressInfo address_info { get; set; }
    }

    public class SellerAddress
    {
        public long address_id { get; set; }
        public long user_id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string address { get; set; }
        public long status { get; set; }
        public long ctime { get; set; }
        public long mtime { get; set; }
        public string zip_code { get; set; }
        public long def_time { get; set; }
        public string full_address { get; set; }
        public string district { get; set; }
        public string town { get; set; }
        public long logistics_status { get; set; }
        public string icno { get; set; }
        public ExtInfo ext_info { get; set; }
    }

    public class ItemModel
    {
        public long model_id { get; set; }
        public long item_id { get; set; }
        public string name { get; set; }
        public string price { get; set; }
        public string currency { get; set; }
        public long stock { get; set; }
        public long status { get; set; }
        public string price_before_discount { get; set; }
        public long promotion_id { get; set; }
        public string rebate_price { get; set; }
        public long sold { get; set; }
        public long ctime { get; set; }
        public long mtime { get; set; }
        public string sku { get; set; }
    }

    public class Product
    {
        public string name { get; set; }
        public List<string> images { get; set; }
        public string branch { get; set; }
        public string brand { get; set; }
        public long cat_id { get; set; }
        public long cmt_count { get; set; }
        public long condition { get; set; }
        public long ctime { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public long estimated_days { get; set; }
        public bool is_pre_order { get; set; }
        public long item_id { get; set; }
        public long model_id { get; set; }
        public long liked_count { get; set; }
        public string price { get; set; }
        public string price_before_discount { get; set; }
        public long shop_id { get; set; }
        public long sold { get; set; }
        public long status { get; set; }
        public long stock { get; set; }
        public string sku { get; set; }
        public long snapshot_id { get; set; }
    }

    public class OrderItem
    {
        public long add_on_deal_id { get; set; }
        public long amount { get; set; }
        public long bundle_deal_id { get; set; }
        public string group_id { get; set; }
        public bool is_add_on_sub_item { get; set; }
        public bool is_wholesale { get; set; }
        public string item_price { get; set; }
        public long item_id { get; set; }
        public long model_id { get; set; }
        public string order_price { get; set; }
        public string price_before_bundle { get; set; }
        public long snapshot_id { get; set; }
        public long status { get; set; }
        public long shop_id { get; set; }
        public long order_item_id { get; set; }
        public bool is_virtual_sku { get; set; }
        public string comm_fee_rate { get; set; }
        public object bundle_deal { get; set; }
        public ItemModel item_model { get; set; }
        public List<object> bundle_deal_model { get; set; }
        public List<object> item_list { get; set; }
        public Product product { get; set; }
        public List<object> bundle_deal_product { get; set; }
        public long sub_type { get; set; }
    }

    public class Access
    {
        public long hide_likes { get; set; }
        public long wallet_setting { get; set; }
        public long seller_coin_setting { get; set; }
        public long seller_wholesale_setting { get; set; }
        public long seller_ads_setting { get; set; }
    }

    public class AddressInfo
    {
        public long mcount { get; set; }
        public long mcount_create_time { get; set; }
        public bool in_white_list { get; set; }
    }

    public class BuyerUser
    {
        public long delivery_succ_count { get; set; }
        public long hide_likes { get; set; }
        public long user_id { get; set; }
        public string user_name { get; set; }
        public string portrait { get; set; }
        public bool phone_public { get; set; }
        public string language { get; set; }
        public long cb_option { get; set; }
        public ExtInfo ext_info { get; set; }
        public long delivery_order_count { get; set; }
        public long buyer_rating { get; set; }
        public List<long> rating_count { get; set; }
        public float rating_star { get; set; }
        public long shop_id { get; set; }
        public bool followed { get; set; }
    }

    public class Order
    {
        /// <summary>
        /// Add thêm vào
        /// </summary>
        public string package_number { get; set; }
        public long shop_id { get; set; }
        public long order_id { get; set; }
        public string order_sn { get; set; }
        public string shipping_address { get; set; }
        public string shipping_fee { get; set; }
        public string actual_carrier { get; set; }
        public long payment_method { get; set; }
        public string remark { get; set; }
        public long status { get; set; }
        public long complete_time { get; set; }
        public long status_ext { get; set; }
        public long arrange_pickup_by_date { get; set; }
        public long auto_cancel_3pl_ack_date { get; set; }
        public long auto_cancel_arrange_ship_date { get; set; }
        public long buyer_is_rated { get; set; }
        public DropshippingInfo dropshipping_info { get; set; }
        public long pickup_attempts { get; set; }
        public long pickup_cutoff_time { get; set; }
        public SellerAddress seller_address { get; set; }
        public long seller_address_id { get; set; }
        public long seller_due_date { get; set; }
        public string shipping_proof { get; set; }
        public string buyer_address_name { get; set; }
        public string buyer_address_phone { get; set; }
        public object cancellation_end_date { get; set; }
        public bool order_ratable { get; set; }
        /// <summary>
        /// Các mặt hàng của 1 đơn hàng
        /// </summary>
        public List<OrderItem> order_items { get; set; }
        public BuyerUser buyer_user { get; set; }
        public long list_type { get; set; }
        public long invoice_data_status { get; set; }
        public string fulfillment_carrier_name { get; set; }
        public string GoiYDuSanPham { get; set; } //Gợi ý đủ hàng còn trong kho không
    }

    public class Data
    {
        public string viewtype { get; set; }
        public List<Order> orders { get; set; } = new List<Order>();
    }

    public class Root
    {
        public long code { get; set; }
        public Data data { get; set; } = new Data();
        public string message { get; set; }
        public string user_message { get; set; }
    }


}

namespace SDK.Shopee.Root_ShopInfo
{
    public class Data
    {
        public string shop_region { get; set; }
        public string name { get; set; }
        public bool official_shop { get; set; }
        public long shop_id { get; set; }
        public bool is_sip_primary { get; set; }
        public long cb_option { get; set; }
    }

    public class Root
    {
        public string message { get; set; }
        public long code { get; set; }
        public Data data { get; set; }
        public string user_message { get; set; }
    }
}

namespace SDK.Shopee.Root_OrderPost
{
    public class Order
    {
        public long order_id { get; set; }
        public long shop_id { get; set; }
        public string region_id { get; set; }
    }

    public class Root
    {
        public bool from_seller_data { get; set; } = false;
        public List<Order> orders { get; set; } = new List<Order>();
    }
}

namespace SDK.Shopee.Root_OrderInfo
{
    public class CardTxnFeeInfo
    {
        public string card_txn_fee { get; set; }
        public long rule_id { get; set; }
    }

    public class DropshippingInfo
    {
        public long enabled { get; set; }
        public string name { get; set; }
        public string phone_number { get; set; }
    }

    public class ExtInfo
    {
        public string geo_info { get; set; }
        public Access access { get; set; }
        public bool disable_new_device_login_otp { get; set; }
        public long gender { get; set; }
        public long smid_status { get; set; }
        public long ba_check_status { get; set; }
        public bool feed_private { get; set; }
        public long delivery_address_id { get; set; }
        public long birth_timestamp { get; set; }
        public bool holiday_mode_on { get; set; }
        public long tos_accepted_time { get; set; }
        public AddressInfo address_info { get; set; }
    }

    public class SellerAddress
    {
        public long address_id { get; set; }
        public long user_id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string address { get; set; }
        public long status { get; set; }
        public long ctime { get; set; }
        public long mtime { get; set; }
        public string zip_code { get; set; }
        public long def_time { get; set; }
        public string full_address { get; set; }
        public string district { get; set; }
        public string town { get; set; }
        public long logistics_status { get; set; }
        public string icno { get; set; }
        public ExtInfo ext_info { get; set; }
    }

    public class ItemModel
    {
        public long model_id { get; set; }
        public long item_id { get; set; }
        public string name { get; set; }
        public string price { get; set; }
        public string currency { get; set; }
        public long stock { get; set; }
        public long status { get; set; }
        public string price_before_discount { get; set; }
        public long promotion_id { get; set; }
        public string rebate_price { get; set; }
        public long sold { get; set; }
        public long ctime { get; set; }
        public long mtime { get; set; }
        public string sku { get; set; }
    }

    public class Product
    {
        public string name { get; set; }
        public List<string> images { get; set; }
        public string branch { get; set; }
        public string brand { get; set; }
        public long cat_id { get; set; }
        public long cmt_count { get; set; }
        public long condition { get; set; }
        public long ctime { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public long estimated_days { get; set; }
        public bool is_pre_order { get; set; }
        public long item_id { get; set; }
        public long model_id { get; set; }
        public long liked_count { get; set; }
        public string price { get; set; }
        public string price_before_discount { get; set; }
        public long shop_id { get; set; }
        public long sold { get; set; }
        public long status { get; set; }
        public long stock { get; set; }
        public string sku { get; set; }
        public long snapshot_id { get; set; }
    }

    public class OrderItem
    {
        public long add_on_deal_id { get; set; }
        public long amount { get; set; }
        public long bundle_deal_id { get; set; }
        public string group_id { get; set; }
        public bool is_add_on_sub_item { get; set; }
        public bool is_wholesale { get; set; }
        public string item_price { get; set; }
        public long item_id { get; set; }
        public long model_id { get; set; }
        public string order_price { get; set; }
        public string price_before_bundle { get; set; }
        public long snapshot_id { get; set; }
        public long status { get; set; }
        public long shop_id { get; set; }
        public long order_item_id { get; set; }
        public bool is_virtual_sku { get; set; }
        public string comm_fee_rate { get; set; }
        public object bundle_deal { get; set; }
        public ItemModel item_model { get; set; }
        public List<object> bundle_deal_model { get; set; }
        public List<object> item_list { get; set; }
        public Product product { get; set; }
        public List<object> bundle_deal_product { get; set; }
        public long sub_type { get; set; }
    }

    public class Access
    {
        public long hide_likes { get; set; }
        public long wallet_setting { get; set; }
        public long seller_coin_setting { get; set; }
        public long seller_wholesale_setting { get; set; }
        public long seller_ads_setting { get; set; }
    }

    public class AddressInfo
    {
        public long mcount { get; set; }
        public long mcount_create_time { get; set; }
        public bool in_white_list { get; set; }
    }

    public class BuyerUser
    {
        public long delivery_succ_count { get; set; }
        public long hide_likes { get; set; }
        public long user_id { get; set; }
        public string user_name { get; set; }
        public string portrait { get; set; }
        public bool phone_public { get; set; }
        public string language { get; set; }
        public long cb_option { get; set; }
        public ExtInfo ext_info { get; set; }
        public long delivery_order_count { get; set; }
        public long buyer_rating { get; set; }
        public List<long> rating_count { get; set; }
        public long rating_star { get; set; }
        public long shop_id { get; set; }
        public bool followed { get; set; }
    }

    public class Data
    {
        public string TenShop { get; set; }
        public string MaVanDon { get; set; }


        public long shop_id { get; set; }
        public long user_id { get; set; }
        public long order_id { get; set; }
        public string order_sn { get; set; }
        public string total_price { get; set; }
        public string actual_price { get; set; }
        public string paid_amount { get; set; }
        public string currency { get; set; }
        public long shipping_method { get; set; }
        public string shipping_address { get; set; }
        public string shipping_fee { get; set; }
        public string shipping_traceno { get; set; }
        public string actual_carrier { get; set; }
        public long order_type { get; set; }
        public long payment_method { get; set; }
        public string remark { get; set; }
        public long status { get; set; }
        public long create_time { get; set; }
        public long delivery_time { get; set; }
        public long complete_time { get; set; }
        public long checkout_id { get; set; }
        public long status_ext { get; set; }
        public long logistics_status { get; set; }
        public long pickup_time { get; set; }
        public long shipping_confirm_time { get; set; }
        public long list_type { get; set; }
        public long carrier_shipping_fee { get; set; }
        public string actual_shipping_fee { get; set; }
        public long add_on_deal_id { get; set; }
        public long arrange_pickup_by_date { get; set; }
        public long auto_cancel_3pl_ack_date { get; set; }
        public long auto_cancel_arrange_ship_date { get; set; }
        public long buyer_is_rated { get; set; }
        public long buyer_last_change_address_time { get; set; }
        public string buyer_txn_fee { get; set; }
        public long buyer_cancel_reason { get; set; }
        public long cancel_reason_ext { get; set; }
        public long cancel_userid { get; set; }
        public CardTxnFeeInfo card_txn_fee_info { get; set; }
        public string coin_offset { get; set; }
        public string comm_fee { get; set; }
        public string credit_card_promotion_discount { get; set; }
        public DropshippingInfo dropshipping_info { get; set; }
        public long escrow_release_time { get; set; }
        public long first_item_count { get; set; }
        public bool first_item_is_wholesale { get; set; }
        public string first_item_model { get; set; }
        public string first_item_name { get; set; }
        public bool first_item_return { get; set; }
        public bool instant_buyercancel_toship { get; set; }
        public bool is_buyercancel_toship { get; set; }
        public long item_count { get; set; }
        public long logid { get; set; }
        public long logistics_channel { get; set; }
        public string logistics_extra_data { get; set; }
        public long logistics_flag { get; set; }
        public string origin_shipping_fee { get; set; }
        public long payby_date { get; set; }
        public long pickup_attempts { get; set; }
        public long pickup_cutoff_time { get; set; }
        public string price_before_discount { get; set; }
        public long rate_by_date { get; set; }
        public long ratecancel_by_date { get; set; }
        public SellerAddress seller_address { get; set; }
        public long seller_address_id { get; set; }
        public long seller_due_date { get; set; }
        public long ship_by_date { get; set; }
        public string shipping_proof { get; set; }
        public long shipping_proof_status { get; set; }
        public string tax_amount { get; set; }
        public long used_voucher { get; set; }
        public bool voucher_absorbed_by_seller { get; set; }
        public string voucher_code { get; set; }
        public string voucher_price { get; set; }
        public string trans_detail_shipping_fee { get; set; }
        public string wallet_discount { get; set; }
        public string cancel_by { get; set; }
        public long cancel_time { get; set; }
        public string forder_id { get; set; }
        public long parcel_no { get; set; }
        public long fulfillment_channel_id { get; set; }
        public string fulfillment_carrier_name { get; set; }
        public long fulfillment_shipping_method { get; set; }
        public long masking_channel_id { get; set; }
        public string masking_carrier_name { get; set; }
        public long checkout_shipping_method { get; set; }
        public string checkout_carrier_name { get; set; }
        public long checkout_channel_id { get; set; }
        public long shipping_fee_discount { get; set; }
        public string buyer_paid_amount { get; set; }
        public string coins_cash_by_voucher { get; set; }
        public string seller_service_fee { get; set; }
        public string buyer_address_name { get; set; }
        public string buyer_address_phone { get; set; }
        public object cancellation_end_date { get; set; }
        public string coin_used { get; set; }
        public long coins_by_voucher { get; set; }
        public bool is_request_cancellation { get; set; }
        public bool pay_by_credit_card { get; set; }
        public string note { get; set; }
        public long note_mtime { get; set; }
        public long return_id { get; set; }
        public long express_channel { get; set; }
        public bool order_ratable { get; set; }
        public List<OrderItem> order_items { get; set; }
        public BuyerUser buyer_user { get; set; }
        public bool shipment_config { get; set; }
        public bool fulfill_by_shopee { get; set; }
        public object preferred_delivery_timeslot { get; set; }
        public string buyer_cpf_id { get; set; }
        public string channel_hotline { get; set; }
        public bool is_affiliated_shop_order { get; set; }
        public object order_item_price_infos { get; set; }
        public long invoice_data_status { get; set; }
    }

    public class Root
    {
        public long code { get; set; }
        public Data data { get; set; }
        public string message { get; set; }
        public string user_message { get; set; }
    }
}

namespace SDK.Shopee.Root_TrackInfo
{
    public class Item
    {
        public object item_id { get; set; }
        public object model_id { get; set; }
        public long item_price { get; set; }
        public long quantity { get; set; }
        public long virtual_item_id { get; set; }
        public object order_item_id { get; set; }
        public long virtual_model_id { get; set; }
        public string group_id { get; set; }
    }

    public class TrackingInfo
    {
        public long id { get; set; }
        public long flag { get; set; }
        public string description { get; set; }
        public long ctime { get; set; }
        public long type { get; set; }
        public long status { get; set; }
        public long logistics_status { get; set; }
        public long logid { get; set; }
        public long system_time { get; set; }
    }

    public class PackageList
    {
        public string package_number { get; set; }
        public long parcel_no { get; set; }
        public string third_party_tn { get; set; }
        public long total_price { get; set; }
        public long shipping_fee { get; set; }
        public long shipping_fee_discount { get; set; }
        public long status { get; set; }
        public long package_logistics_status { get; set; }
        public List<Item> items { get; set; }
        public List<TrackingInfo> tracking_info { get; set; }
        public string first_mile_tn { get; set; }
        public string consignment_no { get; set; }
        public long channel_id { get; set; }
        public string carrier_name { get; set; }
        public long logistics_flag { get; set; }
        public long forder_status { get; set; }
        public long package_status { get; set; }
        public long shipping_method { get; set; }
        public string seller_remark { get; set; }
        public long shipping_status { get; set; }
        public long allocating_status { get; set; }
        public long masking_channel_id { get; set; }
        public string masking_carrier_name { get; set; }
        public long fulfillment_channel_id { get; set; }
        public string fulfillment_carrier_name { get; set; }
        public long fulfillment_shipping_method { get; set; }
        public long auto_cancel_layer2 { get; set; }
        public string checkout_carrier_name { get; set; }
        public long checkout_channel_id { get; set; }
        public string plp_number { get; set; }
        public long code { get; set; }
        public string transify_key { get; set; }
    }

    public class OrderAction
    {
        public long can_unsplit { get; set; }
        public long can_split { get; set; }
    }

    public class OrderInfo
    {
        public long order_id { get; set; }
        public long split_up { get; set; }
        public long order_status { get; set; }
        public List<PackageList> package_list { get; set; }
        public OrderAction order_action { get; set; }
    }

    public class Data
    {
        public OrderInfo order_info { get; set; }
    }

    public class Root
    {
        public long code { get; set; }
        public Data data { get; set; }
        public string message { get; set; }
        public string user_message { get; set; }
    }


}

namespace SDK.Shopee.Root_PickUpInfo
{
    public class PollingMode
    {
        public long init_longerval_time { get; set; }
        public long increase_longerval_time { get; set; }
        public long total_time { get; set; }
    }

    public class Data
    {
        public long order_id { get; set; }
        public long channel_id { get; set; }
        public string selected_pickup_time_slot { get; set; }
        public string selected_pickup_time_slot_time { get; set; }
        public string remark { get; set; }
        public bool exists { get; set; }
        public bool editable { get; set; }
        public long pickup_time { get; set; }
        public object driver { get; set; }
        public string driver_name { get; set; }
        public string consignment_no { get; set; }
        public long seller_address_id { get; set; }
        public long pickup_address_id { get; set; }
        public string seller_real_name { get; set; }
        public PollingMode polling_mode { get; set; }
    }

    public class Root
    {
        public long code { get; set; }
        public Data data { get; set; }
        public string message { get; set; }
        public string user_message { get; set; }
    }


}

namespace SDK.Shopee.Root_PickUpAddress
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class List
    {
        public long address_id { get; set; }
        public long user_id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string address { get; set; }
        public long status { get; set; }
        public string zip_code { get; set; }
        public string district { get; set; }
        public string town { get; set; }
        public long can_pickup_code { get; set; }
        public string geo_info { get; set; }
    }

    public class Data
    {
        public List<List> list { get; set; }
    }

    public class Root
    {
        public long code { get; set; }
        public Data data { get; set; }
        public string message { get; set; }
        public string user_message { get; set; }
    }


}

namespace SDK.Shopee.Root_PickUpTime
{
    public class TimeSlot
    {
        public string date { get; set; }
        public bool volume_limit { get; set; }
        public long value { get; set; }
        public List<object> slots { get; set; }
    }

    public class Data
    {
        //thêm vào
        public string DonViGiaohang { get; set; }
        public List<TimeSlot> time_slots { get; set; }
    }

    public class Root
    {
        /// <summary>
        /// ref....
        /// </summary>
        public Root_PostInitStatus.Root PostInit { get; set; }
        public long code { get; set; }
        public Data data { get; set; } = new Data();
        public string message { get; set; }
        public string user_message { get; set; }
    }
}

/// <summary>
/// Kiểm tra tình trạng đơn hàng khi khởi tạo /xác nhận
/// </summary>
namespace SDK.Shopee.Root_CheckStatus
{
    public class OrdersStatus
    {
        public long order_id { get; set; }
        public string package_number { get; set; }
        public long shipping_status { get; set; }
        public string third_party_tn { get; set; }
        public string booking_number { get; set; }
        public string message { get; set; }
        public bool can_print_waybill { get; set; }
        public bool can_print_normal_waybill { get; set; }
        public bool can_print_c2c_waybill { get; set; }
        public bool can_print_c2c_waybill_by_awb_tool { get; set; }
        public bool can_print_job_tickets { get; set; }
        public long code { get; set; }
        public string transify_key { get; set; }
        public object init_status { get; set; }
    }

    public class PollingMode
    {
        public long init_interval_time { get; set; }
        public long increase_interval_time { get; set; }
        public long total_time { get; set; }
    }

    public class Data
    {
        public long total_num { get; set; }
        public long success_num { get; set; }
        public List<OrdersStatus> orders_status { get; set; }
        public PollingMode polling_mode { get; set; }
    }

    public class Root
    {
        public long code { get; set; }
        public Data data { get; set; }
        public string message { get; set; }
        public string user_message { get; set; }
    }
}

/// <summary>
/// template post to get return order status
/// </summary>
namespace SDK.Shopee.Root_PostGetStatus
{
    public class OrderList
    {
        public long order_id { get; set; }
        public string package_number { get; set; }
        public long channel_id { get; set; }
        public string region_id { get; set; }
        public long shop_id { get; set; }
    }

    public class Root
    {
        public List<OrderList> order_list { get; set; }
    }
}

/// <summary>
/// template post to process init order
/// </summary>
namespace SDK.Shopee.Root_PostInitStatus
{
    public class Root
    {
        public long order_id { get; set; }
        public string package_number { get; set; }
        public string remark { get; set; }
        public long pickup_time { get; set; }
        public long pickup_address_id { get; set; }
        public string seller_real_name { get; set; }
        public string shipping_mode { get; set; }
    }
}

namespace SDK.Shopee.Root_MetaShopInfo
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ShippingMethods
    {
    }

    public class ShopList
    {
        public int shop_id { get; set; }
        public string region_id { get; set; }
        public int all { get; set; }
        public int to_process { get; set; }
        public int processed { get; set; }
        public int in_transit { get; set; }
        public int allocating { get; set; }
        public ShippingMethods shipping_methods { get; set; }
    }

    public class Data
    {
        public List<ShopList> shop_list { get; set; }
    }

    public class Root
    {
        public int code { get; set; }
        public Data data { get; set; }
        public string message { get; set; }
        public string user_message { get; set; }
    }
}

namespace SDK.Shopee.Root_MetaShopPost
{
    public class ShopList
    {
        public string region_id { get; set; }
        public long shop_id { get; set; }
    }

    public class Root
    {
        public bool is_mass_ship { get; set; }
        public List<ShopList> shop_list { get; set; } = new List<ShopList>();
    }
}

namespace SDK.Shopee.Root_ShipmentList
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ItemMaxDimension
    {
        public long height { get; set; }
        public long length { get; set; }
        public long unit { get; set; }
        public long width { get; set; }
    }

    public class Limits
    {
        public long channel_id { get; set; }
        public long item_max_weight { get; set; }
        public double item_min_weight { get; set; }
        public long order_max_weight { get; set; }
        public double order_min_weight { get; set; }
        public long checkout_max_weight { get; set; }
        public double checkout_min_weight { get; set; }
        public long dimension_sum { get; set; }
        public ItemMaxDimension item_max_dimension { get; set; }
        public long item_max_volume { get; set; }
        public long item_min_volume { get; set; }
    }

    public class Datum
    {
        public long channel_id { get; set; }
        public string name { get; set; }
        public string display_name { get; set; }
        public bool is_mask_channel { get; set; }
        public string name_key { get; set; }
        public string flag { get; set; }
        public bool enabled { get; set; }
        public long lmtn_necessary { get; set; }
        public long parent_channel_id { get; set; }
        public long category_id { get; set; }
        public Limits limits { get; set; }
        public long pickup_same_day_cutoff_hour { get; set; }
        public long priority { get; set; }
    }

    public class Root
    {
        public long code { get; set; }
        public List<Datum> data { get; set; }
        public string message { get; set; }
        public string user_message { get; set; }
    }


}