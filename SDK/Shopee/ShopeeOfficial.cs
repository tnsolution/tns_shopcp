﻿using Newtonsoft.Json;
using System;
using System.Security.Cryptography;
using System.Text;
using xNet;

namespace SDK.Shopee
{
    public partial class ShopeeClient
    {
        public static string CreateShopAuthorization()
        {
            DateTime start = DateTime.Now;
            long timest = ((DateTimeOffset)start).ToUnixTimeSeconds();
            string host = "https://partner.shopeemobile.com";
            string path = "/api/v2/shop/auth_partner";
            string redirectUrl = "https://google.com";
            long partner_id = 2002266;
            string tmp_partner_key = "36dfa98a894559881e1a0b845ee1452313b6d361b67fa3981d0cace65d688973";
            string tmp_base_string = string.Format("{0}{1}{2}", partner_id, path, timest);
            byte[] partner_key = Encoding.UTF8.GetBytes(tmp_partner_key);
            byte[] base_string = Encoding.UTF8.GetBytes(tmp_base_string);
            var hash = new HMACSHA256(partner_key);
            byte[] tmp_sign = hash.ComputeHash(base_string);
            string sign = BitConverter.ToString(tmp_sign).Replace("-", "").ToLower();
            string url = string.Format(host + path + "?partner_id={0}&timestamp={1}&sign={2}&redirect={3}", partner_id, timest, sign, redirectUrl);
            return url;
        }

        public static string GetToken()
        {
            DateTime start = DateTime.Now;
            long timest = ((DateTimeOffset)start).ToUnixTimeSeconds();
            string host = "https://partner.shopeemobile.com";
            string path = "/api/v2/auth/token/get";
            long partner_id = 2002266;
            string tmp_partner_key = "36dfa98a894559881e1a0b845ee1452313b6d361b67fa3981d0cace65d688973";
            string tmp_base_string = string.Format("{0}{1}{2}", partner_id, path, timest);
            byte[] partner_key = Encoding.UTF8.GetBytes(tmp_partner_key);
            byte[] base_string = Encoding.UTF8.GetBytes(tmp_base_string);
            var hash = new HMACSHA256(partner_key);
            byte[] tmp_sign = hash.ComputeHash(base_string);
            string sign = BitConverter.ToString(tmp_sign).Replace("-", "").ToLower();
            string url = string.Format(host + path);

            HttpResponse zRes;
            var zWeb = new HttpRequest();
            zWeb.AddUrlParam("sign", sign)
                    .AddUrlParam("partner_id", partner_id)
                    .AddUrlParam("timestamp", timest);

            var zReqParam = JsonConvert.SerializeObject(new
            {
                code = "1c863ac6a861ec2e0b450e82f54dfbe9",
                partner_id = 2002266,
                shop_id = 161761730,
                main_account = "",
            });

            try
            {


                zRes = zWeb.Post(url, zReqParam, "application/json");
                var zResult = zRes.ToString();
                return zResult;
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
                var zResult = zWeb.Response.ToString();
                return string.Empty;
            }
        }

        public static string GetTokenRefesh()
        {
            DateTime start = DateTime.Now;
            long timest = ((DateTimeOffset)start).ToUnixTimeSeconds();
            string host = "https://partner.shopeemobile.com";
            string path = "/api/v2/auth/access_token/get";
            long partner_id = 2002266;
            string tmp_partner_key = "36dfa98a894559881e1a0b845ee1452313b6d361b67fa3981d0cace65d688973";
            string tmp_base_string = string.Format("{0}{1}{2}", partner_id, path, timest);
            byte[] partner_key = Encoding.UTF8.GetBytes(tmp_partner_key);
            byte[] base_string = Encoding.UTF8.GetBytes(tmp_base_string);
            var hash = new HMACSHA256(partner_key);
            byte[] tmp_sign = hash.ComputeHash(base_string);
            string sign = BitConverter.ToString(tmp_sign).Replace("-", "").ToLower();
            string url = string.Format(host + path);

            HttpResponse zRes;
            var zWeb = new HttpRequest();
            zWeb.AddUrlParam("sign", sign)
                    .AddUrlParam("partner_id", partner_id)
                    .AddUrlParam("timestamp", timest);

            var zReqParam = JsonConvert.SerializeObject(new
            {
                refresh_token = "10f71aa0c77eda901bf3de6312348d0b",
                partner_id = 2002266,
                shop_id = 161761730,
                main_account = "",
            });

            try
            {


                zRes = zWeb.Post(url, zReqParam, "application/json");
                var zResult = JsonConvert.DeserializeObject<dynamic>(zRes.ToString());
                return zResult;
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
                var zResult = zWeb.Response.ToString();
                return string.Empty;
            }
        }
    }
}

