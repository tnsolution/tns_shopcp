﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using xNet;
using XHttpRequest = xNet.HttpRequest;
namespace SDK.Lazada
{
    public class LazadaClient
    {
        public string Message { get; set; } = "";
        static string _ShopeeLog = "wwwroot/LazadaLog/";
        static string _UserName = "";
        static string _Password = "";
        static string _Cookie = "";
        XHttpRequest _Request;

        public LazadaClient()
        {
            _Request = new();
            _Request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.44");
            _Request.AddHeader("Accept", "application/json");
            _Request.AddHeader("Accept-Language", "en-US,en;q=0.9,vi;q=0.8");
            _Request.AddHeader("Origin", "https://sellercenter.lazada.vn");
            _Request.AddHeader("Referer", "https://sellercenter.lazada.vn/");
            _Request.AddHeader("sec-ch-ua", "\"Chromium\";v=\"94\", \"Microsoft Edge\";v=\"94\", \"; Not A Brand\";v=\"99\"");
            _Request.AddHeader("sec-ch-ua-mobile", "?0");
            _Request.AddHeader("sec-ch-ua-platform", "\"Windows\"");
            _Request.AddHeader("Sec-Fetch-Site", "same-site");
            _Request.AddHeader("Sec-Fetch-Mode", "cors");
            _Request.AddHeader("Sec-Fetch-Dest", "empty");
            _Request.AddHeader("ua", "140#BVQDPR3azzPsLQo22iFbA6Soh9hjS67FhKjjqoVB7JkNTPMa1OsIPAWlqgosx8pOxE0jbZAfEsp2zbkM3ROtcIXHv3hqzzn16JBUQT2zzX8jIHoBUbzx2DD3VthqzFKh2EWzltQrxPrbL8/qlbrD0pI42WbhzFSV6FrR4njIPtrPHpcozbxPJiKe7MO3aoJ4AWBPSkpWwOfTPRnG1pcyGUf8sxTSWIcLUpKhzJEX2c+IUmpV39Icof9Fwk7Oz5ya51elDEdJlCs9UDLW5zxS+f6MnFWimaYi4DT+L8gxmIkR2ZR9uNUSX/7Sz34mMKjvtvWmJk9pVGtR5ejojCXeVOLlI+tYKXpzLdKs5iaJODVqvxVR4gnSbKLUFQrBVfVsx3PFSVZzHyVVfB8tN8Wc/fMG6g+CxS7MtA4Vxtm16vH7p2TKI/rtDwQbXkO/tomUkp/d00DibJQg4C4/pVGfq0i3rI2kOUu97HblS4anjs3KhLKF2vgQKY6rxzB3pxjB7M+LpPQIFgD7O0zSkNa83rp+wGrEH+fQegPAABxSD204Gy/P/+l+dlQ0WjyXCRQwUUvdokGP4Mn4tzly1aEFex8Lmr5vOMRB6cZicp34I0NPnGSMi4XjIYA5Ku4Ot3M+0L//AixCHZ3oxl3XUdXW4IrcEb4KCcccMy0WUAZoS/14W907No9JyHZpF7mRkPO8zrLj3B0h6mOyp35CXftJiqdcWwstU3sAk5mdKJRw+ZUxG1J+4lADbxNPbPpcF3agGCaYPbsrXvcyevFDYI8rkUOzQkJ3yzdm0aDm1W+yPUGadydJcX5870xpOyol1e8APIx1cqrDuyhhEiLIYq6TgRlJ3G8Vy8q9SvWgTJhs1/zmOsi/XBxCWMGgSXaU1y4g+fEXAQXhFsfEai21e274JLlZ46PTbzA8idD1KzbN2l3P0USPLrZBbIQdnjYMMjvQGaU/A8P1RpUTMr9i/nvz66r7csvlhFU4M09jjwJFF0EWZcjZzN8Iw5mQhVfaEt3YbDQCQTqXnvWQRBdvThJq0XJvao2961uLlMyvbikcaaBGSGHD9diHyLmwdQBZWLM1r65wa/66KMM//HHA/cYV9syD8hs/h6k/XrGsI2uAbMlRsacsTWnJuy+=");
            _Request.AddHeader("x-umidToken", "T2gA12ZPJk3gD4oNgnc3-ESIAPJUqSNK9MsK2t3Ng41VQukTj7T498dcdO9OhFZqjbE=");

            string unixTimestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString();
            string unixSecon = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
            string random32 = TN_Utils.RandomString(32);
            string guid = Guid.NewGuid().ToString();

            _Request.Cookies = new CookieDictionary();
            //_Request.Cookies.Add("lzd_cid", "87772510-83dc-4673-d1be-d6cd11df8ddd");
            //_Request.Cookies.Add("cna", "94yqGdXifWoCARsD4YWe4BR2");
            //_Request.Cookies.Add("miidlaz", "miidgg5uub1fe117ntnsqqb");
            //_Request.Cookies.Add("_lang", "vi_VN");
            //_Request.Cookies.Add("hng", "VN|vi|VND|704");
            //_Request.Cookies.Add("t_fv", "1634823795");
            //_Request.Cookies.Add("t_uid", "WETIXOEAWONVJNQFHMTXNXQYTBFZAODJ");
            //_Request.Cookies.Add("_gcl_au", "1.1.163878058.1632794499");
            //_Request.Cookies.Add("xlly_s", "1");
            //_Request.Cookies.Add("lzd_click_id", "clkgg5t0c1fhq32c9b564h");
            //_Request.Cookies.Add("_uetsid", "bd32dca02b4211ec898aa7942659912b");
            //_Request.Cookies.Add("_uetvid", "10a53c20e2ee11ebb1f50f72106707d4");
            //_Request.Cookies.Add("isg", "BPPzqM8Klv9ep1sil9WzQUBTgvcdKIfqBFgTJ6WTaJJopBtGG_mUO8N1WsxKLN_i");
            //_Request.Cookies.Add("tfstk", "cB6VB0xhq-e4-Lv_9K9aCAGDd5FAaKWGd7-9isvkthqjZyOX8sfiW3V3ra7K4VAc.");
            //_Request.Cookies.Add("l", "eBP6MoW4j7EfV0Y1BO5aKurza77tVQOfcIVzaNbMiInca6MlZemVPNCLVfRyodtjQt5ApCxrWpLgQdUp8zUU-AkDBeYQ_Xv_JUJw-e1..");
            //_Request.Cookies.Add("t_sid", "ZPYdg2gNSeYcZxSmCgc39FaBoFTrAbJr");
            //_Request.Cookies.Add("utm_channel", "NA");
            //_Request.Cookies.Add("_m_h5_tk", "e24de2623deeef604919de2f8fb1e0cb_1634099364893");
            //_Request.Cookies.Add("_m_h5_tk_enc", "0d46f58ee51caf32cfe0e5503615ea71");
            //_Request.Cookies.Add("gmp_sid", "-1");
            //_Request.Cookies.Add("JSID", "1edbe6b15e1f08d1fec932e7fd42ea2f");
            //_Request.Cookies.Add("CSRFT", "e4636fef13b11");
            //_Request.Cookies.Add("TID", "1c978606fb15bdd3f987322888e8901e");
            //_Request.Cookies.Add("utm_origin", "https://r.srvtrck.com/");
        }
        public Root_Account.Root Login()
        {
            var zRoot = new Root_Account.Root();
            try
            {
                DateTime start = DateTime.Now;
                long timest = ((DateTimeOffset)start).ToUnixTimeSeconds();
                string host = "https://acs-m.lazada.vn";
                string path = "/h5/mtop.global.seller.login.loginbypassword/1.0/";
                string tmp_partner_key = "4272";
                string tmp_base_string = string.Format("{0}{1}", tmp_partner_key, timest);
                byte[] partner_key = Encoding.UTF8.GetBytes(tmp_partner_key);
                byte[] base_string = Encoding.UTF8.GetBytes(tmp_base_string);
                var hash = new HMACSHA256(partner_key);
                byte[] tmp_sign = hash.ComputeHash(base_string);
                string sign = BitConverter.ToString(tmp_sign).Replace("-", "").ToLower();


                var zResponse = _Request.Get("https://sellercenter.lazada.vn/apps/seller/login");
                var zResult = zResponse.ToString();
                Message = zResult;

                _Request
                 .AddUrlParam("jsv", "2.6.1")
                 .AddUrlParam("appKey", "4272")
                 .AddUrlParam("t", timest.ToString())
                 .AddUrlParam("sign", sign)
                 .AddUrlParam("v", "1.0")
                 .AddUrlParam("timeout", "30000")
                 .AddUrlParam("H5Request", "true")
                 .AddUrlParam("url", "mtop.global.seller.login.loginByPassword")
                 .AddUrlParam("type", "originaljson")
                 .AddUrlParam("method", "POST")
                 .AddUrlParam("api", "mtop.global.seller.login.loginByPassword")
                 .AddUrlParam("dataType", "json")
                 .AddUrlParam("valueType", "original")
                 .AddUrlParam("x-i18n-regionID", "LAZADA_VN");

                _Request.AddParam("data",
                    JsonConvert.SerializeObject(new
                    {
                        _timezone = -7,
                        redirect_url = "https://sellercenter.lazada.vn/apps/seller/",
                        platform = "PC",
                        countryCallingCode = "84",
                        phoneNumber = "0378404149",
                        password = "123456Aa@",
                        riskInfo = ""
                    }));

                zResponse = _Request.Post("https://acs-m.lazada.vn/h5/mtop.global.seller.login.loginbypassword/1.0/");
                zResult = zResponse.ToString();
                Message = zResult;
                //var zLogged = JsonConvert.DeserializeObject<Root_Account.Root>(zResult);

            }
            catch (Exception ex)
            {
                Message = "Error" + _Request.Response.ToString();
            }

            return zRoot;
        }
    }
}
