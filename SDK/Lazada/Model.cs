﻿using System.Collections.Generic;

namespace SDK.Lazada.Root_Account
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ActionParameters
    {
        public string loginJumpUrl { get; set; }
        public long loginUserId { get; set; }
    }

    public class CookieList
    {
        public string domain { get; set; }
        public bool httpOnly { get; set; }
        public long maxAge { get; set; }
        public string name { get; set; }
        public string path { get; set; }
        public bool secure { get; set; }
        public string value { get; set; }
        public long version { get; set; }
    }

    public class LoginSession
    {
        public List<CookieList> cookieList { get; set; }
        public List<string> cookies { get; set; }
        public long sessionExpiredTime { get; set; }
        public string sid { get; set; }
        public long userId { get; set; }
    }

    public class LoginUserDTO
    {
        public string email { get; set; }
        public string phone { get; set; }
        public long userId { get; set; }
        public string userName { get; set; }
        public string userType { get; set; }
    }

    public class Data
    {
        public ActionParameters actionParameters { get; set; }
        public LoginSession loginSession { get; set; }
        public LoginUserDTO loginUserDTO { get; set; }
        public bool success { get; set; }
    }

    public class Root
    {
        public string api { get; set; }
        public Data data { get; set; }
        public List<string> ret { get; set; }
        public string v { get; set; }
    }


}
