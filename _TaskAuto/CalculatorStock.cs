﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ShopCP
{
    public class CalculatorStock : ICalculatorStock
    {
        List<QtyStock> ReportStock(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            var zTable = new DataTable();
            string zSQL = @"SAL_QtyStockMonth_V2";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            var zList = new List<QtyStock>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new QtyStock()
                {
                    ProductKey = r["ProductKey"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    SKU = r["SKU"].ToString(),
                    BeginQty = r["BeginQty"].ToFloat(),
                    BeginMoney = r["BeginMoney"].ToDouble(),
                    InQty = r["InQty"].ToFloat(),
                    InMoney = r["InMoney"].ToDouble(),
                    OutQty = r["OutQty"].ToFloat(),
                    OutMoney = r["OutMoney"].ToDouble(),
                    EndQty = r["EndQty"].ToFloat(),
                    EndMoney = r["EndMoney"].ToDouble(),
                });
            }
            return zList;
        }

        public void ProcessStock()
        {
            DateTime date = DateTime.Now.AddDays(-1);
            DateTime fromdate = new DateTime(date.Year, date.Month, 1, 0, 0, 0);
            DateTime todate = fromdate.AddMonths(1).AddDays(-1);
            todate = new DateTime(todate.Year, todate.Month, todate.Day, 23, 59, 59);

            string CloseMonth = DateTime.Now.ToString("dd/MM");
            string ParterNumber = TN_Helper.PartnerNumber;

            var ListData = ReportStock(ParterNumber, fromdate, todate, out _);

            StringBuilder zSb = new StringBuilder();
            foreach (var rec in ListData)
            {
                zSb.AppendLine(@"
INSERT INTO SAL_CloseMonth (
ProductKey , ProductID , CloseMonth, [Year], PartnerNumber , BeginQty , BeginMoney , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName )
VALUES (
@ProductKey , @ProductID , @CloseMonth , @Year, @PartnerNumber , @BeginQty , @BeginMoney , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName )");

                zSb.Replace("@ProductKey", "'" + rec.ProductKey + "'");
                zSb.Replace("@ProductID", "'" + rec.SKU + "'");
                zSb.Replace("@CloseMonth", "'" + CloseMonth + "'");
                zSb.Replace("@Year", "'" + todate.Year.ToString() + "'");
                zSb.Replace("@PartnerNumber", "'" + ParterNumber + "'");
                zSb.Replace("@BeginQty", "'" + rec.EndQty.ToString() + "'");
                zSb.Replace("@BeginMoney", "'" + rec.EndMoney.ToString() + "'");
                zSb.Replace("@RecordStatus", "0");
                zSb.Replace("@CreatedBy", "'Auto'");
                zSb.Replace("@CreatedName", "'Auto'");
                zSb.Replace("@ModifiedBy", "'Auto'");
                zSb.Replace("@ModifiedName", "'Auto'");
            }

            try
            {
                string zConnectionString = TN_Helper.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSb.ToString(), zConnect);
                zCommand.ExecuteNonQuery();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception)
            {

            }
            finally
            {

            }
        }
    }
}
