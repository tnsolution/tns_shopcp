﻿using Newtonsoft.Json;
using SDK.Shopee;
using System;
using System.Linq;

namespace ShopCP
{
    public class CheckShopeeOrder : ICheckShopeeOrder
    {

        public void CheckOrder()
        {
            var zInfo = new Link3rt_Info(TN_Helper.PartnerNumber, "SHOPEE");
            string Username = zInfo.Link3rt.UserName;
            string Password = zInfo.Link3rt.Password;
            string Cook = zInfo.Link3rt.Cookies;

            string gsession = Guid.NewGuid().ToString();
            ShopeeClient zClient = new(gsession, Cook);
            var zRoot = zClient.LoginOTP(Username, Password);
            if (zRoot.data == null)
            {
                //
            }

            // LẤY HẾT CÁC ĐƠN HÀNG ĐANG GIAO - CHỜ XÁC NHẬN HOÀN THÀNH
            var zList = Order_Data.List(TN_Helper.PartnerNumber, 1, DateTime.MinValue, DateTime.MinValue, string.Empty, out _);
            zList = zList.Where(s => s.S_OrderID != string.Empty).ToList();
            var total = 0;
            foreach (var rec in zList)
            {
                var item = JsonConvert.DeserializeObject<SDK.Shopee.Root_OrderInfo.Data>(rec.S_JSON);
                if (item != null)
                {
                    item = zClient.OrderInfo(item.order_id).data;
                    if (item.status == 4)
                    {
                        total++;

                        var OrderInfo = new Order_Info(rec.OrderKey);
                        OrderInfo.Order.Slug = 302;
                        OrderInfo.Close();
                        TN_Mics.Create_Receipt(OrderInfo.Order, item.complete_time, out _);
                    }
                }
            }
        }
    }
}
