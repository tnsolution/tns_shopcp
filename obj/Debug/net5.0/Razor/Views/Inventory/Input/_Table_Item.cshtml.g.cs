#pragma checksum "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "008e5312ed502bb96c176c3b4efd774444b640dc"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Inventory_Input__Table_Item), @"mvc.1.0.view", @"/Views/Inventory/Input/_Table_Item.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\tns_shopcp\Views\_ViewImports.cshtml"
using ShopCP;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\tns_shopcp\Views\_ViewImports.cshtml"
using ShopCP.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"008e5312ed502bb96c176c3b4efd774444b640dc", @"/Views/Inventory/Input/_Table_Item.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"52d1388f9c3eeec799e6da174b162d620cca532c", @"/Views/_ViewImports.cshtml")]
    public class Views_Inventory_Input__Table_Item : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Purchase_Model>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
  
    Layout = null;
    if (Model.ListItem.Count > 0)
    {
        foreach (var rec in Model.ListItem)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"            <div class=""hvr-border-fade w-100"" style=""cursor:pointer"">
                <div class=""d-flex m-2"">
                    <div class=""align-self-center mr-2"" style=""width:70px"">
                        <button type=""button"" class=""btn btn-danger h-100""");
            BeginWriteAttribute("onclick", " onclick=\"", 412, "\"", 448, 3);
            WriteAttributeValue("", 422, "removeCart(\'", 422, 12, true);
#nullable restore
#line 12 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
WriteAttributeValue("", 434, rec.ItemKey, 434, 12, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 446, "\')", 446, 2, true);
            EndWriteAttribute();
            WriteLiteral(">\r\n                            <i class=\"fas fa-times\"></i>\r\n                        </button>\r\n                        <div class=\"btn-group-vertical h-100\">\r\n                            <button type=\"button\" class=\"btn btn-default btn-xs\"");
            BeginWriteAttribute("onclick", " onclick=\"", 689, "\"", 722, 3);
            WriteAttributeValue("", 699, "addCart(\'", 699, 9, true);
#nullable restore
#line 16 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
WriteAttributeValue("", 708, rec.ItemKey, 708, 12, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 720, "\')", 720, 2, true);
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <i class=\"fas fa-chevron-up\"></i>\r\n                            </button>\r\n                            <button type=\"button\" class=\"btn btn-default btn-xs\"");
            BeginWriteAttribute("onclick", " onclick=\"", 912, "\"", 948, 3);
            WriteAttributeValue("", 922, "deleteCart(\'", 922, 12, true);
#nullable restore
#line 19 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
WriteAttributeValue("", 934, rec.ItemKey, 934, 12, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 946, "\')", 946, 2, true);
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <i class=\"fas fa-chevron-down\"></i>\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                    <div");
            BeginWriteAttribute("class", " class=\"", 1144, "\"", 1152, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n                        <img class=\"imgShopee\"");
            BeginWriteAttribute("src", " src=\"", 1202, "\"", 1235, 1);
#nullable restore
#line 25 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
WriteAttributeValue("", 1208, Url.Content(rec.ItemPhoto), 1208, 27, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n                    </div>\r\n                    <div class=\"p-2 flex-grow-1\" style=\"flex: 0 0 100px;\">\r\n                        <b>");
#nullable restore
#line 28 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
                      Write(rec.ItemQty);

#line default
#line hidden
#nullable disable
            WriteLiteral(". X </b>\r\n                        ");
#nullable restore
#line 29 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
                   Write(rec.ItemName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                    <div class=\"align-self-center\">\r\n                        <div class=\"input-group\">\r\n                            <input type=\"text\" class=\"form-control number\"");
            BeginWriteAttribute("name", " name=\"", 1639, "\"", 1646, 0);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 1647, "\"", 1669, 1);
#nullable restore
#line 33 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
WriteAttributeValue("", 1655, rec.ItemPrice, 1655, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" style=\"width:120px\" />\r\n                            <span class=\"input-group-text number\" style=\"width:110px; font-size:0.9rem\">\r\n                                ");
#nullable restore
#line 35 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
                           Write(rec.ItemMoney);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n");
#nullable restore
#line 41 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
        }
    }
    else
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"alert alert-warning\">\r\n            <strong>Chưa có mặt hàng nào !</strong>.\r\n        </div>\r\n");
#nullable restore
#line 48 "E:\tns_shopcp\Views\Inventory\Input\_Table_Item.cshtml"
    }

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Purchase_Model> Html { get; private set; }
    }
}
#pragma warning restore 1591
