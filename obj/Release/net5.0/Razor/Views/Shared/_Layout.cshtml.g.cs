#pragma checksum "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d05568ac0273df8239328fbc15b53db70ca59ef8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__Layout), @"mvc.1.0.view", @"/Views/Shared/_Layout.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\_Project\_Gits\tns_shopee\Views\_ViewImports.cshtml"
using ShopCP;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\_Project\_Gits\tns_shopee\Views\_ViewImports.cshtml"
using ShopCP.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
using Newtonsoft.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d05568ac0273df8239328fbc15b53db70ca59ef8", @"/Views/Shared/_Layout.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"52d1388f9c3eeec799e6da174b162d620cca532c", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__Layout : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/themes/admin/img/avatar.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("Joseph Doe"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("rounded-circle"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-lock-picture", new global::Microsoft.AspNetCore.Html.HtmlString("img/!logged-user.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("object-fit:cover"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("35"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("menuitem"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Auth", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("loading-overlay-showing"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 4 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
  
    var logo = "~/themes/admin/img/logodefault.png";
    var username = "Quản trị viên";
    var user = HttpContextAccessor.HttpContext.Session.GetString("UserLogged");
    if (user != null)
    {
        username = JsonConvert.DeserializeObject<User_Model>(user).UserName;
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("<!DOCTYPE html>\r\n<html class=\"fixed sidebar-light\">\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d05568ac0273df8239328fbc15b53db70ca59ef87704", async() => {
                WriteLiteral(@"
    <meta charset=""utf-8"" />
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
    <title>Quản trị</title>
    <!-- Web Fonts  -->
    <link href=""https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light""
          rel=""stylesheet"" type=""text/css"">

    <!-- Vendor CSS -->
    <link rel=""stylesheet""");
                BeginWriteAttribute("href", " href=\"", 856, "\"", 928, 1);
#nullable restore
#line 24 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 863, Url.Content("~/themes/admin/vendor/bootstrap/css/bootstrap.css"), 863, 65, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 960, "\"", 1031, 1);
#nullable restore
#line 25 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 967, Url.Content("~/themes/admin/vendor/animate/animate.compat.css"), 967, 64, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1063, "\"", 1123, 1);
#nullable restore
#line 26 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 1070, Url.Content("~/themes/admin/vendor/hover/hover.css"), 1070, 53, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">\r\n\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1155, "\"", 1228, 1);
#nullable restore
#line 28 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 1162, Url.Content("~/themes/admin/vendor/font-awesome/css/all.min.css"), 1162, 66, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1260, "\"", 1334, 1);
#nullable restore
#line 29 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 1267, Url.Content("~/themes/admin/vendor/boxicons/css/boxicons.min.css"), 1267, 67, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1366, "\"", 1444, 1);
#nullable restore
#line 30 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 1373, Url.Content("~/themes/admin/vendor/magnific-popup/magnific-popup.css"), 1373, 71, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1476, "\"", 1571, 1);
#nullable restore
#line 31 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 1483, Url.Content("~/themes/admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css"), 1483, 88, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n\r\n    <!-- Specific Page Vendor CSS -->\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1644, "\"", 1712, 1);
#nullable restore
#line 34 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 1651, Url.Content("~/themes/admin/vendor/jquery-ui/jquery-ui.css"), 1651, 61, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1744, "\"", 1818, 1);
#nullable restore
#line 35 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 1751, Url.Content("~/themes/admin/vendor/jquery-ui/jquery-ui.theme.css"), 1751, 67, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1850, "\"", 1932, 1);
#nullable restore
#line 36 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 1857, Url.Content("~/themes/admin/vendor/jquery-confirm/css/jquery-confirm.css"), 1857, 75, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1964, "\"", 2032, 1);
#nullable restore
#line 37 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 1971, Url.Content("~/themes/admin/vendor/select2/css/select2.css"), 1971, 61, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 2064, "\"", 2135, 1);
#nullable restore
#line 38 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 2071, Url.Content("~/themes/admin/vendor/pnotify/pnotify.custom.css"), 2071, 64, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <!-- themes CSS -->\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 2192, "\"", 2243, 1);
#nullable restore
#line 40 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 2199, Url.Content("~/themes/admin/css/theme.css"), 2199, 44, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <!-- Skin CSS -->\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 2298, "\"", 2357, 1);
#nullable restore
#line 42 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 2305, Url.Content("~/themes/admin/css/skins/default.css"), 2305, 52, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 2389, "\"", 2441, 1);
#nullable restore
#line 43 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 2396, Url.Content("~/themes/admin/css/custom.css"), 2396, 45, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 2473, "\"", 2516, 1);
#nullable restore
#line 44 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 2480, Url.Content("~/themes/loading.css"), 2480, 36, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    ");
#nullable restore
#line 45 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
Write(await RenderSectionAsync("Styles", false));

#line default
#line hidden
#nullable disable
                WriteLiteral(@"
    <style>
        .imgShopee {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 60px;
            flex: 0 0 60px;
            height: 60px;
            width: 60px;
            background-repeat: no-repeat;
            background-position: 50%;
            background-size: cover;
            margin-right: 10px;
            position: relative;
            border: 1px solid #e8e8e8;
        }
    </style>
    <!-- Head Libs -->
    <script");
                BeginWriteAttribute("src", " src=\"", 3039, "\"", 3105, 1);
#nullable restore
#line 62 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 3045, Url.Content("~/themes/admin/vendor/modernizr/modernizr.js"), 3045, 60, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d05568ac0273df8239328fbc15b53db70ca59ef817205", async() => {
                WriteLiteral(@"
    <div class=""se-pre-con"" style=""background: #3498db; display:none;"">
        <div class=""book"">
            <div class=""book__page""></div>
            <div class=""book__page""></div>
            <div class=""book__page""></div>
        </div>
    </div>
    <section class=""body"">
        <header class=""header"">
            <div class=""logo-container"">
                <a");
                BeginWriteAttribute("href", " href=\"", 3570, "\"", 3604, 1);
#nullable restore
#line 75 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 3577, Url.Action("Index","Home"), 3577, 27, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"logo\">\r\n                    <img");
                BeginWriteAttribute("src", " src=\"", 3645, "\"", 3669, 1);
#nullable restore
#line 76 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 3651, Url.Content(logo), 3651, 18, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(@" height=""35"" alt=""logo"">
                </a>
                <div class=""d-md-none toggle-sidebar-left"" data-toggle-class=""sidebar-left-opened"" data-target=""html""
                     data-fire-event=""sidebar-left-opened"">
                    <i class=""fas fa-bars"" aria-label=""Toggle sidebar""></i>
                </div>
            </div>

            <!-- start: search & user box -->
            <div class=""header-right"">
                <div id=""userbox"" class=""userbox"">
                    <a href=""#"" data-toggle=""dropdown"">
                        <figure class=""profile-picture"">
                            ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "d05568ac0273df8239328fbc15b53db70ca59ef819292", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n                        </figure>\r\n                        <div class=\"profile-info\" data-lock-name=\"John Doe\" data-lock-email=\"..\">\r\n                            <span class=\"name\">");
#nullable restore
#line 93 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
                                          Write(username);

#line default
#line hidden
#nullable disable
                WriteLiteral(@"</span>
                        </div>
                        <i class=""fa custom-caret""></i>
                    </a>
                    <div class=""dropdown-menu"">
                        <ul class=""list-unstyled mb-2"">
                            <li>
                                ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d05568ac0273df8239328fbc15b53db70ca59ef821544", async() => {
                    WriteLiteral("\r\n                                    <i class=\"fas fa-power-off\"></i>\r\n                                    Đăng xuất\r\n                                ");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_7.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_8.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_8);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <div class=""inner-wrapper"" style=""background-image: url('')"">
            <aside id=""sidebar-left"" class=""sidebar-left"">
                <div class=""sidebar-header"">
                    <div class=""sidebar-title"">

                    </div>
                    <div class=""sidebar-toggle d-none d-md-block"" data-toggle-class=""sidebar-left-collapsed""
                         data-target=""html"" data-fire-event=""sidebar-left-toggle"">
                        <i class=""fas fa-bars"" aria-label=""Toggle sidebar""></i>
                    </div>
                </div>
                <div class=""nano"">
                    <div class=""nano-content"">
                        <nav id=""menu"" class=""nav-main"" role=""navigation"">
                            ");
#nullable restore
#line 125 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
                       Write(await Component.InvokeAsync("LeftSidebar"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                        </nav>\r\n                    </div>\r\n                </div>\r\n            </aside>\r\n            <section role=\"main\" class=\"content-body\">\r\n                ");
#nullable restore
#line 131 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
           Write(RenderBody());

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n            </section>\r\n        </div>\r\n    </section>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6479, "\"", 6543, 1);
#nullable restore
#line 135 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 6485, Url.Content("~/themes/admin/vendor/jquery/jquery.min.js"), 6485, 58, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6567, "\"", 6657, 1);
#nullable restore
#line 136 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 6573, Url.Content("~/themes/admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js"), 6573, 84, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6681, "\"", 6749, 1);
#nullable restore
#line 137 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 6687, Url.Content("~/themes/admin/vendor/popper/umd/popper.min.js"), 6687, 62, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6773, "\"", 6842, 1);
#nullable restore
#line 138 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 6779, Url.Content("~/themes/admin/vendor/bootstrap/js/bootstrap.js"), 6779, 63, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6866, "\"", 6926, 1);
#nullable restore
#line 139 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 6872, Url.Content("~/themes/admin/vendor/common/common.js"), 6872, 54, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6950, "\"", 7022, 1);
#nullable restore
#line 140 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 6956, Url.Content("~/themes/admin/vendor/nanoscroller/nanoscroller.js"), 6956, 66, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7046, "\"", 7129, 1);
#nullable restore
#line 141 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 7052, Url.Content("~/themes/admin/vendor/magnific-popup/jquery.magnific-popup.js"), 7052, 77, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7153, "\"", 7213, 1);
#nullable restore
#line 142 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 7159, Url.Content("~/themes/admin/vendor/moment/moment.js"), 7159, 54, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <!-- themes Base, Components and Settings -->\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7288, "\"", 7357, 1);
#nullable restore
#line 144 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 7294, Url.Content("~/themes/admin/vendor/pnotify/pnotify.custom.js"), 7294, 63, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7381, "\"", 7446, 1);
#nullable restore
#line 145 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 7387, Url.Content("~/themes/admin/vendor/select2/js/select2.js"), 7387, 59, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7470, "\"", 7561, 1);
#nullable restore
#line 146 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 7476, Url.Content("~/themes/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"), 7476, 85, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7585, "\"", 7655, 1);
#nullable restore
#line 147 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 7591, Url.Content("~/themes/admin/vendor/ios7-switch/ios7-switch.js"), 7591, 64, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7679, "\"", 7743, 1);
#nullable restore
#line 148 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 7685, Url.Content("~/themes/admin/vendor/autosize/autosize.js"), 7685, 58, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7767, "\"", 7847, 1);
#nullable restore
#line 149 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 7773, Url.Content("~/themes/admin/vendor/jquery-validation/jquery.validate.js"), 7773, 74, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7871, "\"", 7958, 1);
#nullable restore
#line 150 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 7877, Url.Content("~/themes/admin/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"), 7877, 81, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7982, "\"", 8061, 1);
#nullable restore
#line 151 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 7988, Url.Content("~/themes/admin/vendor/jquery-confirm/js/jquery-confirm.js"), 7988, 73, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8085, "\"", 8159, 1);
#nullable restore
#line 152 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 8091, Url.Content("~/themes/admin/vendor/jquery-appear/jquery.appear.js"), 8091, 68, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8183, "\"", 8267, 1);
#nullable restore
#line 153 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 8189, Url.Content("~/themes/admin/vendor/jquery-maskedinput/jquery.maskedinput.js"), 8189, 78, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <!-- themes Initialization Files -->\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8333, "\"", 8389, 1);
#nullable restore
#line 155 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 8339, Url.Content("~/themes/admin/js/jquery.number.js"), 8339, 50, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8413, "\"", 8461, 1);
#nullable restore
#line 156 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 8419, Url.Content("~/themes/admin/js/theme.js"), 8419, 42, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8485, "\"", 8533, 1);
#nullable restore
#line 157 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 8491, Url.Content("~/themes/admin/js/Utils.js"), 8491, 42, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8557, "\"", 8615, 1);
#nullable restore
#line 158 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 8563, Url.Content("~/themes/admin/js/session-timeout.js"), 8563, 52, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8639, "\"", 8688, 1);
#nullable restore
#line 159 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 8645, Url.Content("~/themes/admin/js/custom.js"), 8645, 43, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8712, "\"", 8765, 1);
#nullable restore
#line 160 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
WriteAttributeValue("", 8718, Url.Content("~/themes/admin/js/theme.init.js"), 8718, 47, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    ");
#nullable restore
#line 161 "C:\_Project\_Gits\tns_shopee\Views\Shared\_Layout.cshtml"
Write(await RenderSectionAsync("Scripts", required: false));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            BeginWriteTagHelperAttribute();
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("data-loading-overlay", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Microsoft.AspNetCore.Http.IHttpContextAccessor HttpContextAccessor { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
