#pragma checksum "C:\_Project\_Gits\tns_shopee\Views\Components\SelectCategory.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c9fd421e2201723047390e6c5023a8747c7ba7f5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Components_SelectCategory), @"mvc.1.0.view", @"/Views/Components/SelectCategory.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\_Project\_Gits\tns_shopee\Views\_ViewImports.cshtml"
using ShopCP;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\_Project\_Gits\tns_shopee\Views\_ViewImports.cshtml"
using ShopCP.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c9fd421e2201723047390e6c5023a8747c7ba7f5", @"/Views/Components/SelectCategory.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"52d1388f9c3eeec799e6da174b162d620cca532c", @"/Views/_ViewImports.cshtml")]
    public class Views_Components_SelectCategory : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\_Project\_Gits\tns_shopee\Views\Components\SelectCategory.cshtml"
  
    var _List = new List<Product_Category_Model>();
    if (ViewBag.List != null)
    {
        _List = ViewBag.List as List<Product_Category_Model>;
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 10 "C:\_Project\_Gits\tns_shopee\Views\Components\SelectCategory.cshtml"
 if (_List.Count > 0)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <ul class=\"list-unstyled\">\r\n");
#nullable restore
#line 13 "C:\_Project\_Gits\tns_shopee\Views\Components\SelectCategory.cshtml"
         foreach (var rec in _List)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <li style=\"cursor:pointer\"");
            BeginWriteAttribute("id", " id=\"", 316, "\"", 337, 1);
#nullable restore
#line 15 "C:\_Project\_Gits\tns_shopee\Views\Components\SelectCategory.cshtml"
WriteAttributeValue("", 321, rec.CategoryKey, 321, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"p-2\"><b>");
#nullable restore
#line 15 "C:\_Project\_Gits\tns_shopee\Views\Components\SelectCategory.cshtml"
                                                                       Write(rec.CategoryName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</b></li>\r\n");
#nullable restore
#line 16 "C:\_Project\_Gits\tns_shopee\Views\Components\SelectCategory.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </ul>\r\n");
#nullable restore
#line 18 "C:\_Project\_Gits\tns_shopee\Views\Components\SelectCategory.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            DefineSection("scripts", async() => {
                WriteLiteral(@"
    <script>
        $(""#pnLeft"").on(""click"", ""li"", function () {
            $tr = $(this);
            var id = $tr.attr(""id"");
            $(""#pnLeft li"").removeClass(""active"");
            $(""#pnLeft li[id="" + id + ""]"").addClass(""active"");
            GetName(1);
            Load_Category(id);
        });
        $(""#pnRight"").on(""click"", ""li"", function () {
            $tr = $(this);
            var id = $tr.attr(""id"");
            $(""#pnRight li"").removeClass(""active"");
            $(""#pnRight li[id="" + id + ""]"").addClass(""active"");

            GetName(2);
        });

        var key = $(""#txt_ProductKey"").val();
        if (key.length >= 36) {
            var catePath = $(""#txt_CategoryPath"").val();
            var temp = catePath.split(""-"");
            if (temp.length > 2) {
                var lvl1 = temp[1];
                var lvl2 = temp[2];

                $(""#pnLeft li[id="" + lvl1 + ""]"").addClass(""active"");
                Load_Category(lvl1, lvl2);
            ");
                WriteLiteral(@"}
            else {
                var lvl1 = temp[1];
                $(""#pnLeft li[id="" + lvl1 + ""]"").addClass(""active"");
            }
        }

        function Load_Category(Parent, select) {
            $.ajax({
                url: URL_Category,
                type: ""GET"",
                data: {
                    ""Parent"": Parent
                },
                beforeSend: function () {
                    $(""#pnRight"").empty();
                },
                success: function (r) {
                    $(""#pnRight"").append(r);

                },
                error: function (err) {
                    Utils.OpenNotify(err.responseText, ""Thông báo"", ""error"");
                },
                complete: function () {
                    if (select !== undefined) {
                        $(""#pnRight li[id="" + select + ""]"").addClass(""active"");
                    }
                }
            });
        }
        function GetName(Action) {
            ");
                WriteLiteral(@"if (Action == 1) {
                var id1 = """";
                var name1 = """";

                id1 = $(""#pnLeft li.active"").attr(""id"");
                name1 = $(""#pnLeft li.active"").text();

                var cateVal = ""0 - "" + id1;
                var cateName = name1;

                $(""#txt_CategoryPath"").val(cateVal);
                $(""#txt_CategoryName"").val(cateName);
                $(""#txt_CateName"").text(cateName);
                $(""#txt_CategoryKey"").val(id1);
            }

            if (Action == 2) {
                var id1 = """";
                var name1 = """";

                id1 = $(""#pnLeft li.active"").attr(""id"");
                name1 = $(""#pnLeft li.active"").text();

                var id2 = """";
                var name2 = """";

                id2 = $(""#pnRight li.active"").attr(""id"");
                name2 = $(""#pnRight li.active"").text();

                var cateVal = ""0 - "" + id1 + "" - "" + id2;
                var cateName = name1 + "" > "" + name2");
                WriteLiteral(@"

                $(""#txt_CategoryPath"").val(cateVal);
                $(""#txt_CategoryName"").val(cateName);
                $(""#txt_CateName"").text(cateName);
                $(""#txt_CategoryKey"").val(id2);
            }

            console.log($(""#txt_CategoryPath"").val());
        }
    </script>
");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
