﻿var error_obj = null;
var error_prefix = "Phải nhập ";
var error_message = "<div class='text-danger error'>{msg}</div>";


var Utils = {

    validate: function (element) {
        $('.error').remove();
        var control = element.find("input[required], textarea[required], select[required]");
        control.each(function (idx, item) {
            Utils.checkError($(item));
        });
        if ($('div.error').length > 0) {
            return false;
        }
        return true;
    },
    checkError: function (txt) {
        var val = $.trim(txt.val());
        if (val === "") {
            var title = error_prefix + $.trim(txt.closest(".form-group-sm, .form-group").find("label").html());
            var error = error_message.replace("{msg}", title);
            txt.parent().append($(error));
            if (error_obj === null) {
                error_obj = txt;
            }
        }
    },
    //

    RemoveRow: function (elem) {
        elem.addClass("on-remove").fadeOut(1000,
            function () {
                $(elem).remove();
            });
    },
    BackWithRefresh: function () {
        //window.history.back();
        this.LoadIn();
        var last_url = document.referrer;
        var current_url = $(location).attr("href");
        if (last_url === current_url) {
            window.history.back(-3);
        } else {
            window.location.href = last_url;
        }
    },
    PrintOnDesktop: function (id) {
        var divToPrint = document.getElementById(id);
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write('<html><body onload="window.print()" style="margin: 10px !important; font-size: 0.7rem !important; font-family: tahoma !important"><style>table {font-size: 0.7rem !important } </style>' + divToPrint.innerHTML + '</body></html>');
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
            location.reload(true);
        }, 10);
    },
    ConvertToDDMMYYYY: function (input) {
        var today = new Date(input);
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        if (yyyy === 1 &&
            dd === '01' &&
            mm === '01') {
            return '';
        }
        return today = dd + '/' + mm + '/' + yyyy;
    },
    ConvertToYYYYMMDD: function (input) {
        var date = input;
        var d = new Date(date.split("/").reverse().join("-"));
        var dd = d.getDate();
        var mm = d.getMonth() + 1;
        var yy = d.getFullYear();
        var newdate = yy + "-" + mm + "-" + dd;
        return newdate;
    },
    PreviewImg: function (input, output) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(output).attr('src', e.target.result);
            };
            // convert to base64 string
            reader.readAsDataURL(input.files[0]);
        }
    },
    OpenMagnific: function (id) {
        $.magnificPopup.open({
            callbacks: {
                beforeOpen: function () {
                    this.wrap.removeAttr('tabindex')
                },
                open: function () {
                    console.log('Popup open has been initiated');
                },
                beforeClose: function () {
                    console.log('Popup before close has been initiated');
                },
                close: function () {
                    console.log('Popup close has been initiated');
                },
                afterClose: function () {
                    console.log('Popup after close has been initiated');
                },
            },
            items: {
                src: id
            },
            type: 'inline',

            fixedContentPos: false,
            fixedBgPos: true,

            overflowY: 'auto',
            closeBtnInside: true,
            preloader: true,

            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            modal: true
        });
    },
    CloseMagnific: function (id) {
        $(id).magnificPopup('close');
        //$.magnificPopup.close();
    },
    OpenNotify: function (title, text, css) {

        new PNotify({
            title: title, //'Đã xóa thành công !.'
            text: text,
            type: css //success
        });

    },
    ClearUI: function (element) {
        $(element).find('input,textarea,select').not(':input[type=button], :input[type=submit], :input[type=reset]').val('');
        $(element).find('select:not([multiple])').each(function () {
            $(this).val($(this).find("option:first").val());
        });
    },
    GetDate: function () {
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        return output;
    },
    //----------------------------------------------------------------------------------Date time
    GetFirstDayYear: function () {
        var d = new Date();

        d.setFullYear(d.getFullYear());
        return '01/01/' + d.getFullYear();
    },
    GetLastDayYear: function () {
        var d = new Date();

        d.setFullYear(d.getFullYear());
        return '31/12/' + d.getFullYear();
    },
    GetLastDayMonth: function () {
        var date = new Date();
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        var month = ((lastDay.getMonth() + 1) > 9) ? (lastDay.getMonth() + 1) : '0' + (lastDay.getMonth() + 1);
        var day = lastDay.getDate() + "/" + month + "/" + lastDay.getFullYear();
        return day;
    },
    GetFirstDayMonth: function () {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var month = ((firstDay.getMonth() + 1) > 9) ? (firstDay.getMonth() + 1) : '0' + (firstDay.getMonth() + 1);

        var day = firstDay.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + "/" + month + "/" + firstDay.getFullYear();
        return output;
    },
    GetCurrentMonth: function () {
        var d = new Date();

        var month = d.getMonth() + 1;
        var day = d.getDate();

        var output = (month < 10 ? '0' : '') + month + '/' + d.getFullYear();
        return output;
    },
    GetCurrentDate: function () {
        var d = new Date();

        var month = d.getMonth() + 1;
        var day = d.getDate();

        var output = (day < 10 ? '0' : '') + day + '/' + (month < 10 ? '0' : '') + month + '/' + d.getFullYear();
        return output;
    },
    GetCurrentHour: function () {
        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();
        return (h < 10 ? '0' : '') + h + ":" + (m < 10 ? '0' : '') + m;
    },
    LoadIn: function () {
        $(".se-pre-con").fadeIn("slow");
    },
    LoadOut: function () {
        $(".se-pre-con").fadeOut("slow");
    },
    Toggle: function (id) {
        var e = document.getElementById(id);
        if (e.style.display === 'block')
            e.style.display = 'none';
        else
            e.style.display = 'block';
    },
    GetUrlParameter: function (sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return typeof sParameterName[1] === undefined ? "" : decodeURIComponent(sParameterName[1]);
            }
        }
        return "";
    },
};
jQuery.loadScript = function (url, callback) {
    jQuery.ajax({
        url: url,
        dataType: 'script',
        success: callback,
        async: true
    });
};
