﻿using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ShopCP
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            TN_Helper.PartnerNumber = Configuration.GetSection("PartnerNumber").Value;
            TN_Helper.BusinessName = Configuration.GetSection("Business").Value;
            TN_Helper.ConnectionString = Configuration.GetSection("Data").GetSection("ConnectionString").Value;

            //TN_Helper.Begin_PriceStock();
            //TN_Helper.Begin_ExecuteOutput(DateTime.Now);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //// dịch vụ chạy tự động
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetSection("Data").GetSection("ConnectionHangfire").Value, new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    DisableGlobalLocks = true
                }));

            //// các hàm xử lý chạy tự động
            services.AddHangfireServer();
            services.AddSingleton<ICalculatorStock, CalculatorStock>();
            services.AddSingleton<ICheckShopeeOrder, CheckShopeeOrder>();

            //// sử dụng session
            services.AddSession(so =>
            {
                so.IdleTimeout = TimeSpan.FromHours(8);
            });

            services
                .AddHttpContextAccessor()
                .AddControllersWithViews()
                .AddRazorRuntimeCompilation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            IBackgroundJobClient backgroundJobClient,
            IRecurringJobManager recurringJobManager,
            IServiceProvider serviceProvider)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}

            app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseHangfireDashboard();
            backgroundJobClient.Enqueue(() => Console.WriteLine("Start BGW"));
            recurringJobManager.AddOrUpdate(
                "Chạy mỗi ngày lúc 23 giờ",
                () => serviceProvider.GetService<ICheckShopeeOrder>().CheckOrder(),
                "0 23 * * *");
            recurringJobManager.AddOrUpdate(
                "Chạy ngày 1 đầu tháng lúc 12h",
                () => serviceProvider.GetService<ICalculatorStock>().ProcessStock(),
                "0 0 1 * *");
        }
    }
}
