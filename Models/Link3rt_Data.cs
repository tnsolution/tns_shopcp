﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Link3rt_Data
    {
        public static List<Link3rt_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Link3rt WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Link3rt_Model> zList = new List<Link3rt_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Link3rt_Model()
                {
                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : long.Parse(r["AutoKey"].ToString())),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    UserName = (r["UserName"] == DBNull.Value ? "" : r["UserName"].ToString()),
                    Password = (r["Password"] == DBNull.Value ? "" : r["Password"].ToString()),
                    Cookies = (r["Cookies"] == DBNull.Value ? "" : r["Cookies"].ToString()),
                    Web = (r["Web"] == DBNull.Value ? "" : r["Web"].ToString()),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
    }
}
