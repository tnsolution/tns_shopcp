﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

public class User_Info
{
    public User_Model User = new User_Model();
    private string _Message = string.Empty;

    public string Code
    {
        get
        {
            if (Message.Length >= 3)
            {
                return Message.Substring(0, 3);
            }
            else
            {
                return "";
            }
        }
    }
    public string Message
    {
        get
        {
            return _Message;
        }

        set
        {
            _Message = value;
        }
    }
    #region [ Constructor Get Information ]
    public User_Info()
    {
        User.UserKey = Guid.NewGuid().ToString();
    }
    public User_Info(string UserKey)
    {
        string zSQL = "SELECT * FROM SYS_User WHERE UserKey = @UserKey AND RecordStatus != 99 ";
        string zConnectionString = TN_Helper.ConnectionString;
        SqlConnection zConnect = new SqlConnection(zConnectionString);
        zConnect.Open();
        try
        {
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
            SqlDataReader zReader = zCommand.ExecuteReader();
            if (zReader.HasRows)
            {
                zReader.Read();
                User.UserKey = zReader["UserKey"].ToString();
                User.UserAPI = zReader["UserAPI"].ToString();
                User.UserName = zReader["UserName"].ToString();
                User.Password = zReader["Password"].ToString();
                User.PIN = zReader["PIN"].ToString();
                User.Description = zReader["Description"].ToString();
                User.GroupName = zReader["GroupName"].ToString();
                User.PartnerNumber = zReader["PartnerNumber"].ToString();
                if (zReader["BusinessKey"] != DBNull.Value)
                {
                    User.BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
                }

                if (zReader["Activate"] != DBNull.Value)
                {
                    User.Activate = (bool)zReader["Activate"];
                }

                if (zReader["ExpireDate"] != DBNull.Value)
                {
                    User.ExpireDate = (DateTime)zReader["ExpireDate"];
                }

                if (zReader["LastLoginDate"] != DBNull.Value)
                {
                    User.LastLoginDate = (DateTime)zReader["LastLoginDate"];
                }

                if (zReader["FailedPasswordCount"] != DBNull.Value)
                {
                    User.FailedPasswordCount = int.Parse(zReader["FailedPasswordCount"].ToString());
                }
                if (zReader["Slug"] != DBNull.Value)
                {
                    User.Slug = int.Parse(zReader["Slug"].ToString());
                }

                if (zReader["RecordStatus"] != DBNull.Value)
                {
                    User.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                }

                if (zReader["CreatedOn"] != DBNull.Value)
                {
                    User.CreatedOn = (DateTime)zReader["CreatedOn"];
                }

                User.CreatedBy = zReader["CreatedBy"].ToString();
                User.CreatedName = zReader["CreatedName"].ToString();
                if (zReader["ModifiedOn"] != DBNull.Value)
                {
                    User.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }

                User.ModifiedBy = zReader["ModifiedBy"].ToString();
                User.ModifiedName = zReader["ModifiedName"].ToString();
                _Message = "200 OK";
            }
            else
            {
                _Message = "404 Not Found";
            }
            zReader.Close();
            zCommand.Dispose();
        }
        catch (Exception Err)
        {
            _Message = "500 " + Err.ToString();
        }
        finally
        {
            zConnect.Close();
        }
    }
    public User_Info(string UserName, string Password)
    {
        string zSQL = @"
SELECT A.*
FROM SYS_User A
WHERE A.RecordStatus != 99 
AND A.UserName = @UserName 
AND A.Password = @Password 
";
        string zConnectionString = TN_Helper.ConnectionString;
        SqlConnection zConnect = new SqlConnection(zConnectionString);
        zConnect.Open();
        try
        {
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
            zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = Password;
            SqlDataReader zReader = zCommand.ExecuteReader();
            if (zReader.HasRows)
            {
                zReader.Read();

                #region [User]
                User.UserKey = zReader["UserKey"].ToString();
                User.UserAPI = zReader["UserAPI"].ToString();
                User.UserName = zReader["UserName"].ToString();
                User.Password = zReader["Password"].ToString();
                User.PIN = zReader["PIN"].ToString();
                User.Description = zReader["Description"].ToString();
                User.GroupName = zReader["GroupName"].ToString();
                User.PartnerNumber = zReader["PartnerNumber"].ToString().ToUpper();
                if (zReader["BusinessKey"] != DBNull.Value)
                {
                    User.BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
                }

                if (zReader["Activate"] != DBNull.Value)
                {
                    User.Activate = (bool)zReader["Activate"];
                }

                if (zReader["ExpireDate"] != DBNull.Value)
                {
                    User.ExpireDate = (DateTime)zReader["ExpireDate"];
                }

                if (zReader["LastLoginDate"] != DBNull.Value)
                {
                    User.LastLoginDate = (DateTime)zReader["LastLoginDate"];
                }

                if (zReader["FailedPasswordCount"] != DBNull.Value)
                {
                    User.FailedPasswordCount = int.Parse(zReader["FailedPasswordCount"].ToString());
                }
                if (zReader["Slug"] != DBNull.Value)
                {
                    User.Slug = int.Parse(zReader["Slug"].ToString());
                }

                if (zReader["RecordStatus"] != DBNull.Value)
                {
                    User.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                }

                if (zReader["CreatedOn"] != DBNull.Value)
                {
                    User.CreatedOn = (DateTime)zReader["CreatedOn"];
                }

                User.CreatedBy = zReader["CreatedBy"].ToString();
                User.CreatedName = zReader["CreatedName"].ToString();
                if (zReader["ModifiedOn"] != DBNull.Value)
                {
                    User.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }

                User.ModifiedBy = zReader["ModifiedBy"].ToString();
                User.ModifiedName = zReader["ModifiedName"].ToString();
                #endregion

                _Message = "200 OK";
            }
            else
            {
                _Message = "404 Not Found";
            }
            zReader.Close();
            zCommand.Dispose();
        }
        catch (Exception Err)
        {
            _Message = "500 " + Err.ToString();
        }
        finally
        {
            zConnect.Close();
        }
    }
    #endregion
    #region [ Constructor Update Information ]
    public string ResetPass(string UserKey, string PasswordNew)
    {
        string zResult = "";
        //---------- String SQL Access Database ---------------
        string zSQL = @"UPDATE SYS_User SET Password = @Password, ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName, ModifiedOn = GetDate() WHERE UserKey = @UserKey ";
        string zConnectionString = TN_Helper.ConnectionString;
        SqlConnection zConnect = new SqlConnection(zConnectionString);
        zConnect.Open();

        try
        {
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

            zCommand.CommandType = CommandType.Text;

            zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
            zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = PasswordNew;
            zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = User.ModifiedBy;
            zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = User.ModifiedName;
            zResult = zCommand.ExecuteNonQuery().ToString();
            _Message = "200 OK";
            zCommand.Dispose();
        }
        catch (Exception Err)
        {
            _Message = "500" + Err.ToString();
        }
        finally
        {
            zConnect.Close(); ;
        }

        return zResult;

    }
    public string UpdateLogged()
    {
        string zSQL = "UPDATE SYS_User SET "
                    + " LastLoginDate = GetDate(),"
                    + " ModifiedOn = GetDate(),"
                    + " ModifiedBy = @ModifiedBy,"
                    + " ModifiedName = @ModifiedName"
                    + " WHERE UserKey = @UserKey";
        string zResult = "";
        string zConnectionString = TN_Helper.ConnectionString;
        SqlConnection zConnect = new SqlConnection(zConnectionString);
        zConnect.Open();
        try
        {
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User.UserKey);
            zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = User.ModifiedBy;
            zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = User.ModifiedName;
            zResult = zCommand.ExecuteNonQuery().ToString();
            zCommand.Dispose();
            _Message = "200 OK";
        }
        catch (Exception Err)
        {
            _Message = "500 " + Err.ToString();
        }
        finally
        {
            zConnect.Close();
        }
        return zResult;
    }
    #endregion
}
