﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Link3rt_Info
    {

        public Link3rt_Model Link3rt = new Link3rt_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Link3rt_Info()
        {
        }
        public Link3rt_Info(string PartnerNumber, string Web, string UserName)
        {
            string zSQL = "SELECT * FROM SYS_Link3rt WHERE PartnerNumber = @PartnerNumber AND Web = @Web AND UserName = @UserName ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = Web;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Link3rt.AutoKey = long.Parse(zReader["AutoKey"].ToString());
                    Link3rt.UserName = zReader["UserName"].ToString();
                    Link3rt.Password = zReader["Password"].ToString();
                    Link3rt.Cookies = zReader["Cookies"].ToString();
                    Link3rt.Web = zReader["Web"].ToString();
                    Link3rt.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Link3rt.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Link3rt.CreatedBy = zReader["CreatedBy"].ToString();
                    Link3rt.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Link3rt.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Link3rt.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Link3rt.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public Link3rt_Info(string PartnerNumber, string Web)
        {
            string zSQL = "SELECT * FROM SYS_Link3rt WHERE PartnerNumber = @PartnerNumber AND Web = @Web";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = Web;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Link3rt.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["AutoKey"] != DBNull.Value)
                        Link3rt.AutoKey = long.Parse(zReader["AutoKey"].ToString());
                    Link3rt.UserName = zReader["UserName"].ToString();
                    Link3rt.Password = zReader["Password"].ToString();
                    Link3rt.Cookies = zReader["Cookies"].ToString();
                    Link3rt.Web = zReader["Web"].ToString();
                    Link3rt.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Link3rt.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Link3rt.CreatedBy = zReader["CreatedBy"].ToString();
                    Link3rt.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Link3rt.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Link3rt.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Link3rt.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Link3rt ("
         + " UserName , Password , Cookies , Web , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @UserName , @Password , @Cookies , @Web , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = Link3rt.UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = Link3rt.Password;
                zCommand.Parameters.Add("@Cookies", SqlDbType.NVarChar).Value = Link3rt.Cookies;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = Link3rt.Web;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Link3rt.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Link3rt.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Link3rt.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Link3rt.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Link3rt.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Link3rt("
         + " AutoKey , UserName , Password , Cookies , Web , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @UserName , @Password , @Cookies , @Web , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = Link3rt.UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = Link3rt.Password;
                zCommand.Parameters.Add("@Cookies", SqlDbType.NVarChar).Value = Link3rt.Cookies;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = Link3rt.Web;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Link3rt.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Link3rt.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Link3rt.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Link3rt.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Link3rt.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_Link3rt SET "
                        + " UserName = @UserName,"
                        + " Password = @Password,"
                        + " Cookies = @Cookies,"
                        + " Web = @Web,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE Web = @Web AND PartnerNumber = @PartnerNumber";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = Link3rt.UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = Link3rt.Password;
                zCommand.Parameters.Add("@Cookies", SqlDbType.NVarChar).Value = Link3rt.Cookies;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = Link3rt.Web;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Link3rt.PartnerNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Link3rt.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Link3rt.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Link3rt SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.BigInt).Value = Link3rt.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateStatus()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Link3rt SET RecordStatus = @RecordStatus WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.BigInt).Value = Link3rt.AutoKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Link3rt.RecordStatus;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
