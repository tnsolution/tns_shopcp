﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ShopCP.Models
{
    public partial class Report
    {
        public static DataTable Customer(string PartnerNumber)
        {
            DataTable zTable = new();
            //---------- String SQL Access Database ---------------
            string zSQL = @"
SELECT DISTINCT
UPPER(BuyerName) AS BuyerName, BuyerPhone, ShippingAddress
FROM SAL_ORDER
WHERE RecordStatus <> 99
AND Slug <> 9
AND PartnerNumber = @PartnerNumber
ORDER BY BuyerName";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
        //tất cả đơn hàng
        public static DataTable ChartOrder(string PartnerNumber, DateTime FromDate, DateTime ToDate, string Option, string Color, out string Message)
        {
            string PivotDate = "";
            for (var i = FromDate.Date; i.Date <= ToDate.Date; i = i.AddDays(1))
            {
                PivotDate += "[" + i.Day + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            string[] temp = Option.Split(';');
            string[] color = Color.Split(';');

            string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(500) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT,
    Color NVARCHAR(50)
)";
            foreach (string s in temp)
            {
                switch (s.Trim())
                {
                    default:
                        {
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Tất cả', 1, 0, 0, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Tất cả',
DAY(CreatedOn),
COUNT(OrderKey), 
0, '@Color'
FROM SAL_Order
WHERE 
RecordStatus <> 99
AND OrderStatus IN (0, 1, 2, 9)
AND PartnerNumber = @PartnerNumber 
AND Slug <> 9   --9 LÀ ĐƠN SẢN XUẤT
AND (CreatedOn BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(CreatedOn)";
                            zSQL = zSQL.Replace("@Color", color[0].Trim());
                        }
                        break;
                    case "Shopee":
                        {
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Shopee', 1, 0, 1, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Shopee',
DAY(CreatedOn),
COUNT(OrderKey), 
1, '@Color'
FROM SAL_Order
WHERE 
RecordStatus <> 99
AND OrderStatus IN (0, 1, 2, 9)
AND Slug = 1
AND PartnerNumber = @PartnerNumber 
AND (CreatedOn BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(CreatedOn)";
                            zSQL = zSQL.Replace("@Color", color[1].Trim());
                        }
                        break;

                    case "Lazada":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Lazada', 1, 0, 2, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[2].Trim());
                        }
                        break;
                    case "Tiki":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Tiki', 1, 0, 3, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[3].Trim());
                        }
                        break;
                    case "Khác":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Khác', 1, 0, 4, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[4].Trim());
                        }
                        break;
                    case "Offline":
                        {
                            zSQL += @"INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Offline', 1, 0, 5, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Offline',
DAY(CreatedOn),
COUNT(OrderKey), 
5, '@Color'
FROM SAL_Order
WHERE RecordStatus <> 99
AND Slug = 11   -- tại cửa hàng
AND PartnerNumber = @PartnerNumber 
AND (CreatedOn BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(CreatedOn)";
                            zSQL = zSQL.Replace("@Color", color[5].Trim());
                        }
                        break;
                }
            }

            zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total, Color, [Rank]
	FROM #temp
) X
PIVOT 
( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY [Rank] DROP TABLE #temp";

            DataTable zTable = new();
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
        //tất cả đơn hàng đã thu tiền
        public static DataTable ChartComplete(string PartnerNumber, DateTime FromDate, DateTime ToDate, string Option, string Color, out string Message)
        {
            string PivotDate = "";
            for (var i = FromDate.Date; i.Date <= ToDate.Date; i = i.AddDays(1))
            {
                PivotDate += "[" + i.Day + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            string[] temp = Option.Split(';');
            string[] color = Color.Split(';');

            string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(500) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT,
    Color NVARCHAR(50)
)";
            foreach (string s in temp)
            {
                switch (s.Trim())
                {
                    default:
                        {
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Tất cả', 1, 0, 0, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Tất cả',
DAY(B.ReceiptDate),
COUNT(OrderKey), 
0, '@Color'
FROM SAL_Order A LEFT JOIN FNC_Receipt B ON CONVERT(NVARCHAR(50), A.OrderKey) = B.DocumentID
WHERE 
A.RecordStatus <> 99
AND B.RecordStatus <> 99
AND A.OrderStatus =2
AND A.PartnerNumber = @PartnerNumber 
AND A.Slug <> 9   --9 LÀ ĐƠN SẢN XUẤT
AND (B.ReceiptDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(B.ReceiptDate)";
                            zSQL = zSQL.Replace("@Color", color[0].Trim());
                        }
                        break;
                    case "Shopee":
                        {
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Shopee', 1, 0, 1, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Shopee',
DAY(B.ReceiptDate),
COUNT(OrderKey), 
1, '@Color'
FROM SAL_Order A LEFT JOIN FNC_Receipt B ON CONVERT(NVARCHAR(50), A.OrderKey) = B.DocumentID
WHERE 
A.RecordStatus <> 99
AND B.RecordStatus <> 99
AND A.OrderStatus =2
AND A.Slug = 1
AND A.PartnerNumber = @PartnerNumber 
AND (B.ReceiptDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(B.ReceiptDate)";
                            zSQL = zSQL.Replace("@Color", color[1].Trim());
                        }
                        break;

                    case "Lazada":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Lazada', 1, 0, 2, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[2].Trim());
                        }
                        break;
                    case "Tiki":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Tiki', 1, 0, 3, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[3].Trim());
                        }
                        break;
                    case "Khác":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Khác', 1, 0, 4, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[4].Trim());
                        }
                        break;
                    case "Offline":
                        {
                            zSQL += @"INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Offline', 1, 0, 5, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Offline',
DAY(B.ReceiptDate),
COUNT(OrderKey), 
5, '@Color'
FROM SAL_Order A LEFT JOIN FNC_Receipt B ON CONVERT(NVARCHAR(50), A.OrderKey) = B.DocumentID
WHERE 
A.RecordStatus <> 99
AND B.RecordStatus <> 99
AND A.Slug = 11   -- tại cửa hàng
AND A.PartnerNumber = @PartnerNumber 
AND (B.ReceiptDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(B.ReceiptDate)";
                            zSQL = zSQL.Replace("@Color", color[5].Trim());
                        }
                        break;
                }
            }

            zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total, Color, [Rank]
	FROM #temp
) X
PIVOT 
( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY [Rank] DROP TABLE #temp";

            DataTable zTable = new();
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
        //Doanh số
        public static DataTable ChartMoney(string PartnerNumber, DateTime FromDate, DateTime ToDate, string Option, string Color)
        {
            string PivotDate = "";
            for (var i = FromDate.Date; i.Date <= ToDate.Date; i = i.AddDays(1))
            {
                PivotDate += "[" + i.Day + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            string[] temp = Option.Split(';');
            string[] color = Color.Split(';');

            string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(500) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT,
    Color NVARCHAR(50)
)";
            foreach (string s in temp)
            {
                switch (s.Trim())
                {
                    default:
                        {
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Tất cả', 1, 0, 0, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Tất cả',
DAY(B.ReceiptDate),
SUM(B.AmountCurrencyMain),
0, '@Color'
FROM SAL_Order A LEFT JOIN FNC_Receipt B ON CONVERT(NVARCHAR(50), A.OrderKey) = B.DocumentID
WHERE 
A.RecordStatus <> 99
AND B.RecordStatus <> 99
AND A.OrderStatus = 2
AND A.PartnerNumber = @PartnerNumber 
AND A.Slug <> 9   --9 LÀ ĐƠN SẢN XUẤT
AND (B.ReceiptDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(B.ReceiptDate)";
                            zSQL = zSQL.Replace("@Color", color[0].Trim());
                        }
                        break;
                    case "Shopee":
                        {
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Shopee', 1, 0, 1, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Shopee',
DAY(B.ReceiptDate),
SUM(B.AmountCurrencyMain),
1, '@Color'
FROM SAL_Order A LEFT JOIN FNC_Receipt B ON CONVERT(NVARCHAR(50), A.OrderKey) = B.DocumentID
WHERE 
A.RecordStatus <> 99
AND B.RecordStatus <> 99
AND A.OrderStatus = 2
AND A.Slug = 1
AND A.PartnerNumber = @PartnerNumber 
AND (B.ReceiptDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(B.ReceiptDate)";
                            zSQL = zSQL.Replace("@Color", color[1].Trim());
                        }
                        break;

                    case "Lazada":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Lazada', 1, 0, 2, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[2].Trim());
                        }
                        break;
                    case "Tiki":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Tiki', 1, 0, 3, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[3].Trim());
                        }
                        break;
                    case "Khác":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Khác', 1, 0, 4, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[4].Trim());
                        }
                        break;
                    case "Offline":
                        {
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Offline', 1, 0, 5, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Offline',
DAY(B.ReceiptDate),
SUM(B.AmountCurrencyMain),
5, '@Color'
FROM SAL_Order A LEFT JOIN FNC_Receipt B ON CONVERT(NVARCHAR(50), A.OrderKey) = B.DocumentID
WHERE 
A.RecordStatus <> 99
AND B.RecordStatus <> 99
AND A.OrderStatus = 2
AND A.Slug = 11   -- tại cửa hàng
AND A.PartnerNumber = @PartnerNumber 
AND (B.ReceiptDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(B.ReceiptDate)";
                            zSQL = zSQL.Replace("@Color", color[5].Trim());
                        }
                        break;
                }
            }

            zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total, Color, [Rank]
	FROM #temp
) X
PIVOT 
( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY [Rank] DROP TABLE #temp";

            DataTable zTable = new();
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
        //Lợi nhuận
        public static DataTable ChartProfit(string PartnerNumber, DateTime FromDate, DateTime ToDate, string Option, string Color)
        {
            string PivotDate = "";
            for (var i = FromDate.Date; i.Date <= ToDate.Date; i = i.AddDays(1))
            {
                PivotDate += "[" + i.Day + "],";
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            string[] temp = Option.Split(';');
            string[] color = Color.Split(';');

            string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(500) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT,
    Color NVARCHAR(50)
)";
            foreach (string s in temp)
            {
                switch (s.Trim())
                {
                    default:
                        {
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Tất cả', 1, 0, 0, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Tất cả',
DAY(CreatedOn),
SUM(TotalMoney) - SUM(dbo.SAL_OrderStandCost(OrderKey)),
0, '@Color'
FROM SAL_Order
WHERE RecordStatus <> 99
AND OrderStatus = 2
AND PartnerNumber = @PartnerNumber 
AND Slug <> 9   --9 LÀ ĐƠN SẢN XUẤT
AND (CreatedOn BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(CreatedOn)";
                            zSQL = zSQL.Replace("@Color", color[0].Trim());
                        }
                        break;
                    case "Shopee":
                        {
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Shopee', 1, 0, 1, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Shopee',
DAY(CreatedOn),
SUM(TotalMoney) - SUM(dbo.SAL_OrderStandCost(OrderKey)),
1, '@Color'
FROM SAL_Order
WHERE RecordStatus <> 99
AND Slug = 1
AND OrderStatus = 2
AND PartnerNumber = @PartnerNumber 
AND (CreatedOn BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(CreatedOn)";
                            zSQL = zSQL.Replace("@Color", color[1].Trim());
                        }
                        break;

                    case "Lazada":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Lazada', 1, 0, 2, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[2].Trim());
                        }
                        break;
                    case "Tiki":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Tiki', 1, 0, 3, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[3].Trim());
                        }
                        break;
                    case "Khác":
                        {
                            zSQL += "INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Khác', 1, 0, 4, '@Color')";
                            zSQL = zSQL.Replace("@Color", color[4].Trim());
                        }
                        break;
                    case "Offline":
                        {
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Offline', 1, 0, 5, '@Color')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Offline',
DAY(CreatedOn),
SUM(TotalMoney) - SUM(dbo.SAL_OrderStandCost(OrderKey)),
5, '@Color'
FROM SAL_Order
WHERE RecordStatus <> 99
AND Slug = 11   -- tại cửa hàng
AND PartnerNumber = @PartnerNumber 
AND (CreatedOn BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(CreatedOn)";
                            zSQL = zSQL.Replace("@Color", color[5].Trim());
                        }
                        break;
                }
            }

            zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total, Color, [Rank]
	FROM #temp
) X
PIVOT 
( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY [Rank] DROP TABLE #temp";

            DataTable zTable = new();
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
        //Số lượng sản phẩm bán
        public static DataTable ProductTop(string PartnerNumber, DateTime FromDate, DateTime ToDate, string CategoryKey, out string Message)
        {
            CategoryKey = CategoryKey.Replace("'", "");
            string zFilter = "";
            if (CategoryKey != "")
            {
                zFilter += " AND C.CategoryKey IN (  " + CategoryKey + " ) ";
            }
            string zSQL = @"SELECT A.ItemKey,A.ItemName,SUM(ItemQty) AS Qty FROM [dbo].[SAL_Order_Item] A
LEFT JOIN [dbo].[SAL_Order] B ON B.OrderKey=A.OrderKey
LEFT JOIN [dbo].[PDT_Product] C ON C.ProductKey =A.ItemKey
WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99 AND C.RecordStatus <> 99  AND B.OrderStatus <> 9 --HuyDon
AND A.PartnerNumber = @PartnerNumber AND B.Slug <> 9 --Sản xuất
AND B.OrderDate BETWEEN @FromDate AND @ToDate @Filter
GROUP BY A.ItemKey,A.ItemName
ORDER BY Qty DESC ";
            zSQL = zSQL.Replace("@Filter", zFilter);
            DataTable zTable = new();
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.NVarChar).Value = CategoryKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
        //Số lượng sản phẩm bán theo nhóm
        public static DataTable ProductGroupTop(string PartnerNumber, DateTime FromDate, DateTime ToDate, string CategoryKey, out string Message)
        {
            CategoryKey = CategoryKey.Replace("'", "");
            string zFilter = "";
            if (CategoryKey != "")
            {
                zFilter += " AND C.CategoryKey IN ( " + CategoryKey + " ) ";
            }

            string zSQL = @"SELECT 
(SELECT H.ProductName FROM [dbo].[PDT_Product] H WHERE H.RecordStatus <> 99 AND CAST(H.ProductKey AS Nvarchar(50)) = X.ProductModel AND H.ProductModel IS NOT NULL) AS ProductName,
Qty FROM (
		SELECT C.ProductModel AS ProductModel,SUM(A.ItemQty) AS Qty FROM [dbo].[SAL_Order_Item] A
		LEFT JOIN [dbo].[SAL_Order] B ON B.OrderKey=A.OrderKey
		LEFT JOIN [dbo].[PDT_Product] C ON C.ProductKey =A.ItemKey
		WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99 AND C.RecordStatus <> 99 AND B.OrderStatus <> 9 --HuyDon
        AND A.PartnerNumber = @PartnerNumber AND B.Slug <> 9 --Sản xuất
		AND B.OrderDate BETWEEN @FromDate AND @ToDate @Filter
		GROUP BY  C.ProductModel
)X

ORDER BY Qty DESC";
            zSQL = zSQL.Replace("@Filter", zFilter);
            DataTable zTable = new();
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.NVarChar).Value = CategoryKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
        //Danh sách người mua hàng
        public static List<Order_Model> List_Buyer(string PartnerNumber, string Filter, int Option, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT BuyerPhone,BuyerName,
CASE 
WHEN BuyerAddress !='' THEN BuyerAddress
ELSE ShippingAddress
END AS BuyerAddress,
COUNT(BuyerPhone) AS SoLan, SUM(TotalMoney) AS ToTalMoney from [dbo].[SAL_Order]
WHERE RecordStatus <> 99 AND OrderStatus <> 9 AND Slug <> 9
AND LEN(BuyerPhone) >8 AND TotalMoney > 0  @Paramater 
GROUP BY BuyerPhone,BuyerName,BuyerAddress,ShippingAddress 
";
            string Paramater = "";
            if (Filter != string.Empty)
            {
                Paramater += " AND(BuyerPhone LIKE @Filter OR BuyerName LIKE @Filter)";
            }
            if (Option == 0)
            {
                zSQL += "ORDER BY TotalMoney DESC";
            }
            else
            {
                zSQL += "ORDER BY SoLan DESC";
            }
            zSQL = zSQL.Replace("@Paramater", Paramater);
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Filter", SqlDbType.NVarChar).Value = "%" + Filter + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    BuyerAddress = r["BuyerAddress"].ToString(),
                    TotalItems = r["SoLan"].ToFloat(),
                    TotalMoney = r["TotalMoney"].ToDouble()
                });
            }
            return zList;
        }
        //Tổng tiền nhập hàng
        public static DataTable NhapHang(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {

            string zSQL = @"
SELECT 
MONTH(A.CreatedOn),
SUM(B.ItemPrice * B.ItemQty)
FROM SAL_Purchase A
LEFT JOIN SAL_Purchase_Item B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND A.Slug IN (101)
AND A.CreatedOn BETWEEN @FromDate AND @ToDate
AND B.RecordStatus <> 99
GROUP BY MONTH(A.CreatedOn)";
            DataTable zTable = new();
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
        //Tổng tiền doanh số
        public static DataTable Doanhso(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {

            string zSQL = @"
SELECT 
MONTH(A.CreatedOn),
SUM(B.ItemPrice * B.ItemQty)
FROM SAL_Order A
LEFT JOIN SAL_Order_Item B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND A.Slug IN (202, 402)
AND A.CreatedOn BETWEEN @FromDate AND @ToDate
AND B.RecordStatus <> 99
GROUP BY MONTH(A.CreatedOn)";
            DataTable zTable = new();
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
    }
}