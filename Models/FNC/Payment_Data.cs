﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Payment_Data
    {
        public static List<Payment_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Payment WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY PaymentDate DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Payment_Model> zList = new List<Payment_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payment_Model()
                {
                    PaymentKey = r["PaymentKey"].ToString(),
                    PaymentID = r["PaymentID"].ToString(),
                    PaymentDate = (r["PaymentDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["PaymentDate"]),
                    PaymentDescription = r["PaymentDescription"].ToString(),
                    DocumentID = r["DocumentID"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    StyleKey = r["StyleKey"].ToInt(),
                    StyleName = r["StyleName"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    Receiver = r["Receiver"].ToString(),
                    ReceiverName = r["ReceiverName"].ToString(),
                    Address = r["Address"].ToString(),
                    AmountCurrencyMain = r["AmountCurrencyMain"].ToDouble(),
                    AmountCurrencyForeign = r["AmountCurrencyForeign"].ToDouble(),
                    CurrencyIDForeign = r["CurrencyIDForeign"].ToString(),
                    CurrencyRate = r["CurrencyRate"].ToDouble(),
                    BankName = r["BankName"].ToString(),
                    BankAccount = r["BankAccount"].ToString(),
                    BankVAT = r["BankVAT"].ToDouble(),
                    BankFee = r["BankFee"].ToDouble(),
                    IsFeeInside = r["IsFeeInside"].ToBool(),
                    Slug = r["Slug"].ToInt(),
                    DebitNo = r["DebitNo"].ToString(),
                    CreditNo = r["CreditNo"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static string Auto_Payment_ID(string PartnerNumber)
        {
            string zResult = "";
            string zSQL = @"
                DECLARE @Today nvarchar(50)
                DECLARE @TodayOld NVARCHAR(50)
                DECLARE @AssetIDNew nvarchar(50)
                --Tháng hiện tại
                SET @Today = LEFT(CONVERT(nvarchar, getdate(), 12), 4)
                SET @Today = REPLACE(@Today, '.', '');
                --Tháng cũ
                SELECT @TodayOld = ISNULL(MAX(LEFT(PaymentID, 5)), 0000)  FROM dbo.FNC_Payment
                WHERE LEN(PaymentID) = 10  AND RecordStatus !=99 AND PartnerNumber=@PartnerNumber 
                --Lấy 4 số cuối + 1
                SELECT @AssetIDNew = ISNULL(MAX(RIGHT(PaymentID, 4)) + 1, 1)  FROM FNC_Payment
                WHERE LEN(PaymentID) = 10 
                AND ISNUMERIC(RIGHT(PaymentID,4)) = 1 
                AND RecordStatus!=99 
                AND PartnerNumber=@PartnerNumber
                SELECT 'P'+@Today + '-' + RIGHT('000' + @AssetIDNew, 4)
";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
    }
}
