﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Receipt_Info
    {
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public Receipt_Model Receipt = new Receipt_Model();

        #region [ Constructor Get Information ]
        public Receipt_Info()
        {
        }
        public Receipt_Info(string ReceiptKey)
        {
            string zSQL = "SELECT * FROM FNC_Receipt WHERE ReceiptKey = @ReceiptKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString; ;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ReceiptKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Receipt.ReceiptKey = zReader["ReceiptKey"].ToString();
                    Receipt.ReceiptID = zReader["ReceiptID"].ToString();
                    if (zReader["ReceiptDate"] != DBNull.Value)
                        Receipt.ReceiptDate = (DateTime)zReader["ReceiptDate"];
                    Receipt.ReceiptDescription = zReader["ReceiptDescription"].ToString();
                    Receipt.DocumentID = zReader["DocumentID"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        Receipt.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    Receipt.CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["StyleKey"] != DBNull.Value)
                        Receipt.StyleKey = int.Parse(zReader["StyleKey"].ToString());
                    Receipt.StyleName = zReader["StyleName"].ToString();
                    Receipt.CustomerKey = zReader["CustomerKey"].ToString();
                    Receipt.CustomerName = zReader["CustomerName"].ToString();
                    Receipt.CustomerPhone = zReader["CustomerPhone"].ToString();
                    Receipt.Depositor = zReader["Depositor"].ToString();
                    Receipt.DepositorName = zReader["DepositorName"].ToString();
                    Receipt.Address = zReader["Address"].ToString();
                    if (zReader["AmountCurrencyMain"] != DBNull.Value)
                        Receipt.AmountCurrencyMain = double.Parse(zReader["AmountCurrencyMain"].ToString());
                    if (zReader["AmountCurrencyForeign"] != DBNull.Value)
                        Receipt.AmountCurrencyForeign = double.Parse(zReader["AmountCurrencyForeign"].ToString());
                    Receipt.CurrencyIDForeign = zReader["CurrencyIDForeign"].ToString();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                        Receipt.CurrencyRate = double.Parse(zReader["CurrencyRate"].ToString());
                    Receipt.BankName = zReader["BankName"].ToString();
                    Receipt.BankAccount = zReader["BankAccount"].ToString();
                    if (zReader["BankVAT"] != DBNull.Value)
                        Receipt.BankVAT = double.Parse(zReader["BankVAT"].ToString());
                    if (zReader["BankFee"] != DBNull.Value)
                        Receipt.BankFee = double.Parse(zReader["BankFee"].ToString());
                    if (zReader["IsFeeInside"] != DBNull.Value)
                        Receipt.IsFeeInside = (bool)zReader["IsFeeInside"];
                    if (zReader["Slug"] != DBNull.Value)
                        Receipt.Slug = int.Parse(zReader["Slug"].ToString());
                    Receipt.DebitNo = zReader["DebitNo"].ToString();
                    Receipt.CreditNo = zReader["CreditNo"].ToString();
                    Receipt.OrganizationID = zReader["OrganizationID"].ToString();
                    Receipt.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Receipt.BranchKey = zReader["BranchKey"].ToString();
                    Receipt.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Receipt.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Receipt.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Receipt.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Receipt.CreatedBy = zReader["CreatedBy"].ToString();
                    Receipt.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Receipt.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Receipt.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Receipt.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Receipt ("
         + " ReceiptID , ReceiptDate , ReceiptDescription , DocumentID , CategoryKey , CategoryName , StyleKey , StyleName , CustomerKey , CustomerName , CustomerPhone , Depositor , DepositorName , Address , AmountCurrencyMain , AmountCurrencyForeign , CurrencyIDForeign , CurrencyRate , BankName , BankAccount , BankVAT , BankFee , IsFeeInside , Slug , DebitNo , CreditNo , OrganizationID , DepartmentKey , BranchKey , EmployeeKey , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ReceiptID , @ReceiptDate , @ReceiptDescription , @DocumentID , @CategoryKey , @CategoryName , @StyleKey , @StyleName , @CustomerKey , @CustomerName , @CustomerPhone , @Depositor , @DepositorName , @Address , @AmountCurrencyMain , @AmountCurrencyForeign , @CurrencyIDForeign , @CurrencyRate , @BankName , @BankAccount , @BankVAT , @BankFee , @IsFeeInside , @Slug , @DebitNo , @CreditNo , @OrganizationID , @DepartmentKey , @BranchKey , @EmployeeKey , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString; ;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = Receipt.ReceiptID;

                zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = Receipt.ReceiptDate;
                zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = Receipt.ReceiptDescription;
                zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = Receipt.DocumentID;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Receipt.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Receipt.CategoryName;
                zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = Receipt.StyleKey;
                zCommand.Parameters.Add("@StyleName", SqlDbType.NVarChar).Value = Receipt.StyleName;
                if (Receipt.CustomerKey != "" && Receipt.CustomerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Receipt.CustomerKey);
                }
                else
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Receipt.CustomerName;
                zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = Receipt.CustomerPhone;
                zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = Receipt.Depositor;
                zCommand.Parameters.Add("@DepositorName", SqlDbType.NVarChar).Value = Receipt.DepositorName;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Receipt.Address;
                zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = Receipt.AmountCurrencyMain;
                zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = Receipt.AmountCurrencyForeign;
                zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = Receipt.CurrencyIDForeign;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Receipt.CurrencyRate;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Receipt.BankName;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Receipt.BankAccount;
                zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = Receipt.BankVAT;
                zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = Receipt.BankFee;

                zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = Receipt.IsFeeInside;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Receipt.Slug;
                zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = Receipt.DebitNo;
                zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = Receipt.CreditNo;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Receipt.OrganizationID;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Receipt.DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Receipt.BranchKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Receipt.EmployeeKey;
                if (Receipt.PartnerNumber != "" && Receipt.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Receipt.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Receipt.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Receipt.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Receipt.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Receipt.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Receipt.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Receipt("
         + " ReceiptKey , ReceiptID , ReceiptDate , ReceiptDescription , DocumentID , CategoryKey , CategoryName , StyleKey , StyleName , CustomerKey , CustomerName , CustomerPhone , Depositor , DepositorName , Address , AmountCurrencyMain , AmountCurrencyForeign , CurrencyIDForeign , CurrencyRate , BankName , BankAccount , BankVAT , BankFee , IsFeeInside , Slug , DebitNo , CreditNo , OrganizationID , DepartmentKey , BranchKey , EmployeeKey , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ReceiptKey , @ReceiptID , @ReceiptDate , @ReceiptDescription , @DocumentID , @CategoryKey , @CategoryName , @StyleKey , @StyleName , @CustomerKey , @CustomerName , @CustomerPhone , @Depositor , @DepositorName , @Address , @AmountCurrencyMain , @AmountCurrencyForeign , @CurrencyIDForeign , @CurrencyRate , @BankName , @BankAccount , @BankVAT , @BankFee , @IsFeeInside , @Slug , @DebitNo , @CreditNo , @OrganizationID , @DepartmentKey , @BranchKey , @EmployeeKey , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString; ;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                Receipt.ReceiptKey = Guid.NewGuid().ToString();
                if (Receipt.ReceiptKey != "" && Receipt.ReceiptKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Receipt.ReceiptKey);
                }
                else
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = Receipt.ReceiptID;
                if (Receipt.ReceiptDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = Receipt.ReceiptDate;
                zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = Receipt.ReceiptDescription;
                zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = Receipt.DocumentID;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Receipt.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Receipt.CategoryName;
                zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = Receipt.StyleKey;
                zCommand.Parameters.Add("@StyleName", SqlDbType.NVarChar).Value = Receipt.StyleName;
                if (Receipt.CustomerKey != "" && Receipt.CustomerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Receipt.CustomerKey);
                }
                else
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Receipt.CustomerName;
                zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = Receipt.CustomerPhone;
                zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = Receipt.Depositor;
                zCommand.Parameters.Add("@DepositorName", SqlDbType.NVarChar).Value = Receipt.DepositorName;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Receipt.Address;
                zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = Receipt.AmountCurrencyMain;
                zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = Receipt.AmountCurrencyForeign;
                zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = Receipt.CurrencyIDForeign;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Receipt.CurrencyRate;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Receipt.BankName;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Receipt.BankAccount;
                zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = Receipt.BankVAT;
                zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = Receipt.BankFee;

                zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = Receipt.IsFeeInside;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Receipt.Slug;
                zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = Receipt.DebitNo;
                zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = Receipt.CreditNo;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Receipt.OrganizationID;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Receipt.DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Receipt.BranchKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Receipt.EmployeeKey;
                if (Receipt.PartnerNumber != "" && Receipt.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Receipt.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Receipt.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Receipt.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Receipt.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Receipt.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Receipt.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE FNC_Receipt SET "
                        + " ReceiptID = @ReceiptID,"
                        + " ReceiptDate = @ReceiptDate,"
                        + " ReceiptDescription = @ReceiptDescription,"
                        + " DocumentID = @DocumentID,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " StyleKey = @StyleKey,"
                        + " StyleName = @StyleName,"
                        + " CustomerKey = @CustomerKey,"
                        + " CustomerName = @CustomerName,"
                        + " CustomerPhone = @CustomerPhone,"
                        + " Depositor = @Depositor,"
                        + " DepositorName = @DepositorName,"
                        + " Address = @Address,"
                        + " AmountCurrencyMain = @AmountCurrencyMain,"
                        + " AmountCurrencyForeign = @AmountCurrencyForeign,"
                        + " CurrencyIDForeign = @CurrencyIDForeign,"
                        + " CurrencyRate = @CurrencyRate,"
                        + " BankName = @BankName,"
                        + " BankAccount = @BankAccount,"
                        + " BankVAT = @BankVAT,"
                        + " BankFee = @BankFee,"
                        + " IsFeeInside = @IsFeeInside,"
                        + " Slug = @Slug,"
                        + " DebitNo = @DebitNo,"
                        + " CreditNo = @CreditNo,"
                        + " OrganizationID = @OrganizationID,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " BranchKey = @BranchKey,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ReceiptKey = @ReceiptKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString; ;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Receipt.ReceiptKey != "" && Receipt.ReceiptKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Receipt.ReceiptKey);
                }
                else
                    zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = Receipt.ReceiptID;
                if (Receipt.ReceiptDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = Receipt.ReceiptDate;
                zCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NVarChar).Value = Receipt.ReceiptDescription;
                zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = Receipt.DocumentID;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Receipt.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Receipt.CategoryName;
                zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = Receipt.StyleKey;
                zCommand.Parameters.Add("@StyleName", SqlDbType.NVarChar).Value = Receipt.StyleName;
                if (Receipt.CustomerKey != "" && Receipt.CustomerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Receipt.CustomerKey);
                }
                else
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Receipt.CustomerName;
                zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = Receipt.CustomerPhone;
                zCommand.Parameters.Add("@Depositor", SqlDbType.NVarChar).Value = Receipt.Depositor;
                zCommand.Parameters.Add("@DepositorName", SqlDbType.NVarChar).Value = Receipt.DepositorName;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Receipt.Address;
                zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = Receipt.AmountCurrencyMain;
                zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = Receipt.AmountCurrencyForeign;
                zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = Receipt.CurrencyIDForeign;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Receipt.CurrencyRate;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Receipt.BankName;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Receipt.BankAccount;
                zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = Receipt.BankVAT;
                zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = Receipt.BankFee;

                zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = Receipt.IsFeeInside;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Receipt.Slug;
                zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = Receipt.DebitNo;
                zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = Receipt.CreditNo;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Receipt.OrganizationID;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Receipt.DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Receipt.BranchKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Receipt.EmployeeKey;
                if (Receipt.PartnerNumber != "" && Receipt.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Receipt.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Receipt.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Receipt.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Receipt.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Receipt SET RecordStatus = 99 WHERE ReceiptKey = @ReceiptKey";
            string zConnectionString = TN_Helper.ConnectionString; ;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Receipt.ReceiptKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Receipt WHERE ReceiptKey = @ReceiptKey";
            string zConnectionString = TN_Helper.ConnectionString; ;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReceiptKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Receipt.ReceiptKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
