﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Receipt_Model
    {
        #region [ Field Name ]
        private string _ReceiptKey = "";
        private string _ReceiptID = "";
        private DateTime _ReceiptDate = DateTime.MinValue;
        private string _ReceiptDescription = "";
        private string _DocumentID = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _StyleKey = 0;
        private string _StyleName = "";
        private string _CustomerKey = "";
        private string _CustomerName = "";
        private string _CustomerPhone = "";
        private string _Depositor = "";
        private string _DepositorName = "";
        private string _Address = "";
        private double _AmountCurrencyMain = 0;
        private double _AmountCurrencyForeign = 0;
        private string _CurrencyIDForeign = "";
        private double _CurrencyRate = 0;
        private string _BankName = "";
        private string _BankAccount = "";
        private double _BankVAT = 0;
        private double _BankFee = 0;
        private bool _IsFeeInside;
        private int _Slug = 0;
        private string _DebitNo = "";
        private string _CreditNo = "";
        private string _OrganizationID = "";
        private string _DepartmentKey = "";
        private string _BranchKey = "";
        private string _EmployeeKey = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public string ReceiptKey
        {
            get { return _ReceiptKey; }
            set { _ReceiptKey = value; }
        }
        public string ReceiptID
        {
            get { return _ReceiptID; }
            set { _ReceiptID = value; }
        }
        public DateTime ReceiptDate
        {
            get { return _ReceiptDate; }
            set { _ReceiptDate = value; }
        }
        public string ReceiptDescription
        {
            get { return _ReceiptDescription; }
            set { _ReceiptDescription = value; }
        }
        public string DocumentID
        {
            get { return _DocumentID; }
            set { _DocumentID = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int StyleKey
        {
            get { return _StyleKey; }
            set { _StyleKey = value; }
        }
        public string StyleName
        {
            get { return _StyleName; }
            set { _StyleName = value; }
        }
        public string CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }
        public string CustomerPhone
        {
            get { return _CustomerPhone; }
            set { _CustomerPhone = value; }
        }
        public string Depositor
        {
            get { return _Depositor; }
            set { _Depositor = value; }
        }
        public string DepositorName
        {
            get { return _DepositorName; }
            set { _DepositorName = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public double AmountCurrencyMain
        {
            get { return _AmountCurrencyMain; }
            set { _AmountCurrencyMain = value; }
        }
        public double AmountCurrencyForeign
        {
            get { return _AmountCurrencyForeign; }
            set { _AmountCurrencyForeign = value; }
        }
        public string CurrencyIDForeign
        {
            get { return _CurrencyIDForeign; }
            set { _CurrencyIDForeign = value; }
        }
        public double CurrencyRate
        {
            get { return _CurrencyRate; }
            set { _CurrencyRate = value; }
        }
        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }
        public string BankAccount
        {
            get { return _BankAccount; }
            set { _BankAccount = value; }
        }
        public double BankVAT
        {
            get { return _BankVAT; }
            set { _BankVAT = value; }
        }
        public double BankFee
        {
            get { return _BankFee; }
            set { _BankFee = value; }
        }
        public bool IsFeeInside
        {
            get { return _IsFeeInside; }
            set { _IsFeeInside = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string DebitNo
        {
            get { return _DebitNo; }
            set { _DebitNo = value; }
        }
        public string CreditNo
        {
            get { return _CreditNo; }
            set { _CreditNo = value; }
        }
        public string OrganizationID
        {
            get { return _OrganizationID; }
            set { _OrganizationID = value; }
        }
        public string DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
    }
}
