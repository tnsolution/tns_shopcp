﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Payment_Info
    {

        public Payment_Model Payment = new Payment_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Payment_Info()
        {
            Payment.PaymentKey = Guid.NewGuid().ToString();
        }
        public Payment_Info(string PaymentKey)
        {
            string zSQL = "SELECT * FROM FNC_Payment WHERE PaymentKey = @PaymentKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PaymentKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Payment.PaymentKey = zReader["PaymentKey"].ToString();
                    Payment.PaymentID = zReader["PaymentID"].ToString();
                    if (zReader["PaymentDate"] != DBNull.Value)
                    {
                        Payment.PaymentDate = (DateTime)zReader["PaymentDate"];
                    }

                    Payment.PaymentDescription = zReader["PaymentDescription"].ToString();
                    Payment.DocumentID = zReader["DocumentID"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                    {
                        Payment.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    }

                    Payment.CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["StyleKey"] != DBNull.Value)
                    {
                        Payment.StyleKey = int.Parse(zReader["StyleKey"].ToString());
                    }

                    Payment.StyleName = zReader["StyleName"].ToString();
                    Payment.CustomerKey = zReader["CustomerKey"].ToString();
                    Payment.CustomerName = zReader["CustomerName"].ToString();
                    Payment.CustomerPhone = zReader["CustomerPhone"].ToString();
                    Payment.Receiver = zReader["Receiver"].ToString();
                    Payment.ReceiverName = zReader["ReceiverName"].ToString();
                    Payment.Address = zReader["Address"].ToString();
                    if (zReader["AmountCurrencyMain"] != DBNull.Value)
                    {
                        Payment.AmountCurrencyMain = double.Parse(zReader["AmountCurrencyMain"].ToString());
                    }

                    if (zReader["AmountCurrencyForeign"] != DBNull.Value)
                    {
                        Payment.AmountCurrencyForeign = double.Parse(zReader["AmountCurrencyForeign"].ToString());
                    }

                    Payment.CurrencyIDForeign = zReader["CurrencyIDForeign"].ToString();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                    {
                        Payment.CurrencyRate = double.Parse(zReader["CurrencyRate"].ToString());
                    }

                    Payment.BankName = zReader["BankName"].ToString();
                    Payment.BankAccount = zReader["BankAccount"].ToString();
                    if (zReader["BankVAT"] != DBNull.Value)
                    {
                        Payment.BankVAT = double.Parse(zReader["BankVAT"].ToString());
                    }

                    if (zReader["BankFee"] != DBNull.Value)
                    {
                        Payment.BankFee = double.Parse(zReader["BankFee"].ToString());
                    }

                    if (zReader["IsFeeInside"] != DBNull.Value)
                    {
                        Payment.IsFeeInside = (bool)zReader["IsFeeInside"];
                    }

                    if (zReader["Slug"] != DBNull.Value)
                    {
                        Payment.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    Payment.DebitNo = zReader["DebitNo"].ToString();
                    Payment.CreditNo = zReader["CreditNo"].ToString();
                    Payment.OrganizationID = zReader["OrganizationID"].ToString();
                    Payment.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Payment.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Payment.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Payment.CreatedBy = zReader["CreatedBy"].ToString();
                    Payment.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Payment.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Payment.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Payment.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Payment ("
         + " PaymentID , PaymentDate , PaymentDescription , DocumentID , CategoryKey , CategoryName , StyleKey , StyleName , CustomerKey , CustomerName , CustomerPhone , Receiver , ReceiverName , Address , AmountCurrencyMain , AmountCurrencyForeign , CurrencyIDForeign , CurrencyRate , BankName , BankAccount , BankVAT , BankFee , IsFeeInside , Slug , DebitNo , CreditNo , OrganizationID , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PaymentID , @PaymentDate , @PaymentDescription , @DocumentID , @CategoryKey , @CategoryName , @StyleKey , @StyleName , @CustomerKey , @CustomerName , @CustomerPhone , @Receiver , @ReceiverName , @Address , @AmountCurrencyMain , @AmountCurrencyForeign , @CurrencyIDForeign , @CurrencyRate , @BankName , @BankAccount , @BankVAT , @BankFee , @IsFeeInside , @Slug , @DebitNo , @CreditNo , @OrganizationID , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = Payment.PaymentID;
                if (Payment.PaymentDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = Payment.PaymentDate;
                }

                zCommand.Parameters.Add("@PaymentDescription", SqlDbType.NVarChar).Value = Payment.PaymentDescription;
                zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = Payment.DocumentID;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Payment.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Payment.CategoryName;
                zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = Payment.StyleKey;
                zCommand.Parameters.Add("@StyleName", SqlDbType.NVarChar).Value = Payment.StyleName;
                if (Payment.CustomerKey != "" && Payment.CustomerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payment.CustomerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Payment.CustomerName;
                zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = Payment.CustomerPhone;
                zCommand.Parameters.Add("@Receiver", SqlDbType.NVarChar).Value = Payment.Receiver;
                zCommand.Parameters.Add("@ReceiverName", SqlDbType.NVarChar).Value = Payment.ReceiverName;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Payment.Address;
                zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = Payment.AmountCurrencyMain;
                zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = Payment.AmountCurrencyForeign;
                zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = Payment.CurrencyIDForeign;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Payment.CurrencyRate;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Payment.BankName;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Payment.BankAccount;
                zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = Payment.BankVAT;
                zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = Payment.BankFee;
                zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = Payment.IsFeeInside;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Payment.Slug;
                zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = Payment.DebitNo;
                zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = Payment.CreditNo;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Payment.OrganizationID;
                if (Payment.PartnerNumber != "" && Payment.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payment.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payment.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payment.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payment.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payment.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payment.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Payment("
         + " PaymentKey , PaymentID , PaymentDate , PaymentDescription , DocumentID , CategoryKey , CategoryName , StyleKey , StyleName , CustomerKey , CustomerName , CustomerPhone , Receiver , ReceiverName , Address , AmountCurrencyMain , AmountCurrencyForeign , CurrencyIDForeign , CurrencyRate , BankName , BankAccount , BankVAT , BankFee , IsFeeInside , Slug , DebitNo , CreditNo , OrganizationID , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PaymentKey , @PaymentID , @PaymentDate , @PaymentDescription , @DocumentID , @CategoryKey , @CategoryName , @StyleKey , @StyleName , @CustomerKey , @CustomerName , @CustomerPhone , @Receiver , @ReceiverName , @Address , @AmountCurrencyMain , @AmountCurrencyForeign , @CurrencyIDForeign , @CurrencyRate , @BankName , @BankAccount , @BankVAT , @BankFee , @IsFeeInside , @Slug , @DebitNo , @CreditNo , @OrganizationID , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Payment.PaymentKey != "" && Payment.PaymentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payment.PaymentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = Payment.PaymentID;
                if (Payment.PaymentDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = Payment.PaymentDate;
                }

                zCommand.Parameters.Add("@PaymentDescription", SqlDbType.NVarChar).Value = Payment.PaymentDescription;
                zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = Payment.DocumentID;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Payment.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Payment.CategoryName;
                zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = Payment.StyleKey;
                zCommand.Parameters.Add("@StyleName", SqlDbType.NVarChar).Value = Payment.StyleName;
                if (Payment.CustomerKey != "" && Payment.CustomerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payment.CustomerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Payment.CustomerName;
                zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = Payment.CustomerPhone;
                zCommand.Parameters.Add("@Receiver", SqlDbType.NVarChar).Value = Payment.Receiver;
                zCommand.Parameters.Add("@ReceiverName", SqlDbType.NVarChar).Value = Payment.ReceiverName;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Payment.Address;
                zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = Payment.AmountCurrencyMain;
                zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = Payment.AmountCurrencyForeign;
                zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = Payment.CurrencyIDForeign;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Payment.CurrencyRate;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Payment.BankName;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Payment.BankAccount;
                zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = Payment.BankVAT;
                zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = Payment.BankFee;
                zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = Payment.IsFeeInside;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Payment.Slug;
                zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = Payment.DebitNo;
                zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = Payment.CreditNo;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Payment.OrganizationID;
                if (Payment.PartnerNumber != "" && Payment.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payment.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payment.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payment.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payment.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payment.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payment.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE FNC_Payment SET "
                        + " PaymentID = @PaymentID,"
                        + " PaymentDate = @PaymentDate,"
                        + " PaymentDescription = @PaymentDescription,"
                        + " DocumentID = @DocumentID,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " StyleKey = @StyleKey,"
                        + " StyleName = @StyleName,"
                        + " CustomerKey = @CustomerKey,"
                        + " CustomerName = @CustomerName,"
                        + " CustomerPhone = @CustomerPhone,"
                        + " Receiver = @Receiver,"
                        + " ReceiverName = @ReceiverName,"
                        + " Address = @Address,"
                        + " AmountCurrencyMain = @AmountCurrencyMain,"
                        + " AmountCurrencyForeign = @AmountCurrencyForeign,"
                        + " CurrencyIDForeign = @CurrencyIDForeign,"
                        + " CurrencyRate = @CurrencyRate,"
                        + " BankName = @BankName,"
                        + " BankAccount = @BankAccount,"
                        + " BankVAT = @BankVAT,"
                        + " BankFee = @BankFee,"
                        + " IsFeeInside = @IsFeeInside,"
                        + " Slug = @Slug,"
                        + " DebitNo = @DebitNo,"
                        + " CreditNo = @CreditNo,"
                        + " OrganizationID = @OrganizationID,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE PaymentKey = @PaymentKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Payment.PaymentKey != "" && Payment.PaymentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payment.PaymentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = Payment.PaymentID;
                if (Payment.PaymentDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = Payment.PaymentDate;
                }

                zCommand.Parameters.Add("@PaymentDescription", SqlDbType.NVarChar).Value = Payment.PaymentDescription;
                zCommand.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = Payment.DocumentID;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Payment.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Payment.CategoryName;
                zCommand.Parameters.Add("@StyleKey", SqlDbType.Int).Value = Payment.StyleKey;
                zCommand.Parameters.Add("@StyleName", SqlDbType.NVarChar).Value = Payment.StyleName;
                if (Payment.CustomerKey != "" && Payment.CustomerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payment.CustomerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Payment.CustomerName;
                zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = Payment.CustomerPhone;
                zCommand.Parameters.Add("@Receiver", SqlDbType.NVarChar).Value = Payment.Receiver;
                zCommand.Parameters.Add("@ReceiverName", SqlDbType.NVarChar).Value = Payment.ReceiverName;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Payment.Address;
                zCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = Payment.AmountCurrencyMain;
                zCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = Payment.AmountCurrencyForeign;
                zCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = Payment.CurrencyIDForeign;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Payment.CurrencyRate;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Payment.BankName;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Payment.BankAccount;
                zCommand.Parameters.Add("@BankVAT", SqlDbType.Money).Value = Payment.BankVAT;
                zCommand.Parameters.Add("@BankFee", SqlDbType.Money).Value = Payment.BankFee;
                zCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = Payment.IsFeeInside;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Payment.Slug;
                zCommand.Parameters.Add("@DebitNo", SqlDbType.NVarChar).Value = Payment.DebitNo;
                zCommand.Parameters.Add("@CreditNo", SqlDbType.NVarChar).Value = Payment.CreditNo;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Payment.OrganizationID;
                if (Payment.PartnerNumber != "" && Payment.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payment.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payment.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payment.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payment.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Payment SET RecordStatus = 99 WHERE PaymentKey = @PaymentKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payment.PaymentKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Payment WHERE PaymentKey = @PaymentKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PaymentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payment.PaymentKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
