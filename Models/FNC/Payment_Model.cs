﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Payment_Model
    {
        #region [ Field Name ]
        private string _PaymentKey = "";
        private string _PaymentID = "";
        private DateTime _PaymentDate = DateTime.MinValue;
        private string _PaymentDescription = "";
        private string _DocumentID = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _StyleKey = 0;
        private string _StyleName = "";
        private string _CustomerKey = "";
        private string _CustomerName = "";
        private string _CustomerPhone = "";
        private string _Receiver = "";
        private string _ReceiverName = "";
        private string _Address = "";
        private double _AmountCurrencyMain = 0;
        private double _AmountCurrencyForeign = 0;
        private string _CurrencyIDForeign = "";
        private double _CurrencyRate = 0;
        private string _BankName = "";
        private string _BankAccount = "";
        private double _BankVAT = 0;
        private double _BankFee = 0;
        private bool _IsFeeInside;
        private int _Slug = 0;
        private string _DebitNo = "";
        private string _CreditNo = "";
        private string _OrganizationID = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string PaymentKey
        {
            get { return _PaymentKey; }
            set { _PaymentKey = value; }
        }
        public string PaymentID
        {
            get { return _PaymentID; }
            set { _PaymentID = value; }
        }
        public DateTime PaymentDate
        {
            get { return _PaymentDate; }
            set { _PaymentDate = value; }
        }
        public string PaymentDescription
        {
            get { return _PaymentDescription; }
            set { _PaymentDescription = value; }
        }
        public string DocumentID
        {
            get { return _DocumentID; }
            set { _DocumentID = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int StyleKey
        {
            get { return _StyleKey; }
            set { _StyleKey = value; }
        }
        public string StyleName
        {
            get { return _StyleName; }
            set { _StyleName = value; }
        }
        public string CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }
        public string CustomerPhone
        {
            get { return _CustomerPhone; }
            set { _CustomerPhone = value; }
        }
        public string Receiver
        {
            get { return _Receiver; }
            set { _Receiver = value; }
        }
        public string ReceiverName
        {
            get { return _ReceiverName; }
            set { _ReceiverName = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public double AmountCurrencyMain
        {
            get { return _AmountCurrencyMain; }
            set { _AmountCurrencyMain = value; }
        }
        public double AmountCurrencyForeign
        {
            get { return _AmountCurrencyForeign; }
            set { _AmountCurrencyForeign = value; }
        }
        public string CurrencyIDForeign
        {
            get { return _CurrencyIDForeign; }
            set { _CurrencyIDForeign = value; }
        }
        public double CurrencyRate
        {
            get { return _CurrencyRate; }
            set { _CurrencyRate = value; }
        }
        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }
        public string BankAccount
        {
            get { return _BankAccount; }
            set { _BankAccount = value; }
        }
        public double BankVAT
        {
            get { return _BankVAT; }
            set { _BankVAT = value; }
        }
        public double BankFee
        {
            get { return _BankFee; }
            set { _BankFee = value; }
        }
        public bool IsFeeInside
        {
            get { return _IsFeeInside; }
            set { _IsFeeInside = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string DebitNo
        {
            get { return _DebitNo; }
            set { _DebitNo = value; }
        }
        public string CreditNo
        {
            get { return _CreditNo; }
            set { _CreditNo = value; }
        }
        public string OrganizationID
        {
            get { return _OrganizationID; }
            set { _OrganizationID = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
