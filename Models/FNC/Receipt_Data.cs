﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Receipt_Data
    {
        public static DataTable List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM FNC_Receipt WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = TN_Helper.ConnectionString; 
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static string Auto_Receipt_ID(string PartnerNumber)
        {
            string zResult = "";
            string zSQL = @"
                DECLARE @Today nvarchar(50)
                DECLARE @TodayOld NVARCHAR(50)
                DECLARE @AssetIDNew nvarchar(50)
                --Tháng hiện tại
                SET @Today = LEFT(CONVERT(nvarchar, getdate(), 12), 4)
                SET @Today = REPLACE(@Today, '.', '');
                --Tháng cũ
                SELECT @TodayOld = ISNULL(MAX(LEFT(ReceiptID, 5)), 0000)  FROM dbo.FNC_Receipt
                WHERE LEN(ReceiptID) = 10  AND RecordStatus !=99 AND PartnerNumber=@PartnerNumber 
                --Lấy 4 số cuối + 1
                SELECT @AssetIDNew = ISNULL(MAX(RIGHT(ReceiptID, 4)) + 1, 1)  FROM FNC_Receipt
                WHERE LEN(ReceiptID) = 10 
                AND ISNUMERIC(RIGHT(ReceiptID,4)) = 1 
                AND RecordStatus!=99 
                AND PartnerNumber=@PartnerNumber
                SELECT 'R'+@Today + '-' + RIGHT('000' + @AssetIDNew, 4)
";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = new Guid(PartnerNumber);
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
    }
}
