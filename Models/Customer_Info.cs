﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Customer_Info
    {

        public Customer_Model Customer = new Customer_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Customer_Info()
        {
        }
        public Customer_Info(string CustomerKey)
        {
            string zSQL = "SELECT * FROM CRM_Customer WHERE CustomerKey = @CustomerKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = CustomerKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Customer.CustomerKey = zReader["CustomerKey"].ToString();
                    Customer.CustomerID = zReader["CustomerID"].ToString();
                    Customer.FullName = zReader["FullName"].ToString();
                    Customer.CompanyName = zReader["CompanyName"].ToString();
                    Customer.Aliases = zReader["Aliases"].ToString();
                    Customer.JobTitle = zReader["JobTitle"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        Customer.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    Customer.CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["CustomerType"] != DBNull.Value)
                        Customer.CustomerType = int.Parse(zReader["CustomerType"].ToString());
                    Customer.CustomerTypeName = zReader["CustomerTypeName"].ToString();
                    if (zReader["CustomerVendor"] != DBNull.Value)
                        Customer.CustomerVendor = int.Parse(zReader["CustomerVendor"].ToString());
                    Customer.TaxNumber = zReader["TaxNumber"].ToString();
                    Customer.IDCard = zReader["IDCard"].ToString();
                    if (zReader["Birthday"] != DBNull.Value)
                        Customer.Birthday = (DateTime)zReader["Birthday"];
                    Customer.Address = zReader["Address"].ToString();
                    Customer.City = zReader["City"].ToString();
                    Customer.Country = zReader["Country"].ToString();
                    Customer.ZipCode = zReader["ZipCode"].ToString();
                    Customer.Phone = zReader["Phone"].ToString();
                    Customer.Email = zReader["Email"].ToString();
                    Customer.Note = zReader["Note"].ToString();
                    Customer.Style = zReader["Style"].ToString();
                    Customer.Class = zReader["Class"].ToString();
                    Customer.CodeLine = zReader["CodeLine"].ToString();
                    Customer.BankAccount = zReader["BankAccount"].ToString();
                    Customer.BankName = zReader["BankName"].ToString();
                    Customer.Referrer = zReader["Referrer"].ToString();
                    Customer.Password = zReader["Password"].ToString();
                    Customer.Parent = zReader["Parent"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                        Customer.Slug = int.Parse(zReader["Slug"].ToString());
                    Customer.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Customer.Contact = zReader["Contact"].ToString();
                    Customer.OrganizationID = zReader["OrganizationID"].ToString();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        Customer.DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        Customer.BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Customer.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Customer.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Customer.CreatedBy = zReader["CreatedBy"].ToString();
                    Customer.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Customer.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Customer.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Customer.ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["LastLoginDate"] != DBNull.Value)
                        Customer.LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Customer ("
         + " CustomerID , FullName , CompanyName , Aliases , JobTitle , CategoryKey , CategoryName , CustomerType , CustomerTypeName , CustomerVendor , TaxNumber , IDCard , Birthday, Address , City , Country , ZipCode , Phone , Email , Note , Style , Class , CodeLine , BankAccount , BankName , Referrer , Password , Parent , Slug , PartnerNumber , Contact , OrganizationID , DepartmentKey , BranchKey , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName , LastLoginDate ) "
         + " VALUES ( "
         + " @CustomerID , @FullName , @CompanyName , @Aliases , @JobTitle , @CategoryKey , @CategoryName , @CustomerType , @CustomerTypeName , @CustomerVendor , @TaxNumber , @IDCard , @Birthday, @Address , @City , @Country , @ZipCode , @Phone , @Email , @Note , @Style , @Class , @CodeLine , @BankAccount , @BankName , @Referrer , @Password , @Parent , @Slug , @PartnerNumber , @Contact , @OrganizationID , @DepartmentKey , @BranchKey , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName , @LastLoginDate ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = Customer.CustomerID;
                zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = Customer.FullName;
                zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = Customer.CompanyName;
                zCommand.Parameters.Add("@Aliases", SqlDbType.NVarChar).Value = Customer.Aliases;
                zCommand.Parameters.Add("@JobTitle", SqlDbType.NVarChar).Value = Customer.JobTitle;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Customer.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Customer.CategoryName;
                zCommand.Parameters.Add("@CustomerType", SqlDbType.Int).Value = Customer.CustomerType;
                zCommand.Parameters.Add("@CustomerTypeName", SqlDbType.NVarChar).Value = Customer.CustomerTypeName;
                zCommand.Parameters.Add("@CustomerVendor", SqlDbType.Int).Value = Customer.CustomerVendor;
                zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = Customer.TaxNumber;
                zCommand.Parameters.Add("@IDCard", SqlDbType.NVarChar).Value = Customer.IDCard;
                if (Customer.Birthday == DateTime.MinValue)
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = Customer.Birthday;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Customer.Address;
                zCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = Customer.City;
                zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = Customer.Country;
                zCommand.Parameters.Add("@ZipCode", SqlDbType.NVarChar).Value = Customer.ZipCode;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Customer.Phone;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = Customer.Email;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = Customer.Note;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Customer.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Customer.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Customer.CodeLine;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Customer.BankAccount;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Customer.BankName;
                zCommand.Parameters.Add("@Referrer", SqlDbType.NVarChar).Value = Customer.Referrer;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = Customer.Password;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Customer.Parent;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Customer.Slug;
                if (Customer.PartnerNumber != "" && Customer.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Customer.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@Contact", SqlDbType.NVarChar).Value = Customer.Contact;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Customer.OrganizationID;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Customer.DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Customer.BranchKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Customer.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Customer.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Customer.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Customer.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Customer.ModifiedName;
                if (Customer.LastLoginDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = Customer.LastLoginDate;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Customer("
         + " CustomerKey , CustomerID , FullName , CompanyName , Aliases , JobTitle , CategoryKey , CategoryName , CustomerType , CustomerTypeName , CustomerVendor , TaxNumber , IDCard , Birthday, Address , City , Country , ZipCode , Phone , Email , Note , Style , Class , CodeLine , BankAccount , BankName , Referrer , Password , Parent , Slug , PartnerNumber , Contact , OrganizationID , DepartmentKey , BranchKey , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName , LastLoginDate ) "
         + " VALUES ( "
         + " @CustomerKey , @CustomerID , @FullName , @CompanyName , @Aliases , @JobTitle , @CategoryKey , @CategoryName , @CustomerType , @CustomerTypeName , @CustomerVendor , @TaxNumber , @IDCard ,@Birthday, @Address , @City , @Country , @ZipCode , @Phone , @Email , @Note , @Style , @Class , @CodeLine , @BankAccount , @BankName , @Referrer , @Password , @Parent , @Slug , @PartnerNumber , @Contact , @OrganizationID , @DepartmentKey , @BranchKey , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName , @LastLoginDate ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = Customer.CustomerKey;
                zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = Customer.CustomerID;
                zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = Customer.FullName;
                zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = Customer.CompanyName;
                zCommand.Parameters.Add("@Aliases", SqlDbType.NVarChar).Value = Customer.Aliases;
                zCommand.Parameters.Add("@JobTitle", SqlDbType.NVarChar).Value = Customer.JobTitle;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Customer.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Customer.CategoryName;
                zCommand.Parameters.Add("@CustomerType", SqlDbType.Int).Value = Customer.CustomerType;
                zCommand.Parameters.Add("@CustomerTypeName", SqlDbType.NVarChar).Value = Customer.CustomerTypeName;
                zCommand.Parameters.Add("@CustomerVendor", SqlDbType.Int).Value = Customer.CustomerVendor;
                zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = Customer.TaxNumber;
                zCommand.Parameters.Add("@IDCard", SqlDbType.NVarChar).Value = Customer.IDCard;
                if (Customer.Birthday == DateTime.MinValue)
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = Customer.Birthday;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Customer.Address;
                zCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = Customer.City;
                zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = Customer.Country;
                zCommand.Parameters.Add("@ZipCode", SqlDbType.NVarChar).Value = Customer.ZipCode;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Customer.Phone;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = Customer.Email;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = Customer.Note;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Customer.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Customer.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Customer.CodeLine;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Customer.BankAccount;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Customer.BankName;
                zCommand.Parameters.Add("@Referrer", SqlDbType.NVarChar).Value = Customer.Referrer;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = Customer.Password;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Customer.Parent;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Customer.Slug;
                if (Customer.PartnerNumber != "" && Customer.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Customer.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@Contact", SqlDbType.NVarChar).Value = Customer.Contact;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Customer.OrganizationID;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Customer.DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Customer.BranchKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Customer.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Customer.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Customer.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Customer.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Customer.ModifiedName;
                if (Customer.LastLoginDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = Customer.LastLoginDate;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE CRM_Customer SET "
                        + " CustomerID = @CustomerID,"
                        + " FullName = @FullName,"
                        + " CompanyName = @CompanyName,"
                        + " Aliases = @Aliases,"
                        + " JobTitle = @JobTitle,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " CustomerType = @CustomerType,"
                        + " CustomerTypeName = @CustomerTypeName,"
                        + " CustomerVendor = @CustomerVendor,"
                        + " TaxNumber = @TaxNumber,"
                        + " IDCard = @IDCard,"
                        + " Birthday = @Birthday,"
                        + " Address = @Address,"
                        + " City = @City,"
                        + " Country = @Country,"
                        + " ZipCode = @ZipCode,"
                        + " Phone = @Phone,"
                        + " Email = @Email,"
                        + " Note = @Note,"
                        + " Style = @Style,"
                        + " Class = @Class,"
                        + " CodeLine = @CodeLine,"
                        + " BankAccount = @BankAccount,"
                        + " BankName = @BankName,"
                        + " Referrer = @Referrer,"
                        + " Password = @Password,"
                        + " Parent = @Parent,"
                        + " Slug = @Slug,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " Contact = @Contact,"
                        + " OrganizationID = @OrganizationID,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " BranchKey = @BranchKey,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " LastLoginDate = @LastLoginDate"
                        + " WHERE CustomerKey = @CustomerKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = Customer.CustomerKey;
                zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = Customer.CustomerID;
                zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = Customer.FullName;
                zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = Customer.CompanyName;
                zCommand.Parameters.Add("@Aliases", SqlDbType.NVarChar).Value = Customer.Aliases;
                zCommand.Parameters.Add("@JobTitle", SqlDbType.NVarChar).Value = Customer.JobTitle;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Customer.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Customer.CategoryName;
                zCommand.Parameters.Add("@CustomerType", SqlDbType.Int).Value = Customer.CustomerType;
                zCommand.Parameters.Add("@CustomerTypeName", SqlDbType.NVarChar).Value = Customer.CustomerTypeName;
                zCommand.Parameters.Add("@CustomerVendor", SqlDbType.Int).Value = Customer.CustomerVendor;
                zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = Customer.TaxNumber;
                zCommand.Parameters.Add("@IDCard", SqlDbType.NVarChar).Value = Customer.IDCard;
                if (Customer.Birthday == DateTime.MinValue)
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = Customer.Birthday;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Customer.Address;
                zCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = Customer.City;
                zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = Customer.Country;
                zCommand.Parameters.Add("@ZipCode", SqlDbType.NVarChar).Value = Customer.ZipCode;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Customer.Phone;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = Customer.Email;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = Customer.Note;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Customer.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Customer.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Customer.CodeLine;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Customer.BankAccount;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Customer.BankName;
                zCommand.Parameters.Add("@Referrer", SqlDbType.NVarChar).Value = Customer.Referrer;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = Customer.Password;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Customer.Parent;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Customer.Slug;
                if (Customer.PartnerNumber != "" && Customer.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Customer.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@Contact", SqlDbType.NVarChar).Value = Customer.Contact;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Customer.OrganizationID;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Customer.DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Customer.BranchKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Customer.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Customer.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Customer.ModifiedName;
                if (Customer.LastLoginDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = Customer.LastLoginDate;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE CRM_Customer SET RecordStatus = 99 WHERE CustomerKey = @CustomerKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = Customer.CustomerKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM CRM_Customer WHERE CustomerKey = @CustomerKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = Customer.CustomerKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
