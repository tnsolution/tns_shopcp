﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ShopCP
{
    public static class Track
    {
        public static DataTable ProcessFIFO(DateTime FromDate, DateTime ToDate, string PartnerNumber, string ProductKey, float QtySold, double PriceSold)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SAL_TRACK_FIFO_V2";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                zCommand.Parameters.Add("@QtySold", SqlDbType.Float).Value = QtySold;
                zCommand.Parameters.Add("@PriceSold", SqlDbType.Money).Value = PriceSold;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {

            }

            return zTable;
        }

        public static bool CreateRecordFIFO(List<Track_Item> zList)
        {
            bool Result = true;
            StringBuilder zSb = new StringBuilder();
            foreach (var rec in zList)
            {
                zSb.AppendLine(@"
INSERT INTO ATO_Track_FIFO 
([OrderStock], [OrderSale], [OrderPurchase], [ProductKey], [Qty], [PriceStock], [PriceSold], [QtyRemain], [Description], [PartnerNumber], [CreatedBy], [CreatedName], [ModifiedBy], [ModifiedName])
VALUES 
('', '" + rec.OrderSale + "', '" + rec.OrderPurchase + "', '" + rec.ProductKey + "', " + rec.Qty.ToString() + ", " + rec.PriceStock.ToString() + ", " + rec.PriceSold.ToString() + ", '" + rec.QtyRemain.ToString() + "', '" + rec.Message + "', '" + rec.PartnerNumber + "', 'AUTO', 'AUTO', 'AUTO', 'AUTO')");
            }

            try
            {
                string zConnectionString = TN_Helper.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSb.ToString(), zConnect);
                zCommand.ExecuteNonQuery();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception)
            {
                Result = false;
            }

            return Result;
        }
    }
}
