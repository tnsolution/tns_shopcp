﻿using System;
namespace ShopCP
{
    public partial class Track_Item
    {
        #region [ Field Name ]
        private long _AutoKey = 0;
        private string _OrderSale = "";
        private string _OrderPurchase = "";
        private string _ProductKey = "";
        private float _Qty = 0;
        private double _PriceStock = 0;        
        private double _PriceSold = 0;
        private double _QtyRemain = 0;
        private string _Message = "";
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private string _PartnerNumber = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public long AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string OrderSale
        {
            get { return _OrderSale; }
            set { _OrderSale = value; }
        }
        public string OrderPurchase
        {
            get { return _OrderPurchase; }
            set { _OrderPurchase = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public float Qty
        {
            get { return _Qty; }
            set { _Qty = value; }
        }
        public double PriceStock
        {
            get { return _PriceStock; }
            set { _PriceStock = value; }
        }
        public double PriceSold
        {
            get { return _PriceSold; }
            set { _PriceSold = value; }
        }
        public double QtyRemain
        {
            get { return _QtyRemain; }
            set { _QtyRemain = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
