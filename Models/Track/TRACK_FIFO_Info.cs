﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class TRACK_FIFO_Info
    {

        public TRACK_FIFO_Model K_FIFO = new TRACK_FIFO_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public TRACK_FIFO_Info()
        {
        }
        public TRACK_FIFO_Info(long AutoKey)
        {
            string zSQL = "SELECT * FROM TRACK_FIFO WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.BigInt).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        K_FIFO.AutoKey = long.Parse(zReader["AutoKey"].ToString());
                    K_FIFO.OrderSale = zReader["OrderSale"].ToString();
                    K_FIFO.OrderPurchase = zReader["OrderPurchase"].ToString();
                    K_FIFO.ProductKey = zReader["ProductKey"].ToString();
                    if (zReader["Qty"] != DBNull.Value)
                        K_FIFO.Qty = float.Parse(zReader["Qty"].ToString());
                    if (zReader["Price"] != DBNull.Value)
                        K_FIFO.Price = double.Parse(zReader["Price"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        K_FIFO.Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["QtyRemain"] != DBNull.Value)
                        K_FIFO.QtyRemain = double.Parse(zReader["QtyRemain"].ToString());
                    K_FIFO.Description = zReader["Description"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                        K_FIFO.Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        K_FIFO.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    K_FIFO.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        K_FIFO.CreatedOn = (DateTime)zReader["CreatedOn"];
                    K_FIFO.CreatedBy = zReader["CreatedBy"].ToString();
                    K_FIFO.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        K_FIFO.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    K_FIFO.ModifiedBy = zReader["ModifiedBy"].ToString();
                    K_FIFO.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO TRACK_FIFO ("
         + " OrderSale , OrderPurchase , ProductKey , Qty , Price , Amount , QtyRemain , Description , Slug , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderSale , @OrderPurchase , @ProductKey , @Qty , @Price , @Amount , @QtyRemain , @Description , @Slug , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderSale", SqlDbType.NVarChar).Value = K_FIFO.OrderSale;
                zCommand.Parameters.Add("@OrderPurchase", SqlDbType.NVarChar).Value = K_FIFO.OrderPurchase;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = K_FIFO.ProductKey;
                zCommand.Parameters.Add("@Qty", SqlDbType.Float).Value = K_FIFO.Qty;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = K_FIFO.Price;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = K_FIFO.Amount;
                zCommand.Parameters.Add("@QtyRemain", SqlDbType.Money).Value = K_FIFO.QtyRemain;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = K_FIFO.Description;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = K_FIFO.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = K_FIFO.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = K_FIFO.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = K_FIFO.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = K_FIFO.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = K_FIFO.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = K_FIFO.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO TRACK_FIFO("
         + " AutoKey , OrderSale , OrderPurchase , ProductKey , Qty , Price , Amount , QtyRemain , Description , Slug , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @OrderSale , @OrderPurchase , @ProductKey , @Qty , @Price , @Amount , @QtyRemain , @Description , @Slug , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderSale", SqlDbType.NVarChar).Value = K_FIFO.OrderSale;
                zCommand.Parameters.Add("@OrderPurchase", SqlDbType.NVarChar).Value = K_FIFO.OrderPurchase;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = K_FIFO.ProductKey;
                zCommand.Parameters.Add("@Qty", SqlDbType.Float).Value = K_FIFO.Qty;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = K_FIFO.Price;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = K_FIFO.Amount;
                zCommand.Parameters.Add("@QtyRemain", SqlDbType.Money).Value = K_FIFO.QtyRemain;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = K_FIFO.Description;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = K_FIFO.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = K_FIFO.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = K_FIFO.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = K_FIFO.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = K_FIFO.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = K_FIFO.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = K_FIFO.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE TRACK_FIFO SET "
                        + " OrderSale = @OrderSale,"
                        + " OrderPurchase = @OrderPurchase,"
                        + " ProductKey = @ProductKey,"
                        + " Qty = @Qty,"
                        + " Price = @Price,"
                        + " Amount = @Amount,"
                        + " QtyRemain = @QtyRemain,"
                        + " Description = @Description,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderSale", SqlDbType.NVarChar).Value = K_FIFO.OrderSale;
                zCommand.Parameters.Add("@OrderPurchase", SqlDbType.NVarChar).Value = K_FIFO.OrderPurchase;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = K_FIFO.ProductKey;
                zCommand.Parameters.Add("@Qty", SqlDbType.Float).Value = K_FIFO.Qty;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = K_FIFO.Price;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = K_FIFO.Amount;
                zCommand.Parameters.Add("@QtyRemain", SqlDbType.Money).Value = K_FIFO.QtyRemain;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = K_FIFO.Description;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = K_FIFO.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = K_FIFO.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = K_FIFO.PartnerNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = K_FIFO.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = K_FIFO.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE TRACK_FIFO SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.BigInt).Value = K_FIFO.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM TRACK_FIFO WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.BigInt).Value = K_FIFO.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
