﻿using System;
namespace ShopCP
{
    public class CloseMonth_Model
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _ProductKey = "";
        private string _CloseMonth = "";
        private string _PartnerNumber = "";
        private int _Year = 0;
        private float _BeginQty = 0;
        private double _BeginMoney = 0;
        private float _InQty = 0;
        private double _InMoney = 0;
        private float _OutQty = 0;
        private double _OutMoney = 0;
        private float _EndQty = 0;
        private double _EndMoney = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string CloseMonth
        {
            get { return _CloseMonth; }
            set { _CloseMonth = value; }
        }
        public int Year
        {
            get { return _Year; }
            set { _Year = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public float BeginQty
        {
            get { return _BeginQty; }
            set { _BeginQty = value; }
        }
        public double BeginMoney
        {
            get { return _BeginMoney; }
            set { _BeginMoney = value; }
        }
        public float InQty
        {
            get { return _InQty; }
            set { _InQty = value; }
        }
        public double InMoney
        {
            get { return _InMoney; }
            set { _InMoney = value; }
        }
        public float OutQty
        {
            get { return _OutQty; }
            set { _OutQty = value; }
        }
        public double OutMoney
        {
            get { return _OutMoney; }
            set { _OutMoney = value; }
        }
        public float EndQty
        {
            get { return _EndQty; }
            set { _EndQty = value; }
        }
        public double EndMoney
        {
            get { return _EndMoney; }
            set { _EndMoney = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
