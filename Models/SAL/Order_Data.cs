﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
/*
 -- Slug = 9 là đơn hàng sản xuất
 -- Slug = 1 đơn hàng bán shopee
-- Slug ==11 đơn hàng bán tnsolution
 */
/*

    **  nhập hàng    **
mua                     - slug = 101 x nhập kho
khách trả               - slug = 102 x  nhập kho

    **  xuất hàng   **
huỷ                      -  slug = 201 x    xuất kho    xx tính giá
trả nhà cung cấp    - slug = 202 x  xuất kho    xx tính giá

    **  bán hàng shopee   **
giao hàng      - slug = 301  -   OrderStatus = 1    > xuất kho
hoàn thành    - slug = 302  -   OrderStatus = 2 xx tính giá
huỷ                - slug = 303  -   OrderStatus = 9

    **  bán hàng cửa hàng   **
giao hàng      - slug = 401  -   OrderStatus = 1    > xuất kho
hoàn thành    - slug = 402  -   OrderStatus = 2 xx tính giá
huỷ                - slug = 403  -   OrderStatus = 9

    **  sản xuất    **
nhap -  slug = 501 x    nhập kho
xuat -  slug = 502 x    xuất kho

 */
namespace ShopCP
{
    public class Order_Data
    {
        public static List<Order_Model> List_Adjust(string PartnerNumber, DateTime FromDate, DateTime ToDate, string Name, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * 
FROM SAL_Order 
WHERE 
RecordStatus <> 99 
AND Slug = 111
AND PartnerNumber = @PartnerNumber";

            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate";
            }
            if (Name != string.Empty)
            {
                zSQL += " AND CreatedName = @CreatedName";
            }
            zSQL += " ORDER BY CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = "%" + Name + "%";
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    S_OrderID = r["S_OrderID"].ToString(),
                    S_JSON = r["S_JSON"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    OrderStatus = r["OrderStatus"].ToInt(),
                    Description = r["Description"].ToString(),
                    BuyerKey = r["BuyerKey"].ToString(),
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    BuyerAddress = r["BuyerAddress"].ToString(),
                    BuyerEmail = r["BuyerEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    TotalItems = r["TotalItems"].ToFloat(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }


        //TẤT CẢ ĐƠN BÁN HÀNG
        public static List<Order_Model> List(string PartnerNumber, int Status, DateTime FromDate, DateTime ToDate, string Name, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * 
FROM SAL_Order 
WHERE 
RecordStatus <> 99   
AND PartnerNumber = @PartnerNumber 
AND Slug = 301 ";// tru san xuat

            if (Status != 0)
            {
               
            }
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate";
            }
            if (Name != string.Empty)
            {
                zSQL += " AND (BuyerName LIKE @Name OR BuyerPhone LIKE @Name  OR OrderID = @Name OR S_OrderID = @Name)";
            }
            zSQL += " ORDER BY CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Status", SqlDbType.NVarChar).Value = Status;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    S_OrderID = r["S_OrderID"].ToString(),
                    S_JSON = r["S_JSON"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    OrderStatus = r["OrderStatus"].ToInt(),
                    Description = r["Description"].ToString(),
                    BuyerKey = r["BuyerKey"].ToString(),
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    BuyerAddress = r["BuyerAddress"].ToString(),
                    BuyerEmail = r["BuyerEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    TotalItems = r["TotalItems"].ToFloat(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Order_Model> ListByItem(string PartnerNumber, string Id, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.* 
FROM SAL_Order A 
LEFT JOIN SAL_Order_Item B ON A.OrderKey = B.OrderKey
WHERE 
A.RecordStatus <> 99 
AND B.ItemKey = @Id
AND A.OrderStatus = 1
AND A.Slug <> 9   
AND A.PartnerNumber = @PartnerNumber";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Id", SqlDbType.NVarChar).Value = Id;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            var zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    S_OrderID = r["S_OrderID"].ToString(),
                    S_JSON = r["S_JSON"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    OrderStatus = r["OrderStatus"].ToInt(),
                    Description = r["Description"].ToString(),
                    BuyerKey = r["BuyerKey"].ToString(),
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    BuyerAddress = r["BuyerAddress"].ToString(),
                    BuyerEmail = r["BuyerEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    TotalItems = r["TotalItems"].ToFloat(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Order_Item_Model> TrackItem(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, 
dbo.SAL_QtyStockOne(A.ItemKey) AS Qty
FROM SAL_Order_Item A 
LEFT JOIN SAL_Order B ON A.OrderKey = B.OrderKey
WHERE B.RecordStatus != 99 
AND B.PartnerNumber = @PartnerNumber 
AND B.OrderStatus = 1
AND B.Slug IN (1, 11) 
ORDER BY B.CreatedOn DESC";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            var zList = new List<Order_Item_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Item_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    ItemKey = r["ItemKey"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    ItemQty = r["ItemQty"].ToFloat(),
                    ItemPrice = r["ItemPrice"].ToDouble(),
                    ItemPhoto = r["ItemPhoto"].ToString(),
                    Stock = r["Qty"].ToFloat(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        //chỉ chạy 1 lần update tồn kho mỗi tháng
        //public static void UpdateTotalMoney(string PartnerNumber)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = "SELECT * FROM SAL_Order WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Slug = 1 ORDER BY CreatedOn DESC";   //Slug = 1 đơn hàng bán shopee
        //    string zConnectionString = TN_Helper.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close(); 
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    List<Order_Model> zList = new List<Order_Model>();
        //    foreach (DataRow r in zTable.Rows)
        //    {
        //        zList.Add(new Order_Model()
        //        {
        //            OrderKey = r["OrderKey"].ToString(),
        //            OrderID = r["OrderID"].ToString(),
        //            S_OrderID = r["S_OrderID"].ToString(),
        //            S_JSON = r["S_JSON"].ToString(),
        //            OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
        //            OrderStatus = r["OrderStatus"].ToInt(),
        //            Description = r["Description"].ToString(),
        //            BuyerKey = r["BuyerKey"].ToString(),
        //            BuyerName = r["BuyerName"].ToString(),
        //            BuyerPhone = r["BuyerPhone"].ToString(),
        //            BuyerAddress = r["BuyerAddress"].ToString(),
        //            BuyerEmail = r["BuyerEmail"].ToString(),
        //            ShippingAddress = r["ShippingAddress"].ToString(),
        //            ShippingCity = r["ShippingCity"].ToString(),
        //            TotalItems = r["TotalItems"].ToFloat(),
        //            TotalMoney = r["TotalMoney"].ToDouble(),
        //            Slug = r["Slug"].ToInt(),
        //            RecordStatus = r["RecordStatus"].ToInt(),
        //            CreditAccount = r["CreditAccount"].ToString(),
        //            DebitAccount = r["DebitAccount"].ToString(),
        //            PartnerNumber = r["PartnerNumber"].ToString(),
        //            Organization = r["Organization"].ToString(),
        //            CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
        //            CreatedBy = r["CreatedBy"].ToString(),
        //            CreatedName = r["CreatedName"].ToString(),
        //            ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
        //            ModifiedBy = r["ModifiedBy"].ToString(),
        //            ModifiedName = r["ModifiedName"].ToString(),
        //        });
        //    }

        //    foreach (var item in zList)
        //    {

        //        //giá thực thu từ shopee
        //        var Shopee = JsonConvert.DeserializeObject<SDK.Shopee.Root_OrderInfo.Data>(item.S_JSON);
        //        var totalItem = Shopee.order_items.Sum(s => (s.item_price.ToDouble() * s.amount));   //tổng tiền hàng * đơn giá - phí shopee
        //        item.TotalMoney = totalItem - Shopee.card_txn_fee_info.card_txn_fee.ToDouble();

        //        var Info = new Order_Info();
        //        Info.UpdateTotalMoney(item.OrderKey, item.TotalMoney);
        //    }
        //}

        public static List<Order_Model> Search_Shopee(string PartnerNumber, DateTime FromDate, DateTime ToDate, string Name, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Order WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Slug IN (301)";
            if (Name != string.Empty)
            {
                zSQL += " AND ( BuyerName LIKE @Name OR S_OrderID LIKE @Name OR BuyerPhone LIKE @Name)";
            }
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate";
            }
            zSQL += " ORDER BY CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    S_OrderID = r["S_OrderID"].ToString(),
                    S_JSON = r["S_JSON"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    OrderStatus = r["OrderStatus"].ToInt(),
                    Description = r["Description"].ToString(),
                    BuyerKey = r["BuyerKey"].ToString(),
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    BuyerAddress = r["BuyerAddress"].ToString(),
                    BuyerEmail = r["BuyerEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    TotalItems = r["TotalItems"].ToFloat(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Order_Model> Search_Offline(string PartnerNumber, DateTime FromDate, DateTime ToDate, string Name, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Order WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Slug IN (400, 401)";  
            if (Name != string.Empty)
            {
                zSQL += " AND ( BuyerName LIKE @Name OR OrderID LIKE @Name OR BuyerPhone LIKE @Name)";
            }
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate";
            }
            zSQL += " ORDER BY CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    S_OrderID = r["S_OrderID"].ToString(),
                    S_JSON = r["S_JSON"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    OrderStatus = r["OrderStatus"].ToInt(),
                    Description = r["Description"].ToString(),
                    BuyerKey = r["BuyerKey"].ToString(),
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    BuyerAddress = r["BuyerAddress"].ToString(),
                    BuyerEmail = r["BuyerEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    TotalItems = r["TotalItems"].ToFloat(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static string Check_Produce_In(string ProduceKey, out string Message)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT ISNULL(OrderKey,'') FROM SAL_Order WHERE RecordStatus <> 99 AND ProduceKey = @ProduceKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProduceKey", SqlDbType.NVarChar).Value = ProduceKey;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception Err)
            {
                Message = Err.ToString().GetFirstLine();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;

        }
        public static string Check_Produce_Out(string ProduceKey, out string Message)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT ISNULL(OrderKey,'') FROM SAL_Purchase WHERE RecordStatus <> 99 AND ProduceKey = @ProduceKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProduceKey", SqlDbType.NVarChar).Value = ProduceKey;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception Err)
            {
                Message = Err.ToString().GetFirstLine();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;

        }

        public static List<Order_Model> DanhSachTienLoi(string PartnerNumber, string Status, DateTime FromDate, DateTime ToDate, string Name, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
--DECLARE @FromDate DATETIME = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0);
--DECLARE @ToDate DATETIME = DATEADD(SECOND, -1, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0));
--DECLARE @PartnerNumber NVARCHAR(50) = 'B27D0646-F8E7-49F1-9CE9-C4078C29EB47'

SELECT 
A.OrderKey,A.OrderID,A.S_OrderID, A.OrderDate,A.BuyerPhone,A.BuyerName, A.[Description],
A.TotalMoney ,
dbo.LAYGIALOI_DONBANHANG(A.OrderKey) TIENLOI
FROM SAL_Order A
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
";

            if (Status != "")
            {
                zSQL += " AND A.Slug IN ( "+Status+" )";
            }
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.CreatedOn BETWEEN @FromDate AND @ToDate";
            }
            if (Name != string.Empty)
            {
                zSQL += " AND (A.BuyerName LIKE @Name OR A.BuyerPhone LIKE @Name  OR A.OrderID = @Name OR A.S_OrderID = @Name)";
            }
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Status", SqlDbType.NVarChar).Value = Status;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    S_OrderID = r["S_OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    
                    TotalItems = r["TIENLOI"].ToFloat(),
                    TotalMoney = r["TotalMoney"].ToDouble()
                });
            }
            return zList;
        }
        public static List<Order_Model> DanhSachDoanhSo(string PartnerNumber, string Status, DateTime FromDate, DateTime ToDate, string Name, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
--DECLARE @FromDate DATETIME = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0);
--DECLARE @ToDate DATETIME = DATEADD(SECOND, -1, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0));
--DECLARE @PartnerNumber NVARCHAR(50) = 'B27D0646-F8E7-49F1-9CE9-C4078C29EB47'

SELECT 
A.OrderKey,A.OrderID,A.S_OrderID, A.OrderDate,A.BuyerPhone,A.BuyerName, A.[Description],
A.TotalMoney 
FROM SAL_Order A
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
";

            if (Status != "")
            {
                zSQL += " AND A.Slug IN ( " + Status + " )";
            }
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.CreatedOn BETWEEN @FromDate AND @ToDate";
            }
            if (Name != string.Empty)
            {
                zSQL += " AND (A.BuyerName LIKE @Name OR A.BuyerPhone LIKE @Name  OR A.OrderID = @Name OR A.S_OrderID = @Name)";
            }
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Status", SqlDbType.NVarChar).Value = Status;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    S_OrderID = r["S_OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),

                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),

                    TotalMoney = r["TotalMoney"].ToDouble()
                });
            }
            return zList;
        }
        public static List<Purchase_Model> DanhSachMuaHang(string PartnerNumber, string Status, DateTime FromDate, DateTime ToDate, string Name, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
--DECLARE @FromDate DATETIME = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0);
--DECLARE @ToDate DATETIME = DATEADD(SECOND, -1, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0));
--DECLARE @PartnerNumber NVARCHAR(50) = 'B27D0646-F8E7-49F1-9CE9-C4078C29EB47'

SELECT 
A.OrderKey,A.OrderID, A.OrderDate,A.VendorPhone,A.VendorName, A.[Description],
A.TotalMoney 
FROM SAL_Purchase A
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
";

            if (Status != "")
            {
                zSQL += " AND A.Slug IN ( " + Status + " )";
            }
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.CreatedOn BETWEEN @FromDate AND @ToDate";
            }
            if (Name != string.Empty)
            {
                zSQL += " AND (A.VendorName LIKE @Name OR A.VendorPhone LIKE @Name  OR A.OrderID = @Name )";
            }
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Status", SqlDbType.NVarChar).Value = Status;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Purchase_Model> zList = new List<Purchase_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Purchase_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),

                    VendorName = r["VendorName"].ToString(),
                    VendorPhone = r["VendorPhone"].ToString(),

                    TotalMoney = r["TotalMoney"].ToDouble(),
                });
            }
            return zList;
        }
    }
}