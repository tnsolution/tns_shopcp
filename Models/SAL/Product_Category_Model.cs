﻿using System;
namespace ShopCP
{
    public class Product_Category_Model
    {
        #region [ Field Name ]
        private int _CategoryKey = 0;
        private string _CategoryID = "";
        private string _CategoryName = "";
        private string _Description = "";
        private int _Parent = 0;
        private bool _Publish = true;
        private int _Rank = 0;
        private string _CategoryPath = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public bool Publish
        {
            get { return _Publish; }
            set { _Publish = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string CategoryPath
        {
            get { return _CategoryPath; }
            set { _CategoryPath = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
