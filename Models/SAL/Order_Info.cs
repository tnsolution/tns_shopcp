﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Order_Info
    {

        public Order_Model Order = new Order_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Order_Info()
        {
            Order.OrderKey = Guid.NewGuid().ToString();
        }
        public Order_Info(string OrderKey)
        {
            string zSQL = "SELECT * FROM SAL_Order WHERE OrderKey = @OrderKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OrderKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();

                    if (zReader["Discount"] != DBNull.Value)
                        Order.Discount = double.Parse(zReader["Discount"].ToString());

                    Order.OrderKey = zReader["OrderKey"].ToString();
                    Order.OrderID = zReader["OrderID"].ToString();
                    Order.S_OrderID = zReader["S_OrderID"].ToString();
                    Order.S_JSON = zReader["S_JSON"].ToString();
                    if (zReader["OrderDate"] != DBNull.Value)
                        Order.OrderDate = (DateTime)zReader["OrderDate"];
                    if (zReader["OrderStatus"] != DBNull.Value)
                        Order.OrderStatus = int.Parse(zReader["OrderStatus"].ToString());
                    Order.Description = zReader["Description"].ToString();
                    Order.BuyerKey = zReader["BuyerKey"].ToString();
                    Order.BuyerName = zReader["BuyerName"].ToString();
                    Order.BuyerPhone = zReader["BuyerPhone"].ToString();
                    Order.BuyerAddress = zReader["BuyerAddress"].ToString();
                    Order.BuyerEmail = zReader["BuyerEmail"].ToString();
                    Order.ShippingAddress = zReader["ShippingAddress"].ToString();
                    Order.ShippingCity = zReader["ShippingCity"].ToString();
                    if (zReader["TotalItems"] != DBNull.Value)
                        Order.TotalItems = float.Parse(zReader["TotalItems"].ToString());
                    if (zReader["TotalMoney"] != DBNull.Value)
                        Order.TotalMoney = double.Parse(zReader["TotalMoney"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        Order.Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Order.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Order.CreditAccount = zReader["CreditAccount"].ToString();
                    Order.DebitAccount = zReader["DebitAccount"].ToString();
                    Order.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Order.Organization = zReader["Organization"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Order.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Order.CreatedBy = zReader["CreatedBy"].ToString();
                    Order.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Order.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Order.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Order.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Order ("
         + " OrderID , S_OrderID , S_JSON , OrderDate , OrderStatus , Description , BuyerKey , BuyerName , BuyerPhone , BuyerAddress , BuyerEmail , ShippingAddress , ShippingCity , Discount, TotalItems , TotalMoney , Slug , RecordStatus, ProduceKey , CreditAccount , DebitAccount , PartnerNumber , Organization , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderID , @S_OrderID , @S_JSON , @OrderDate , @OrderStatus , @Description , @BuyerKey , @BuyerName , @BuyerPhone , @BuyerAddress , @BuyerEmail , @ShippingAddress , @ShippingCity , @Discount, @TotalItems , @TotalMoney , @Slug , @RecordStatus, @ProduceKey , @CreditAccount , @DebitAccount , @PartnerNumber , @Organization , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Discount", SqlDbType.Money).Value = Order.Discount;

                zCommand.Parameters.Add("@ProduceKey", SqlDbType.NVarChar).Value = Order.ProduceKey;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Order.OrderID;
                zCommand.Parameters.Add("@S_OrderID", SqlDbType.NVarChar).Value = Order.S_OrderID;
                zCommand.Parameters.Add("@S_JSON", SqlDbType.NVarChar).Value = Order.S_JSON;
                if (Order.OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Order.OrderDate;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = Order.OrderStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order.Description;
                zCommand.Parameters.Add("@BuyerKey", SqlDbType.NVarChar).Value = Order.BuyerKey;
                zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = Order.BuyerName;
                zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = Order.BuyerPhone;
                zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = Order.BuyerAddress;
                zCommand.Parameters.Add("@BuyerEmail", SqlDbType.NVarChar).Value = Order.BuyerEmail;
                zCommand.Parameters.Add("@ShippingAddress", SqlDbType.NVarChar).Value = Order.ShippingAddress;
                zCommand.Parameters.Add("@ShippingCity", SqlDbType.NVarChar).Value = Order.ShippingCity;
                zCommand.Parameters.Add("@TotalItems", SqlDbType.Float).Value = Order.TotalItems;
                zCommand.Parameters.Add("@TotalMoney", SqlDbType.Money).Value = Order.TotalMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order.RecordStatus;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Order.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Order.DebitAccount;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Order.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Order.Organization;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Order.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Order.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Order("
         + " OrderKey , OrderID , S_OrderID , S_JSON , OrderDate , OrderStatus , Description , BuyerKey , BuyerName , BuyerPhone , BuyerAddress , BuyerEmail , ShippingAddress , ShippingCity ,Discount, TotalItems , TotalMoney , Slug , RecordStatus , ProduceKey,CreditAccount , DebitAccount , PartnerNumber , Organization , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderKey , @OrderID , @S_OrderID , @S_JSON , @OrderDate , @OrderStatus , @Description , @BuyerKey , @BuyerName , @BuyerPhone , @BuyerAddress , @BuyerEmail , @ShippingAddress , @ShippingCity ,@Discount, @TotalItems , @TotalMoney , @Slug , @RecordStatus ,@ProduceKey, @CreditAccount , @DebitAccount , @PartnerNumber , @Organization , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Discount", SqlDbType.Money).Value = Order.Discount;
                zCommand.Parameters.Add("@ProduceKey", SqlDbType.NVarChar).Value = Order.ProduceKey;
                if (Order.OrderKey != "" && Order.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                }
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Order.OrderID;
                zCommand.Parameters.Add("@S_OrderID", SqlDbType.NVarChar).Value = Order.S_OrderID;
                zCommand.Parameters.Add("@S_JSON", SqlDbType.NVarChar).Value = Order.S_JSON;
                if (Order.OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Order.OrderDate;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = Order.OrderStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order.Description;
                zCommand.Parameters.Add("@BuyerKey", SqlDbType.NVarChar).Value = Order.BuyerKey;
                zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = Order.BuyerName;
                zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = Order.BuyerPhone;
                zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = Order.BuyerAddress;
                zCommand.Parameters.Add("@BuyerEmail", SqlDbType.NVarChar).Value = Order.BuyerEmail;
                zCommand.Parameters.Add("@ShippingAddress", SqlDbType.NVarChar).Value = Order.ShippingAddress;
                zCommand.Parameters.Add("@ShippingCity", SqlDbType.NVarChar).Value = Order.ShippingCity;
                zCommand.Parameters.Add("@TotalItems", SqlDbType.Float).Value = Order.TotalItems;
                zCommand.Parameters.Add("@TotalMoney", SqlDbType.Money).Value = Order.TotalMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order.RecordStatus;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Order.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Order.DebitAccount;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Order.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Order.Organization;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Order.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Order.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SAL_Order SET "
                        + " OrderID = @OrderID,"
                        + " S_OrderID = @S_OrderID,"
                        + " S_JSON = @S_JSON,"
                        + " OrderDate = @OrderDate,"
                        //+ " OrderStatus = @OrderStatus,"
                        + " Description = @Description,"
                        + " BuyerKey = @BuyerKey,"
                        + " BuyerName = @BuyerName,"
                        + " BuyerPhone = @BuyerPhone,"
                        + " BuyerAddress = @BuyerAddress,"
                        + " BuyerEmail = @BuyerEmail,"
                        + " ShippingAddress = @ShippingAddress,"
                        + " ShippingCity = @ShippingCity,"
                        + " TotalItems = @TotalItems,"
                        + " TotalMoney = @TotalMoney, Discount = @Discount,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus, ProduceKey=@ProduceKey,"
                        + " CreditAccount = @CreditAccount,"
                        + " DebitAccount = @DebitAccount,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " Organization = @Organization,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE OrderKey = @OrderKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Discount", SqlDbType.Money).Value = Order.Discount;
                zCommand.Parameters.Add("@ProduceKey", SqlDbType.NVarChar).Value = Order.ProduceKey;
                if (Order.OrderKey != "" && Order.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                }
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Order.OrderID;
                zCommand.Parameters.Add("@S_OrderID", SqlDbType.NVarChar).Value = Order.S_OrderID;
                zCommand.Parameters.Add("@S_JSON", SqlDbType.NVarChar).Value = Order.S_JSON;
                if (Order.OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Order.OrderDate;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = Order.OrderStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order.Description;
                zCommand.Parameters.Add("@BuyerKey", SqlDbType.NVarChar).Value = Order.BuyerKey;
                zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = Order.BuyerName;
                zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = Order.BuyerPhone;
                zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = Order.BuyerAddress;
                zCommand.Parameters.Add("@BuyerEmail", SqlDbType.NVarChar).Value = Order.BuyerEmail;
                zCommand.Parameters.Add("@ShippingAddress", SqlDbType.NVarChar).Value = Order.ShippingAddress;
                zCommand.Parameters.Add("@ShippingCity", SqlDbType.NVarChar).Value = Order.ShippingCity;
                zCommand.Parameters.Add("@TotalItems", SqlDbType.Float).Value = Order.TotalItems;
                zCommand.Parameters.Add("@TotalMoney", SqlDbType.Money).Value = Order.TotalMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order.RecordStatus;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Order.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Order.DebitAccount;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Order.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Order.Organization;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE SAL_Order SET RecordStatus = 99 WHERE OrderKey = @OrderKey
UPDATE SAL_Order_Item SET RecordStatus = 99 WHERE OrderKey = @OrderKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Order WHERE OrderKey = @OrderKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteItem()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Order_Item WHERE OrderKey = @OrderKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = Order.OrderKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion

        public string UpdateTotalMoney(string oderkey, double money)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_Order SET TotalMoney = " + money + " WHERE OrderKey = '" + oderkey + "'";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Cancel()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE 
SAL_Order 
SET 
Slug = @Slug,
OrderStatus = 9, 
ModifiedOn = GetDate(), 
ModifiedBy = @ModifiedBy,
ModifiedName = @ModifiedName 
WHERE OrderKey = @OrderKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order.Slug;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        /// <summary>
        /// ĐƠN HÀNG HOÀN THÀNH SET OrderStatus = 2
        /// </summary>
        /// <returns></returns>
        public string Close()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE SAL_Order 
SET 
OrderStatus = 2, 
Slug = @Slug,
ModifiedOn = GetDate(), 
ModifiedBy = @ModifiedBy,
ModifiedName = @ModifiedName 
WHERE OrderKey = @OrderKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order.OrderKey);
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order.Slug;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}