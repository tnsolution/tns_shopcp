﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Package_Item_Data
    {
        public static List<Package_Item_Model> List(string PartnerNumber, string PackageKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Package_Item WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND PackageKey = @PackageKey";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@PackageKey", SqlDbType.NVarChar).Value = PackageKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Package_Item_Model> zList = new List<Package_Item_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Package_Item_Model()
                {
                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : int.Parse(r["AutoKey"].ToString())),
                    PackageKey = (r["PackageKey"] == DBNull.Value ? "" : r["PackageKey"].ToString()),
                    ItemKey = (r["ItemKey"] == DBNull.Value ? "" : r["ItemKey"].ToString()),
                    ItemName = (r["ItemName"] == DBNull.Value ? "" : r["ItemName"].ToString()),
                    ItemPhoto = (r["ItemPhoto"] == DBNull.Value ? "" : r["ItemPhoto"].ToString()),
                    ItemQty = (r["ItemQty"] == DBNull.Value ? 0 : float.Parse(r["ItemQty"].ToString())),
                    ItemPrice = (r["ItemPrice"] == DBNull.Value ? 0 : double.Parse(r["ItemPrice"].ToString())),
                    ItemMoney = (r["ItemMoney"] == DBNull.Value ? 0 : double.Parse(r["ItemMoney"].ToString())),
                    Slug = (r["Slug"] == DBNull.Value ? 0 : int.Parse(r["Slug"].ToString())),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    Organization = (r["Organization"] == DBNull.Value ? "" : r["Organization"].ToString()),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
    }
}
