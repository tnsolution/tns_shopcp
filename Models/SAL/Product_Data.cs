﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Product_Data
    {
        public static List<Product_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*,
ISNULL(dbo.SAL_QtyStockOne(A.ProductKey),0) AS Qty
FROM PDT_Product  A
LEFT JOIN [dbo].[PDT_Product_Category] B ON B.CategoryKey=A.CategoryKey
WHERE A.RecordStatus != 99 AND B.RecordStatus <> 99 
AND A.PartnerNumber =  @PartnerNumber  
";

            zSQL += " ORDER BY B.Parent,B.Rank,A.SKU";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    SKU = r["SKU"].ToString(),
                    Qty = r["Qty"].ToFloat(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    PhotoList = r["PhotoList"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    QuantityLog = r["QuantityLog"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Product_Model> List(string PartnerNumber, string ProductName, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*,
ISNULL(dbo.SAL_QtyStockOne(A.ProductKey),0) AS Qty
FROM PDT_Product  A
LEFT JOIN [dbo].[PDT_Product_Category] B ON B.CategoryKey=A.CategoryKey
WHERE A.RecordStatus != 99 AND B.RecordStatus <> 99 
AND A.PartnerNumber =  @PartnerNumber  ";
            if (ProductName != string.Empty)
                zSQL += " AND A.ProductName LIKE (@ProductName)";
            zSQL += " ORDER BY B.Parent,B.Rank,A.SKU";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = "%" + ProductName + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    SKU = r["SKU"].ToString(),
                    Qty = r["Qty"].ToFloat(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    PhotoList = r["PhotoList"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    QuantityLog = r["QuantityLog"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Product_Model> List(string PartnerNumber, int CategoryKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*,
ISNULL(dbo.SAL_QtyStockOne(A.ProductKey),0) AS Qty
FROM PDT_Product  A
LEFT JOIN [dbo].[PDT_Product_Category] B ON B.CategoryKey=A.CategoryKey
WHERE A.RecordStatus != 99 AND B.RecordStatus <> 99 
AND A.PartnerNumber =  @PartnerNumber ";
            if (CategoryKey != 0)
                zSQL += " AND A.CategoryKey IN (@CategoryKey)";
            zSQL += "ORDER BY B.Parent,B.Rank,A.SKU ";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.NVarChar).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    Qty = r["Qty"].ToFloat(),
                    SKU = r["SKU"].ToString(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    PhotoList = r["PhotoList"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    QuantityLog = r["QuantityLog"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        /// <summary>
        /// kiem tra tồn kho sl của 1 sản phẩm
        /// </summary>
        /// <param name="ProductKey"></param>
        public static float GetStock(string ProductKey)
        {
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT ISNULL(dbo.SAL_QtyStockOne(@ProductKey),0)";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                zResult = float.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static float GetStockSKU(string Sku)
        {
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT ISNULL(dbo.SAL_QtyStockOneSKU(@SKU),0)";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SKU", SqlDbType.NVarChar).Value = Sku;
                zResult = float.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static List<QtyStock> ReportStock(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            var zTable = new DataTable();
            string zSQL = @"SAL_QtyStockMonth_V2";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); 
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            var zList = new List<QtyStock>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new QtyStock()
                {
                    ProductKey = r["ProductKey"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    ProductModel= r["ProductModel"].ToString(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    SKU = r["SKU"].ToString(),
                    BeginQty = r["BeginQty"].ToFloat(),
                    BeginMoney = r["BeginMoney"].ToDouble(),
                    InQty = r["InQty"].ToFloat(),
                    InMoney = r["InMoney"].ToDouble(),
                    OutQty = r["OutQty"].ToFloat(),
                    OutMoney = r["OutMoney"].ToDouble(),
                    EndQty = r["EndQty"].ToFloat(),
                    EndMoney = r["EndMoney"].ToDouble(),
                });
            }
            return zList;
        }
        public static bool CheckExistsID(string SKU, string PartnerNumber)
        {
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"
WITH X AS(
SELECT ProductKey AS [KEY] FROM PDT_Product WHERE SKU = @SKU AND PartnerNumber = @PartnerNumber AND RecordStatus <> 99
UNION
SELECT PackageKey AS [KEY] FROM SAL_Package WHERE SKU = @SKU AND PartnerNumber = @PartnerNumber AND RecordStatus <> 99
) 
SELECT COUNT([KEY]) FROM X";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SKU", SqlDbType.NVarChar).Value = SKU;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = float.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close();
            }

            if (zResult > 0)
                return true;
            else
                return false;
        }
        public static List<Product_Model> ListModel(string PartnerNumber, string ProductKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, 0 Qty
FROM PDT_Product A
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber ";
                zSQL += " AND (A.ProductKey <> @ProductKey AND A.ProductModel = (@ProductKey))";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); 
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    SKU = r["SKU"].ToString(),
                    Qty = r["Qty"].ToFloat(),
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    PhotoList = r["PhotoList"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    QuantityLog = r["QuantityLog"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}