﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Produce_Info
    {

        public Produce_Model Produce = new Produce_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Produce_Info()
        {
        }
        public Produce_Info(long AutoKey)
        {
            string zSQL = "SELECT * FROM SAL_Produce WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.BigInt).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Produce.AutoKey = long.Parse(zReader["AutoKey"].ToString());
                    Produce.ProduceID = zReader["ProduceID"].ToString();
                    if (zReader["ProduceDate"] != DBNull.Value)
                        Produce.ProduceDate = (DateTime)zReader["ProduceDate"];
                    Produce.Description = zReader["Description"].ToString();
                    if (zReader["QtyLoss"] != DBNull.Value)
                        Produce.QtyLoss = float.Parse(zReader["QtyLoss"].ToString());
                    Produce.InputRaw = zReader["InputRaw"].ToString();
                    Produce.OutputFinished = zReader["OutputFinished"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                        Produce.Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Produce.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Produce.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Produce.Organization = zReader["Organization"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Produce.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Produce.CreatedBy = zReader["CreatedBy"].ToString();
                    Produce.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Produce.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Produce.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Produce.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Produce ("
         + " ProduceID , ProduceDate , Description , QtyLoss , InputRaw , OutputFinished , Slug , RecordStatus , PartnerNumber , Organization , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ProduceID , @ProduceDate , @Description , @QtyLoss , @InputRaw , @OutputFinished , @Slug , @RecordStatus , @PartnerNumber , @Organization , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            zSQL += "SELECT SCOPE_IDENTITY() FROM SAL_PRODUCE";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProduceID", SqlDbType.NVarChar).Value = Produce.ProduceID;
                if (Produce.ProduceDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ProduceDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ProduceDate", SqlDbType.DateTime).Value = Produce.ProduceDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Produce.Description;
                zCommand.Parameters.Add("@QtyLoss", SqlDbType.Float).Value = Produce.QtyLoss;
                zCommand.Parameters.Add("@InputRaw", SqlDbType.NVarChar).Value = Produce.InputRaw;
                zCommand.Parameters.Add("@OutputFinished", SqlDbType.NVarChar).Value = Produce.OutputFinished;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Produce.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Produce.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Produce.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Produce.Organization;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Produce.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Produce.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Produce.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Produce.ModifiedName;
                Produce.AutoKey = long.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Produce("
         + " AutoKey , ProduceID , ProduceDate , Description , QtyLoss , InputRaw , OutputFinished , Slug , RecordStatus , PartnerNumber , Organization , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @ProduceID , @ProduceDate , @Description , @QtyLoss , @InputRaw , @OutputFinished , @Slug , @RecordStatus , @PartnerNumber , @Organization , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProduceID", SqlDbType.NVarChar).Value = Produce.ProduceID;
                if (Produce.ProduceDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ProduceDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ProduceDate", SqlDbType.DateTime).Value = Produce.ProduceDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Produce.Description;
                zCommand.Parameters.Add("@QtyLoss", SqlDbType.Float).Value = Produce.QtyLoss;
                zCommand.Parameters.Add("@InputRaw", SqlDbType.NVarChar).Value = Produce.InputRaw;
                zCommand.Parameters.Add("@OutputFinished", SqlDbType.NVarChar).Value = Produce.OutputFinished;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Produce.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Produce.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Produce.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Produce.Organization;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Produce.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Produce.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Produce.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Produce.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SAL_Produce SET "
                        + " ProduceID = @ProduceID,"
                        + " ProduceDate = @ProduceDate,"
                        + " Description = @Description,"
                        + " QtyLoss = @QtyLoss,"
                        + " InputRaw = @InputRaw,"
                        + " OutputFinished = @OutputFinished,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " Organization = @Organization,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProduceID", SqlDbType.NVarChar).Value = Produce.ProduceID;
                if (Produce.ProduceDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ProduceDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ProduceDate", SqlDbType.DateTime).Value = Produce.ProduceDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Produce.Description;
                zCommand.Parameters.Add("@QtyLoss", SqlDbType.Float).Value = Produce.QtyLoss;
                zCommand.Parameters.Add("@InputRaw", SqlDbType.NVarChar).Value = Produce.InputRaw;
                zCommand.Parameters.Add("@OutputFinished", SqlDbType.NVarChar).Value = Produce.OutputFinished;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Produce.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Produce.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Produce.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Produce.Organization;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Produce.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Produce.ModifiedName;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.BigInt).Value = Produce.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            string zSQL = @"
DECLARE @ProduceKey NVARCHAR(50) = (SELECT CAST(AutoKey AS NVARCHAR(50)) + '-' + ProduceID FROM SAL_Produce WHERE RecordStatus <> 99 AND AutoKey = @AutoKey)
UPDATE SAL_Produce SET RecordStatus = 99 WHERE AutoKey = @AutoKey

DECLARE @IN NVARCHAR(50) = (SELECT OrderKey FROM SAL_Order WHERE RecordStatus <> 99 AND ProduceKey = @ProduceKey)
UPDATE SAL_Order SET RecordStatus = 99 WHERE OrderKey = @IN
UPDATE SAL_Order_Item SET RecordStatus = 99 WHERE OrderKey =@IN

DECLARE @OUT NVARCHAR(50) = (SELECT OrderKey FROM SAL_Purchase WHERE RecordStatus <> 99 AND ProduceKey = @ProduceKey)
UPDATE SAL_Purchase SET RecordStatus = 99 WHERE OrderKey = @OUT
UPDATE SAL_Purchase_Item SET RecordStatus = 99 WHERE OrderKey = @OUT
";

            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.NVarChar).Value = Produce.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Produce WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.BigInt).Value = Produce.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
