﻿using System;
namespace ShopCP
{
    public class Purchase_Item_Model
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _OrderKey = "";
        private string _ItemKey = "";
        private string _ItemName = "";
        private string _ItemPhoto = "";
        private float _ItemQty = 0;
        private double _ItemPrice = 0;
        private double _ItemMoney = 0;
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private string _Description = "";
        private string _Organization = "";
        private string _PartnerNumber = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string ItemKey
        {
            get { return _ItemKey; }
            set { _ItemKey = value; }
        }
        public string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }
        public string ItemPhoto
        {
            get { return _ItemPhoto; }
            set { _ItemPhoto = value; }
        }
        public float ItemQty
        {
            get { return _ItemQty; }
            set { _ItemQty = value; }
        }
        public double ItemMoney
        {
            get { return _ItemMoney; }
            set { _ItemMoney = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }

        public double ItemPrice { get => _ItemPrice; set => _ItemPrice = value; }
        #endregion
    }
}
