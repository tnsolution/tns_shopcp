﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Purchase_Item_Info
    {
        public Purchase_Item_Model Purchase_Item = new Purchase_Item_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Purchase_Item_Info()
        {
        }
        public Purchase_Item_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SAL_Purchase_Item WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Purchase_Item.AutoKey = int.Parse(zReader["AutoKey"].ToString());

                    Purchase_Item.OrderKey = zReader["OrderKey"].ToString();
                    Purchase_Item.ItemKey = zReader["ItemKey"].ToString();
                    Purchase_Item.ItemName = zReader["ItemName"].ToString();
                    Purchase_Item.ItemPhoto = zReader["ItemPhoto"].ToString();
                    if (zReader["ItemQty"] != DBNull.Value)
                        Purchase_Item.ItemQty = float.Parse(zReader["ItemQty"].ToString());
                    if (zReader["ItemPrice"] != DBNull.Value)
                        Purchase_Item.ItemPrice = double.Parse(zReader["ItemPrice"].ToString());
                    if (zReader["ItemMoney"] != DBNull.Value)
                        Purchase_Item.ItemMoney = double.Parse(zReader["ItemMoney"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        Purchase_Item.Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Purchase_Item.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Purchase_Item.Description = zReader["Description"].ToString();
                    Purchase_Item.Organization = zReader["Organization"].ToString();
                    Purchase_Item.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Purchase_Item.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Purchase_Item.CreatedBy = zReader["CreatedBy"].ToString();
                    Purchase_Item.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Purchase_Item.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Purchase_Item.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Purchase_Item.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information 
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Purchase_Item ("
         + " OrderKey , ItemKey , ItemName , ItemPhoto , ItemQty , ItemPrice, ItemMoney , Slug , RecordStatus , Description , Organization , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderKey , @ItemKey , @ItemName , @ItemPhoto , @ItemQty , @ItemPrice, @ItemMoney , @Slug , @RecordStatus , @Description , @Organization , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Purchase_Item.OrderKey != "" && Purchase_Item.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Purchase_Item.OrderKey);
                }
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = Purchase_Item.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Purchase_Item.ItemName;
                zCommand.Parameters.Add("@ItemPhoto", SqlDbType.NVarChar).Value = Purchase_Item.ItemPhoto;
                zCommand.Parameters.Add("@ItemQty", SqlDbType.Float).Value = Purchase_Item.ItemQty;
                zCommand.Parameters.Add("@ItemPrice", SqlDbType.Money).Value = Purchase_Item.ItemPrice;
                zCommand.Parameters.Add("@ItemMoney", SqlDbType.Money).Value = Purchase_Item.ItemMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Purchase_Item.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Purchase_Item.RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Purchase_Item.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Purchase_Item.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Purchase_Item.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Purchase_Item.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Purchase_Item.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Purchase_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Purchase_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Purchase_Item("
         + " AutoKey , OrderKey , ItemKey , ItemName , ItemPhoto , ItemQty , ItemPrice, ItemMoney , Slug , RecordStatus , Description , Organization , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @OrderKey , @ItemKey , @ItemName , @ItemPhoto , @ItemQty , @ItemPrice, @ItemMoney , @Slug , @RecordStatus , @Description , @Organization , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Purchase_Item.AutoKey;
                if (Purchase_Item.OrderKey != "" && Purchase_Item.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Purchase_Item.OrderKey);
                }
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = Purchase_Item.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Purchase_Item.ItemName;
                zCommand.Parameters.Add("@ItemPhoto", SqlDbType.NVarChar).Value = Purchase_Item.ItemPhoto;
                zCommand.Parameters.Add("@ItemQty", SqlDbType.Float).Value = Purchase_Item.ItemQty;
                zCommand.Parameters.Add("@ItemPrice", SqlDbType.Money).Value = Purchase_Item.ItemPrice;
                zCommand.Parameters.Add("@ItemMoney", SqlDbType.Money).Value = Purchase_Item.ItemMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Purchase_Item.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Purchase_Item.RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Purchase_Item.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Purchase_Item.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Purchase_Item.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Purchase_Item.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Purchase_Item.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Purchase_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Purchase_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SAL_Purchase_Item SET "
                        + " OrderKey = @OrderKey,"
                        + " ItemKey = @ItemKey,"
                        + " ItemName = @ItemName,"
                        + " ItemPhoto = @ItemPhoto,"
                        + " ItemQty = @ItemQty, ItemPrice = @ItemPrice,"
                        + " ItemMoney = @ItemMoney,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " Description = @Description,"
                        + " Organization = @Organization,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Purchase_Item.AutoKey;
                if (Purchase_Item.OrderKey != "" && Purchase_Item.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Purchase_Item.OrderKey);
                }
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = Purchase_Item.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Purchase_Item.ItemName;
                zCommand.Parameters.Add("@ItemPhoto", SqlDbType.NVarChar).Value = Purchase_Item.ItemPhoto;
                zCommand.Parameters.Add("@ItemQty", SqlDbType.Float).Value = Purchase_Item.ItemQty;
                zCommand.Parameters.Add("@ItemPrice", SqlDbType.Money).Value = Purchase_Item.ItemPrice;
                zCommand.Parameters.Add("@ItemMoney", SqlDbType.Money).Value = Purchase_Item.ItemMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Purchase_Item.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Purchase_Item.RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Purchase_Item.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Purchase_Item.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Purchase_Item.PartnerNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Purchase_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Purchase_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_Purchase_Item SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Purchase_Item.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Purchase_Item WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Purchase_Item.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
