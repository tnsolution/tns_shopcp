﻿using System;
using System.Collections.Generic;

namespace ShopCP
{
    public class Order_Model
    {
        #region [ Field Name ]
        private string _OrderKey = "";
        private string _OrderID = "";
        private string _S_OrderID = "";
        private string _S_JSON = "";
        private DateTime _OrderDate = DateTime.MinValue;
        private int _OrderStatus = 0;
        private string _Description = "";
        private string _BuyerKey = "";
        private string _BuyerName = "";
        private string _BuyerPhone = "";
        private string _BuyerAddress = "";
        private string _BuyerEmail = "";
        private string _ShippingAddress = "";
        private string _ShippingCity = "";
        private float _TotalItems = 0;
        private double _TotalMoney = 0;
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private string _CreditAccount = "";
        private string _DebitAccount = "";
        private string _PartnerNumber = "";
        private string _Organization = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }
        public string S_OrderID
        {
            get { return _S_OrderID; }
            set { _S_OrderID = value; }
        }
        public string S_JSON
        {
            get { return _S_JSON; }
            set { _S_JSON = value; }
        }
        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }
        public int OrderStatus
        {
            get { return _OrderStatus; }
            set { _OrderStatus = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string BuyerKey
        {
            get { return _BuyerKey; }
            set { _BuyerKey = value; }
        }
        public string BuyerName
        {
            get { return _BuyerName; }
            set { _BuyerName = value; }
        }
        public string BuyerPhone
        {
            get { return _BuyerPhone; }
            set { _BuyerPhone = value; }
        }
        public string BuyerAddress
        {
            get { return _BuyerAddress; }
            set { _BuyerAddress = value; }
        }
        public string BuyerEmail
        {
            get { return _BuyerEmail; }
            set { _BuyerEmail = value; }
        }
        public string ShippingAddress
        {
            get { return _ShippingAddress; }
            set { _ShippingAddress = value; }
        }
        public string ShippingCity
        {
            get { return _ShippingCity; }
            set { _ShippingCity = value; }
        }
        public float TotalItems
        {
            get { return _TotalItems; }
            set { _TotalItems = value; }
        }
        public double TotalMoney
        {
            get { return _TotalMoney; }
            set { _TotalMoney = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }
        public string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion

        public string ProduceKey { get; set; } = "";
        public double Discount { get; set; } = 0;
        public List<Order_Item_Model> ListItem { get; set; } = new List<Order_Item_Model>();
    }
}
