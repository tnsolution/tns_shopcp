﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Package_Info
    {

        public Package_Model Package = new Package_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Package_Info()
        {
            Package.PackageKey = Guid.NewGuid().ToString();
        }
        public Package_Info(string PackageKey)
        {
            string zSQL = "SELECT * FROM SAL_Package WHERE PackageKey = @PackageKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PackageKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PackageKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Package.PackageKey = zReader["PackageKey"].ToString();
                    Package.SKU = zReader["SKU"].ToString();
                    Package.Name = zReader["Name"].ToString();
                    Package.Description = zReader["Description"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                        Package.Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Package.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Package.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Package.Organization = zReader["Organization"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Package.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Package.CreatedBy = zReader["CreatedBy"].ToString();
                    Package.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Package.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Package.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Package.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public Package_Info(string SKU, bool IsSKU)
        {
            string zSQL = "SELECT * FROM SAL_Package WHERE SKU = @SKU AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SKU", SqlDbType.NVarChar).Value = SKU;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Package.PackageKey = zReader["PackageKey"].ToString();
                    Package.SKU = zReader["SKU"].ToString();
                    Package.Name = zReader["Name"].ToString();
                    Package.Description = zReader["Description"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                        Package.Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Package.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Package.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Package.Organization = zReader["Organization"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Package.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Package.CreatedBy = zReader["CreatedBy"].ToString();
                    Package.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Package.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Package.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Package.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Package ("
         + " SKU , Name , Description , Slug , RecordStatus , PartnerNumber , Organization , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @SKU , @Name , @Description , @Slug , @RecordStatus , @PartnerNumber , @Organization , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SKU", SqlDbType.NVarChar).Value = Package.SKU;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Package.Name;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Package.Description;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Package.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Package.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Package.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Package.Organization;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Package.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Package.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Package.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Package.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Package("
         + " PackageKey , SKU , Name , Description , Slug , RecordStatus , PartnerNumber , Organization , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PackageKey , @SKU , @Name , @Description , @Slug , @RecordStatus , @PartnerNumber , @Organization , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Package.PackageKey != "" && Package.PackageKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PackageKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Package.PackageKey);
                }
                else
                {
                    zCommand.Parameters.Add("@PackageKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@SKU", SqlDbType.NVarChar).Value = Package.SKU;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Package.Name;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Package.Description;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Package.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Package.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Package.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Package.Organization;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Package.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Package.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Package.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Package.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SAL_Package SET "
                        + " SKU = @SKU,"
                        + " Name = @Name,"
                        + " Description = @Description,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " Organization = @Organization,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE PackageKey = @PackageKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Package.PackageKey != "" && Package.PackageKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PackageKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Package.PackageKey);
                }
                else
                {
                    zCommand.Parameters.Add("@PackageKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@SKU", SqlDbType.NVarChar).Value = Package.SKU;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Package.Name;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Package.Description;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Package.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Package.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Package.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Package.Organization;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Package.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Package.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_Package SET RecordStatus = 99 WHERE PackageKey = @PackageKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PackageKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Package.PackageKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Package WHERE PackageKey = @PackageKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PackageKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Package.PackageKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteItem()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Package_Item WHERE PackageKey = @PackageKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PackageKey", SqlDbType.NVarChar).Value = Package.PackageKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
