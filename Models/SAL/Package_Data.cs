﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Package_Data
    {
        public static List<Package_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Package WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY SKU";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Package_Model> zList = new List<Package_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Package_Model()
                {
                    PackageKey = (r["PackageKey"] == DBNull.Value ? "" : r["PackageKey"].ToString()),
                    SKU = (r["SKU"] == DBNull.Value ? "" : r["SKU"].ToString()),
                    Name = (r["Name"] == DBNull.Value ? "" : r["Name"].ToString()),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    Slug = (r["Slug"] == DBNull.Value ? 0 : int.Parse(r["Slug"].ToString())),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    Organization = (r["Organization"] == DBNull.Value ? "" : r["Organization"].ToString()),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
    }
}
