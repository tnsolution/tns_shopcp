﻿using System;
using System.Collections.Generic;

namespace ShopCP
{
    public class Purchase_Model
    {
        #region [ Field Name ]
        private string _OrderKey = "";
        private string _OrderID = "";
        private DateTime _OrderDate = DateTime.MinValue;
        private int _OrderStatus = 0;
        private string _Description = "";
        private string _VendorKey = "";
        private string _VendorName = "";
        private string _VendorPhone = "";
        private string _VendorAddress = "";
        private string _VendorEmail = "";
        private string _ShippingAddress = "";
        private string _ShippingCity = "";
        private float _TotalItems = 0;
        private double _TotalMoney = 0;
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private string _CreditAccount = "";
        private string _DebitAccount = "";
        private string _PartnerNumber = "";
        private string _Organization = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }
        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }
        public int OrderStatus
        {
            get { return _OrderStatus; }
            set { _OrderStatus = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string VendorKey
        {
            get { return _VendorKey; }
            set { _VendorKey = value; }
        }
        public string VendorName
        {
            get { return _VendorName; }
            set { _VendorName = value; }
        }
        public string VendorPhone
        {
            get { return _VendorPhone; }
            set { _VendorPhone = value; }
        }
        public string VendorAddress
        {
            get { return _VendorAddress; }
            set { _VendorAddress = value; }
        }
        public string VendorEmail
        {
            get { return _VendorEmail; }
            set { _VendorEmail = value; }
        }
        public string ShippingAddress
        {
            get { return _ShippingAddress; }
            set { _ShippingAddress = value; }
        }
        public string ShippingCity
        {
            get { return _ShippingCity; }
            set { _ShippingCity = value; }
        }
        public float TotalItems
        {
            get { return _TotalItems; }
            set { _TotalItems = value; }
        }
        public double TotalMoney
        {
            get { return _TotalMoney; }
            set { _TotalMoney = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }
        public string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion

        public string ProduceKey { get; set; } = "";
        public List<Purchase_Item_Model> ListItem { get; set; } = new List<Purchase_Item_Model>();
    }
}
