﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Order_Item_Info
    {

        public Order_Item_Model Order_Item = new Order_Item_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Order_Item_Info()
        {
        }
        public Order_Item_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SAL_Order_Item WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Order_Item.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Order_Item.OrderKey = zReader["OrderKey"].ToString();
                    Order_Item.ItemKey = zReader["ItemKey"].ToString();
                    Order_Item.ItemName = zReader["ItemName"].ToString();
                    Order_Item.ItemPhoto = zReader["ItemPhoto"].ToString();
                    if (zReader["ItemQty"] != DBNull.Value)
                        Order_Item.ItemQty = float.Parse(zReader["ItemQty"].ToString());
                    if (zReader["ItemMoney"] != DBNull.Value)
                        Order_Item.ItemMoney = double.Parse(zReader["ItemMoney"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        Order_Item.Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Order_Item.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Order_Item.Description = zReader["Description"].ToString();
                    Order_Item.Organization = zReader["Organization"].ToString();
                    Order_Item.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Order_Item.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Order_Item.CreatedBy = zReader["CreatedBy"].ToString();
                    Order_Item.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Order_Item.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Order_Item.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Order_Item.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Order_Item ("
         + " OrderKey , ItemKey , ItemName , ItemPhoto , ItemQty, ItemPrice, ItemMoney , Slug , RecordStatus , Description , Organization , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderKey , @ItemKey , @ItemName , @ItemPhoto , @ItemQty, @ItemPrice, @ItemMoney , @Slug , @RecordStatus , @Description , @Organization , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Order_Item.OrderKey != "" && Order_Item.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order_Item.OrderKey);
                }
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = Order_Item.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Order_Item.ItemName;
                zCommand.Parameters.Add("@ItemPhoto", SqlDbType.NVarChar).Value = Order_Item.ItemPhoto;
                zCommand.Parameters.Add("@ItemQty", SqlDbType.Float).Value = Order_Item.ItemQty;
                zCommand.Parameters.Add("@ItemPrice", SqlDbType.Money).Value = Order_Item.ItemPrice;
                zCommand.Parameters.Add("@ItemMoney", SqlDbType.Money).Value = Order_Item.ItemMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order_Item.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order_Item.RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order_Item.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Order_Item.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Order_Item.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Order_Item.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Order_Item.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Order_Item("
         + " AutoKey , OrderKey , ItemKey , ItemName , ItemPhoto , ItemQty, ItemPrice, ItemMoney , Slug , RecordStatus , Description , Organization , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @OrderKey , @ItemKey , @ItemName , @ItemPhoto , @ItemQty, @ItemPrice, @ItemMoney , @Slug , @RecordStatus , @Description , @Organization , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Order_Item.AutoKey;
                if (Order_Item.OrderKey != "" && Order_Item.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order_Item.OrderKey);
                }
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = Order_Item.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Order_Item.ItemName;
                zCommand.Parameters.Add("@ItemPhoto", SqlDbType.NVarChar).Value = Order_Item.ItemPhoto;
                zCommand.Parameters.Add("@ItemQty", SqlDbType.Float).Value = Order_Item.ItemQty;
                zCommand.Parameters.Add("@ItemPrice", SqlDbType.Money).Value = Order_Item.ItemPrice;
                zCommand.Parameters.Add("@ItemMoney", SqlDbType.Money).Value = Order_Item.ItemMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order_Item.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order_Item.RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order_Item.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Order_Item.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Order_Item.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Order_Item.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Order_Item.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SAL_Order_Item SET "
                        + " OrderKey = @OrderKey,"
                        + " ItemKey = @ItemKey,"
                        + " ItemName = @ItemName,"
                        + " ItemPhoto = @ItemPhoto,"
                        + " ItemQty = @ItemQty, ItemPrice = @ItemPrice,"
                        + " ItemMoney = @ItemMoney,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " Description = @Description,"
                        + " Organization = @Organization,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Order_Item.AutoKey;
                if (Order_Item.OrderKey != "" && Order_Item.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order_Item.OrderKey);
                }
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = Order_Item.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Order_Item.ItemName;
                zCommand.Parameters.Add("@ItemPhoto", SqlDbType.NVarChar).Value = Order_Item.ItemPhoto;
                zCommand.Parameters.Add("@ItemQty", SqlDbType.Float).Value = Order_Item.ItemQty;
                zCommand.Parameters.Add("@ItemPrice", SqlDbType.Money).Value = Order_Item.ItemPrice;
                zCommand.Parameters.Add("@ItemMoney", SqlDbType.Money).Value = Order_Item.ItemMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Order_Item.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order_Item.RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order_Item.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Order_Item.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Order_Item.PartnerNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_Order_Item SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Order_Item.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Order_Item WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Order_Item.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
