﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Product_Category_Info
    {

        public Product_Category_Model Product_Category = new Product_Category_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Product_Category_Info()
        {
        }
        public Product_Category_Info(int CategoryKey)
        {
            string zSQL = "SELECT * FROM PDT_Product_Category WHERE CategoryKey = @CategoryKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        Product_Category.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    Product_Category.CategoryID = zReader["CategoryID"].ToString();
                    Product_Category.CategoryName = zReader["CategoryName"].ToString();
                    Product_Category.Description = zReader["Description"].ToString();
                    if (zReader["Parent"] != DBNull.Value)
                        Product_Category.Parent = int.Parse(zReader["Parent"].ToString());
                    if (zReader["Publish"] != DBNull.Value)
                        Product_Category.Publish = (bool)zReader["Publish"];
                    if (zReader["Rank"] != DBNull.Value)
                        Product_Category.Rank = int.Parse(zReader["Rank"].ToString());
                    Product_Category.CategoryPath = zReader["CategoryPath"].ToString();
                    Product_Category.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Product_Category.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Product_Category.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Product_Category.CreatedBy = zReader["CreatedBy"].ToString();
                    Product_Category.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Product_Category.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Product_Category.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Product_Category.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product_Category ("
         + " CategoryID , CategoryName , Description , Parent , Publish , Rank , CategoryPath , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CategoryID , @CategoryName , @Description , @Parent , @Publish , @Rank , @CategoryPath , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = Product_Category.CategoryID;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Product_Category.CategoryName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Category.Description;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Product_Category.Parent;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Category.Publish;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Category.Rank;
                zCommand.Parameters.Add("@CategoryPath", SqlDbType.NVarChar).Value = Product_Category.CategoryPath;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Product_Category.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Category.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Category.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Category.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product_Category("
         + " CategoryKey , CategoryID , CategoryName , Description , Parent , Publish , Rank , CategoryPath , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CategoryKey , @CategoryID , @CategoryName , @Description , @Parent , @Publish , @Rank , @CategoryPath , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Product_Category.CategoryKey;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = Product_Category.CategoryID;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Product_Category.CategoryName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Category.Description;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Product_Category.Parent;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Category.Publish;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Category.Rank;
                zCommand.Parameters.Add("@CategoryPath", SqlDbType.NVarChar).Value = Product_Category.CategoryPath;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Product_Category.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Category.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Category.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Category.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE PDT_Product_Category SET "
                        + " CategoryID = @CategoryID,"
                        + " CategoryName = @CategoryName,"
                        + " Description = @Description,"
                        + " Parent = @Parent,"
                        + " Publish = @Publish,"
                        + " Rank = @Rank,"
                        + " CategoryPath = @CategoryPath,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE CategoryKey = @CategoryKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Product_Category.CategoryKey;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = Product_Category.CategoryID;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Product_Category.CategoryName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Category.Description;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Product_Category.Parent;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Category.Publish;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Category.Rank;
                zCommand.Parameters.Add("@CategoryPath", SqlDbType.NVarChar).Value = Product_Category.CategoryPath;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Product_Category.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Category.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Product_Category SET RecordStatus = 99 WHERE CategoryKey = @CategoryKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Product_Category.CategoryKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PDT_Product_Category WHERE CategoryKey = @CategoryKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Product_Category.CategoryKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetPublish(int CategoryKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE PDT_Product_Category SET Publish = (CASE Publish WHEN 'true' THEN 'false' ELSE 'true' END),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE CategoryKey = @CategoryKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }
        #endregion
    }
}
