﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Package_Item_Info
    {

        public Package_Item_Model Package_Item = new Package_Item_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Package_Item_Info()
        {
        }
        public Package_Item_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SAL_Package_Item WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Package_Item.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Package_Item.PackageKey = zReader["PackageKey"].ToString();
                    Package_Item.ItemKey = zReader["ItemKey"].ToString();
                    Package_Item.ItemName = zReader["ItemName"].ToString();
                    Package_Item.ItemPhoto = zReader["ItemPhoto"].ToString();
                    if (zReader["ItemQty"] != DBNull.Value)
                        Package_Item.ItemQty = float.Parse(zReader["ItemQty"].ToString());
                    if (zReader["ItemPrice"] != DBNull.Value)
                        Package_Item.ItemPrice = double.Parse(zReader["ItemPrice"].ToString());
                    if (zReader["ItemMoney"] != DBNull.Value)
                        Package_Item.ItemMoney = double.Parse(zReader["ItemMoney"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        Package_Item.Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Package_Item.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Package_Item.Description = zReader["Description"].ToString();
                    Package_Item.Organization = zReader["Organization"].ToString();
                    Package_Item.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Package_Item.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Package_Item.CreatedBy = zReader["CreatedBy"].ToString();
                    Package_Item.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Package_Item.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Package_Item.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Package_Item.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Package_Item ("
         + " PackageKey , ItemKey , ItemName , ItemPhoto , ItemQty , ItemPrice , ItemMoney , Slug , RecordStatus , Description , Organization , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PackageKey , @ItemKey , @ItemName , @ItemPhoto , @ItemQty , @ItemPrice , @ItemMoney , @Slug , @RecordStatus , @Description , @Organization , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PackageKey", SqlDbType.NVarChar).Value = Package_Item.PackageKey;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = Package_Item.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Package_Item.ItemName;
                zCommand.Parameters.Add("@ItemPhoto", SqlDbType.NVarChar).Value = Package_Item.ItemPhoto;
                zCommand.Parameters.Add("@ItemQty", SqlDbType.Float).Value = Package_Item.ItemQty;
                zCommand.Parameters.Add("@ItemPrice", SqlDbType.Money).Value = Package_Item.ItemPrice;
                zCommand.Parameters.Add("@ItemMoney", SqlDbType.Money).Value = Package_Item.ItemMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Package_Item.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Package_Item.RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Package_Item.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Package_Item.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Package_Item.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Package_Item.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Package_Item.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Package_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Package_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Package_Item("
         + " AutoKey , PackageKey , ItemKey , ItemName , ItemPhoto , ItemQty , ItemPrice , ItemMoney , Slug , RecordStatus , Description , Organization , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @PackageKey , @ItemKey , @ItemName , @ItemPhoto , @ItemQty , @ItemPrice , @ItemMoney , @Slug , @RecordStatus , @Description , @Organization , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Package_Item.AutoKey;
                zCommand.Parameters.Add("@PackageKey", SqlDbType.NVarChar).Value = Package_Item.PackageKey;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = Package_Item.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Package_Item.ItemName;
                zCommand.Parameters.Add("@ItemPhoto", SqlDbType.NVarChar).Value = Package_Item.ItemPhoto;
                zCommand.Parameters.Add("@ItemQty", SqlDbType.Float).Value = Package_Item.ItemQty;
                zCommand.Parameters.Add("@ItemPrice", SqlDbType.Money).Value = Package_Item.ItemPrice;
                zCommand.Parameters.Add("@ItemMoney", SqlDbType.Money).Value = Package_Item.ItemMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Package_Item.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Package_Item.RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Package_Item.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Package_Item.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Package_Item.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Package_Item.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Package_Item.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Package_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Package_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "UPDATE SAL_Package_Item SET "
                        + " PackageKey = @PackageKey,"
                        + " ItemKey = @ItemKey,"
                        + " ItemName = @ItemName,"
                        + " ItemPhoto = @ItemPhoto,"
                        + " ItemQty = @ItemQty,"
                        + " ItemPrice = @ItemPrice,"
                        + " ItemMoney = @ItemMoney,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " Description = @Description,"
                        + " Organization = @Organization,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Package_Item.AutoKey;
                zCommand.Parameters.Add("@PackageKey", SqlDbType.NVarChar).Value = Package_Item.PackageKey;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = Package_Item.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Package_Item.ItemName;
                zCommand.Parameters.Add("@ItemPhoto", SqlDbType.NVarChar).Value = Package_Item.ItemPhoto;
                zCommand.Parameters.Add("@ItemQty", SqlDbType.Float).Value = Package_Item.ItemQty;
                zCommand.Parameters.Add("@ItemPrice", SqlDbType.Money).Value = Package_Item.ItemPrice;
                zCommand.Parameters.Add("@ItemMoney", SqlDbType.Money).Value = Package_Item.ItemMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Package_Item.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Package_Item.RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Package_Item.Description;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Package_Item.Organization;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Package_Item.PartnerNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Package_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Package_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_Package_Item SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Package_Item.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Package_Item WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Package_Item.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
