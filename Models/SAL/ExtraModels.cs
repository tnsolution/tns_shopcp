﻿namespace ShopCP
{
    public partial class QtyModel
    {
        public string id { get; set; } = "";
        public float qty { get; set; } = 0;
    }

    public partial class QtyStock
    {
        public string ProductKey { get; set; } = "";
        public string ProductName { get; set; } = "";
        public string ProductModel { get; set; } = "";
        public string SKU { get; set; } = "";
        public string PhotoPath { get; set; } = "";
        public float BeginQty { get; set; } = 0;
        public double BeginMoney { get; set; } = 0;
        public float InQty { get; set; } = 0;
        public double InMoney { get; set; } = 0;
        public float OutQty { get; set; } = 0;
        public double OutMoney { get; set; } = 0;
        public float EndQty { get; set; } = 0;
        public double EndMoney { get; set; } = 0;
    }

    public partial class ItemRequest
    {
        public string ItemImg { get; set; } = "";
        public string ItemName { get; set; } = "";
        public float ItemQty { get; set; } = 0;
        public float ItemStock { get; set; } = 0;
    }
}