﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class CloseMonth_Data
    {
        public static List<CloseMonth_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_CloseMonth WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<CloseMonth_Model> zList = new List<CloseMonth_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new CloseMonth_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    ProductKey = r["ProductKey"].ToString(),
                    CloseMonth = r["CloseMonth"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    BeginQty = r["BeginQty"].ToFloat(),
                    BeginMoney = r["BeginMoney"].ToDouble(),
                    InQty = r["InQty"].ToFloat(),
                    InMoney = r["InMoney"].ToDouble(),
                    OutQty = r["OutQty"].ToFloat(),
                    OutMoney = r["OutMoney"].ToDouble(),
                    EndQty = r["EndQty"].ToFloat(),
                    EndMoney = r["EndMoney"].ToDouble(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
