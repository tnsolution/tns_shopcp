﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Product_Info
    {

        public Product_Model Product = new Product_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Product_Info()
        {
            Product.ProductKey = Guid.NewGuid().ToString();
        }
        public Product_Info(string ProductKey)
        {
            string zSQL = "SELECT * FROM PDT_Product WHERE ProductKey = @ProductKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ProductKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Product.ProductKey = zReader["ProductKey"].ToString();
                    Product.SKU = zReader["SKU"].ToString();
                    Product.Weight = zReader["Weight"].ToString();
                    Product.ProductID = zReader["ProductID"].ToString();
                    Product.ProductName = zReader["ProductName"].ToString();
                    Product.Technical = zReader["Technical"].ToString();
                    Product.ProductNumber = zReader["ProductNumber"].ToString();
                    Product.ProductSerial = zReader["ProductSerial"].ToString();
                    Product.ProductModel = zReader["ProductModel"].ToString();
                    if (zReader["StandardCost"] != DBNull.Value)
                        Product.StandardCost = double.Parse(zReader["StandardCost"].ToString());
                    if (zReader["SalePrice"] != DBNull.Value)
                        Product.SalePrice = double.Parse(zReader["SalePrice"].ToString());
                    if (zReader["SaleOff"] != DBNull.Value)
                        Product.SaleOff = double.Parse(zReader["SaleOff"].ToString());
                    if (zReader["TransferRate"] != DBNull.Value)
                        Product.TransferRate = float.Parse(zReader["TransferRate"].ToString());
                    if (zReader["TransferMoney"] != DBNull.Value)
                        Product.TransferMoney = double.Parse(zReader["TransferMoney"].ToString());
                    if (zReader["TransferCost"] != DBNull.Value)
                        Product.TransferCost = double.Parse(zReader["TransferCost"].ToString());
                    if (zReader["ProductCost"] != DBNull.Value)
                        Product.ProductCost = double.Parse(zReader["ProductCost"].ToString());
                    if (zReader["VATRate"] != DBNull.Value)
                        Product.VATRate = float.Parse(zReader["VATRate"].ToString());
                    if (zReader["StandardUnitKey"] != DBNull.Value)
                        Product.StandardUnitKey = int.Parse(zReader["StandardUnitKey"].ToString());
                    Product.StandardUnitName = zReader["StandardUnitName"].ToString();
                    if (zReader["DiscontinuedDate"] != DBNull.Value)
                        Product.DiscontinuedDate = (DateTime)zReader["DiscontinuedDate"];
                    if (zReader["SafetyInStock"] != DBNull.Value)
                        Product.SafetyInStock = float.Parse(zReader["SafetyInStock"].ToString());
                    Product.PhotoList = zReader["PhotoList"].ToString();
                    Product.Style = zReader["Style"].ToString();
                    Product.Class = zReader["Class"].ToString();
                    Product.ProductLine = zReader["ProductLine"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        Product.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    Product.CategoryName = zReader["CategoryName"].ToString();
                    Product.CategoryPath = zReader["CategoryPath"].ToString();
                    if (zReader["ProductModeKey"] != DBNull.Value)
                        Product.ProductModeKey = int.Parse(zReader["ProductModeKey"].ToString());
                    Product.PhotoPath = zReader["PhotoPath"].ToString();
                    if (zReader["StatusKey"] != DBNull.Value)
                        Product.StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    Product.StatusName = zReader["StatusName"].ToString();
                    if (zReader["BrandKey"] != DBNull.Value)
                        Product.BrandKey = int.Parse(zReader["BrandKey"].ToString());
                    Product.BrandName = zReader["BrandName"].ToString();
                    if (zReader["Publish"] != DBNull.Value)
                        Product.Publish = (bool)zReader["Publish"];
                    if (zReader["FrontPage"] != DBNull.Value)
                        Product.FrontPage = (bool)zReader["FrontPage"];
                    Product.ProductSummarize = zReader["ProductSummarize"].ToString();
                    Product.Description = zReader["Description"].ToString();
                    if (zReader["QuantityFact"] != DBNull.Value)
                        Product.QuantityFact = float.Parse(zReader["QuantityFact"].ToString());
                    Product.QuantityLog = zReader["QuantityLog"].ToString();
                    Product.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Product.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Product.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Product.CreatedBy = zReader["CreatedBy"].ToString();
                    Product.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Product.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Product.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Product.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public Product_Info(string SKU, bool IsSKU)
        {
            string zSQL = "SELECT * FROM PDT_Product WHERE SKU = @SKU AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SKU", SqlDbType.NVarChar).Value = SKU;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Product.ProductKey = zReader["ProductKey"].ToString();
                    Product.SKU = zReader["SKU"].ToString();
                    Product.Weight = zReader["Weight"].ToString();
                    Product.ProductID = zReader["ProductID"].ToString();
                    Product.ProductName = zReader["ProductName"].ToString();
                    Product.Technical = zReader["Technical"].ToString();
                    Product.ProductNumber = zReader["ProductNumber"].ToString();
                    Product.ProductSerial = zReader["ProductSerial"].ToString();
                    Product.ProductModel = zReader["ProductModel"].ToString();
                    if (zReader["StandardCost"] != DBNull.Value)
                        Product.StandardCost = double.Parse(zReader["StandardCost"].ToString());
                    if (zReader["SalePrice"] != DBNull.Value)
                        Product.SalePrice = double.Parse(zReader["SalePrice"].ToString());
                    if (zReader["SaleOff"] != DBNull.Value)
                        Product.SaleOff = double.Parse(zReader["SaleOff"].ToString());
                    if (zReader["TransferRate"] != DBNull.Value)
                        Product.TransferRate = float.Parse(zReader["TransferRate"].ToString());
                    if (zReader["TransferMoney"] != DBNull.Value)
                        Product.TransferMoney = double.Parse(zReader["TransferMoney"].ToString());
                    if (zReader["TransferCost"] != DBNull.Value)
                        Product.TransferCost = double.Parse(zReader["TransferCost"].ToString());
                    if (zReader["ProductCost"] != DBNull.Value)
                        Product.ProductCost = double.Parse(zReader["ProductCost"].ToString());
                    if (zReader["VATRate"] != DBNull.Value)
                        Product.VATRate = float.Parse(zReader["VATRate"].ToString());
                    if (zReader["StandardUnitKey"] != DBNull.Value)
                        Product.StandardUnitKey = int.Parse(zReader["StandardUnitKey"].ToString());
                    Product.StandardUnitName = zReader["StandardUnitName"].ToString();
                    if (zReader["DiscontinuedDate"] != DBNull.Value)
                        Product.DiscontinuedDate = (DateTime)zReader["DiscontinuedDate"];
                    if (zReader["SafetyInStock"] != DBNull.Value)
                        Product.SafetyInStock = float.Parse(zReader["SafetyInStock"].ToString());
                    Product.PhotoList = zReader["PhotoList"].ToString();
                    Product.Style = zReader["Style"].ToString();
                    Product.Class = zReader["Class"].ToString();
                    Product.ProductLine = zReader["ProductLine"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        Product.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    Product.CategoryName = zReader["CategoryName"].ToString();
                    Product.CategoryPath = zReader["CategoryPath"].ToString();
                    if (zReader["ProductModeKey"] != DBNull.Value)
                        Product.ProductModeKey = int.Parse(zReader["ProductModeKey"].ToString());
                    Product.PhotoPath = zReader["PhotoPath"].ToString();
                    if (zReader["StatusKey"] != DBNull.Value)
                        Product.StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    Product.StatusName = zReader["StatusName"].ToString();
                    if (zReader["BrandKey"] != DBNull.Value)
                        Product.BrandKey = int.Parse(zReader["BrandKey"].ToString());
                    Product.BrandName = zReader["BrandName"].ToString();
                    if (zReader["Publish"] != DBNull.Value)
                        Product.Publish = (bool)zReader["Publish"];
                    if (zReader["FrontPage"] != DBNull.Value)
                        Product.FrontPage = (bool)zReader["FrontPage"];
                    Product.ProductSummarize = zReader["ProductSummarize"].ToString();
                    Product.Description = zReader["Description"].ToString();
                    if (zReader["QuantityFact"] != DBNull.Value)
                        Product.QuantityFact = float.Parse(zReader["QuantityFact"].ToString());
                    Product.QuantityLog = zReader["QuantityLog"].ToString();
                    Product.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Product.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Product.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Product.CreatedBy = zReader["CreatedBy"].ToString();
                    Product.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Product.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Product.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Product.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product ("
         + " ProductID, SKU , Weight, ProductName , Technical , ProductNumber , ProductSerial , ProductModel , StandardCost , SalePrice , SaleOff , TransferRate , TransferMoney , TransferCost , ProductCost , VATRate , StandardUnitKey , StandardUnitName , DiscontinuedDate , SafetyInStock , PhotoList , Style , Class , ProductLine , CategoryKey , CategoryName , CategoryPath , ProductModeKey , PhotoPath , StatusKey , StatusName , BrandKey , BrandName , Publish , FrontPage , ProductSummarize , Description , QuantityFact , QuantityLog , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ProductID, @SKU , @Weight, @ProductName , @Technical , @ProductNumber , @ProductSerial , @ProductModel , @StandardCost , @SalePrice , @SaleOff , @TransferRate , @TransferMoney , @TransferCost , @ProductCost , @VATRate , @StandardUnitKey , @StandardUnitName , @DiscontinuedDate , @SafetyInStock , @PhotoList , @Style , @Class , @ProductLine , @CategoryKey , @CategoryName , @CategoryPath , @ProductModeKey , @PhotoPath , @StatusKey , @StatusName , @BrandKey , @BrandName , @Publish , @FrontPage , @ProductSummarize , @Description , @QuantityFact , @QuantityLog , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Product.ProductID;
                zCommand.Parameters.Add("@SKU", SqlDbType.NVarChar).Value = Product.SKU;
                zCommand.Parameters.Add("@Weight", SqlDbType.NVarChar).Value = Product.Weight;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Product.ProductName;
                zCommand.Parameters.Add("@Technical", SqlDbType.NVarChar).Value = Product.Technical;
                zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = Product.ProductNumber;
                zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = Product.ProductSerial;
                zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = Product.ProductModel;
                zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = Product.StandardCost;
                zCommand.Parameters.Add("@SalePrice", SqlDbType.Money).Value = Product.SalePrice;
                zCommand.Parameters.Add("@SaleOff", SqlDbType.Money).Value = Product.SaleOff;
                zCommand.Parameters.Add("@TransferRate", SqlDbType.Float).Value = Product.TransferRate;
                zCommand.Parameters.Add("@TransferMoney", SqlDbType.Money).Value = Product.TransferMoney;
                zCommand.Parameters.Add("@TransferCost", SqlDbType.Money).Value = Product.TransferCost;
                zCommand.Parameters.Add("@ProductCost", SqlDbType.Money).Value = Product.ProductCost;
                zCommand.Parameters.Add("@VATRate", SqlDbType.Float).Value = Product.VATRate;
                zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = Product.StandardUnitKey;
                zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = Product.StandardUnitName;
                if (Product.DiscontinuedDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = Product.DiscontinuedDate;
                zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = Product.SafetyInStock;
                zCommand.Parameters.Add("@PhotoList", SqlDbType.NVarChar).Value = Product.PhotoList;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Product.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Product.Class;
                zCommand.Parameters.Add("@ProductLine", SqlDbType.NChar).Value = Product.ProductLine;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Product.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Product.CategoryName;
                zCommand.Parameters.Add("@CategoryPath", SqlDbType.NVarChar).Value = Product.CategoryPath;
                zCommand.Parameters.Add("@ProductModeKey", SqlDbType.Int).Value = Product.ProductModeKey;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product.PhotoPath;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Product.StatusKey;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Product.StatusName;
                zCommand.Parameters.Add("@BrandKey", SqlDbType.Int).Value = Product.BrandKey;
                zCommand.Parameters.Add("@BrandName", SqlDbType.NVarChar).Value = Product.BrandName;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product.Publish;
                zCommand.Parameters.Add("@FrontPage", SqlDbType.Bit).Value = Product.FrontPage;
                zCommand.Parameters.Add("@ProductSummarize", SqlDbType.NVarChar).Value = Product.ProductSummarize;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product.Description;
                zCommand.Parameters.Add("@QuantityFact", SqlDbType.Float).Value = Product.QuantityFact;
                zCommand.Parameters.Add("@QuantityLog", SqlDbType.NVarChar).Value = Product.QuantityLog;
                if (Product.PartnerNumber != "" && Product.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product("
         + " ProductKey, SKU , Weight, ProductID , ProductName , Technical , ProductNumber , ProductSerial , ProductModel , StandardCost , SalePrice , SaleOff , TransferRate , TransferMoney , TransferCost , ProductCost , VATRate , StandardUnitKey , StandardUnitName , DiscontinuedDate , SafetyInStock , PhotoList , Style , Class , ProductLine , CategoryKey , CategoryName , CategoryPath , ProductModeKey , PhotoPath , StatusKey , StatusName , BrandKey , BrandName , Publish , FrontPage , ProductSummarize , Description , QuantityFact , QuantityLog , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ProductKey, @SKU , @Weight, @ProductID , @ProductName , @Technical , @ProductNumber , @ProductSerial , @ProductModel , @StandardCost , @SalePrice , @SaleOff , @TransferRate , @TransferMoney , @TransferCost , @ProductCost , @VATRate , @StandardUnitKey , @StandardUnitName , @DiscontinuedDate , @SafetyInStock , @PhotoList , @Style , @Class , @ProductLine , @CategoryKey , @CategoryName , @CategoryPath , @ProductModeKey , @PhotoPath , @StatusKey , @StatusName , @BrandKey , @BrandName , @Publish , @FrontPage , @ProductSummarize , @Description , @QuantityFact , @QuantityLog , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product.ProductKey);
                zCommand.Parameters.Add("@SKU", SqlDbType.NVarChar).Value = Product.SKU;
                zCommand.Parameters.Add("@Weight", SqlDbType.NVarChar).Value = Product.Weight;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Product.ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Product.ProductName;
                zCommand.Parameters.Add("@Technical", SqlDbType.NVarChar).Value = Product.Technical;
                zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = Product.ProductNumber;
                zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = Product.ProductSerial;
                zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = Product.ProductModel;
                zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = Product.StandardCost;
                zCommand.Parameters.Add("@SalePrice", SqlDbType.Money).Value = Product.SalePrice;
                zCommand.Parameters.Add("@SaleOff", SqlDbType.Money).Value = Product.SaleOff;
                zCommand.Parameters.Add("@TransferRate", SqlDbType.Float).Value = Product.TransferRate;
                zCommand.Parameters.Add("@TransferMoney", SqlDbType.Money).Value = Product.TransferMoney;
                zCommand.Parameters.Add("@TransferCost", SqlDbType.Money).Value = Product.TransferCost;
                zCommand.Parameters.Add("@ProductCost", SqlDbType.Money).Value = Product.ProductCost;
                zCommand.Parameters.Add("@VATRate", SqlDbType.Float).Value = Product.VATRate;
                zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = Product.StandardUnitKey;
                zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = Product.StandardUnitName;
                if (Product.DiscontinuedDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = Product.DiscontinuedDate;
                zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = Product.SafetyInStock;
                zCommand.Parameters.Add("@PhotoList", SqlDbType.NVarChar).Value = Product.PhotoList;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Product.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Product.Class;
                zCommand.Parameters.Add("@ProductLine", SqlDbType.NChar).Value = Product.ProductLine;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Product.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Product.CategoryName;
                zCommand.Parameters.Add("@CategoryPath", SqlDbType.NVarChar).Value = Product.CategoryPath;
                zCommand.Parameters.Add("@ProductModeKey", SqlDbType.Int).Value = Product.ProductModeKey;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product.PhotoPath;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Product.StatusKey;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Product.StatusName;
                zCommand.Parameters.Add("@BrandKey", SqlDbType.Int).Value = Product.BrandKey;
                zCommand.Parameters.Add("@BrandName", SqlDbType.NVarChar).Value = Product.BrandName;

                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product.Publish;

                zCommand.Parameters.Add("@FrontPage", SqlDbType.Bit).Value = Product.FrontPage;
                zCommand.Parameters.Add("@ProductSummarize", SqlDbType.NVarChar).Value = Product.ProductSummarize;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product.Description;
                zCommand.Parameters.Add("@QuantityFact", SqlDbType.Float).Value = Product.QuantityFact;
                zCommand.Parameters.Add("@QuantityLog", SqlDbType.NVarChar).Value = Product.QuantityLog;
                if (Product.PartnerNumber != "" && Product.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE PDT_Product SET SKU = @SKU, Weight = @Weight,"
                        + " ProductID = @ProductID,"
                        + " ProductName = @ProductName,"
                        + " Technical = @Technical,"
                        + " ProductNumber = @ProductNumber,"
                        + " ProductSerial = @ProductSerial,"
                        + " ProductModel = @ProductModel,"
                        + " StandardCost = @StandardCost,"
                        + " SalePrice = @SalePrice,"
                        + " SaleOff = @SaleOff,"
                        + " TransferRate = @TransferRate,"
                        + " TransferMoney = @TransferMoney,"
                        + " TransferCost = @TransferCost,"
                        + " ProductCost = @ProductCost,"
                        + " VATRate = @VATRate,"
                        + " StandardUnitKey = @StandardUnitKey,"
                        + " StandardUnitName = @StandardUnitName,"
                        + " DiscontinuedDate = @DiscontinuedDate,"
                        + " SafetyInStock = @SafetyInStock,"

                        + " Style = @Style,"
                        + " Class = @Class,"
                        + " ProductLine = @ProductLine,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " CategoryPath = @CategoryPath,"
                        + " ProductModeKey = @ProductModeKey,"
                        + " @Custom "
                        + " StatusKey = @StatusKey,"
                        + " StatusName = @StatusName,"
                        + " BrandKey = @BrandKey,"
                        + " BrandName = @BrandName,"
                        + " Publish = @Publish,"
                        + " FrontPage = @FrontPage,"
                        + " ProductSummarize = @ProductSummarize,"
                        + " Description = @Description,"
                        + " QuantityFact = @QuantityFact,"
                        + " QuantityLog = @QuantityLog,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " ";
            string zCustom = "";
            if (Product.PhotoPath.Length > 0)
            {
                zCustom += " PhotoPath = @PhotoPath,";
            }
            if (Product.PhotoList.Length > 0)
            {
                zCustom += " PhotoList = @PhotoList,";
            }

            zSQL = zSQL.Replace("@Custom", zCustom);
            zSQL += "WHERE ProductKey = @ProductKey";

            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product.ProductKey);
                zCommand.Parameters.Add("@SKU", SqlDbType.NVarChar).Value = Product.SKU;
                zCommand.Parameters.Add("@Weight", SqlDbType.NVarChar).Value = Product.Weight;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Product.ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Product.ProductName;
                zCommand.Parameters.Add("@Technical", SqlDbType.NVarChar).Value = Product.Technical;
                zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = Product.ProductNumber;
                zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = Product.ProductSerial;
                zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = Product.ProductModel;
                zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = Product.StandardCost;
                zCommand.Parameters.Add("@SalePrice", SqlDbType.Money).Value = Product.SalePrice;
                zCommand.Parameters.Add("@SaleOff", SqlDbType.Money).Value = Product.SaleOff;
                zCommand.Parameters.Add("@TransferRate", SqlDbType.Float).Value = Product.TransferRate;
                zCommand.Parameters.Add("@TransferMoney", SqlDbType.Money).Value = Product.TransferMoney;
                zCommand.Parameters.Add("@TransferCost", SqlDbType.Money).Value = Product.TransferCost;
                zCommand.Parameters.Add("@ProductCost", SqlDbType.Money).Value = Product.ProductCost;
                zCommand.Parameters.Add("@VATRate", SqlDbType.Float).Value = Product.VATRate;
                zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = Product.StandardUnitKey;
                zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = Product.StandardUnitName;
                if (Product.DiscontinuedDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = Product.DiscontinuedDate;
                zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = Product.SafetyInStock;
                zCommand.Parameters.Add("@PhotoList", SqlDbType.NVarChar).Value = Product.PhotoList;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Product.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Product.Class;
                zCommand.Parameters.Add("@ProductLine", SqlDbType.NChar).Value = Product.ProductLine;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Product.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Product.CategoryName;
                zCommand.Parameters.Add("@CategoryPath", SqlDbType.NVarChar).Value = Product.CategoryPath;
                zCommand.Parameters.Add("@ProductModeKey", SqlDbType.Int).Value = Product.ProductModeKey;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product.PhotoPath;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Product.StatusKey;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Product.StatusName;
                zCommand.Parameters.Add("@BrandKey", SqlDbType.Int).Value = Product.BrandKey;
                zCommand.Parameters.Add("@BrandName", SqlDbType.NVarChar).Value = Product.BrandName;

                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product.Publish;

                zCommand.Parameters.Add("@FrontPage", SqlDbType.Bit).Value = Product.FrontPage;
                zCommand.Parameters.Add("@ProductSummarize", SqlDbType.NVarChar).Value = Product.ProductSummarize;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product.Description;
                zCommand.Parameters.Add("@QuantityFact", SqlDbType.Float).Value = Product.QuantityFact;
                zCommand.Parameters.Add("@QuantityLog", SqlDbType.NVarChar).Value = Product.QuantityLog;
                if (Product.PartnerNumber != "" && Product.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Product SET RecordStatus = 99 WHERE ProductKey = @ProductKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product.ProductKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PDT_Product WHERE ProductKey = @ProductKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product.ProductKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetPublish(string ProductKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE PDT_Product SET Publish = (CASE Publish WHEN 'true' THEN 'false' ELSE 'true' END),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE ProductKey = @ProductKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ProductKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }

        public string DeleteV2()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Product SET RecordStatus = 99 WHERE CAST(ProductKey AS NVarchar(50)) = @ProductKey ";
            zSQL += "UPDATE PDT_Product SET RecordStatus = 99 WHERE ProductModel  = @ProductKey ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Product.ProductKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
