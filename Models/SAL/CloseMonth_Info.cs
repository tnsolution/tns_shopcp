﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class CloseMonth_Info
    {

        public CloseMonth_Model CloseMonth = new CloseMonth_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public CloseMonth_Info()
        {
        }
        public CloseMonth_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SAL_CloseMonth WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        CloseMonth.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    CloseMonth.ProductKey = zReader["ProductKey"].ToString();
                    if (zReader["Year"] != DBNull.Value)
                        CloseMonth.Year = int.Parse(zReader["Year"].ToString());
                    CloseMonth.CloseMonth = zReader["CloseMonth"].ToString();
                    CloseMonth.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["BeginQty"] != DBNull.Value)
                        CloseMonth.BeginQty = float.Parse(zReader["BeginQty"].ToString());
                    if (zReader["BeginMoney"] != DBNull.Value)
                        CloseMonth.BeginMoney = double.Parse(zReader["BeginMoney"].ToString());
                    if (zReader["InQty"] != DBNull.Value)
                        CloseMonth.InQty = float.Parse(zReader["InQty"].ToString());
                    if (zReader["InMoney"] != DBNull.Value)
                        CloseMonth.InMoney = double.Parse(zReader["InMoney"].ToString());
                    if (zReader["OutQty"] != DBNull.Value)
                        CloseMonth.OutQty = float.Parse(zReader["OutQty"].ToString());
                    if (zReader["OutMoney"] != DBNull.Value)
                        CloseMonth.OutMoney = double.Parse(zReader["OutMoney"].ToString());
                    if (zReader["EndQty"] != DBNull.Value)
                        CloseMonth.EndQty = float.Parse(zReader["EndQty"].ToString());
                    if (zReader["EndMoney"] != DBNull.Value)
                        CloseMonth.EndMoney = double.Parse(zReader["EndMoney"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        CloseMonth.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        CloseMonth.CreatedOn = (DateTime)zReader["CreatedOn"];
                    CloseMonth.CreatedBy = zReader["CreatedBy"].ToString();
                    CloseMonth.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        CloseMonth.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    CloseMonth.ModifiedBy = zReader["ModifiedBy"].ToString();
                    CloseMonth.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public CloseMonth_Info(string Month,int Year,string ProductKey)
        {
            string zSQL = @"SELECT * FROM SAL_CloseMonth WHERE  RecordStatus != 99 
                            AND CloseMonth = @Month AND Year =@Year AND ProductKey = @ProductKey ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Month", SqlDbType.NVarChar).Value = "01/"+Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        CloseMonth.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    CloseMonth.ProductKey = zReader["ProductKey"].ToString();
                    if (zReader["Year"] != DBNull.Value)
                        CloseMonth.Year = int.Parse(zReader["Year"].ToString());
                    CloseMonth.CloseMonth = zReader["CloseMonth"].ToString();
                    CloseMonth.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["BeginQty"] != DBNull.Value)
                        CloseMonth.BeginQty = float.Parse(zReader["BeginQty"].ToString());
                    if (zReader["BeginMoney"] != DBNull.Value)
                        CloseMonth.BeginMoney = double.Parse(zReader["BeginMoney"].ToString());
                    if (zReader["InQty"] != DBNull.Value)
                        CloseMonth.InQty = float.Parse(zReader["InQty"].ToString());
                    if (zReader["InMoney"] != DBNull.Value)
                        CloseMonth.InMoney = double.Parse(zReader["InMoney"].ToString());
                    if (zReader["OutQty"] != DBNull.Value)
                        CloseMonth.OutQty = float.Parse(zReader["OutQty"].ToString());
                    if (zReader["OutMoney"] != DBNull.Value)
                        CloseMonth.OutMoney = double.Parse(zReader["OutMoney"].ToString());
                    if (zReader["EndQty"] != DBNull.Value)
                        CloseMonth.EndQty = float.Parse(zReader["EndQty"].ToString());
                    if (zReader["EndMoney"] != DBNull.Value)
                        CloseMonth.EndMoney = double.Parse(zReader["EndMoney"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        CloseMonth.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        CloseMonth.CreatedOn = (DateTime)zReader["CreatedOn"];
                    CloseMonth.CreatedBy = zReader["CreatedBy"].ToString();
                    CloseMonth.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        CloseMonth.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    CloseMonth.ModifiedBy = zReader["ModifiedBy"].ToString();
                    CloseMonth.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_CloseMonth ("
         + " ProductKey , CloseMonth , Year, PartnerNumber , BeginQty , BeginMoney , InQty , InMoney , OutQty , OutMoney , EndQty , EndMoney , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ProductKey , @CloseMonth , @Year, @PartnerNumber , @BeginQty , @BeginMoney , @InQty , @InMoney , @OutQty , @OutMoney , @EndQty , @EndMoney , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = CloseMonth.ProductKey;
                zCommand.Parameters.Add("@CloseMonth", SqlDbType.NVarChar).Value = CloseMonth.CloseMonth;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = CloseMonth.Year;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = CloseMonth.PartnerNumber;
                zCommand.Parameters.Add("@BeginQty", SqlDbType.Float).Value = CloseMonth.BeginQty;
                zCommand.Parameters.Add("@BeginMoney", SqlDbType.Money).Value = CloseMonth.BeginMoney;
                zCommand.Parameters.Add("@InQty", SqlDbType.Float).Value = CloseMonth.InQty;
                zCommand.Parameters.Add("@InMoney", SqlDbType.Money).Value = CloseMonth.InMoney;
                zCommand.Parameters.Add("@OutQty", SqlDbType.Float).Value = CloseMonth.OutQty;
                zCommand.Parameters.Add("@OutMoney", SqlDbType.Money).Value = CloseMonth.OutMoney;
                zCommand.Parameters.Add("@EndQty", SqlDbType.Float).Value = CloseMonth.EndQty;
                zCommand.Parameters.Add("@EndMoney", SqlDbType.Money).Value = CloseMonth.EndMoney;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = CloseMonth.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CloseMonth.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CloseMonth.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = CloseMonth.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = CloseMonth.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_CloseMonth("
         + " AutoKey , ProductKey , CloseMonth , Year , PartnerNumber , BeginQty , BeginMoney , InQty , InMoney , OutQty , OutMoney , EndQty , EndMoney , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @ProductKey , @CloseMonth ,@Year, @PartnerNumber , @BeginQty , @BeginMoney , @InQty , @InMoney , @OutQty , @OutMoney , @EndQty , @EndMoney , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = CloseMonth.AutoKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = CloseMonth.ProductKey;
                zCommand.Parameters.Add("@CloseMonth", SqlDbType.NVarChar).Value = CloseMonth.CloseMonth;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = CloseMonth.Year;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = CloseMonth.PartnerNumber;
                zCommand.Parameters.Add("@BeginQty", SqlDbType.Float).Value = CloseMonth.BeginQty;
                zCommand.Parameters.Add("@BeginMoney", SqlDbType.Money).Value = CloseMonth.BeginMoney;
                zCommand.Parameters.Add("@InQty", SqlDbType.Float).Value = CloseMonth.InQty;
                zCommand.Parameters.Add("@InMoney", SqlDbType.Money).Value = CloseMonth.InMoney;
                zCommand.Parameters.Add("@OutQty", SqlDbType.Float).Value = CloseMonth.OutQty;
                zCommand.Parameters.Add("@OutMoney", SqlDbType.Money).Value = CloseMonth.OutMoney;
                zCommand.Parameters.Add("@EndQty", SqlDbType.Float).Value = CloseMonth.EndQty;
                zCommand.Parameters.Add("@EndMoney", SqlDbType.Money).Value = CloseMonth.EndMoney;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = CloseMonth.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CloseMonth.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CloseMonth.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = CloseMonth.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = CloseMonth.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE SAL_CloseMonth SET "
                        + " ProductKey = @ProductKey,"
                        + " CloseMonth = @CloseMonth,"
                        + " Year = @Year,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " BeginQty = @BeginQty,"
                        + " BeginMoney = @BeginMoney,"
                        + " InQty = @InQty,"
                        + " InMoney = @InMoney,"
                        + " OutQty = @OutQty,"
                        + " OutMoney = @OutMoney,"
                        + " EndQty = @EndQty,"
                        + " EndMoney = @EndMoney,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = CloseMonth.AutoKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = CloseMonth.ProductKey;
                zCommand.Parameters.Add("@CloseMonth", SqlDbType.NVarChar).Value = CloseMonth.CloseMonth;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = CloseMonth.Year;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = CloseMonth.PartnerNumber;
                zCommand.Parameters.Add("@BeginQty", SqlDbType.Float).Value = CloseMonth.BeginQty;
                zCommand.Parameters.Add("@BeginMoney", SqlDbType.Money).Value = CloseMonth.BeginMoney;
                zCommand.Parameters.Add("@InQty", SqlDbType.Float).Value = CloseMonth.InQty;
                zCommand.Parameters.Add("@InMoney", SqlDbType.Money).Value = CloseMonth.InMoney;
                zCommand.Parameters.Add("@OutQty", SqlDbType.Float).Value = CloseMonth.OutQty;
                zCommand.Parameters.Add("@OutMoney", SqlDbType.Money).Value = CloseMonth.OutMoney;
                zCommand.Parameters.Add("@EndQty", SqlDbType.Float).Value = CloseMonth.EndQty;
                zCommand.Parameters.Add("@EndMoney", SqlDbType.Money).Value = CloseMonth.EndMoney;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = CloseMonth.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = CloseMonth.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = CloseMonth.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_CloseMonth SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = CloseMonth.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_CloseMonth WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = CloseMonth.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
