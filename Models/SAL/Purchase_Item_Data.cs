﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Purchase_Item_Data
    {
        public static List<Purchase_Item_Model> List(string PartnerNumber, string OrderKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Purchase_Item WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND OrderKey = @OrderKey";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Purchase_Item_Model> zList = new List<Purchase_Item_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Purchase_Item_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    OrderKey = r["OrderKey"].ToString(),
                    ItemKey = r["ItemKey"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    ItemPhoto = r["ItemPhoto"].ToString(),
                    ItemQty = r["ItemQty"].ToFloat(),
                    ItemMoney = r["ItemMoney"].ToDouble(),
                    ItemPrice = r["ItemPrice"].ToDouble(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Description = r["Description"].ToString(),
                    Organization = r["Organization"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
