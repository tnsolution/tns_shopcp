﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Produce_Model
    {
        #region [ Field Name ]
        private long _AutoKey = 0;
        private string _ProduceID = "";
        private DateTime _ProduceDate = DateTime.MinValue;
        private string _Description = "";
        private float _QtyLoss = 0;
        private string _InputRaw = "";
        private string _OutputFinished = "";
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private string _PartnerNumber = "";
        private string _Organization = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public long AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string ProduceID
        {
            get { return _ProduceID; }
            set { _ProduceID = value; }
        }
        public DateTime ProduceDate
        {
            get { return _ProduceDate; }
            set { _ProduceDate = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public float QtyLoss
        {
            get { return _QtyLoss; }
            set { _QtyLoss = value; }
        }
        public string InputRaw
        {
            get { return _InputRaw; }
            set { _InputRaw = value; }
        }
        public string OutputFinished
        {
            get { return _OutputFinished; }
            set { _OutputFinished = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion

        public Order_Model Order { get; set; } = new Order_Model();
        public Purchase_Model Purchase { get; set; } = new Purchase_Model();

        //public List<Order_Item_Model> ListItemIn { get; set; } = new List<Order_Item_Model>();
        //public List<Purchase_Item_Model> ListItemOut { get; set; } = new List<Purchase_Item_Model>();
    }
}
