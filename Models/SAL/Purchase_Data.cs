﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Purchase_Data
    {
        public static List<Purchase_Model> List(string PartnerNumber, int Slug ,out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Purchase WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Slug = @Slug ORDER BY CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Purchase_Model> zList = new List<Purchase_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Purchase_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    OrderStatus = r["OrderStatus"].ToInt(),
                    Description = r["Description"].ToString(),
                    VendorKey = r["VendorKey"].ToString(),
                    VendorName = r["VendorName"].ToString(),
                    VendorPhone = r["VendorPhone"].ToString(),
                    VendorAddress = r["VendorAddress"].ToString(),
                    VendorEmail = r["VendorEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    TotalItems = r["TotalItems"].ToFloat(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Purchase_Model> List_Adjust(string PartnerNumber, DateTime FromDate, DateTime ToDate, string Name, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * 
FROM SAL_Purchase 
WHERE 
RecordStatus != 99 
AND Slug = 111
AND PartnerNumber = @PartnerNumber 
ORDER BY CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = "%" + Name + "%";
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Purchase_Model> zList = new List<Purchase_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Purchase_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    OrderStatus = r["OrderStatus"].ToInt(),
                    Description = r["Description"].ToString(),
                    VendorKey = r["VendorKey"].ToString(),
                    VendorName = r["VendorName"].ToString(),
                    VendorPhone = r["VendorPhone"].ToString(),
                    VendorAddress = r["VendorAddress"].ToString(),
                    VendorEmail = r["VendorEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    TotalItems = r["TotalItems"].ToFloat(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Purchase_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Purchase WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Purchase_Model> zList = new List<Purchase_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Purchase_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    OrderStatus = r["OrderStatus"].ToInt(),
                    Description = r["Description"].ToString(),
                    VendorKey = r["VendorKey"].ToString(),
                    VendorName = r["VendorName"].ToString(),
                    VendorPhone = r["VendorPhone"].ToString(),
                    VendorAddress = r["VendorAddress"].ToString(),
                    VendorEmail = r["VendorEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    TotalItems = r["TotalItems"].ToFloat(),
                    TotalMoney = r["TotalMoney"].ToDouble(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
