﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Purchase_Info
    {

        public Purchase_Model Purchase = new Purchase_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Purchase_Info()
        {
            Purchase.OrderKey = Guid.NewGuid().ToString();
        }
        public Purchase_Info(string OrderKey)
        {
            string zSQL = "SELECT * FROM SAL_Purchase WHERE OrderKey = @OrderKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OrderKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Purchase.OrderKey = zReader["OrderKey"].ToString();
                    Purchase.OrderID = zReader["OrderID"].ToString();
                    if (zReader["OrderDate"] != DBNull.Value)
                        Purchase.OrderDate = (DateTime)zReader["OrderDate"];
                    if (zReader["OrderStatus"] != DBNull.Value)
                        Purchase.OrderStatus = int.Parse(zReader["OrderStatus"].ToString());
                    Purchase.Description = zReader["Description"].ToString();
                    Purchase.VendorKey = zReader["VendorKey"].ToString();
                    Purchase.VendorName = zReader["VendorName"].ToString();
                    Purchase.VendorPhone = zReader["VendorPhone"].ToString();
                    Purchase.VendorAddress = zReader["VendorAddress"].ToString();
                    Purchase.VendorEmail = zReader["VendorEmail"].ToString();
                    Purchase.ShippingAddress = zReader["ShippingAddress"].ToString();
                    Purchase.ShippingCity = zReader["ShippingCity"].ToString();
                    if (zReader["TotalItems"] != DBNull.Value)
                        Purchase.TotalItems = float.Parse(zReader["TotalItems"].ToString());
                    if (zReader["TotalMoney"] != DBNull.Value)
                        Purchase.TotalMoney = double.Parse(zReader["TotalMoney"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        Purchase.Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Purchase.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Purchase.CreditAccount = zReader["CreditAccount"].ToString();
                    Purchase.DebitAccount = zReader["DebitAccount"].ToString();
                    Purchase.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Purchase.Organization = zReader["Organization"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Purchase.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Purchase.CreatedBy = zReader["CreatedBy"].ToString();
                    Purchase.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Purchase.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Purchase.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Purchase.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Purchase ("
         + " OrderID , OrderDate , OrderStatus , Description , VendorKey , VendorName , VendorPhone , VendorAddress , VendorEmail , ShippingAddress , ShippingCity , TotalItems , TotalMoney , Slug , RecordStatus , CreditAccount , DebitAccount, ProduceKey , PartnerNumber , Organization , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderID , @OrderDate , @OrderStatus , @Description , @VendorKey , @VendorName , @VendorPhone , @VendorAddress , @VendorEmail , @ShippingAddress , @ShippingCity , @TotalItems , @TotalMoney , @Slug , @RecordStatus , @CreditAccount , @DebitAccount ,@ProduceKey, @PartnerNumber , @Organization , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProduceKey", SqlDbType.NVarChar).Value = Purchase.ProduceKey;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Purchase.OrderID;
                if (Purchase.OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Purchase.OrderDate;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = Purchase.OrderStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Purchase.Description;
                zCommand.Parameters.Add("@VendorKey", SqlDbType.NVarChar).Value = Purchase.VendorKey;
                zCommand.Parameters.Add("@VendorName", SqlDbType.NVarChar).Value = Purchase.VendorName;
                zCommand.Parameters.Add("@VendorPhone", SqlDbType.NVarChar).Value = Purchase.VendorPhone;
                zCommand.Parameters.Add("@VendorAddress", SqlDbType.NVarChar).Value = Purchase.VendorAddress;
                zCommand.Parameters.Add("@VendorEmail", SqlDbType.NVarChar).Value = Purchase.VendorEmail;
                zCommand.Parameters.Add("@ShippingAddress", SqlDbType.NVarChar).Value = Purchase.ShippingAddress;
                zCommand.Parameters.Add("@ShippingCity", SqlDbType.NVarChar).Value = Purchase.ShippingCity;
                zCommand.Parameters.Add("@TotalItems", SqlDbType.Float).Value = Purchase.TotalItems;
                zCommand.Parameters.Add("@TotalMoney", SqlDbType.Money).Value = Purchase.TotalMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Purchase.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Purchase.RecordStatus;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Purchase.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Purchase.DebitAccount;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Purchase.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Purchase.Organization;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Purchase.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Purchase.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Purchase.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Purchase.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Purchase("
         + " OrderKey , OrderID , OrderDate , OrderStatus , Description , VendorKey , VendorName , VendorPhone , VendorAddress , VendorEmail , ShippingAddress , ShippingCity , TotalItems , TotalMoney , Slug , RecordStatus , CreditAccount , DebitAccount ,ProduceKey, PartnerNumber , Organization , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderKey , @OrderID , @OrderDate , @OrderStatus , @Description , @VendorKey , @VendorName , @VendorPhone , @VendorAddress , @VendorEmail , @ShippingAddress , @ShippingCity , @TotalItems , @TotalMoney , @Slug , @RecordStatus , @CreditAccount , @DebitAccount ,@ProduceKey, @PartnerNumber , @Organization , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProduceKey", SqlDbType.NVarChar).Value = Purchase.ProduceKey;
                if (Purchase.OrderKey != "" && Purchase.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Purchase.OrderKey);
                }
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Purchase.OrderID;
                if (Purchase.OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Purchase.OrderDate;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = Purchase.OrderStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Purchase.Description;
                zCommand.Parameters.Add("@VendorKey", SqlDbType.NVarChar).Value = Purchase.VendorKey;
                zCommand.Parameters.Add("@VendorName", SqlDbType.NVarChar).Value = Purchase.VendorName;
                zCommand.Parameters.Add("@VendorPhone", SqlDbType.NVarChar).Value = Purchase.VendorPhone;
                zCommand.Parameters.Add("@VendorAddress", SqlDbType.NVarChar).Value = Purchase.VendorAddress;
                zCommand.Parameters.Add("@VendorEmail", SqlDbType.NVarChar).Value = Purchase.VendorEmail;
                zCommand.Parameters.Add("@ShippingAddress", SqlDbType.NVarChar).Value = Purchase.ShippingAddress;
                zCommand.Parameters.Add("@ShippingCity", SqlDbType.NVarChar).Value = Purchase.ShippingCity;
                zCommand.Parameters.Add("@TotalItems", SqlDbType.Float).Value = Purchase.TotalItems;
                zCommand.Parameters.Add("@TotalMoney", SqlDbType.Money).Value = Purchase.TotalMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Purchase.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Purchase.RecordStatus;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Purchase.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Purchase.DebitAccount;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Purchase.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Purchase.Organization;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Purchase.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Purchase.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Purchase.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Purchase.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SAL_Purchase SET "
                        + " OrderID = @OrderID,"
                        + " OrderDate = @OrderDate,"
                        + " OrderStatus = @OrderStatus,"
                        + " Description = @Description,"
                        + " VendorKey = @VendorKey,"
                        + " VendorName = @VendorName,"
                        + " VendorPhone = @VendorPhone,"
                        + " VendorAddress = @VendorAddress,"
                        + " VendorEmail = @VendorEmail,"
                        + " ShippingAddress = @ShippingAddress,"
                        + " ShippingCity = @ShippingCity,"
                        + " TotalItems = @TotalItems,"
                        + " TotalMoney = @TotalMoney,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus, ProduceKey=@ProduceKey,"
                        + " CreditAccount = @CreditAccount,"
                        + " DebitAccount = @DebitAccount,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " Organization = @Organization,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE OrderKey = @OrderKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProduceKey", SqlDbType.NVarChar).Value = Purchase.ProduceKey;

                if (Purchase.OrderKey != "" && Purchase.OrderKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Purchase.OrderKey);
                }
                else
                    zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Purchase.OrderID;
                if (Purchase.OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Purchase.OrderDate;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = Purchase.OrderStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Purchase.Description;
                zCommand.Parameters.Add("@VendorKey", SqlDbType.NVarChar).Value = Purchase.VendorKey;
                zCommand.Parameters.Add("@VendorName", SqlDbType.NVarChar).Value = Purchase.VendorName;
                zCommand.Parameters.Add("@VendorPhone", SqlDbType.NVarChar).Value = Purchase.VendorPhone;
                zCommand.Parameters.Add("@VendorAddress", SqlDbType.NVarChar).Value = Purchase.VendorAddress;
                zCommand.Parameters.Add("@VendorEmail", SqlDbType.NVarChar).Value = Purchase.VendorEmail;
                zCommand.Parameters.Add("@ShippingAddress", SqlDbType.NVarChar).Value = Purchase.ShippingAddress;
                zCommand.Parameters.Add("@ShippingCity", SqlDbType.NVarChar).Value = Purchase.ShippingCity;
                zCommand.Parameters.Add("@TotalItems", SqlDbType.Float).Value = Purchase.TotalItems;
                zCommand.Parameters.Add("@TotalMoney", SqlDbType.Money).Value = Purchase.TotalMoney;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Purchase.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Purchase.RecordStatus;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = Purchase.CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = Purchase.DebitAccount;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Purchase.PartnerNumber;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Purchase.Organization;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Purchase.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Purchase.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_Purchase SET RecordStatus = 99 WHERE OrderKey = @OrderKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Purchase.OrderKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Purchase WHERE OrderKey = @OrderKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Purchase.OrderKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteItem()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Purchase_Item WHERE OrderKey = @OrderKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = Purchase.OrderKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}