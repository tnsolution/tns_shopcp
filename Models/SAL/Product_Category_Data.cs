﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public class Product_Category_Data
    {
        public static List<Product_Category_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH R AS 
(
	SELECT CategoryKey, CategoryID, CategoryName, [Description], Publish, 
    PARENT, DEPTH = 0, SORT = CAST(CategoryKey AS VARCHAR(MAX)), [Rank]
	FROM PDT_Product_Category 
	WHERE RecordStatus <> 99 
	AND Parent = 0
	AND PartnerNumber = @PartnerNumber
	UNION ALL
	SELECT 
    Sub.CategoryKey, Sub.CategoryID ,Sub.CategoryName, Sub.[Description], Sub.Publish, 
    Sub.PARENT, DEPTH = R.DEPTH + 1, SORT = R.SORT + '-' + CAST(Sub.CategoryKey AS VARCHAR(MAX)), Sub.[Rank]
	FROM R INNER JOIN PDT_Product_Category Sub ON R.CategoryKey = Sub.Parent 
	WHERE Sub.RecordStatus <> 99 
	AND Sub.PartnerNumber = @PartnerNumber
)
SELECT CategoryName = REPLICATE('---', R.DEPTH * 1) + R.CategoryName, R.CategoryID, R.CategoryKey, R.[Description], R.Publish , R.Parent, R.SORT, R.DEPTH, R.[Rank]
FROM R ORDER BY SORT";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            var zList = new List<Product_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryID = r["CategoryID"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Rank = r["Rank"].ToInt(),
                    CategoryPath = r["DEPTH"].ToString(),                  
                });
            }
            return zList;
        }
        public static List<Product_Category_Model> ListAsMenu(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH R AS 
(
	SELECT CategoryKey, CategoryID, CategoryName, [Description], Publish, 
    PARENT, DEPTH = 0, SORT = CAST(CategoryKey AS VARCHAR(MAX)), [Rank]
	FROM PDT_Product_Category 
	WHERE RecordStatus <> 99 
	AND Parent = 0
	AND PartnerNumber = @PartnerNumber
    AND Publish = 'TRUE'
	UNION ALL
	SELECT 
    Sub.CategoryKey, Sub.CategoryID ,Sub.CategoryName, Sub.[Description], Sub.Publish, 
    Sub.PARENT, DEPTH = R.DEPTH + 1, SORT = R.SORT + '-' + CAST(Sub.CategoryKey AS VARCHAR(MAX)), Sub.[Rank]
	FROM R INNER JOIN PDT_Product_Category Sub ON R.CategoryKey = Sub.Parent 
	WHERE Sub.RecordStatus <> 99 
	AND Sub.PartnerNumber = @PartnerNumber
    AND Sub.Publish = 'TRUE'
)
SELECT R.CategoryName, R.CategoryID, R.CategoryKey, R.[Description], R.Publish , R.Parent, R.SORT, R.DEPTH, R.[Rank]
FROM R ORDER BY SORT";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            var zList = new List<Product_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryID = r["CategoryID"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Rank = r["Rank"].ToInt(),
                    CategoryPath = r["DEPTH"].ToString(),
                });
            }
            return zList;
        }

        public static List<Product_Category_Model> List(string PartnerNumber, int CategoryKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
	SELECT CategoryKey, CategoryID, CategoryName, [Description], Publish, 
    PARENT, [Rank]
	FROM PDT_Product_Category 
	WHERE RecordStatus <> 99 
	AND Parent = @CategoryKey
	AND PartnerNumber = @PartnerNumber";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            var zList = new List<Product_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryID = r["CategoryID"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Rank = r["Rank"].ToInt(),
                });
            }
            return zList;
        }

        public static List<Product_Category_Model> List(string PartnerNumber, string CategoryKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
	SELECT CategoryKey, CategoryID, CategoryName, [Description], Publish, 
    PARENT, [Rank]
	FROM PDT_Product_Category 
	WHERE RecordStatus <> 99 
	AND Parent = @CategoryKey
	AND PartnerNumber = @PartnerNumber";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            var zList = new List<Product_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryID = r["CategoryID"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Rank = r["Rank"].ToInt(),
                });
            }
            return zList;
        }

        public static List<Product_Category_Model> ListChild(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
	SELECT CategoryKey, CategoryID, CategoryName, [Description], Publish, 
    PARENT, [Rank]
	FROM PDT_Product_Category 
	WHERE RecordStatus <> 99 
	AND Parent <> 0
	AND PartnerNumber = @PartnerNumber";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            var zList = new List<Product_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryID = r["CategoryID"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Rank = r["Rank"].ToInt(),
                });
            }
            return zList;
        }
    }
}
