﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Produce_Data
    {
        public static List<Produce_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Produce WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Produce_Model> zList = new List<Produce_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Produce_Model()
                {
                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : long.Parse(r["AutoKey"].ToString())),
                    ProduceID = (r["ProduceID"] == DBNull.Value ? "" : r["ProduceID"].ToString()),
                    ProduceDate = (r["ProduceDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ProduceDate"]),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    QtyLoss = (r["QtyLoss"] == DBNull.Value ? 0 : float.Parse(r["QtyLoss"].ToString())),
                    InputRaw = (r["InputRaw"] == DBNull.Value ? "" : r["InputRaw"].ToString()),
                    OutputFinished = (r["OutputFinished"] == DBNull.Value ? "" : r["OutputFinished"].ToString()),
                    Slug = (r["Slug"] == DBNull.Value ? 0 : int.Parse(r["Slug"].ToString())),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    Organization = (r["Organization"] == DBNull.Value ? "" : r["Organization"].ToString()),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
    }
}
