﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ShopCP
{
    public partial class Customer_Data
    {
        public static List<Customer_Model> List_Customer(string PartnerNumber, string Filter, out string Message)
        {
            //0: Khách hàng
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM CRM_Customer WHERE RecordStatus != 99 
                        AND PartnerNumber = @PartnerNumber
                        AND CustomerVendor = 0 ";
            if (Filter != string.Empty)
            {
                zSQL += " AND(FullName LIKE @Filter OR Phone LIKE @Filter)";
            }
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Filter", SqlDbType.NVarChar).Value = "%" + Filter + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Customer_Model> zList = new List<Customer_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Customer_Model()
                {
                    CustomerKey = (r["CustomerKey"] == DBNull.Value ? "" : r["CustomerKey"].ToString()),
                    CustomerID = (r["CustomerID"] == DBNull.Value ? "" : r["CustomerID"].ToString()),
                    FullName = (r["FullName"] == DBNull.Value ? "" : r["FullName"].ToString()),
                    CompanyName = (r["CompanyName"] == DBNull.Value ? "" : r["CompanyName"].ToString()),
                    Aliases = (r["Aliases"] == DBNull.Value ? "" : r["Aliases"].ToString()),
                    JobTitle = (r["JobTitle"] == DBNull.Value ? "" : r["JobTitle"].ToString()),
                    CategoryKey = (r["CategoryKey"] == DBNull.Value ? 0 : int.Parse(r["CategoryKey"].ToString())),
                    CategoryName = (r["CategoryName"] == DBNull.Value ? "" : r["CategoryName"].ToString()),
                    CustomerType = (r["CustomerType"] == DBNull.Value ? 0 : int.Parse(r["CustomerType"].ToString())),
                    CustomerTypeName = (r["CustomerTypeName"] == DBNull.Value ? "" : r["CustomerTypeName"].ToString()),
                    CustomerVendor = (r["CustomerVendor"] == DBNull.Value ? 0 : int.Parse(r["CustomerVendor"].ToString())),
                    TaxNumber = (r["TaxNumber"] == DBNull.Value ? "" : r["TaxNumber"].ToString()),
                    IDCard = (r["IDCard"] == DBNull.Value ? "" : r["IDCard"].ToString()),
                    Birthday = (r["Birthday"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Birthday"]),
                    Address = (r["Address"] == DBNull.Value ? "" : r["Address"].ToString()),
                    City = (r["City"] == DBNull.Value ? "" : r["City"].ToString()),
                    Country = (r["Country"] == DBNull.Value ? "" : r["Country"].ToString()),
                    ZipCode = (r["ZipCode"] == DBNull.Value ? "" : r["ZipCode"].ToString()),
                    Phone = (r["Phone"] == DBNull.Value ? "" : r["Phone"].ToString()),
                    Email = (r["Email"] == DBNull.Value ? "" : r["Email"].ToString()),
                    Note = (r["Note"] == DBNull.Value ? "" : r["Note"].ToString()),
                    Style = (r["Style"] == DBNull.Value ? "" : r["Style"].ToString()),
                    Class = (r["Class"] == DBNull.Value ? "" : r["Class"].ToString()),
                    CodeLine = (r["CodeLine"] == DBNull.Value ? "" : r["CodeLine"].ToString()),
                    BankAccount = (r["BankAccount"] == DBNull.Value ? "" : r["BankAccount"].ToString()),
                    BankName = (r["BankName"] == DBNull.Value ? "" : r["BankName"].ToString()),
                    Referrer = (r["Referrer"] == DBNull.Value ? "" : r["Referrer"].ToString()),
                    Password = (r["Password"] == DBNull.Value ? "" : r["Password"].ToString()),
                    Parent = (r["Parent"] == DBNull.Value ? "" : r["Parent"].ToString()),
                    Slug = (r["Slug"] == DBNull.Value ? 0 : int.Parse(r["Slug"].ToString())),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    Contact = (r["Contact"] == DBNull.Value ? "" : r["Contact"].ToString()),
                    OrganizationID = (r["OrganizationID"] == DBNull.Value ? "" : r["OrganizationID"].ToString()),
                    DepartmentKey = (r["DepartmentKey"] == DBNull.Value ? 0 : int.Parse(r["DepartmentKey"].ToString())),
                    BranchKey = (r["BranchKey"] == DBNull.Value ? 0 : int.Parse(r["BranchKey"].ToString())),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                    LastLoginDate = (r["LastLoginDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["LastLoginDate"]),
                });
            }
            return zList;
        }
        public static bool CheckCustomerPhone(string Phone, string PartnerNumber)
        {
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(CustomerKey) FROM CRM_Customer 
                            WHERE Phone = @Phone 
                            AND PartnerNumber = @PartnerNumber
                            AND RecordStatus <> 99
                            AND CustomerVendor = 0"; //0: khach hang
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = float.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close();
            }

            if (zResult > 0)
                return true;
            else
                return false;
        }
        public static List<Customer_Model> List_Vendor(string PartnerNumber, string Filter, out string Message)
        {
            //1: Nhà cung cấp
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM CRM_Customer WHERE RecordStatus != 99 
                        AND PartnerNumber = @PartnerNumber
                        AND CustomerVendor = 1";
            if (Filter != string.Empty)
            {
                zSQL += " AND(FullName LIKE @Filter OR Phone LIKE @Filter)";
            }
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Filter", SqlDbType.NVarChar).Value = "%" + Filter + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Customer_Model> zList = new List<Customer_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Customer_Model()
                {
                    CustomerKey = (r["CustomerKey"] == DBNull.Value ? "" : r["CustomerKey"].ToString()),
                    CustomerID = (r["CustomerID"] == DBNull.Value ? "" : r["CustomerID"].ToString()),
                    FullName = (r["FullName"] == DBNull.Value ? "" : r["FullName"].ToString()),
                    CompanyName = (r["CompanyName"] == DBNull.Value ? "" : r["CompanyName"].ToString()),
                    Aliases = (r["Aliases"] == DBNull.Value ? "" : r["Aliases"].ToString()),
                    JobTitle = (r["JobTitle"] == DBNull.Value ? "" : r["JobTitle"].ToString()),
                    CategoryKey = (r["CategoryKey"] == DBNull.Value ? 0 : int.Parse(r["CategoryKey"].ToString())),
                    CategoryName = (r["CategoryName"] == DBNull.Value ? "" : r["CategoryName"].ToString()),
                    CustomerType = (r["CustomerType"] == DBNull.Value ? 0 : int.Parse(r["CustomerType"].ToString())),
                    CustomerTypeName = (r["CustomerTypeName"] == DBNull.Value ? "" : r["CustomerTypeName"].ToString()),
                    CustomerVendor = (r["CustomerVendor"] == DBNull.Value ? 0 : int.Parse(r["CustomerVendor"].ToString())),
                    TaxNumber = (r["TaxNumber"] == DBNull.Value ? "" : r["TaxNumber"].ToString()),
                    IDCard = (r["IDCard"] == DBNull.Value ? "" : r["IDCard"].ToString()),
                    Birthday = (r["Birthday"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Birthday"]),
                    Address = (r["Address"] == DBNull.Value ? "" : r["Address"].ToString()),
                    City = (r["City"] == DBNull.Value ? "" : r["City"].ToString()),
                    Country = (r["Country"] == DBNull.Value ? "" : r["Country"].ToString()),
                    ZipCode = (r["ZipCode"] == DBNull.Value ? "" : r["ZipCode"].ToString()),
                    Phone = (r["Phone"] == DBNull.Value ? "" : r["Phone"].ToString()),
                    Email = (r["Email"] == DBNull.Value ? "" : r["Email"].ToString()),
                    Note = (r["Note"] == DBNull.Value ? "" : r["Note"].ToString()),
                    Style = (r["Style"] == DBNull.Value ? "" : r["Style"].ToString()),
                    Class = (r["Class"] == DBNull.Value ? "" : r["Class"].ToString()),
                    CodeLine = (r["CodeLine"] == DBNull.Value ? "" : r["CodeLine"].ToString()),
                    BankAccount = (r["BankAccount"] == DBNull.Value ? "" : r["BankAccount"].ToString()),
                    BankName = (r["BankName"] == DBNull.Value ? "" : r["BankName"].ToString()),
                    Referrer = (r["Referrer"] == DBNull.Value ? "" : r["Referrer"].ToString()),
                    Password = (r["Password"] == DBNull.Value ? "" : r["Password"].ToString()),
                    Parent = (r["Parent"] == DBNull.Value ? "" : r["Parent"].ToString()),
                    Slug = (r["Slug"] == DBNull.Value ? 0 : int.Parse(r["Slug"].ToString())),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    Contact = (r["Contact"] == DBNull.Value ? "" : r["Contact"].ToString()),
                    OrganizationID = (r["OrganizationID"] == DBNull.Value ? "" : r["OrganizationID"].ToString()),
                    DepartmentKey = (r["DepartmentKey"] == DBNull.Value ? 0 : int.Parse(r["DepartmentKey"].ToString())),
                    BranchKey = (r["BranchKey"] == DBNull.Value ? 0 : int.Parse(r["BranchKey"].ToString())),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                    LastLoginDate = (r["LastLoginDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["LastLoginDate"]),
                });
            }
            return zList;
        }
        public static bool CheckVendorPhone(string Phone, string PartnerNumber)
        {
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(CustomerKey) FROM CRM_Customer 
                            WHERE Phone = @Phone 
                            AND PartnerNumber = @PartnerNumber
                            AND RecordStatus <> 99
                            AND CustomerVendor = 1"; //0: khach hang
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = float.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close();
            }

            if (zResult > 0)
                return true;
            else
                return false;
        }
    }
}
